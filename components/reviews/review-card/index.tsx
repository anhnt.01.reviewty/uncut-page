import IMAGES from "@/assets/img/images";
import { Gender, GetListReviewQuery } from "@/graphql/reviewty-user/graphql";
import { desc, renderDescRating } from "@/pages/orders/[id]/review";
import { UserOutlined } from "@ant-design/icons";
import { Avatar, Col, Row } from "antd";
import { formatDistance } from "date-fns";
import { vi } from "date-fns/locale";
import Image from "next/legacy/image";

import LightGallery from "lightgallery/react";
import lgVideo from "lightgallery/plugins/video";

// lightgallery styles
import "lightgallery/css/lightgallery.css";
import "lightgallery/css/lg-video.css";
import GalleryCard from "@/components/gallery";

// videojs styles
// import "video.js/dist/video-js.css";
// import "@videojs/themes/dist/fantasy/index.css";

interface IReviewCard {
  data: GetListReviewQuery["evaluations"][0];
}

const ReviewCard = ({ data }: IReviewCard) => {
  const images = data?.images?.map((item) => item.url);
  const user = data.fundingOrder.userOrder;
  return (
    <div className="p-4">
      <div className="flex">
        <div>
          <Avatar src={user?.avatar?.url} size={68} icon={<UserOutlined />} />
        </div>

        <div className="pl-2 flex flex-col">
          <label className="text-[1rem] pb-1 text-[#404040] font-semibold">
            {user.account}
          </label>
          <span className="text-[#404040] pb-1">
            {new Date().getFullYear() - user.birthYear!} Tuổi{" "}
            {"|" && user.baumannSkinType}
            <span className="ml-1">
              <Image
                src={
                  user.gender === Gender.Female
                    ? IMAGES.femaleIcon
                    : IMAGES.maleIcon
                }
                alt="gender-icon"
              />
            </span>
          </span>
          <span className="text-[#9B9B9B]">
            {" "}
            {formatDistance(new Date(data.createdAt), new Date(), {
              locale: vi,
              addSuffix: true,
            })}
          </span>
        </div>
      </div>
      <div className="flex items-center py-2">
        {renderDescRating(desc[data.rate! - 1])}
      </div>
      <p className="text-[1rem] text-[#404040] pb-4">{data.comment}</p>

      {data.images && <GalleryCard images={data.images} />}
    </div>
  );
};

export default ReviewCard;
