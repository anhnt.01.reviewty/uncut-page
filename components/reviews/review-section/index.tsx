import { setIsSpin } from "@/components/auth/authSlice";
import LoadingProduct from "@/components/skeleton/loading-product";
import {
  FundingOrdersQuery,
  FundingOrderStatus,
  GetListReviewQuery,
  OrderBy,
  useGetListReviewLazyQuery,
} from "@/graphql/reviewty-user/graphql";
import { useAppDispatch } from "@/store/store";
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import ReviewCard from "../review-card";

const ReviewSection = ({ productId }: { productId: number }) => {
  const router = useRouter();
  const [data, setData] = useState<GetListReviewQuery>();

  const dispatch = useAppDispatch();
  const [getList, { data: dataReview, fetchMore }] = useGetListReviewLazyQuery({
    variables: {
      first: 10,
      where: {
        productId: productId,
      },
      orderBy: {
        id: OrderBy.Desc,
      },
    },
    onCompleted(data) {
      setData(data);
    },
  });

  useEffect(() => {
    if (productId) {
      getList();
    }
  }, [getList, productId]);

  const handleFetchMore = useCallback(async () => {
    dispatch(setIsSpin(true));

    const { data: dataOrders } = await fetchMore({
      variables: {
        first: 10,
        skip: data?.evaluations.length || 0,
        where: {
          productId: productId,
        },
        orderBy: {
          id: OrderBy.Desc,
        },
      },
    });
    if (dataOrders && typeof data !== "undefined") {
      setData((prv) => {
        if (prv) {
          return {
            ...prv,
            evaluations: [...prv.evaluations, ...dataOrders.evaluations],
          };
        }
        return;
      });
    }
    dispatch(setIsSpin(false));
  }, [dispatch, fetchMore, data, productId]);
  return (
    <>
      {data?.evaluationConnection.aggregate.countEvaluation! > 0 && (
        <div className="p-4 bg-[#fafafa] comments-md">
          {data?.evaluationConnection.aggregate.countEvaluation} Đánh giá
        </div>
      )}

      <InfiniteScroll
        dataLength={data?.evaluations?.length! || 0}
        next={handleFetchMore}
        hasMore={
          data?.evaluations?.length! ===
          data?.evaluationConnection.aggregate.countEvaluation
            ? false
            : true
        }
        loader={<LoadingProduct />}
      >
        {data &&
          data.evaluations.map((evaluation, index) => {
            return <ReviewCard key={evaluation.id} data={evaluation} />;
          })}
      </InfiniteScroll>
    </>
  );
};

export default ReviewSection;
