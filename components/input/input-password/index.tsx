import IMAGES from "@/assets/img/images";
import useTrans from "@/hooks/useTrans";
import { Input } from "antd";
import clsx from "clsx";
import Image from "next/image";
import { ChangeEvent, ChangeEventHandler, useState } from "react";
import styles from "./inputPassword.module.scss";

const InputPassword = ({
  value,
  onChange,
}: {
  value?: string;
  onChange?: Function;
}) => {
  const trans = useTrans()
  const [inputValue, setInputValue] = useState("");
  const [isFocused, setIsFocused] = useState(false);
  const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value);
    if (onChange) onChange(e.target.value);
  };
  return (
    <div className="relative">
      <Input
        type={!isFocused ? "password" : "text"}
        placeholder={trans.common.password}
        className={clsx(
          "border ease-in-out duration-300 rounded border-[#ECECEC] hover:border-[#8592ab] p-2 px-4 w-full focus:border-[#222]",
          styles.inputPassword
        )}
        onChange={onInputChange}
        value={inputValue}
      />

      <div
        className="cursor-pointer absolute right-4 top-1/2 -translate-y-1/2"
        onClick={() => setIsFocused(!isFocused)}
      >
        {isFocused ? (
          <Image src={IMAGES.eye} alt="eye-icon" />
        ) : (
          <Image src={IMAGES.eyeClose} alt="eye-icon" />
        )}
      </div>
    </div>
  );
};

export default InputPassword;
