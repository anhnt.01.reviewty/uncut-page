// import GoTopButton from "@/components/go-top-button";
import Head from "next/head";
import React, { FC, ReactNode } from "react";

// import Footer from '../Footer';
// import GoTopButton from '../GoTopButton';
// import HeaderDocument from '../Header/HeaderDocument';

import styles from "./DocLayout.module.scss";

export const DocLayout: FC<{ title?: string; children: ReactNode }> = ({
  children,
  title,
}) => {
  return (
    <>
      <Head>
        <title>{title ? `${title} - Limpi` : "Limpi"}</title>
      </Head>
      {/* <HeaderDocument /> */}
      <main className={styles.mainDoc}>{children}</main>
      {/* <GoTopButton /> */}
      {/* <Footer /> */}
    </>
  );
};

export default DocLayout;
