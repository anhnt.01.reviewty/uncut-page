import IMAGES from "@/assets/img/images";
import toCurrency from "@/common/toCurrency";
import LoadingMain from "@/components/skeleton/loading-main";
import { FundingsQuery } from "@/graphql/reviewty/graphql";
import { Skeleton } from "antd";
import clsx from "clsx";
import Image from "next/legacy/image";
import { useRouter } from "next/router";

import useTrans from "@/hooks/useTrans";
import styles from "@components/main/main.module.scss";
import { useCallback, useEffect, useState } from "react";
import { onlyUnique, sentEventLog } from "@/common/contains";
import { debounce } from "lodash";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { counter as counterSlice } from "@/components/auth/authSlice";
import { idsElement, selectedFunding } from "../mainSlice";
import EventDetailCard from "@/components/event/event-detail-card";
import FundingOrderCard from "./funding-order-card";

interface IFundingCard {
  data?: FundingsQuery;
  loadMore: () => void;
  loading: boolean;
  counter?: number;
  isFetching?: boolean;
}

const FundingCard = ({
  data,
  loadMore,
  loading,
  counter,
  isFetching,
}: IFundingCard) => {
  const dispatch = useAppDispatch();

  const [selectedId, setSelectedId] = useState<number[]>([]);

  const router = useRouter();
  const trans = useTrans();
  const getMe = useAppSelector((slice) => slice.authSlice);
  const mainSlice = useAppSelector((slice) => slice.mainSlice);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleScroll = useCallback(
    debounce(() => {
      // Calculate the card that is in the center of the viewport when scrolling stops
      const cardElements = document.querySelectorAll(".id-card");
      const middleOfViewport = window.innerHeight / 2;

      let newSelectedId: any = null; // number | null

      cardElements.forEach((cardElement) => {
        const rect = cardElement.getBoundingClientRect();

        if (rect.top < middleOfViewport) {
          newSelectedId = parseInt(cardElement.getAttribute("data-id")!, 10);
        }
      });

      if (newSelectedId) {
        setSelectedId((prev) => {
          return [...prev, newSelectedId].filter(onlyUnique);
        });
      }
    }, 0),
    []
  );

  useEffect(() => {
    // Attach the scroll event listener
    window.addEventListener("scroll", handleScroll);

    // Clean up the event listener when the component unmounts
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [handleScroll]);
  useEffect(() => {
    dispatch(idsElement(selectedId));

    if (
      getMe.me &&
      selectedId.length &&
      mainSlice.idsElement.length !== selectedId.length
    ) {
      sentEventLog({
        eventName: "Home",
        type: "view",
        asPath: router.pathname,
        userId: getMe.me?.id,
        fundingIds: selectedId,
      });
    }
  }, [
    getMe.me,
    getMe.me?.id,
    router.pathname,
    getMe.count,
    selectedId.length,
    dispatch,
    selectedId,
    mainSlice,
  ]);
  if (isFetching) {
    return (
      <div className={clsx("h-[480x] w-full col-span-12", styles.skeletonMain)}>
        <LoadingMain counter={10} />
      </div>
    );
  }

  return (
    <>
      {data?.fundings.length
        ? data?.fundings.map((item, index) => {
            if (item.event?.id) {
              return (
                <EventDetailCard
                  key={item.id}
                  data={item.event!}
                  index={index}
                />
              );
            }
            return (
              <FundingOrderCard
                key={item.id}
                item={item}
                index={index}
              ></FundingOrderCard>
            );
          })
        : null}
      <div
        className={clsx("flex col-span-12 justify-center items-center pt-4", {
          hidden: data?.fundings.length === counter,
        })}
      >
        <button
          className="flex items-center justify-center mr-2 w-[30%] text-[#8d8d8d] py-3 hover:opacity-50 transition-opacity rounded-lg"
          onClick={() => loadMore()}
        >
          {trans.common.loadMore}
          <span className="pt-1 pl-1">
            <Image src={IMAGES.arrowDown2} alt="arrow-down" />
          </span>
        </button>
      </div>
    </>
  );
};

export default FundingCard;
