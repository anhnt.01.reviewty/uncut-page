import IMAGES from "@/assets/img/images";
import toCurrency from "@/common/toCurrency";
import Progressbar from "@/components/progressbar";
import { FundingsQuery } from "@/graphql/reviewty/graphql";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { Skeleton } from "antd";
import clsx from "clsx";
import Image from "next/image";
import { useRouter } from "next/router";
import { selectedFunding } from "../../mainSlice";
import Link from "next/link";

interface IFundingOrderCard {
  item: FundingsQuery["fundings"][0];
  index?: number;
}

const FundingOrderCard = ({ item, index }: IFundingOrderCard) => {
  // debugger;
  const router = useRouter();
  const dispatch = useAppDispatch();
  const mainSlice = useAppSelector((slice) => slice.mainSlice);
  const onClickHandler = (item: FundingsQuery["fundings"][0]) => {
    dispatch(selectedFunding(item));
    router.push({
      pathname: `/product-detail/${item.id}`,
      query: {
        name: item.shortDescription,
      },
    });
  };

  const handleClick = (
    name: string,
    productId: number,
    fundingId: number,
    product: any
  ) => {
    router.push({
      pathname: "/product-detail/funding/[slug]",
      query: {
        slug: name,
        productId: Number(productId),
        fundingId: Number(fundingId),
      },
    });
  };

  return (
    <>
      {item && (
        <div
          data-id={item.id}
          key={item.id}
          className={clsx(
            "id-card relative col-span-12 grid grid-cols-12  sm:col-span-12  md:col-span-12 border-t-8",
            {
              "border-none": index === 0,
            }
          )}
        >
          <div className=" col-span-12  md:col-span-12">
            <div className="">
              <div className="">
                {item.coverUrl === "" ? (
                  ""
                ) : JSON.parse(item.coverUrl!)?.[0].url?.includes(".mp4") ? (
                  <video
                    disableRemotePlayback={true}
                    autoPlay
                    preload="metadata"
                    playsInline={true}
                    controls={true}
                    muted
                    loop
                    className={"w-full  h-auto block "}
                  >
                    <source
                      type="video/mp4"
                      src={
                        (JSON.parse(item.coverUrl!)?.[0].url as string) +
                        "?playsinline"
                      }
                    />
                  </video>
                ) : (
                  <Image
                    onClick={() => onClickHandler(item)}
                    priority
                    src={
                      JSON.parse(item.coverUrl!)?.[0]?.url || (
                        <Skeleton.Image active />
                      )
                    }
                    width="0"
                    height="0"
                    sizes="100vw"
                    className="w-full cursor-pointer h-auto block "
                    alt="img"
                  />
                )}
              </div>

              {/* {item.preOrder && (
                <div className="absolute right-[3%] top-[3%] w-[30%]">
                  <Image src={IMAGES.PreOrder} alt="pre-order-icon" />
                </div>
              )} */}

              {item.preOrder && (
                <div className="absolute left-[0%] top-[0%]  px-2">
                  <Image
                    width={50}
                    height={50}
                    src={IMAGES.Event}
                    alt="event"
                  />
                </div>
              )}
            </div>
          </div>
          <div className="col-span-12 md:col-span-12  ">
            <div className="flex flex-col justify-around h-[100%] p-3 mb-4">
              <span
                onClick={() => onClickHandler(item)}
                dangerouslySetInnerHTML={{
                  __html: item.title!.replace("/break", "<br>"),
                }}
                className="text-[24px] ellipsis-description  leading-[2rem]  mb-2 font-medium  text-[#222222]"
              ></span>
              <label
                onClick={() => onClickHandler(item)}
                className={clsx("text-[#7b7a7a]", {
                  "pb-6": !item.preOrder,
                })}
                dangerouslySetInnerHTML={{
                  __html: item?.subTitle?.replace("/break", "<br>") || "",
                }}
              ></label>
              {item.preOrder && (
                <Progressbar
                  classNameCustom="mb-6 mt-2"
                  hideText
                  containerColor="#4D57FF"
                  bgcolor="#DDDFFB"
                  completed={
                    item?.productNbOfPreOrder! > item?.preOrderQuantity!
                      ? item?.preOrderQuantity!
                      : item?.productNbOfPreOrder!
                  }
                  limit={item?.preOrderQuantity!}
                />
              )}
              {item.fundingProducts!.map((product, indexProduct) => {
                return (
                  <div
                    onClick={() =>
                      handleClick(
                        product.product?.translations[0].name!,
                        product.product?.id!,
                        item.id,
                        product
                      )
                    }
                    className="grid grid-cols-12 gap-2  pb-2  cursor-pointer"
                    key={product.product?.id}
                  >
                    {item.preOrder && (
                      <div className="absolute right-[0%] top-[0%] bg-[red] px-2 py-1 rounded-bl-xl text-white">
                        <span className="text-[white]">
                          {Math.floor(
                            ((product.product?.price! - product.discount!) /
                              product.product?.price!) *
                              100
                          ) + "%"}
                        </span>
                      </div>
                    )}
                    <div className="col-span-3">
                      <Image
                        priority
                        src={product.product?.thumbnail?.url!}
                        width="0"
                        height="0"
                        sizes="100vw"
                        alt="img"
                        className="w-[72px] h-[72px]"
                        style={{ objectFit: "cover" }}
                      />
                    </div>
                    <div className="col-span-9 flex flex-col justify-between">
                      <div className=" text-elipsis text-[gray]">
                        <span className="text-black">
                          [{product.product?.brand.translations[0].name}]
                        </span>{" "}
                        {product.product?.translations[0].name}
                      </div>

                      <div className="flex items-center">
                        <span className="mr-1 text-[#D10017]">
                          {Math.floor(
                            ((product.product?.price! - product.discount!) /
                              product.product?.price!) *
                              100
                          ) + "%"}
                        </span>
                        <p className="mr-1 text-[#222222]  font-medium">
                          {toCurrency (product.discount!)}
                        </p>
                        <p className="mr-1 line-through text-[#CCCCCC]  text-[12px]">
                          {toCurrency(product.product?.price!)}
                        </p>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default FundingOrderCard;
