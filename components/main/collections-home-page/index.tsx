import IMAGES from "@/assets/img/images";
import EventDetailCard from "@/components/event/event-detail-card";
import { ObjectType, OrderBy } from "@/graphql/reviewty-user/graphql";
import {
  FundingsQuery,
  useFundingsLazyQuery,
} from "@/graphql/reviewty/graphql";
import type { Swiper as SwiperClass } from "swiper";
import "swiper/css";
import { Swiper, SwiperSlide } from "swiper/react";
import styles from "./collections-home-page.module.scss";

import { Navigation, Pagination } from "swiper/modules";

import Image from "next/image";
import { useRouter } from "next/router";
import { MouseEvent, useEffect, useRef, useState } from "react";
import Slider from "react-slick";
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
import toCurrency from "@/common/toCurrency";
import LoadingMain from "@/components/skeleton/loading-main";
import { useAppDispatch } from "@/store/store";
import { Skeleton } from "antd";
import clsx from "clsx";
import { selectedFunding } from "../mainSlice";
import useTrans from "@/hooks/useTrans";

interface ICollections {
  onClickViewAll?: (statusFilter: ObjectType) => {};
  type: ObjectType;
  title: string;
  isShowPagination: boolean;
  itemsPerPage: number;
  headerOnly?: boolean;
  autoPlay?: boolean;
  autoPlaySpeed?: number;
  isDisabledAbsoluteNextAndPrv?: boolean;
}

const CollectionsHomePage = ({
  title,
  isShowPagination = false,
  itemsPerPage,
  type,
  headerOnly = false,
  autoPlay = false,
  autoPlaySpeed,
  isDisabledAbsoluteNextAndPrv = true,
  onClickViewAll,
}: ICollections) => {
  const trans = useTrans();

  const sliderRef = useRef<Slider>(null);
  const dispatch = useAppDispatch();

  const onClickHandler = (
    event: MouseEvent<HTMLImageElement> | MouseEvent<HTMLSpanElement>,
    item: FundingsQuery["fundings"][0]
  ) => {
    event.preventDefault();
    event.stopPropagation();

    dispatch(selectedFunding(item));
    router.push({
      pathname: `/product-detail/${item.id}`,
      query: {
        name: item.shortDescription,
      },
    });
  };


  
  const router = useRouter();
  const [getFundings, { data, loading }] = useFundingsLazyQuery({
    variables: {
      orderBy: {
        id: OrderBy.Desc,
      },
      first: 10,
      skip: 0,
      where: {
        isVisible: true,
        objectType: type,
      },
    },
  });

  const handleClick = (
    name: string,
    productId: number,
    fundingId: number,
    product: any
  ) => {
    router.push({
      pathname: "/product-detail/funding/[slug]",
      query: {
        slug: name,
        productId: Number(productId),
        fundingId: Number(fundingId),
      },
    });
  };

  useEffect(() => {
    if (type !== ObjectType.PreOrder) {
      getFundings();
    }
  }, [getFundings, router.query, type]);
  const handleViewAll = () => {
    if (onClickViewAll && type) {
      onClickViewAll(type);
    }
  };

  if (headerOnly) {
    return (
      <div className="w-full px-4">
        <div className="flex justify-between items-center py-4">
          <span className="text-[1.125rem] text-[#222] font-medium">
            {title}
          </span>
          <span
            className="cursor-pointer text-[#8d8d8d] text-[0.875rem] flex items-center"
            onClick={handleViewAll}
          >
            Tất cả{" "}
            <span className="pl-2">
              <Image
                src={IMAGES.arrowRight}
                width={12}
                height={12}
                alt="arrow-right"
              />
            </span>
          </span>
        </div>
      </div>
    );
  }

  return (
    <div className="w-full px-4 relative">
      <div className="flex justify-between items-center py-4">
        <span className="text-[1.125rem] text-[#222] font-medium">{title}</span>
        <span
          className="cursor-pointer text-[#8d8d8d] text-[0.875rem] flex items-center"
          onClick={handleViewAll}
        >
          Tất cả{" "}
          <span className="pl-2">
            <Image
              src={IMAGES.arrowRight}
              width={12}
              height={12}
              alt="arrow-right"
            />
          </span>
        </span>
      </div>

      <Swiper
        pagination={{
          el: styles.swiperPagination,
          clickable: true,
          dynamicBullets: true,
          dynamicMainBullets: 3,
        }}
        wrapperClass={clsx("", {
          "pb-4": type === ObjectType.Funding,
          "pb-6": type !== ObjectType.Funding,
        })}
        onSwiper={(swiper: SwiperClass) => swiper}
        modules={[Pagination, Navigation]}
        // autoplay={{ delay: 5000, disableOnInteraction: false }}
        // loop={true}
        slidesPerView={itemsPerPage || 1}
        // ref={swiperRef}
        // slidesToScroll={2}
        // draggable={true}
        // ref={sliderRef}
        // // {...settings}
        // autoplay={autoPlay}
        // autoplaySpeed={autoPlaySpeed}
        // infinite={true}
      >
        {data?.fundings.length === 0 && (
          <div className="col-span-12  px-8 text-center">
            <div className=" flex flex-col items-center justify-center">
              <Image src={IMAGES.eventNotYet} alt="event-not-yet" />
              <h3 className="text-[1.25rem] font-medium py-3">
                {trans.common.eventNotYet}
              </h3>
              <p className="text-[0.875rem] text-[#747474]">
                {trans.common.eventNotYetContent}
              </p>
            </div>
          </div>
        )}
        {!loading ? (
          data?.fundings.map((item, index) => {
            if (item.event?.id) {
              return (
                <SwiperSlide key={item.id}>
                  <EventDetailCard
                    type="collections"
                    data={item.event!}
                    index={index}
                  />
                </SwiperSlide>
              );
            }

            return (
              <SwiperSlide key={item.id}>
                <div
                  data-id={item.id}
                  key={item.id}
                  className={clsx(
                    "id-card col-span-12 grid grid-cols-12  sm:col-span-12  md:col-span-12 ",
                    {
                      "border-none": index === 0,
                    }
                  )}
                >
                  <div className="col-span-12  md:col-span-12">
                    <div className="relative">
                      <div
                        className="pr-2 "
                        onClick={(event) => {
                          event.preventDefault();
                          event.stopPropagation();
                        }}
                      >
                        {item.coverUrl === "" ? (
                          ""
                        ) : JSON.parse(item.coverUrl!)?.[0].url?.includes(
                            ".mp4"
                          ) ? (
                          <video
                            disableRemotePlayback={true}
                            autoPlay
                            preload="metadata"
                            playsInline={true}
                            controls={true}
                            muted
                            loop
                            className={"w-full  block "}
                          >
                            <source
                              type="video/mp4"
                              src={
                                (JSON.parse(item.coverUrl!)?.[0]
                                  .url as string) + "?playsinline"
                              }
                            />
                          </video>
                        ) : (
                          <Image
                            onClick={(event) => onClickHandler(event, item)}
                            priority
                            src={
                              JSON.parse(item.coverUrl!)?.[0]?.url || (
                                <Skeleton.Image active />
                              )
                            }
                            width="0"
                            height="0"
                            sizes="100vw"
                            className="w-full  cursor-pointer  block relative"
                            alt="img"
                            style={{
                              objectFit:
                                type !== ObjectType.Funding
                                  ? "cover"
                                  : "contain",
                              // height: "158px",
                            }}
                            // fill
                          />
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="col-span-12 md:col-span-12">
                    <div className="flex flex-col  h-[100%]">
                      <span
                        onClick={(event) => onClickHandler(event, item)}
                        dangerouslySetInnerHTML={{
                          __html: item.title!.replace("/break", "<br>"),
                        }}
                        className="my-2 pr-2 text-[1rem] ellipsis-description  text-[#222222]"
                      ></span>

                      {item.fundingProducts!.map((product, indexProduct) => {
                        return (
                          <div
                            onClick={() =>
                              handleClick(
                                product.product?.translations[0].name!,
                                product.product?.id!,
                                item.id,
                                product
                              )
                            }
                            className="grid grid-cols-12 gap-2  pb-2 relative cursor-pointer"
                            key={product.product?.id}
                          >
                            <div className="col-span-9 flex flex-col justify-between">
                              <p className="mr-1 text-[11px] line-through text-[#CCCCCC]  ">
                                {toCurrency(product.product?.price!)}
                              </p>

                              <div className="flex justify-start items-center">
                                <span className="mr-1 text-[#D10017] text-[14px]">
                                  {Math.floor(
                                    ((product.product?.price! -
                                      product.discount!) /
                                      product.product?.price!) *
                                      100
                                  ) + "%"}
                                </span>
                                <p className="mr-1 text-[#222222] ">
                                  {toCurrency(product.discount!)}
                                </p>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </SwiperSlide>
            );
          })
        ) : (
          <LoadingMain counter={1} />
        )}
      </Swiper>
      {/* {!isDisabledAbsoluteNextAndPrv && (
        <>
          <span
            className={clsx(
              "absolute  top-1/3 -translate-y-1/3 left-0   cursor-pointer"
            )}
            onClick={() => sliderRef?.current?.slickPrev()}
          >
            <Image
              src={IMAGES.arrowRight}
              alt="arrow-right"
              className="rotate-180"
            />
          </span>
          <span
            className={clsx(
              "cursor-pointer absolute top-1/3 right-0 -translate-y-1/3",
              {
                // hidden: Math.ceil(currentSlide + 1) >= data?.fundings.length!,
              }
            )}
            onClick={() => sliderRef?.current?.slickNext()}
          >
            <Image src={IMAGES.arrowRight} alt="arrow-right" />
          </span>
        </>
      )} */}
    </div>
  );
};

export default CollectionsHomePage;
