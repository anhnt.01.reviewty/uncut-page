import { useCallback, useEffect, useState } from "react";

import "swiper/css";
import "swiper/css/pagination";

import { clsx } from "clsx";

import {
  FundingsQuery,
  FundingStatus,
  OrderBy,
  useFundingsLazyQuery,
} from "@/graphql/reviewty/graphql";

import { useAppDispatch } from "@/store/store";
import { useRouter } from "next/router";
import { NextPageWithLayout } from "../../pages/_app";

import IMAGES from "@/assets/img/images";
import { setIsSpin } from "@/components/auth/authSlice";
import DragScroll from "@/components/drag-scroll";
import FundingCard from "@/components/main/fundings-card";
import LoadingMain from "@/components/skeleton/loading-main";
import { ObjectType } from "@/graphql/reviewty-user/graphql";
import { useActiveCustomer } from "@/hooks/useActiveCustomer";
import useTrans from "@/hooks/useTrans";
import { useViewGGAnalysis } from "@/hooks/useViewGGANL";
import Image from "next/legacy/image";
import EventCollections from "./collections-home-page";
import styles from "./main.module.scss";
import { REVIEWTY_TOKEN } from "@/common/contains";
import { setTimeout } from "timers/promises";
import { useSearchParams } from "next/navigation";

const Main: NextPageWithLayout = () => {
  const [ref, setRef] = useState<HTMLSpanElement | null>(null);
  const [isFetching, setIsFetching] = useState(true);
  const router = useRouter();
  const searchParams = useSearchParams();

  const { ObjectType: ObjectTypeQuery, timeEvent: TimeEventQuery } =
    router.query;
  const [agent, setAgent] = useState("");
  const dispatch = useAppDispatch();
  const trans = useTrans();
  useActiveCustomer();

  useViewGGAnalysis({
    eventName: "user_view_main",
    eventCategory: "MAIN",
    eventLabel: "VIEW",
  });

  const [data, setData] = useState<FundingsQuery>();
  const [isLoadMore, setIsLoadMore] = useState(false);
  const [getFundings, { data: dataFundings, fetchMore, variables }] =
    useFundingsLazyQuery({
      variables: {
        orderBy: {
          id: OrderBy.Desc,
        },
        first: 10,
        skip: 0,
        where: {
          isVisible: true,

          ...(ObjectTypeQuery !== "" && {
            objectType: ObjectTypeQuery as ObjectType.PreOrder,
          }),
        },
      },
      onCompleted(data) {
        setData(data);
      },
    });

  useEffect(() => {
    setAgent(localStorage.getItem(REVIEWTY_TOKEN) || "");
  }, []);
  useEffect(() => {
    if (!ref) {
      return;
    }
    ref.scrollIntoView({
      behavior: "smooth",
      block: "end",
      inline: "end",
    });
  }, [ref]);
  useEffect(() => {
    (async () => {
      setIsFetching(true);

      await getFundings({
        variables: {
          orderBy: {
            id: OrderBy.Desc,
          },
          first: 10,
          skip: 0,
          where: {
            isVisible: true,

            objectType: ObjectType.Event,

            statusFilter: FundingStatus.Running,
          },
        },
      });
      router.push(
        {
          pathname: "/",
          query: {
            ObjectType: ObjectType.Event,
            timeEvent: FundingStatus.Running,
          },
        },
        undefined,
        { shallow: false }
      );

      // router.push(
      //   {
      //     pathname: "/",
      //     query: {
      //       // ...(statusFilter && { ObjectType: statusFilter }),
      //       // timeEvent: FundingStatus.Running,
      //     },
      //   },
      //   undefined,
      //   { shallow: false }
      // );

      await new Promise((resolve) => {
        resolve("");
      }).then(() => {
        setIsFetching(false);
      });
    })();
  }, []);

  // const barItems = [
  //   {
  //     id: "all",
  //     label: `${trans?.home.common.all}`,
  //     status: undefined,
  //   },
  //   {
  //     id: "inprogress",
  //     label: `Pre-Order`,
  //     status: ObjectType.PreOrder,
  //   },
  //   {
  //     id: "up-comming",
  //     label: `Sự kiện`,
  //     status: ObjectType.Event,
  //   },
  //   {
  //     id: "took-place",
  //     label: `Funding`,
  //     status: ObjectType.Funding,
  //   },
  // ];
  const filterEvent = [
    {
      id: "inprogress",
      label: `${trans.home.common.inprogress}`,
      statusEvent: FundingStatus.Running,
    },
    {
      id: "up-comming",
      label: `${trans.home.common.upcomming}`,

      statusEvent: FundingStatus.Created,
    },
    {
      id: "took-place",
      label: `${trans.home.common.stopped}`,

      statusEvent: FundingStatus.Stopped,
    },
  ];
  const handleClickBarITems = useCallback(
    async (statusFilter?: ObjectType, timeEvent?: FundingStatus) => {
      dispatch(setIsSpin(true));
      setIsLoadMore(true);
      setData(undefined);

      router.push(
        {
          pathname: "/",
          query: {
            ...(statusFilter && { ObjectType: statusFilter }),
            ...((timeEvent || statusFilter === ObjectType.Event) && {
              timeEvent: timeEvent || FundingStatus.Running,
            }),
          },
        },
        undefined,
        { shallow: false }
      );

      await getFundings({
        variables: {
          orderBy: {
            id: OrderBy.Desc,
          },
          first: 10,
          skip: 0,
          where: {
            isVisible: true,
            // objectType: ObjectType.PreOrder,
            // statusFilter: timeEvent,
            ...(statusFilter && { objectType: statusFilter }),
            ...((timeEvent || statusFilter === ObjectType.Event) && {
              statusFilter: timeEvent || FundingStatus.Running,
            }),
          },
        },
        fetchPolicy: "no-cache",
      });
      setIsLoadMore(false);

      dispatch(setIsSpin(false));

      return;
    },
    [dispatch, getFundings, router]
  );

  const handleLoadMore = useCallback(async () => {
    setIsLoadMore(true);

    const { data: response } = await fetchMore({
      variables: {
        first: 10,
        skip: data?.fundings.length || 0,
        orderBy: {
          id: OrderBy.Desc,
        },
        where: {
          ...(ObjectTypeQuery !== "" && {
            objectType: ObjectTypeQuery as ObjectType,
          }),
          ...((TimeEventQuery || ObjectTypeQuery === ObjectType.Event) && {
            statusFilter:
              (TimeEventQuery as FundingStatus) || FundingStatus.Running,
          }),
        },
      },
    });

    if (response && typeof data !== "undefined") {
      setData((prv) => {
        if (prv) {
          return {
            ...prv,
            fundings: [...prv.fundings, ...response.fundings],
          };
        }
      });
    }

    setIsLoadMore(false);

    return;
  }, [ObjectTypeQuery, TimeEventQuery, data, fetchMore]);

  return (
    <div>
      <div className="w-full">
        <DragScroll isSticky={true}>
          <div className=" cursor-pointer flex  scrollbar">
            {/* {barItems.map((item, index) => (
              <span
                ref={(ref) => {
                  if (item.status === ObjectTypeQuery) {
                    setRef(ref);
                  }
                }}
                key={item.id}
                onClick={() => {
                  handleClickBarITems(item.status);
                }}
                className={clsx(
                  " cursor-pointer w-[100%]  scrollbar sm:px-2 py-2 col-span-3 text-center  sm:text-[1rem]  pt-2  ",
                  {
                    "text-[#BA92D9] border-b-2 border-b-[#BA92D9]":
                      item.status === ObjectTypeQuery,

                    "border-b-[#ececec] border-b-2 text-[#8D8D8D]":
                      item.status !== ObjectTypeQuery,
                  }
                )}
              >
                {item.label}
              </span>
            ))} */}
          </div>
        </DragScroll>
        {/* {!ObjectTypeQuery && (
          <>
            <EventCollections
              onClickViewAll={handleClickBarITems}
              type={ObjectType.Event}
              title="Reviewty Events"
              itemsPerPage={1}
              isShowPagination
            />
            <EventCollections
              onClickViewAll={handleClickBarITems}
              type={ObjectType.Funding}
              title="Funding"
              itemsPerPage={2.5}
              isShowPagination={false}
              isDisabledAbsoluteNextAndPrv={false}
            />
            <EventCollections
              onClickViewAll={handleClickBarITems}
              type={ObjectType.PreOrder}
              headerOnly
              title="Pre-Order"
              itemsPerPage={2.7}
              isShowPagination={false}
            />
          </>
        )} */}
        <div className="pl-4 py-4">
          {/* {console.log(variables)} */}
          {variables?.where?.objectType === "EVENT" &&
            filterEvent.map((item, index) => (
              <span
                key={item.id}
                onClick={() => {
                  handleClickBarITems(ObjectType.Event, item.statusEvent);
                }}
                className={clsx(
                  " cursor-pointer w-[30%] mr-2 rounded-[60px] scrollbar px-3 py-1 col-span-3 text-center  sm:text-[1rem]  pt-1  ",
                  {
                    "text-[#BA92D9] border-2 border-[#BA92D9] bg-[#ba92d91f]":
                      item.statusEvent === router.query["timeEvent"],

                    "border-[#ececec] border-2 text-[#8D8D8D]":
                      item.statusEvent !== router.query["timeEvent"],
                  }
                )}
              >
                {item.label}
              </span>
            ))}
        </div>

        <div className="max-w-screen-xl grid grid-cols-12  bg-[white]  m-auto">
          {data?.fundings.length === 0 && (
            <div className="col-span-12  px-8 text-center">
              <div className="h-[80vh] flex flex-col items-center justify-center">
                <Image src={IMAGES.eventNotYet} alt="event-not-yet" />
                <h3 className="text-[1.25rem] font-medium py-3">
                  {trans.common.eventNotYet}
                </h3>
                <p className="text-[0.875rem] text-[#747474]">
                  {trans.common.eventNotYetContent}
                </p>
              </div>
            </div>
          )}

          <FundingCard
            isFetching={isFetching}
            data={data}
            loadMore={handleLoadMore}
            loading={isLoadMore}
            counter={data?.fundingsConnection.aggregate.countFunding}
          ></FundingCard>
          {isLoadMore && (
            <div
              className={clsx(
                "h-[480x] w-full col-span-12",
                styles.skeletonMain
              )}
            >
              <LoadingMain counter={10} />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Main;
