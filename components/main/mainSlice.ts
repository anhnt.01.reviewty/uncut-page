import {
  createAsyncThunk,
  createSlice,
  PayloadAction,
  SliceCaseReducers,
} from "@reduxjs/toolkit";

// import { initializeApollo } from "@lib/apollo";
import {
  OrderStatisticOfUserDocument,
  OrderStatisticOfUserQuery,
  OrderStatisticOfUserQueryVariables,
} from "@/graphql/reviewty-user/graphql";
import {
  FundingDetailQuery,
  FundingsQuery,
  GetEventListDocument,
  GetEventListQuery,
  GetEventListQueryVariables,
} from "@/graphql/reviewty/graphql";
import { initializeApollo } from "@/hooks/useApollo";
// import { AppState, AppThunkStatus } from '@lib/redux';
export enum EAction {
  pickAddressBeforeJoinEvent = "pickAddressBeforeJoinEvent",
}

interface AuthState {
  data?: GetEventListQuery;
  NumberOrderStatistic?: OrderStatisticOfUserQuery;
  idsElement: number[];
  isOpen: boolean;
  selectedItem?: FundingDetailQuery["funding"];
  counter: number;
  action?: EAction;
  dataDetail: {
    data?: FundingsQuery["fundings"][0];
    product?: FundingDetailQuery["funding"]["fundingProducts"][0];
  };
  historyBack?: string;
  // detailData?:
}

const initialState: AuthState = {
  data: undefined,
  NumberOrderStatistic: undefined,
  idsElement: [],
  isOpen: false,
  selectedItem: undefined,
  counter: 1,
  action: undefined,
  dataDetail: {
    data: undefined,
    product: undefined,
  },
  historyBack: undefined,
};

export const fetchEventList = createAsyncThunk(
  "main/getEventList",
  async () => {
    const apolloClient = initializeApollo();
    const { data, error, errors } = await apolloClient.query<
      GetEventListQuery,
      GetEventListQueryVariables
    >({
      query: GetEventListDocument,
    });
    if (error || errors) {
      throw error || errors;
    }
    return data;
  }
);

export const fetchNumberOrderStatistic = createAsyncThunk(
  "main/getNumberOrderStatistic",
  async () => {
    const apolloClient = initializeApollo();
    const { data, error, errors } = await apolloClient.query<
      OrderStatisticOfUserQuery,
      OrderStatisticOfUserQueryVariables
    >({
      query: OrderStatisticOfUserDocument,
      fetchPolicy: "no-cache",
    });
    if (error || errors) {
      throw error || errors;
    }
    return data;
  }
);

const mainSlice = createSlice<AuthState, SliceCaseReducers<AuthState>, string>({
  name: "auth",
  initialState,
  reducers: {
    action: (state, action: PayloadAction<EAction>) => {
      state.action = action.payload;
    },
    clearAction: (state) => {
      state.action = undefined;
    },
    addHistoryBack: (state, action) => {
      state.historyBack = action.payload;
    },
    counter: (state, action) => {
      state.counter++;
    },
    selectedFunding: (state, action) => {
      state.selectedItem = action.payload;
    },
    selectedDetail: (state, action) => {
      state.dataDetail = action.payload;
    },
    resetMainSlice: (state) => {
      return initialState;
    },

    idsElement: (state, action) => {
      state.idsElement = action.payload;
    },
    setIsOpen: (state, action) => {
      state.isOpen = !state.isOpen;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchEventList.pending, (state) => {
        // state.status = "loading";
      })
      .addCase(fetchEventList.fulfilled, (state, action) => {
        // state.status = "succeeded";
        state.data = action.payload;
        // state.customer = action.payload.customer || undefined;
      })
      .addCase(fetchEventList.rejected, (state, action) => {
        // state.status = "failed";
        // state.error = action.error;
      })
      .addCase(fetchNumberOrderStatistic.pending, (state) => {
        // state.status = "loading";
      })
      .addCase(fetchNumberOrderStatistic.fulfilled, (state, action) => {
        // state.status = "succeeded";
        state.NumberOrderStatistic = action.payload;
        // state.customer = action.payload.customer || undefined;
      })
      .addCase(fetchNumberOrderStatistic.rejected, (state, action) => {
        // state.status = "failed";
        // state.error = action.error;
      });
  },
});

export const {
  selectedDetail,
  clearAction,
  action,
  counter,
  addHistoryBack,
  selectedFunding,
  idsElement,
  setIsOpen,
  resetMainSlice,
} = mainSlice.actions;

export default mainSlice.reducer;
