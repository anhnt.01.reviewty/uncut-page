import toCurrency from "@/common/toCurrency";
import { GetOneFundingOrderQuery } from "@/graphql/reviewty-user/graphql";
import useTrans from "@/hooks/useTrans";
import { Skeleton } from "antd";
import clsx from "clsx";
import Image from "next/legacy/image";

interface IFundingProduct {
  data: GetOneFundingOrderQuery;
  isHidePrice?: boolean;
}

const FundingProductCard = ({ data, isHidePrice = true }: IFundingProduct) => {
  const trans = useTrans();

  return (
    <>
      {data?.fundingOrder.fundingProducts.length ? (
        data?.fundingOrder?.fundingProducts.map((fundingProduct, index) => {
          return (
            <div key={fundingProduct.id}>
              <div className="grid grid-cols-12" key={Math.random()}>
                <div className="col-span-3">
                  <Image
                    width={70}
                    height={70}
                    src={fundingProduct.product?.thumbnail?.url!}
                    alt={fundingProduct.product?.translations[0].name}
                  />
                </div>
                <label
                  className={clsx("col-span-6", {
                    "col-span-9": !isHidePrice,
                  })}
                >
                  <span className="ellipsis-description ">
                    {fundingProduct.product?.translations[0].name}
                  </span>
                  <p className="text-[#8D8D8D] pt-2">
                    {data?.fundingOrder.quantity}{" "}
                    {trans.common.product.toLowerCase()}
                  </p>
                </label>
                {isHidePrice && (
                  <label className="col-span-3 flex justify-end items-center">
                    <span className="ellipsis-description text-[#BE2827]">
                      {toCurrency(
                        data.fundingOrder.discount! *
                          data.fundingOrder.quantity!
                      )}
                    </span>
                  </label>
                )}
              </div>
            </div>
          );
        })
      ) : (
        <Skeleton active paragraph={{ rows: 2 }} />
      )}
    </>
  );
};

export default FundingProductCard;
