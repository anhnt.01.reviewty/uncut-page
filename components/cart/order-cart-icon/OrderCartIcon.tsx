import IMAGES from "@/assets/img/images";
import { useActiveCustomer } from "@/hooks/useActiveCustomer";
import clsx from "clsx";
import Image from "next/legacy/image";
import { useRouter } from "next/router";
import { FC, useCallback } from "react";

// import { Cart } from "@components/ui/icons";
// import { useActiveCustomer } from "@lib/hook/useActiveCustomer";
// import { useCart } from "@lib/hook/useCart";

interface OrderCartIconProps {
  className?: string | string[];
  iconClassName?: string | string[];
}

const OrderCartIcon: FC<OrderCartIconProps> = ({
  className,
  iconClassName,
}) => {
  const router = useRouter();
  const { data, status } = useActiveCustomer();

  const handleClick = useCallback(() => {
    if (!data?.id) {
      router.push({
        pathname: "/auth/login",
        // query: { callbackUrl: "/cart" },
      });
      return;
    }
    router.push("/cart");
  }, [data?.id, router]);

  return (
    <div
      className={clsx("relative cursor-pointer ", className)}
      onClick={handleClick}
    >
      <div className="relative">
        <Image width={24} height={24} src={IMAGES.card} alt="cart-icon" />
        {/* <Cart className={clsx("h-6 w-6", iconClassName)} /> */}
        <div className="flex justify-center items-center w-[18px] h-[18px] bg-[red] text-white text-xs font-semibold rounded-full absolute -top-[10px] -right-[10px]">
          <span>1111</span>
        </div>
      </div>
    </div>
  );
};

export default OrderCartIcon;
