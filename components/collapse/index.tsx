import IMAGES from "@/assets/img/images";
import clsx from "clsx";
import Image from "next/legacy/image";
import React, { FC, ReactNode, useState } from "react";

interface CollapseProps {
  title: string;
  className?: string;
  children: ReactNode;
}
// TODO: Collapse(List), Collapse.Panel, expandIcon, activeKey....
const Collapse: FC<CollapseProps> = ({ title, children, className }) => {
  const [showInfo, setShowInfo] = useState(false);
  return (
    <div className={clsx("w-full", className)}>
      <div
        className="min-h-8  w-full py-4 cursor-pointer"
        onClick={() => setShowInfo((p) => !p)}
      >
        <div className="flex items-center justify-between text-default">
          <span className="mr-1 font-semibold">{title}</span>
          <div className="w-4 h-4">
            <div>
              <Image
                className={clsx("", {
                  "rotate-180": showInfo,
                })}
                src={IMAGES.arrowDown}
                alt="arrow-down"
                style={{ transition: "all 0.5s" }}
              />
            </div>
          </div>
        </div>
      </div>
      <div
        className={clsx(" pb-2", {
          hidden: !showInfo,
        })}
      >
        {children}
      </div>
    </div>
  );
};

export default Collapse;
