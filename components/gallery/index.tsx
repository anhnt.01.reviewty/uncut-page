// import { LightGallery } from "lightgallery/lightgallery";
import LightGallery from "lightgallery/react";

import { useCallback, useEffect, useRef, useState } from "react";
import lgZoom from "lightgallery/plugins/zoom";
import lgVideo from "lightgallery/plugins/video";
import styles from "./gallery.module.scss";
import IMAGES from "@/assets/img/images";
import clsx from "clsx";

interface IGalleryCard {
  images: {
    url: string;
    name: string;
  }[];
}

const GalleryCard = ({ images }: IGalleryCard) => {
  return (
    <div className="">
      <LightGallery
        download={false}
        speed={500}
        plugins={[lgVideo]}
        videojs={false}
        autoplay={false}
        videojsTheme="vjs-theme-fantasy"
        elementClassNames="grid grid-cols-4 gap-4"
      >
        {images?.map((img) => {
          if (img.url.includes(".mp4")) {
            return (
              <div
                style={{
                  background: `url(${IMAGES.VideoDefault.src}) no-repeat right center`,
                  backgroundPosition: "center",
                  backgroundSize: "contain",
                  backgroundRepeat: "no-repeat",

                  height: "90px",
                }}
                key={img.url}
                className={clsx("", styles.previewVideo)}
                data-video={`{"source": [{"src":"${img.url}", "type":"video/mp4"}], "attributes": {"preload": true, "controls": true,"autoplay":false}}`}
              ></div>
            );
          }

          return (
            <div
              key={img.url}
              className={clsx(styles.previewImg)}
              data-src={img.url}
              style={{
                background: `url(${img.url})`,
                backgroundPosition: "bottom",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                height: "90px",
              }}
            ></div>
          );
        })}
      </LightGallery>
    </div>
  );
};

export default GalleryCard;
