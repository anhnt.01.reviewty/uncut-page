import IMAGES from "@/assets/img/images";
import { handleCheckAuth, IS_MOBILE, REVIEWTY_TOKEN } from "@/common/contains";
import { resetAllSlices } from "@/common/resetAllSlice";
import { DeviceDetectContext } from "@/context/DeviceContext";
import { FundingOrderStatus } from "@/graphql/reviewty-user/graphql";
import useTrans from "@/hooks/useTrans";
import { useAppSelector } from "@/store/store";
import { Col, Drawer, Modal, Row } from "antd";
import clsx from "clsx";
import Image from "next/legacy/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { MouseEvent, useContext, useEffect, useMemo, useState } from "react";
import { useDispatch } from "react-redux";
import { clearMe, fetchActiveCustomer } from "../auth/authSlice";
import SelectLanguage from "../select-language";

const NavigationMenu = () => {
  const devices = useContext(DeviceDetectContext);
  const mainSlice = useAppSelector((slice) => slice.mainSlice);
  const { NumberOrderStatistic } = mainSlice;
  const dispatch = useDispatch();
  const router = useRouter();
  const trans = useTrans();
  const [open, setOpen] = useState(false);
  const activeCustomer = useAppSelector((state) => state.authSlice);
  const onClose = () => {
    setOpen(false);
  };

  const menuNavigators = useMemo(() => {
    const navigators = [
      {
        href: "/profile/address-list",
        label: trans.home.navigationMenu.addressInformation,
        enable: true,
      },
      {
        href: "/auth/login",
        label: trans.home.navigationMenu.logout,
        onClick: (e: MouseEvent<HTMLAnchorElement>) => {
          localStorage.removeItem(REVIEWTY_TOKEN);
          // dispatch(clearMe({}));
          dispatch(resetAllSlices());
        },
        // &&
        //   !devices.isWebViewAndroid &&
        //   !devices.isWebViewIOS &&
        //   !devices.isWebView
        enable: activeCustomer.me ? true : false,
      },
    ];
    const enabledNavigators = navigators.filter((menu) => menu.enable);
    return enabledNavigators;
  }, [
    activeCustomer.me,
    devices.isWebViewAndroid,
    devices.isWebViewIOS,
    dispatch,
    trans.home.navigationMenu.addressInformation,
    trans.home.navigationMenu.logout,
  ]);

  const renderNavigateOrder = [
    {
      imgUrl: IMAGES.toPay,
      type: FundingOrderStatus.InProcessing,
      text: trans.home.navigationMenu.waitForPay,
      alt: "to-pay",
      counter: NumberOrderStatistic?.nbOrderStatisticOfUser.filter(
        (numberOrderStatistic) =>
          numberOrderStatistic.orderStatus === FundingOrderStatus.InProcessing
      ),
    },
    {
      imgUrl: IMAGES.toShip,
      type: FundingOrderStatus.WaitDelivering,
      text: trans.home.navigationMenu.waitDelivering,
      alt: "to-ship",
      counter: NumberOrderStatistic?.nbOrderStatisticOfUser.filter(
        (numberOrderStatistic) =>
          numberOrderStatistic.orderStatus === FundingOrderStatus.WaitDelivering
      ),
    },
    {
      imgUrl: IMAGES.toReceive,
      type: FundingOrderStatus.Delivering,
      text: trans.home.navigationMenu.delivering,
      alt: "to-receive",
      counter: NumberOrderStatistic?.nbOrderStatisticOfUser.filter(
        (numberOrderStatistic) =>
          numberOrderStatistic.orderStatus === FundingOrderStatus.Delivering
      ),
    },
    {
      imgUrl: IMAGES.delivered,
      type: FundingOrderStatus.Completed,
      text: "Đánh giá",
      alt: "delivered",
      counter: NumberOrderStatistic?.nbOrderStatisticOfUser.filter(
        (numberOrderStatistic) =>
          numberOrderStatistic.orderStatus === FundingOrderStatus.Completed
      ),
    },
  ];

  const render = useMemo(() => {
    return activeCustomer.me?.id ? (
      <div className="flex justify-between items-center">
        <label className="">
          {trans.home.navigationMenu.hi},{" "}
          <span className="text-[#404040] font-medium">
            {activeCustomer.me?.email || activeCustomer.me.account || ""}
          </span>
        </label>
      </div>
    ) : devices.isWebViewAndroid || devices.isWebViewIOS ? (
      <></>
    ) : (
      <label
        className="cursor-pointer bg-[#ba92d9] text-white p-3 rounded-lg"
        onClick={handleCheckAuth}
      >
        {trans.common.signIn}
      </label>
    );
  }, [
    activeCustomer.me?.account,
    activeCustomer.me?.email,
    activeCustomer.me?.id,
    devices.isWebViewAndroid,
    devices.isWebViewIOS,
    trans.common.signIn,
    trans.home.navigationMenu.hi,
  ]);

  return (
    <>
      <Drawer
        title={render}
        placement="right"
        closable={false}
        onClose={onClose}
        open={open}
        extra={
          <span onClick={onClose} className="cursor-pointer">
            <Image src={IMAGES.closeIcon} alt="close-icon" />
          </span>
        }
      >
        {/* <div className="flex justify-between items-center pb-3">
          <label className="text-[1.125rem]  text-[#222] font-medium">
            {trans.home.navigationMenu.language}
          </label>
          <div className="text-[#8d8d8d] flex items-center">
            <div className="flex justify-between items-center">
              <SelectLanguage />
            </div>
          </div>
        </div> */}
        {/* {activeCustomer.me?.id && (
          <div>
            <div className="flex justify-between items-center">
              <label className="text-[1.125rem] text-[#222] font-medium">
                {trans.home.navigationMenu.myOrder}
              </label>
              <div
                onClick={() => {
                  router.push({
                    pathname: "orders",
                    query: {
                      type: FundingOrderStatus.InProcessing,
                    },
                  });
                }}
                className="text-[#8d8d8d] flex items-center"
              >
                <label className="mr-1 cursor-pointer">
                  {trans.home.navigationMenu.orderHistory}
                </label>
                <span className="pt-2">
                  <Image src={IMAGES.arrowRight} alt="arrow-right" />
                </span>
              </div>
            </div>
            <Row className="text-center my-6" gutter={[8, 0]}>
              {renderNavigateOrder.map((item) => {
                return (
                  <Col
                    key={item.type}
                    span={6}
                    className="flex  flex-col cursor-pointer"
                    onClick={() => {
                      router.push({
                        pathname: "orders",
                        query: {
                          type: item.type,
                        },
                      });
                    }}
                  >
                    <span className="flex items-center justify-center">
                      <span className="relative">
                        <Image src={item.imgUrl} alt={item.alt}></Image>
                        {item.counter && (
                          <p className="bg-[#BA92D9] text-[white] border absolute -right-2 -top-2 p-2 text-[12px] w-[1rem] h-[1rem] rounded-full flex items-center justify-center ">
                            {item.counter[0].orderStatus ===
                            FundingOrderStatus.Completed
                              ? item.counter[0].orderTotal! -
                                item.counter[0].evaluationTotal!
                              : item.counter?.[0]?.orderTotal}
                          </p>
                        )}
                      </span>
                    </span>

                    <span className="text-[0.75rem] pt-3">{item.text}</span>
                  </Col>
                );
              })}
            </Row>
            <div>
              {menuNavigators.map(({ label, href, onClick }, index) => {
                return (
                  <Link href={href} key={href} onClick={onClick}>
                    <div
                      className={clsx(
                        "py-6 bg-white mb-[1px]  flex items-center  justify-between border-t-2",
                        {
                          "border-b-2": index === menuNavigators.length - 1,
                        }
                      )}
                    >
                      <span className="text-black text-[1.125rem] font-medium">
                        {label}
                      </span>
                      <Image src={IMAGES.arrowRight} alt="arrow-right" />
                    </div>
                  </Link>
                );
              })}
            </div>
          </div>
        )} */}
      </Drawer>
      <button
        onClick={() => {
          // if (!localStorage.getItem(REVIEWTY_TOKEN)) {
          //   setIsModalOpen(!isModalOpen);
          //   return;
          // }
          setOpen(!open);
        }}
      >
        <Image src={IMAGES.sideBar} alt="side-bar" />
      </button>
    </>
  );
};

export default NavigationMenu;
