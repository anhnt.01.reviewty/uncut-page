const ProgressBar = (props: {
  bgcolor: string;
  completed: number;
  limit?: number;
  hideText?: boolean;
  containerColor?: string;
  classNameCustom?: string;
}) => {
  const {
    bgcolor,
    completed,
    limit,
    hideText = false,
    containerColor,
    classNameCustom,
  } = props;

  const containerStyles = {
    height: 8,
    width: "100%",
    backgroundColor: bgcolor || "#e0e0de",
    borderRadius: 50,
    // margin: 16,
  };

  const fillerStyles = {
    height: "100%",
    width: `${(completed / limit!) * 100}%`,
    backgroundColor: containerColor || "#F2C342",
    borderRadius: "inherit",
    right: 0,
  };

  return (
    <div className={classNameCustom || "relative my-4"} style={containerStyles}>
      <div className="" style={fillerStyles}>
        {!hideText && (
          <span className="absolute left-1/2 top-1/2 -translate-x-1/2 text-[14px] -translate-y-1/2 padding-1 text-[#7003BB] ">{`${completed}/${limit}`}</span>
        )}
      </div>
    </div>
  );
};

export default ProgressBar;
