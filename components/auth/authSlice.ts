import { REVIEWTY_LOCALE, REVIEWTY_TOKEN } from "@/common/contains";
import {
  GetMeDocument,
  GetMeQuery,
  GetMeQueryVariables,
  GetShippingAddressesDocument,
  GetShippingAddressesQuery,
  GetShippingAddressesQueryVariables,
  OrderBy,
} from "@/graphql/reviewty-user/graphql";
import {
  AUTH_TOKEN,
  initializeApollo,
  REVIEWTY_CLIENT,
} from "@/hooks/useApollo";
import { AppState, AppThunkStatus } from "@/store/store";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import Router from "next/router";

interface AuthState {
  count: number;
  lang?: "vi" | "en";
  me?: GetMeQuery["me"];
  status: AppThunkStatus;
  error: any;
  addresses: {
    status: AppThunkStatus;
    data: GetShippingAddressesQuery["shippingAddresses"];
    default: GetShippingAddressesQuery["shippingAddresses"];
    error: any;
  };
  spinning: boolean;
}

const initialState: AuthState = {
  count: 0,
  lang: undefined,
  status: "idle",
  error: null,
  me: undefined,
  addresses: {
    status: "idle",
    data: [],
    default: [],
    error: null,
  },
  spinning: false,
};

export const fetchActiveCustomer = createAsyncThunk(
  "auth/fetchActiveCustomer",
  async () => {
    try {
      const apolloClient = initializeApollo();
      const { data, error, errors } = await apolloClient.query<
        GetMeQuery,
        GetMeQueryVariables
      >({
        query: GetMeDocument,
        context: {
          clientName: REVIEWTY_CLIENT,
          headers: {
            Authorization: `Bearer ${localStorage.getItem(AUTH_TOKEN)}`,
          },
        },
        fetchPolicy: "no-cache",
      });
      return {
        me: data.me,
      };
    } catch (error) {
      if (
        (error as any).message ===
        "Access denied! You need to be authorized to perform this action!"
      ) {
        // localStorage.removeItem(REVIEWTY_TOKEN);
        // router.push("https://review-ty.com/auth");
      }
    }
  }
);

export const fetchAddresses = createAsyncThunk<
  GetShippingAddressesQuery["shippingAddresses"]
>("auth/fetchAddresses", async () => {
  const apolloClient = initializeApollo();
  const { data, error, errors } = await apolloClient.query<
    GetShippingAddressesQuery,
    GetShippingAddressesQueryVariables
  >({
    query: GetShippingAddressesDocument,
    // variables: {
    //   orderBy: {
    //     id: OrderBy.Desc,
    //   },
    // },
    context: {
      clientName: REVIEWTY_CLIENT,
      headers: {
        Authorization: `Bearer ${localStorage.getItem(AUTH_TOKEN)}`,
      },
    },
    fetchPolicy: "no-cache",
  });

  if (error || errors) {
    // throw error || errors;
  }

  return data.shippingAddresses;
});

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    resetAuthSlice: (state) => initialState,
    counter: (state, action) => {
      state.count++;
    },
    clearMe: (state, action) => {
      state.me = undefined;
      state.status = "idle";
    },
    setIsSpin: (state, action) => {
      state.spinning = action.payload;
    },
    setDefaultLang: (state, action) => {
      state.lang = action.payload;
    },
    deleteAddress: (state, action) => {
      state.addresses.data = state.addresses.data.filter(
        (d) => d.id !== action.payload
      );
    },
    selectedAddress: (state, action) => {
      state.addresses.default = action.payload;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(fetchActiveCustomer.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchActiveCustomer.fulfilled, (state, action) => {
        state.status = "succeeded";
        if (action.payload?.me) {
          state.me = action.payload.me;
        }
      })
      .addCase(fetchActiveCustomer.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error;
      })
      .addCase(fetchAddresses.pending, (state) => {
        state.addresses.status = "loading";
      })
      .addCase(fetchAddresses.fulfilled, (state, action) => {
        state.addresses.status = "succeeded";
        state.addresses.data = action.payload;
        state.addresses.default = action.payload.filter(
          (shippingAddress) => shippingAddress.default === true
        );
      })
      .addCase(fetchAddresses.rejected, (state, action) => {
        state.addresses.status = "failed";
        state.addresses.error = action.error;
      });
  },
});

export const selectAddresses = (state: AppState) => state.authSlice.addresses;

export const {
  deleteAddress,
  clearMe,
  setIsSpin,
  setDefaultLang,
  selectedAddress,
  counter,
  resetAuthSlice,
} = authSlice.actions;

export default authSlice.reducer;
