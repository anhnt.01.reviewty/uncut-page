import React from "react";
import { DownOutlined, SmileOutlined } from "@ant-design/icons";
import type { MenuProps } from "antd";
import { Dropdown, Space } from "antd";
import { useAppDispatch, useAppSelector } from "@/store/store";
import Image from "next/legacy/image";
import IMAGES from "@/assets/img/images";
import { REVIEWTY_LOCALE } from "@/common/contains";
import { setDefaultLang } from "../auth/authSlice";

const SelectLanguage: React.FC = () => {
  const dispatch = useAppDispatch();
  const items: MenuProps["items"] = [
    {
      key: "1",
      label: <span>VI</span>,
      onClick: () => handleChangeLang("vi"),
    },
    {
      key: "2",
      label: <span>EN</span>,
      onClick: () => handleChangeLang("en"),
      // icon: <Image src={IMAGES.language} alt="language" />,
    },
  ];

  const authSlice = useAppSelector((slice) => slice.authSlice);

  const handleChangeLang = (lang: "vi" | "en") => {
    dispatch(setDefaultLang(lang));

    localStorage.setItem(REVIEWTY_LOCALE, lang);
    return;
  };
  return (
    <Dropdown menu={{ items }}>
      <a onClick={(e) => e.preventDefault()}>
        <div className="flex justify-between items-center">
          <Image src={IMAGES.language} alt="language" />
          <span className="pl-2">{authSlice.lang?.toUpperCase()}</span>
        </div>
      </a>
    </Dropdown>
  );
};

export default SelectLanguage;
