import clsx from "clsx";
import React, { useState, useRef } from "react";

interface DragScrollProps {
  children: React.ReactNode;
  isSticky?: boolean;
}

const DragScroll: React.FC<DragScrollProps> = ({
  isSticky = false,
  children,
}) => {
  const containerRef = useRef<HTMLDivElement>(null);
  const [isDragging, setIsDragging] = useState(false);
  const [startX, setStartX] = useState(0);
  const [scrollLeft, setScrollLeft] = useState(0);

  const handleMouseDown = (e: React.MouseEvent) => {
    e.preventDefault();
    setIsDragging(true);
    setStartX(e.clientX - (containerRef.current?.offsetLeft || 0));
    setScrollLeft(containerRef.current?.scrollLeft || 0);
  };

  const handleMouseUp = () => {
    setIsDragging(false);
  };

  const handleMouseMove = (e: React.MouseEvent) => {
    if (!isDragging || !containerRef.current) return;
    const x = e.clientX - (containerRef.current?.offsetLeft || 0);
    const scrollX = x - startX;
    containerRef.current.scrollLeft = scrollLeft - scrollX;
  };

  return (
    <div
      ref={containerRef}
      className={clsx("hidden-scrollbar", {
        "top-[72px] sticky bg-white z-[2]": isSticky,
      })}
      style={{ overflowX: "scroll", cursor: isDragging ? "grabbing" : "grab" }}
      onMouseDown={handleMouseDown}
      onMouseUp={handleMouseUp}
      onMouseMove={handleMouseMove}
    >
      {children}
    </div>
  );
};

export default DragScroll;
