import IMAGES from "@/assets/img/images";
import { setIsOpen } from "@/components/main/mainSlice";
import { DeviceDetectContext } from "@/context/DeviceContext";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { Modal } from "antd";
import Image from "next/legacy/image";
import { useContext, useState } from "react";

const ModalDownloadApp = () => {
  const devices = useContext(DeviceDetectContext);

  const dispatch = useAppDispatch();
  const mainSlice = useAppSelector((slice) => slice.mainSlice);
  // const [isModalOpen, setIsModalOpen] = useState(false);

  const handleOk = () => {
    // setIsModalOpen(false);
    dispatch(setIsOpen({}));
  };

  const handleCancel = () => {
    dispatch(setIsOpen({}));
  };
  const downloadAppHandler = () => {
    if (devices.isIOS) {
      window.open(
        "https://apps.apple.com/vn/app/reviewty-m%E1%BB%B9-ph%E1%BA%A9m-l%C3%A0m-%C4%91%E1%BA%B9p/id1468579853",
        "_blank"
      );
    } else {
      window.open(
        "https://play.google.com/store/apps/details?id=com.reviewty.reviewty",
        "_blank"
      );
    }
  };
  return (
    <>
      <div className="">
        <Modal
          width={480}
          centered
          closable={false}
          className=" text-center"
          open={mainSlice.isOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          footer={false}
        >
          <div className="flex flex-col justify-center items-center text-center">
            <div>
              <Image
                width={64}
                height={64}
                src={IMAGES.SquareLogo}
                alt="square-logo"
              />
            </div>

            <p className="py-4 text-[1rem] font-medium">
              Trải nghiệm đầy đủ trên ứng dụng
            </p>

            <span className="text-[#747474] pb-5">
              Khám phá những review / chia sẻ chân thật từ những người dùng
              thông thái và sáng tạo nội dung của riêng bạn
            </span>
            <div>
              <Image src={IMAGES.QRCode} alt="qr-code" />
            </div>
            <span className="text-[#747474]">Quét mã QR để tải app</span>

            <div
              onClick={downloadAppHandler}
              className="bg-black text-white w-full cursor-pointer text-[1rem] font-semibold py-3 rounded-md mt-4 hover:bg-[rgba(0,0,0,0.5)]"
            >
              Tải app ngay
            </div>
            <button
              onClick={handleCancel}
              className="text-[#404040] cursor-pointer py-3 w-full"
            >
              Để sau
            </button>
          </div>
        </Modal>
      </div>
    </>
  );
};

export default ModalDownloadApp;
