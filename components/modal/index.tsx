import { Col, Row } from "antd";
import clsx from "clsx";
import { useRouter } from "next/router";
import { ReactNode, useEffect, useRef } from "react";
import styles from "./modal.module.scss";

interface IMainModal {
  title: string;
  children: ReactNode;
  isOpen: boolean;
  isDangerOk?: boolean;
  handleOk: Function;
  handleCancel: Function;
  isRounded?: boolean;
  // content:
}

const MainModal = ({
  isRounded = true,
  title,
  children,
  isOpen = false,
  isDangerOk = false,
  handleOk,
  handleCancel,
}: IMainModal) => {
  const router = useRouter();
  const modalEl = useRef<HTMLDivElement>(null);

  const handleCancelBtn = () => {
    if (handleCancel) {
      handleCancel();
    }
    // router.back();
  };

  const handleOkBtn = () => {
    if (handleOk) {
      handleOk();
    }
  };

  return (
    <>
      <div
        ref={modalEl}
        className={clsx(
          "block fixed z-[51]",
          {
            hidden: !isOpen,
          },
          styles.wrapper
        )}
      >
        <div className="flex justify-center items-center z-50 h-auto">
          <div
            className={clsx(
              "bg-white absolute top-1/2 -translate-y-1/2 p-4 w-[268px] rounded-2xl text-center",
              {
                "rounded-2xl": isRounded,
              }
            )}
          >
            <p className="text-center text-[1rem] font-bold">{title}</p>
            <div className="pb-4 pt-3 text-[1rem] text-[#404040]">
              {children}
            </div>
            <Row className="-ml-4 -mr-4 border-t-[#DDDDDD] border-t-2">
              <Col span={12} className="border-r-[#DDDDDD] border-r-2 -mb-4">
                <button
                  className="text-[1rem] text-[#404040] pt-4 "
                  onClick={handleCancelBtn}
                >
                  Rời khỏi trang
                </button>
              </Col>
              <Col span={12}>
                <button
                  onClick={handleOkBtn}
                  className={clsx(
                    "text-[1rem]  font-semibold text-[#404040] pt-4",
                    {
                      "text-[#ea4a4a]": isDangerOk,
                    }
                  )}
                >
                  Ở lại trang
                </button>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    </>
  );
};

export default MainModal;
