import { MouseEventHandler, ReactNode } from "react";
import "./button.module.scss";

interface IButtonComponent {
  children: ReactNode;
  type?: "primary" | "dashed" | "submit";
  htmlType?: "submit" | "button";
  className?: string;
  disabled?: boolean;
  onClick?: MouseEventHandler<HTMLAnchorElement> &
    MouseEventHandler<HTMLButtonElement>;
}

const ButtonComponent = ({
  type = "primary",
  children,
  htmlType = "button",
  className = "",
  disabled = false,
  ...props
}: IButtonComponent) => {
  return (
    <button
      disabled={disabled}
      type={htmlType}
      {...props}
      className={`${className} text-white py-1 px-4 rounded-md bg-[#7003BB] cursor-pointer hover:opacity-50 transition-all duration-300`}
    >
      {children}
    </button>
  );
};

export default ButtonComponent;
