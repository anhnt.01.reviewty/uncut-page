import IMAGES from "@/assets/img/images";
import { REVIEWTY_TOKEN } from "@/common/contains";
import { useSignInByFacebookMutation } from "@/graphql/reviewty-user/graphql";
import { REVIEWTY_CLIENT } from "@/hooks/useApollo";
import useTrans from "@/hooks/useTrans";
import { useAppDispatch } from "@/store/store";
import Image from "next/image";
import { useRouter } from "next/router";
import Script from "next/script";
import { useCallback } from "react";
import { fetchActiveCustomer } from "../auth/authSlice";

// import styles from "./FacebookLogin.module.scss";

const FacebookLoginBtn = () => {
  const dispatch = useAppDispatch();
  const trans = useTrans();
  const router = useRouter();
  // const [login] = useAuthenticateMutation();

  const [login] = useSignInByFacebookMutation();

  const redirectToHomePage = useCallback(async () => {
    await dispatch(fetchActiveCustomer()).unwrap();
    router.replace("/");
  }, [dispatch, router]);

  const handleFacebookStatusChange = useCallback(
    async (response: any, openLoginDialog: any) => {
      const { status, authResponse } = response;
      if (status === "connected") {
        const result = await login({
          variables: {
            facebookId: authResponse?.accessToken,
          },
        });
        if (result.data?.loginByFacebookV2.__typename === "AuthPayload") {
          localStorage.setItem(
            REVIEWTY_TOKEN,
            result.data?.loginByFacebookV2.token
          );
          redirectToHomePage();
          return;
        }
      } else if (openLoginDialog) {
        FB.login((response: any) =>
          handleFacebookStatusChange(response, false)
        );
      }
    },
    [login, redirectToHomePage]
  );
  const getFacebookLoginStatus = useCallback(() => {
    FB.getLoginStatus((response: any) =>
      handleFacebookStatusChange(response, true)
    );
  }, [handleFacebookStatusChange]);

  return (
    <>
      <Script
        id="fb-sdk"
        dangerouslySetInnerHTML={{
          __html: `
          window.fbAsyncInit = function() {
            FB.init({
              appId      : ${process.env.NEXT_PUBLIC_FACEBOOK_APP_ID},
              cookie     : true,
              xfbml      : true,
              version    : 'v14.0'
            });
          };
          `,
        }}
      />
      <Script
        src="https://connect.facebook.net/en_US/sdk.js"
        strategy="lazyOnload"
      />

      <>
        <div
          onClick={getFacebookLoginStatus}
          className="py-2 ease-in-out duration-300 cursor-pointer m-auto flex justify-center items-center border border-[#E5E6EA] hover:border-[#8592ab] rounded-full"
        >
          <span className=" ">
            <Image src={IMAGES.fbLogo!} alt="face-book-icon" />
          </span>
          <span>{trans.login.signinWithFacebook}</span>
        </div>
      </>
    </>
  );
};

export default FacebookLoginBtn;
