// components/DynamicOGImage.tsx
import React from "react";
// import Head from "next/head";
import Head from "next/head";

// import logo from "@public/funding-logo.png";

interface IDynamicOGImageProps {
  data?: {
    url: string;
    title: string;
  };
}

const DynamicOGImage: React.FC<IDynamicOGImageProps> = ({ data }) => {
  return (
    <Head>
      {/* <title>Sub title - Kem lót FOCALLURE 25g trang điểm kiềm dầu nâng tông</title> */}
      {/* <meta name="description" content="undefined"/> */}

      {/* <meta property="og:url" content="https://r-uncut.vercel.app/product-detail/20?name=Kem+l%C3%B3t+FOCALLURE+25g+trang+%C4%91i%E1%BB%83m+ki%E1%BB%81m+d%E1%BA%A7u+n%C3%A2ng+t%C3%B4ng"/>
  <meta property="og:type" content="website"/>
  <meta property="og:title" content="Sub title - Kem lót FOCALLURE 25g trang điểm kiềm dầu nâng tông"/>
  <meta property="og:description" content="undefined"/>
  <meta property="og:image" content="https://d27j9dvp3kyb6u.cloudfront.net/2023/06/12/8a120228-95d1-4b17-a49e-0fab7cce8acf.png"/>

  <meta name="twitter:card" content="summary_large_image"/>
  <meta property="twitter:domain" content="r-uncut.vercel.app"/>
  <meta property="twitter:url" content="https://r-uncut.vercel.app/product-detail/20?name=Kem+l%C3%B3t+FOCALLURE+25g+trang+%C4%91i%E1%BB%83m+ki%E1%BB%81m+d%E1%BA%A7u+n%C3%A2ng+t%C3%B4ng"/>
  <meta name="twitter:title" content="Sub title - Kem lót FOCALLURE 25g trang điểm kiềm dầu nâng tông"/>
  <meta name="twitter:description" content="undefined"/>
  <meta name="twitter:image" content="https://d27j9dvp3kyb6u.cloudfront.net/2023/06/12/8a120228-95d1-4b17-a49e-0fab7cce8acf.png"/> */}

      {/* <link rel="manifest" href="manifest.json" /> */}
      {/* <link rel="apple-touch-icon" href="/icon.png" /> */}
      <meta name="theme-color" content="#fff" />
      <meta
        property="og:image"
        content={
          data?.url ||
          "https://d27j9dvp3kyb6u.cloudfront.net/2023/06/12/8a120228-95d1-4b17-a49e-0fab7cce8acf.png"
        }
      />
      <meta
        property="og:title"
        content={data?.title || "Uncut - Unveiling gems"}
      />
      <meta property="og:url" content="https://r-uncut.vercel.app/" />

      <meta property="og:type" content="website" />
    </Head>
  );
};

export default DynamicOGImage;
