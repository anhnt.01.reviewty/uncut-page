import { Col, Row, Skeleton } from "antd";
import clsx from "clsx";
import { useEffect, useState } from "react";
import styles from "./loading.module.scss";
export interface ILoading {
  counter?: number;
}

const LoadingMain = ({ counter = 10 }: ILoading) => {
  const [data, _] = useState<string[]>(() => Array(counter).fill(""));

  return (
    <>
      {data.map((_: string, index: number) => (
        <div
          key={index}
          className={clsx("h-[480x] w-full col-span-12", styles.skeletonMain)}
        >
          <Skeleton.Avatar
            style={{ width: "100%", height: "400px" }}
            active
            size="large"
            shape="square"
          />

          <div className="p-4">
            <Skeleton
              className="pb-8"
              active
              paragraph={{ rows: 2 }}
            ></Skeleton>
            <Skeleton
              title={false}
              active
              paragraph={{ rows: 2 }}
              className="pb-4"
            ></Skeleton>
            <Row gutter={[30, 30]}>
              <Col span={4}>
                <Skeleton.Avatar active size="large" shape="square" />
              </Col>
              <Col span={20}>
                <Skeleton active title={false} />
              </Col>
            </Row>
          </div>

          <Skeleton.Avatar
            style={{ width: "100%", height: "400px" }}
            active
            size="large"
            shape="square"
          />

          <div className="p-4">
            <Skeleton
              className="pb-8"
              active
              paragraph={{ rows: 2 }}
            ></Skeleton>
            <Skeleton
              title={false}
              active
              paragraph={{ rows: 2 }}
              className="pb-4"
            ></Skeleton>
            <Row gutter={[30, 30]}>
              <Col span={4}>
                <Skeleton.Avatar active size="large" shape="square" />
              </Col>
              <Col span={20}>
                <Skeleton active title={false} />
              </Col>
            </Row>
          </div>
        </div>
      ))}
    </>
  );
};

export default LoadingMain;
