import { Col, Row, Skeleton } from "antd";
import { useEffect, useState } from "react";

export interface ILoading {
  counter?: number;
}

const LoadingProduct = ({ counter = 2 }: ILoading) => {
  const [data, _] = useState<string[]>(() => Array(counter).fill(""));

  return (
    <div className="border-t-2 ">
      {data.map((_: string, index: number) => (
        <div className="p-4" key={index}>
          <Row gutter={[0, 15]}>
            <Col span={7}>
              <Skeleton.Image active />
            </Col>
            <Col span={17}>
              <Skeleton title={false} paragraph={{ rows: 3 }} />
            </Col>
          </Row>

          <div className="py-6">
            <Skeleton title={false} paragraph={{ rows: 1 }} />
          </div>
          <div className="py-2">
            <Skeleton title={false} paragraph={{ rows: 1 }} />
          </div>
          <div className="py-4">
            <Skeleton title={false} paragraph={{ rows: 2 }} />
          </div>
        </div>
      ))}
    </div>
  );
};

export default LoadingProduct;
