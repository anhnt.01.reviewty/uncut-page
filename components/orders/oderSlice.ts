import {
  GetListReviewDocument,
  GetListReviewQuery,
  GetListReviewQueryVariables,
  GetOneFundingOrderDocument,
  GetOneFundingOrderQuery,
  GetOneFundingOrderQueryVariables,
} from "@/graphql/reviewty-user/graphql";
import { AUTH_TOKEN, initializeApollo } from "@/hooks/useApollo";
import { AppThunkStatus } from "@/store/store";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

interface AuthState {
  data?: GetOneFundingOrderQuery;
  status: AppThunkStatus;
  error: any;
  selectedOrder: any;
}

const initialState: AuthState = {
  status: "idle",
  error: null,
  data: undefined,
  selectedOrder: undefined,
};

export const fetchOrder = createAsyncThunk<GetOneFundingOrderQuery, number>(
  "auth/fetchOrder",
  async (id: number) => {
    const apolloClient = initializeApollo();
    const { data, error, errors } = await apolloClient.query<
      GetOneFundingOrderQuery,
      GetOneFundingOrderQueryVariables
    >({
      query: GetOneFundingOrderDocument,
      variables: {
        where: {
          id,
        },
      },
      context: {
        headers: {
          Authorization: `Bearer ${localStorage.getItem(AUTH_TOKEN)}`,
        },
      },
    });

    if (error || errors) {
      // throw error || errors;
    }

    return data;
  }
);

export const fetchListReview = createAsyncThunk<GetListReviewQuery, number>(
  "auth/fetchListReview",
  async (id: number) => {
    const apolloClient = initializeApollo();
    const { data, error, errors } = await apolloClient.query<
      GetListReviewQuery,
      GetListReviewQueryVariables
    >({
      query: GetListReviewDocument,
      variables: {
        where: {
          id,
        },
      },
      context: {
        headers: {
          Authorization: `Bearer ${localStorage.getItem(AUTH_TOKEN)}`,
        },
      },
    });

    if (error || errors) {
      // throw error || errors;
    }

    return data;
  }
);

const orderSlice = createSlice({
  name: "order",
  initialState,
  reducers: {
    resetOrderSlice: (state) => initialState,

    selectedOrder: (state, action) => {
      state.selectedOrder = action.payload;
    },
    clearOrder: (state, _) => {
      state.data = undefined;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(fetchOrder.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchOrder.fulfilled, (state, action) => {
        state.status = "succeeded";
        if (action.payload) {
          state.data = action.payload;
        }
      })
      .addCase(fetchOrder.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error;
      });
  },
});

export const { clearOrder, selectedOrder, resetOrderSlice } =
  orderSlice.actions;

export default orderSlice.reducer;
