import { convertToText, increaseDate } from "@/common/contains";
import toCurrency from "@/common/toCurrency";
import {
  FundingOrdersQuery,
  FundingOrderStatus,
  usePayBackFundingOrderMutation,
} from "@/graphql/reviewty-user/graphql";
import useTrans from "@/hooks/useTrans";
import { Col, Row } from "antd";
import Image from "next/legacy/image";
import { useRouter } from "next/router";
// import { useRouter } from "next/router";
import { useCallback } from "react";

interface IOrderCard {
  order: FundingOrdersQuery["fundingOrders"][0];
}

const OrderCard = ({ order }: IOrderCard) => {
  const router = useRouter();

  const trans = useTrans();
  const [payback] = usePayBackFundingOrderMutation();

  const handlePayBack = useCallback(
    async (id: number) => {
      const { data, errors } = await payback({
        variables: {
          where: {
            id: id,
          },
        },
      });

      if (errors) {
      }
      if (data?.payBackFundingOrder) {
        router.push(data.payBackFundingOrder);
      }
    },
    [payback, router]
  );

  const handleClickReview = useCallback(() => {
    router.push({
      pathname: "/orders/[id]/review",
      query: {
        id: order.id,
        productId: order.productId,
        evaluationId: order.orderEvaluations?.[0]?.id,
      },
    });
  }, [order.id, order.orderEvaluations, order.productId, router]);

  const renderStatusUI = useCallback(
    (status: FundingOrderStatus) => {
      switch (status) {
        case FundingOrderStatus.InProcessing:
          return (
            <Col span={24}>
              <Row className="pt-3 pb-4">
                <Col span={14}>
                  <p className="text-[#747474]">
                    {trans.common.payForYourOrderBefore}:{" "}
                    <span className="text-[#404040] font-medium">
                      {increaseDate(order.createdAt, 15)}
                    </span>
                  </p>
                </Col>
                <Col span={10} className="flex justify-end">
                  {!order.isPaid && (
                    <button
                      className="bg-[#BA92D9]  right-0 py-[10px] cursor-pointer px-4 rounded-md text-[1rem]"
                      onClick={() => handlePayBack(order.id)}
                    >
                      {trans.common.payback}
                    </button>
                  )}
                </Col>
              </Row>
            </Col>
          );
        case FundingOrderStatus.Completed:
          return (
            <Col span={24}>
              <Row className="pt-3 pb-4">
                <Col span={14} className="pr-4">
                  <p className="text-[#747474]">
                    Trải nghiệm của bạn thế nào về sản phẩm của chúng tôi ?
                  </p>
                </Col>
                <Col span={10} className="flex justify-end">
                  <button
                    onClick={handleClickReview}
                    className="bg-[#BA92D9] w-full sm:text-[1rem] text-white sm:px-6 sm:py-2 rounded-md ml-15"
                  >
                    {order.orderEvaluations?.length === 0
                      ? "Đánh giá"
                      : "Xem đánh giá"}
                  </button>
                </Col>
              </Row>
            </Col>
          );

        default:
          return <></>;
      }
    },
    [
      handleClickReview,
      handlePayBack,
      order.createdAt,
      order.id,
      order.isPaid,
      order.orderEvaluations?.length,
      trans.common.payForYourOrderBefore,
      trans.common.payback,
    ]
  );

  return (
    <div>
      <div className="flex justify-between bg-white items-center ">
        <div className="w-full">
          <Row gutter={[15, 15]} className="">
            {order.status !== FundingOrderStatus.InProcessing && (
              <Col span={24} className="justify-between flex ">
                <span className="text-[#8D8D8D]">
                  {increaseDate(order.createdAt, 0)}
                </span>

                <span className="text-[#BA92D9]">
                  {convertToText(order.status!)}
                </span>
              </Col>
            )}
            <Col span={4}>
              <Image
                src={order.fundingProducts?.[0]?.product?.thumbnail?.url!}
                alt="thumb-img"
                width="0"
                height="0"
                sizes="100vw"
                layout="responsive"
                loading="lazy"
              />
            </Col>
            <Col
              span={20}
              className="cursor-pointer"
              onClick={() => {
                router.push(`/orders/${order.id!.toString()}`);
              }}
            >
              <span className="text-[1rem]">
                {order.fundingProducts?.[0]?.product?.translations[0].name}
              </span>

              <div className="pt-2">
                <span className="mr-1 text-[#D10017]  text-[13px]"></span>
                <span className="line-through text-[#8D8D8D] mr-2">
                  {toCurrency(order.fundingProducts?.[0]?.product?.price!)}
                </span>{" "}
                <span className="text-[#404040] text-[1rem]">
                  {toCurrency(order.discount!)}
                </span>
              </div>
            </Col>
            <div></div>
            <Col
              span={24}
              className="justify-between py-3 flex border-y-2 border-y-[#F5F5F5]"
            >
              <span className="text-[#8D8D8D]">
                {order.quantity && order.quantity + ` ${trans.common.product}`}
              </span>
              {order.totalAmount! && (
                <label className="text-[0.875rem]">
                  {trans.common.intoMoney}:{" "}
                  <span className="text-[#BE2827] text-[1rem]">
                    {toCurrency(order.totalAmount!)}
                  </span>
                </label>
              )}
            </Col>
          </Row>
          <div></div>

          {renderStatusUI(order.status!)}
          {order.statusText &&
            router.query.type === FundingOrderStatus.Cancelled && (
              <p className="py-3 text-[0.875rem] text-[#747474]">
                {trans.common.reasonCancel}: {order.statusText}
              </p>
            )}
        </div>
      </div>
    </div>
  );
};

export default OrderCard;
