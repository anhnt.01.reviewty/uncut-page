import IMAGES from "@/assets/img/images";
import { increaseDate } from "@/common/contains";
import {
  FundingOrderStatus,
  GetOneFundingOrderQuery,
  usePayBackFundingOrderMutation,
} from "@/graphql/reviewty-user/graphql";
import useTrans from "@/hooks/useTrans";
import { Col, Image, Row, Skeleton } from "antd";
import clsx from "clsx";
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";

const RenderButtonOrderDetail = ({
  data,
}: {
  data: GetOneFundingOrderQuery;
}) => {
  const router = useRouter();
  const trans = useTrans();
  const [payback] = usePayBackFundingOrderMutation();

  const handlePayback = useCallback(async () => {
    try {
      const response = await payback({
        variables: {
          where: {
            id: data.fundingOrder.id,
          },
        },
      });

      if (response.data?.payBackFundingOrder) {
        router.push(response.data?.payBackFundingOrder);
      }
    } catch (errors) {}
  }, [payback, router, data.fundingOrder.id]);

  const handleReview = () => {
    router.push({
      pathname: "/orders/[id]/review",
      query: {
        id: data.fundingOrder.id,
        productId: data.fundingOrder.productId,
      },
    });
    return;
  };

  const repurchase = useCallback(
    (fundingId: string, quantity: number) => {
      router.push({
        pathname: `/checkout/[id]`,
        query: {
          id: data.fundingOrder.fundingId,
          quantity: data.fundingOrder.quantity,
        },
      });
    },
    [data.fundingOrder.fundingId, data.fundingOrder.quantity, router]
  );

  const [informationCard, setInformationCard] = useState<
    {
      label: string;
      fn?: Function;
    }[]
  >([
    {
      label: " ",
      fn: undefined,
    },
  ]);

  const handleSawReview = useCallback(
    (dataOrder: GetOneFundingOrderQuery, index: number) => {
      router.push({
        pathname: "/orders/[id]/review",
        query: {
          id: data.fundingOrder.id,
          productId: data.fundingOrder.productId,
          evaluationId: data.fundingOrder.orderEvaluations?.[0]?.id,
        },
      });
    },
    [
      data.fundingOrder.id,
      data.fundingOrder.orderEvaluations,
      data.fundingOrder.productId,
      router,
    ]
  );
  useEffect(() => {
    const info = (() => {
      switch (data.fundingOrder.status) {
        case FundingOrderStatus.InProcessing:
          return {
            label: trans.common.payback,
            fn: handlePayback,
          };
        case FundingOrderStatus.WaitDelivering:
        case FundingOrderStatus.Delivering:
          return {
            label: "",
            fn: undefined,
          };
        case FundingOrderStatus.Completed:
          return {
            label:
              data.fundingOrder.orderEvaluations?.length === 0
                ? "Đánh giá"
                : "Xem đánh giá",
            fn: handleSawReview,
            // label: "Đánh giá",
            // fn: handleReview,
          };

        case FundingOrderStatus.WaitRefunding:
        case FundingOrderStatus.SystemError:
        case FundingOrderStatus.Cancelled:
        case FundingOrderStatus.Refunded:
        case FundingOrderStatus.PaymentError:
          return {
            label: trans.common.repurchase,
            fn: repurchase,
          };
        default:
          return {
            label: "",
            fn: undefined,
          };
      }
    })();

    setInformationCard([info as any]);
  }, [
    data.fundingOrder.createdAt,
    data.fundingOrder.orderEvaluations?.length,
    data.fundingOrder.shipmentStatusText,
    data.fundingOrder.status,
    handlePayback,
    handleSawReview,
    repurchase,
    trans.common.payback,
    trans.common.repurchase,
  ]);

  return (
    <>
      {informationCard.map((info, index) => {
        if (!info.fn) {
          return;
        }
        return (
          <button
            key={info.label}
            onClick={() => info.fn!()}
            className={clsx(
              "col-span-12 mb-4 rounded-md w-full cursor-pointer flex justify-center items-center p-3  bg-[#ba92d9] text-white text-[1.125rem] h-max"
            )}
          >
            {info.label}
          </button>
        );
      })}
    </>
  );
};

export default RenderButtonOrderDetail;
