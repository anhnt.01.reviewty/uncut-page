import IMAGES from "@/assets/img/images";
import { increaseDate } from "@/common/contains";
import {
  FundingOrderStatus,
  GetOneFundingOrderQuery,
} from "@/graphql/reviewty-user/graphql";
import useTrans from "@/hooks/useTrans";
import { Col, Image, Row, Skeleton } from "antd";
import { useEffect, useState } from "react";

const StatusOrder = ({ data }: { data: GetOneFundingOrderQuery }) => {
  const trans = useTrans();

  const [informationCard, setInformationCard] = useState<
    {
      title: string;
      content: JSX.Element;
      image?: any;
    }[]
  >([
    {
      title: "",
      content: <></>,
      image: undefined,
    },
  ]);
  useEffect(() => {
    const info: {
      title: string;
      content: JSX.Element;
      image?: string;
    } = (() => {
      switch (data.fundingOrder.status) {
        case FundingOrderStatus.InProcessing:
          return {
            title: trans.home.navigationMenu.waitForPay,
            content: (
              <>
                {trans.common.payForYourOrderBefore}:{" "}
                <span className="font-medium text-[1rem] text-white">
                  {" "}
                  {increaseDate(data?.fundingOrder.createdAt, 15)}
                </span>
              </>
            ),
            image: IMAGES.invoice,
          };
        case FundingOrderStatus.WaitDelivering:
          return {
            title: trans.home.navigationMenu.waitDelivering,
            content: (
              <>
                Thời gian giao hàng dự kiến:{" "}
                {data.fundingOrder.estimatedDeliverTime}
              </>
            ),
            image: IMAGES.waitDelivering,
          };
        case FundingOrderStatus.Delivering:
          return {
            title: trans.home.navigationMenu.delivering,
            content: (
              <>
                Thời gian giao hàng dự kiến:{" "}
                {data.fundingOrder.estimatedDeliverTime}
              </>
            ),
            image: IMAGES.delivering,
          };
        case FundingOrderStatus.Completed:
          return {
            title: "Đơn hàng đã hoàn thành",
            content: <>Cảm ơn bạn đã tin tưởng và đặt hàng</>,
            image: IMAGES.completed,
          };
        case FundingOrderStatus.WaitRefunding:
        case FundingOrderStatus.SystemError:
        case FundingOrderStatus.Cancelled:
        case FundingOrderStatus.Refunded:
        case FundingOrderStatus.PaymentError:
          return {
            title: "Đơn hàng đã bị hủy",
            content: <>{data.fundingOrder.shipmentStatusText}</>,
            image: IMAGES.cancelled,
          };

        default:
          return {
            title: "",
            content: <></>,
            image: "",
          };
      }
    })();

    setInformationCard([info]);
  }, [
    data.fundingOrder.createdAt,
    data.fundingOrder.estimatedDeliverTime,
    data.fundingOrder.shipmentStatusText,
    data.fundingOrder.status,
    trans.common.payForYourOrderBefore,
    trans.home.navigationMenu.delivered,
    trans.home.navigationMenu.delivering,
    trans.home.navigationMenu.waitDelivering,
    trans.home.navigationMenu.waitForPay,
  ]);

  return (
    <>
      {informationCard.map((info, index) => (
        <Row
          key={info.title + index}
          className="p-4 py-4 bg-[#005a53] text-white flex items-center"
        >
          <Col span={16}>
            <label className="text-[1rem] ">
              {data.fundingOrder.status ? (
                <p className="text-[1rem] font-medium text-white">
                  {info.title}
                </p>
              ) : (
                <Skeleton title={false} active paragraph={{ rows: 1 }} />
              )}
            </label>

            <div className="flex items-center justify-start ">
              <p className="leading-8 text-white">{info.content}</p>
            </div>
          </Col>
          <Col span={4}></Col>
          <Col span={4} className="flex justify-end">
            <Image src={info.image?.src} alt="invoice-icon" />
          </Col>
        </Row>
      ))}
    </>
  );
};

export default StatusOrder;
