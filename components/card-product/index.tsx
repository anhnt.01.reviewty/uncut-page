import { handleCheckAuth, REVIEWTY_TOKEN } from "@/common/contains";
import toCurrency from "@/common/toCurrency";
import { FundingProduct } from "@/graphql/reviewty-user/graphql";
import { FundingDetailQuery } from "@/graphql/reviewty/graphql";
import { useAppDispatch } from "@/store/store";
import { handleDiffDate } from "@/ultis/ultis";
import clsx from "clsx";
import Image from "next/legacy/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useCallback, useEffect } from "react";
import { selectedDetail } from "../main/mainSlice";
import styles from "../../pages/product-detail/funding/funding-detail.module.scss";
import useTrans from "@/hooks/useTrans";

const CardProduct = ({ data }: { data?: FundingDetailQuery["funding"] }) => {
  const trans = useTrans();

  const disabledBtn =
    new Date().getTime() < new Date(data?.startDate).getTime();
  const checkDate = handleDiffDate(data?.endDate);

  const isExpired = checkDate === "Hết hạn";

  const router = useRouter();
  const dispatch = useAppDispatch();
  const handleClick = useCallback(
    (
      product: FundingDetailQuery["funding"]["fundingProducts"][0],
      data: FundingDetailQuery["funding"],
      index: number
    ) => {
      dispatch(
        selectedDetail({
          data,
          product,
        })
      );

      router.push({
        pathname: "funding/[slug]",
        query: {
          slug: product?.product!.translations[0].name,
          productId: product?.product!.id,
          fundingId: data.id,
        },
      });
      sessionStorage.setItem("scrollPosition", window.pageYOffset.toString());
    },
    [dispatch, router]
  );

  useEffect(() => {
    if (data) {
      const scrollPosition = sessionStorage.getItem("scrollPosition");
      if (scrollPosition) {
        window.scrollTo(0, parseInt(scrollPosition, 10));
        sessionStorage.removeItem("scrollPosition");
      }
    }
  }, [data]);
  const handleBuyNow = () => {
    // gTagGGAnalysis({
    //   eventName: "add_payment_info",
    //   eventCategory: "CLICK_TO_CHECKOUT",
    //   eventLabel: "CLICK",
    //   asPath: router.asPath,
    // });

    const token = localStorage.getItem(REVIEWTY_TOKEN);

    if (!token) {
      handleCheckAuth();
    }

    router.push(`/checkout/${data?.id}`);

    return;
  };

  return (
    <React.Fragment>
      {data?.fundingProducts &&
        data?.fundingProducts.map(
          (
            product: FundingDetailQuery["funding"]["fundingProducts"][0],
            index
          ) => (
            <div
              className="cursor-pointer col-span-6  bg-white text-left"
              key={product.product!.id}
              onClick={() => handleClick(product, data, index)}
            >
              <Image
                priority
                src={product.product?.thumbnail?.url!}
                width="0"
                height="0"
                sizes="100vw"
                className="w-full h-auto block "
                alt="img"
              />
              <div className="text-[#222222] font-medium pb-1">
                {product.product?.brand.translations[0].name}
              </div>
              <label className="text-elipsis text-[0.875rem] pb-2">
                {product.product?.translations[0].name}
              </label>
              <div className="flex pb-1">
                <span className="mr-1 text-[#D10017]  text-[13px]">
                  {" "}
                  {Math.floor(
                    ((product.product?.price! - product.discount!) /
                      product.product?.price!) *
                      100
                  ) + "%"}
                </span>
                <p className="mr-1 line-through text-[#99A1A8] font-medium text-[11px]">
                  {toCurrency(product.product?.price!)}
                </p>
              </div>
              <p className="mr-1 text-[#222222] text-[13px] font-medium">
                {toCurrency(product.discount!)}
              </p>
            </div>
          )
        )}

      <div
        className="fixed m-auto  bottom-0 left-0  items-center  justify-center w-full bg-[#f6f6f6]"
        style={{
          boxShadow: "0px -4px 4px 0px rgba(0, 0, 0, 0.08)",
        }}
      >
        <div className="max-w-[600px] m-auto px-4  flex w-full items-center justify-center  bg-white py-2 ">
          <button
            disabled={
              (isExpired && data?.preOrder) ||
              !data?.id ||
              data.fundingProducts[0].quantity! <= 0 ||
              disabledBtn
            }
            onClick={handleBuyNow}
            className={clsx(
              "cursor-pointer bg-[#ba92d9] relative z-50",
              {
                "bg-[gray]":
                  (isExpired && data?.preOrder) ||
                  !data?.id ||
                  data?.fundingProducts[0].quantity! <= 0 ||
                  disabledBtn,
              },
              styles.buyNow
            )}
          >
            {isExpired && data?.preOrder
              ? "Hết hạn đặt trước"
              : data?.fundingProducts[0].quantity! <= 0
              ? trans.products.soldOut
              : disabledBtn
              ? "Sự kiện chưa bắt đầu"
              : data?.preOrder
              ? "Đặt trước"
              : trans.common.buyNow}
          </button>
        </div>
      </div>
    </React.Fragment>
  );
};

export default CardProduct;
