import clsx from "clsx";
import Link from "next/link";
import React from "react";

const MallLink = ({
  className,
  content,
  ...rest
}: JSX.IntrinsicAttributes & React.AnchorHTMLAttributes<HTMLAnchorElement>) => {
  return (
    <Link
      href="https://uncut.review-ty.com"
      className={clsx(className, "no-underline")}
      {...rest}
      target="_blank"
    >
      Uncut
    </Link>
  );
};

export default MallLink;
