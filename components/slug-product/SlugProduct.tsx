import { JoinIcon, ShareIconProductDetail } from "@/assets/icon/icons";
// import DynamicOGImage from ''
import { FundingProductQuery } from "@/graphql/reviewty/graphql";
import { handleDiffDate } from "@/ultis/ultis";
import clsx from "clsx";
import Image from "next/legacy/image";
import { useRouter } from "next/router";
import "swiper/css";
import "swiper/css/pagination";
import styles from "../../pages/product-detail/funding/funding-detail.module.scss";
import RatingStars from "../rating-stars/RatingStars";
// import "index.css";
import IMAGES from "@/assets/img/images";
import {
  formatNumber,
  gTagGGAnalysis,
  handleCheckAuth,
  handleClickViewMobileApp,
  REVIEWTY_TOKEN,
} from "@/common/contains";
import { DeviceDetectContext } from "@/context/DeviceContext";
import useTrans from "@/hooks/useTrans";
import { Button, Input, message, Modal, Skeleton, Space } from "antd";
import { useContext, useState } from "react";
import ReviewSection from "../reviews/review-section";
import { useAppDispatch } from "@/store/store";
import { setIsOpen } from "../main/mainSlice";
import Progressbar from "../progressbar";
import Inprogressbar from "../inprogressbar";

export const SlugProduct = ({
  dataProduct,
  loading,
}: {
  dataProduct: FundingProductQuery;
  loading: boolean;
}) => {
  const dispatch = useAppDispatch();

  const disabledBtn =
    new Date().getTime() <
    new Date(dataProduct?.fundingProduct.funding.startDate).getTime();
  const router = useRouter();
  const [open, setOpen] = useState(false);
  const trans = useTrans();
  const checkDate = handleDiffDate(dataProduct?.fundingProduct.funding.endDate);

  const isExpired = checkDate === "Hết hạn";
  const copyToClipboard = () => {
    message.open({
      type: "success",
      content: trans.message.copyToClipboard,
    });

    navigator?.clipboard?.writeText(
      `${process.env.NEXT_PUBLIC_BASE_URL}${router.asPath}`
    );
    // Clipboard API is only supported for pages served over HTTPS or localhost.
  };

  const handleBuyNow = () => {
    gTagGGAnalysis({
      eventName: "add_payment_info",
      eventCategory: "CLICK_TO_CHECKOUT",
      eventLabel: "CLICK",
      asPath: router.asPath,
    });

    const token = localStorage.getItem(REVIEWTY_TOKEN);

    if (!token) {
      handleCheckAuth();
    }

    router.push(`/checkout/${dataProduct.fundingProduct.funding.id}`);

    return;
  };

  const showModal = () => {
    setOpen(!open);
  };

  const hideModal = () => {
    setOpen(false);
  };

  const handleClickBrand = () => {
    handleClickViewMobileApp(
      `https://review-ty.com/brand/${
        dataProduct?.fundingProduct.product!.brand.id
      }`,
      () => {
        dispatch(setIsOpen({}));
      }
    );
  };

  return (
    <>
      <Modal
        className="text-center"
        centered
        title={<div className="text-center font-bold py-2">Share</div>}
        open={open}
        onOk={hideModal}
        onCancel={hideModal}
        footer={false}
      >
        <div>
          <Space.Compact className="w-full">
            <Input
              disabled
              className="w-full"
              defaultValue={`${process.env.NEXT_PUBLIC_BASE_URL}${router.asPath}`}
            />
            <Button onClick={copyToClipboard}>Copy</Button>
          </Space.Compact>

          {/* <div className="py-4">
            Social Share
            <div className="py-2">
              <FacebookShareButton
                url={`${process.env.NEXT_PUBLIC_BASE_URL}${router.asPath}`}
                quote={"title"}
              >
                <FacebookIcon size={32} round />
              </FacebookShareButton>
            </div>
          </div> */}
        </div>
      </Modal>
      <div className="grid grid-cols-12 gap-4 pt-4">
        <div className="col-span-12 overflow-hidden">
          <div className="relative">
            {[dataProduct?.fundingProduct.product].map((content) => {
              return (
                <div key={Math.random()} className="product product-md">
                  <div className="m-auto w-[275px] h-[275px]">
                    {content?.thumbnail?.url === "" ? (
                      <Skeleton.Image
                        style={{ width: "275px", height: "275px" }}
                      />
                    ) : content?.thumbnail?.url!.includes(".mp4") ? (
                      <video
                        autoPlay
                        playsInline
                        // controls={true}
                        muted
                        loop
                        className="w-full   h-auto block "
                      >
                        <source src={content?.thumbnail?.url!} />
                      </video>
                    ) : content?.thumbnail?.url ? (
                      <Image
                        src={content?.thumbnail?.url}
                        width={275}
                        height={275}
                        className="w-full m-auto h-auto block "
                        alt="img"
                      />
                    ) : (
                      <Skeleton.Image
                        style={{ width: "275px", height: "275px" }}
                        active
                      />
                    )}
                  </div>
                  {dataProduct?.fundingProduct.funding.freeShip && (
                    <div className="bg-[#F9F0FF] text-center items-center justify-center flex px-4 pr-10">
                      <div className="pr-4 flex items-center">
                        <Image src={IMAGES.FreeShip} alt="free-ship" />
                      </div>
                      <span className="text-[#222] text-[0.875rem] font-normal">
                        Miễn phí vận chuyển cho mọi đơn hàng
                      </span>
                    </div>
                  )}
                </div>
              );
            })}
            <div className="relative col-span-12  h-[100%] ">
              <div className="flex flex-col sticky p-4  top-20 col-span-12">
                <h2 className="text-[1.25rem] font-medium pb-1 text-[#222222] mr-10">
                  {dataProduct?.fundingProduct?.funding.shortDescription || (
                    <Skeleton
                      title={false}
                      active
                      paragraph={{ rows: 2 }}
                      className="pb-4"
                    />
                  )}
                </h2>

                <div className="flex items-end mb-5">
                  {loading ? (
                    <Skeleton title={false} active paragraph={{ rows: 1 }} />
                  ) : (
                    <div className="flex justify-between items-center w-full">
                      <div>
                        <RatingStars
                          ratingNumber={
                            dataProduct?.fundingProduct.product!
                              .reviewsConnection.aggregate.avg.rate
                          }
                        />
                        <label className="relative top-1 right-0 pl-1 text-[#222222] text-[0.875rem]">
                          {dataProduct?.fundingProduct.product!.reviewsConnection.aggregate.avg.rate.toFixed(
                            1
                          )}
                        </label>
                        <label className="relative top-1 right-0 pl-1 text-[#9B9B9B] text-[0.875rem]">
                          (
                          {dataProduct?.fundingProduct.product!
                            .reviewsConnection.aggregate.count || 0}
                          )
                        </label>
                      </div>
                      <div>
                        <div className="cursor-pointer" onClick={showModal}>
                          <ShareIconProductDetail />
                        </div>
                        {/* <label
                          className=" ml-6 rounded-md px-2   text-[0.875rem]"
                          style={{ background: "rgba(0,90,83,0.06)" }}
                        >
                          {handleDiffDate(
                            dataProduct?.fundingProduct?.funding.endDate
                          )}
                        </label> */}
                      </div>
                    </div>
                  )}
                </div>

                <div className="flex justify-between text-[#005A53]">
                  {dataProduct?.fundingProduct.funding.preOrder ? (
                    <>
                      <div>
                        <p className="text-[#005A53] pt-2">
                          <span className="text-[1.125rem] text-[#005A53] font-bold">
                            {dataProduct?.fundingProduct.funding.nbOfPreOrder}
                          </span>{" "}
                          người đăng kí đặt trước
                        </p>
                      </div>
                      <div className="py-2">
                        <label
                          className=" ml-6 rounded-md px-2 py-1  text-[#005A53] "
                          style={{ background: "rgba(0,90,83,0.06)" }}
                        >
                          {handleDiffDate(
                            dataProduct?.fundingProduct?.funding.endDate
                          )}
                        </label>
                      </div>
                    </>
                  ) : (
                    <>
                      <div></div>
                      <div className="py-2">
                        <label
                          className=" ml-6 rounded-md px-2 py-1  text-[#005A53] "
                          style={{ background: "rgba(0,90,83,0.06)" }}
                        >
                          {handleDiffDate(
                            dataProduct?.fundingProduct?.funding.endDate
                          )}
                        </label>
                      </div>
                    </>
                  )}
                </div>
                {dataProduct?.fundingProduct.funding.preOrderQuantity &&
                  typeof dataProduct?.fundingProduct.funding
                    .productNbOfPreOrder === "number" && (
                    <div className="text-center bg-[#F7F7F7] py-4 -mx-4">
                      <div className="px-4">
                        <Progressbar
                          hideText
                          containerColor="#4D57FF"
                          bgcolor="#DDDFFB"
                          completed={
                            dataProduct?.fundingProduct.funding
                              .productNbOfPreOrder >
                            dataProduct?.fundingProduct.funding.preOrderQuantity
                              ? dataProduct?.fundingProduct.funding
                                  .preOrderQuantity
                              : dataProduct?.fundingProduct.funding
                                  .productNbOfPreOrder
                          }
                          limit={
                            dataProduct?.fundingProduct.funding.preOrderQuantity
                          }
                        />

                        <p>
                          Cần tối thiểu
                          <span className="text-[#4d57FF] font-bold">
                            {" "}
                            {
                              dataProduct?.fundingProduct.funding
                                .preOrderQuantity
                            }{" "}
                          </span>
                          sản phẩm mua chung!
                        </p>
                      </div>
                    </div>
                  )}

                <div></div>
              </div>
            </div>

            <div className="block w-full col-span-12">
              <div className={styles.brandName}>
                <div className="flex justify-center items-center">
                  {dataProduct?.fundingProduct.product!.brand.logoUrl ? (
                    <div
                      onClick={handleClickBrand}
                      className="p-1 cursor-pointer"
                      // style={{
                      //   border: "1px solid #ECECEC",
                      //   borderRadius: "50%",
                      //   width: "56px",
                      //   // height: "56px",
                      // }}
                    >
                      <div
                        style={{
                          background: `url(${dataProduct?.fundingProduct.product
                            .brand.logoUrl!}) no-repeat right center`,
                          backgroundPosition: "center",
                          backgroundSize: "contain",
                          backgroundRepeat: "no-repeat",
                          borderRadius: "50%",
                          border: "1px solid #ECECEC",
                          height: "56px",
                          width: "56px",
                        }}
                      />
                    </div>
                  ) : (
                    <Space>
                      <div className="items-center flex">
                        <Skeleton.Avatar
                          className="mr-4"
                          active
                          shape="circle"
                          style={{ width: "56px", height: "56px" }}
                        />
                        <Skeleton
                          active
                          title={false}
                          paragraph={{ rows: 1 }}
                          className="w-[250px] mr-6"
                        />
                        <Skeleton
                          active
                          title={false}
                          paragraph={{ rows: 2 }}
                          className="w-[50px] mr-2"
                        />
                      </div>
                    </Space>
                  )}

                  <span onClick={handleClickBrand} className="pl-4 flex">
                    <span className="text-[1rem] cursor-pointer mr-1 font-medium capitalize text-[#000]">
                      {
                        dataProduct?.fundingProduct.product!.brand
                          .translations[0].name
                      }
                    </span>

                    <Image src={IMAGES.arrowRight} alt="arrow-right" />
                  </span>
                </div>
                <div className="flex flex-col justify-center items-center text-center">
                  {loading ? (
                    <Skeleton title={false} active paragraph={{ rows: 1 }} />
                  ) : (
                    <>
                      <Image src={IMAGES.Follower} alt="follower-icon" />
                      <div className="text-[0.875rem]  w-full text-[#8D8D8D] text-left">
                        {formatNumber(
                          dataProduct?.fundingProduct.product!.brand
                            .nbFollowing,
                          "."
                        )}{" "}
                        followers
                      </div>
                    </>
                  )}
                </div>
              </div>
            </div>

            <div className="information-md block mt-9 md:p-0 col-span-12  mb-[7rem]">
              <p
                className={clsx(
                  " text-[#222222] pl-4 font-medium text-[1.25rem] pb-4",
                  styles.skeletonMain
                )}
              >
                Project Story
              </p>
              {!dataProduct?.fundingProduct.funding?.longDescription ? (
                <Skeleton.Image
                  style={{ width: "600px", height: "100vh" }}
                  active
                />
              ) : (
                <div
                  className="pb-16"
                  dangerouslySetInnerHTML={{
                    __html:
                      dataProduct?.fundingProduct.funding?.longDescription ||
                      "",
                  }}
                ></div>
              )}
              <ReviewSection productId={Number(router.query.productId)} />
            </div>

            <div
              className="fixed m-auto  bottom-0 left-0  items-center  justify-center w-full bg-[#f6f6f6]"
              style={{
                boxShadow: "0px -4px 4px 0px rgba(0, 0, 0, 0.08)",
              }}
            >
              {dataProduct?.fundingProduct.quantity! < 10 &&
                dataProduct?.fundingProduct.quantity! > 0 && (
                  <p className="text-center py-1 text-[#d10017]">
                    Còn lại {dataProduct?.fundingProduct.quantity} sản phẩm{" "}
                  </p>
                )}
              <div className="max-w-[600px] m-auto px-4  flex w-full items-center justify-center  bg-white py-2 ">
                <button
                  disabled={
                    (isExpired &&
                      dataProduct?.fundingProduct.funding.preOrder) ||
                    !dataProduct?.fundingProduct.funding.id ||
                    dataProduct?.fundingProduct.quantity! <= 0 ||
                    disabledBtn
                  }
                  onClick={handleBuyNow}
                  className={clsx(
                    "cursor-pointer bg-[#ba92d9] relative z-50",
                    {
                      "bg-[gray]":
                        (isExpired &&
                          dataProduct?.fundingProduct.funding.preOrder) ||
                        !dataProduct?.fundingProduct.funding.id ||
                        dataProduct?.fundingProduct.quantity! <= 0 ||
                        disabledBtn,
                    },
                    styles.buyNow
                  )}
                >
                  {isExpired && dataProduct?.fundingProduct.funding.preOrder
                    ? "Hết hạn đặt trước"
                    : dataProduct?.fundingProduct.quantity! <= 0
                    ? trans.products.soldOut
                    : disabledBtn
                    ? "Sự kiện chưa bắt đầu"
                    : dataProduct?.fundingProduct.funding.preOrder
                    ? "Đặt trước"
                    : trans.common.buyNow}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
