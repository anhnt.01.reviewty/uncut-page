import Head from "next/head";
import IMAGES from "@/assets/img/images";
import { useRouter } from "next/router";
import { DefaultSeo } from "next-seo";
const HeadMeta = () => {
  const router = useRouter();

  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1"
        />
        <meta
          property="og:image"
          content="https://d27j9dvp3kyb6u.cloudfront.net/2023/06/12/8a120228-95d1-4b17-a49e-0fab7cce8acf.png"
        />
        <meta
          property="og:description"
          content="Thỏa sức mua sắm tại Uncut với các thương hiệu chính hãng 100%, cao cấp, uy tín hàng đầu trong nước và quốc tế. Nhiều ưu đãi độc quyền. Miễn phí đổi trả trong vòng 7 ngày, miễn phí giao hàng trên toàn quốc. Mua hàng tại Uncut ngay hôm nay!"
        />
        <meta property="og:title" content={"Uncut - Unveiling gems"} />
        <meta property="og:type" content="website" />

        <link rel="icon" href={IMAGES.faviconLogo.src} />
      </Head>
    </>
  );
};

export default HeadMeta;
