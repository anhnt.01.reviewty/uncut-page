import { ShareIconProductDetail } from "@/assets/icon/icons";
import React, { useRef } from "react";

const CopyButton = ({ text }: { text: string }) => {
  const inputRef = useRef<HTMLInputElement>(null);

  const copyToClipboard = () => {
    inputRef.current!.select();
    document.execCommand("copy");
  };

  return (
    <div>
      <input
        ref={inputRef}
        type="text"
        value={text}
        style={{ display: "none" }}
        readOnly
      />
      <button onClick={copyToClipboard}>
        <ShareIconProductDetail />
      </button>
    </div>
  );
};

export default CopyButton;
