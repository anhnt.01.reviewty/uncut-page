import { ReadEventCommentQuery } from "@/graphql/reviewty/graphql";
import Image from "next/image";

import emptyImage from "@public/icon-profile-empty.png";
import { calculateOld } from "@/ultis/ultis";
import moment from "moment";

const CommentCard = ({
  data,
}: {
  data: ReadEventCommentQuery["eventComments"][0];
}) => {
  return (
    <div className="grid grid-cols-12 py-4  border-b-2 items-center justify-center">
      <div className="col-span-1 min-w-[40px] w-full ">
        <div
          className="rounded-full"
          style={{
            background: `url(${data.user.avatar?.url || emptyImage.src})`,
            backgroundSize: "cover",
            height: "50px",
            width: "50px",
            backgroundPosition: "center center",
            backgroundRepeat: "no-repeat",
          }}
        ></div>
      </div>

      <div className="col-span-11 pl-8 md:pl-4">
        <p className="text-[#BA92D9]">
          {data.user.account} ({calculateOld(data.user.birthYear!) + ` Tuổi`})
        </p>

        <p>{data.content}</p>

        <label className="text-[#9b9b9b]">
          {moment(data.createdAt).format("DD.MM.YYYY hh:mm:ss A")}
        </label>
      </div>
    </div>
  );
};

export default CommentCard;
