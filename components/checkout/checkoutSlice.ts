import { FundingDetailQuery } from "@/graphql/reviewty/graphql";
import { AppThunkStatus } from "@/store/store";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { message } from "antd";

interface AuthState {
  status: AppThunkStatus;
  error: any;
  info?: FundingDetailQuery;
  id?: number;
  root?: FundingDetailQuery;
}

const initialState: AuthState = {
  root: undefined,
  id: undefined,
  status: "idle",
  error: null,
  info: undefined,
};

const checkoutSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    infoOrders: (
      state,
      action: PayloadAction<{
        data: FundingDetailQuery;
        quantity?: number;
      }>
    ) => {
      state.status = "succeeded";
      const convertData = {
        funding: {
          ...action.payload.data.funding,
          fundingProducts: action.payload.data?.funding.fundingProducts?.map(
            (item) => {
              return {
                ...item,
                quantity:
                  action.payload.data.funding.id === state.info?.funding.id
                    ? state.info?.funding.fundingProducts?.[0].quantity
                    : action.payload.quantity || 1,
              };
            }
          ),
        },
      };
      state.root = action.payload.data;
      state.info = convertData;
    },
    increement: (
      state,
      action: PayloadAction<{
        quantity: number;
      }>
    ) => {
      if (state.info) {
        state.info = {
          funding: {
            ...state.info.funding,
            fundingProducts: state?.info.funding.fundingProducts?.map((item) => {
              if (item?.quantity! >= action.payload.quantity) {
                message.error({
                  content: `Chỉ còn lại ${action.payload.quantity} sản phẩm`,
                });
              }
              return {
                ...item,
                quantity:
                  item?.quantity! < action.payload.quantity
                    ? item?.quantity! + 1
                    : item.quantity,
              };
            }),
          },
        };
      }
    },
    decreement: (state, _) => {
      if (state.info) {
        state.info = {
          funding: {
            ...state.info.funding,
            fundingProducts: state?.info.funding.fundingProducts?.map((item) => {
              return {
                ...item,
                quantity:
                  item?.quantity! >= 2 ? item?.quantity! - 1 : item.quantity,
              };
            }),
          },
        };
      }
    },

    update: (state, payload: PayloadAction<{ id: number }>) => {},

    clear: (state, _) => {
      state.status = "idle";
      state.error = null;
      state.info = undefined;
    },
  },
  extraReducers(builder) {},
});

export const { clear, infoOrders, update, increement, decreement } =
  checkoutSlice.actions;

export default checkoutSlice.reducer;
