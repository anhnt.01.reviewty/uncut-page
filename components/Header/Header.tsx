import Image from "next/legacy/image";
import { useRouter } from "next/router";
import { Props } from "next/script";
import { FC, ReactNode, useEffect, useRef, useState } from "react";
// import reviewtyImg from "@public/reviewty.png";
import { BackIcon, HomeIcon } from "@/assets/icon/icons";
import Link from "next/link";

import IMAGES from "@/assets/img/images";
import useTrans from "@/hooks/useTrans";
import { useAppDispatch, useAppSelector } from "@/store/store";
import clsx from "clsx";
import MainModal from "../modal";
import NavigationMenu from "../nav-menu/NavigationMenu";
import styles from "./Header.module.scss";
import ModalDownloadApp from "../modal/download-app";
import { fetchNumberOrderStatistic, resetMainSlice } from "../main/mainSlice";
import { REVIEWTY_TOKEN } from "@/common/contains";

export enum EKeyItems {
  Logout = "Logout",
}

interface IProps {
  title?: string;
  children?: ReactNode;
  type?: "main" | "normal";
  referer?: string;
}

const Header: FC<IProps> = ({ title, children, type = "normal", referer }) => {
  const dispatch = useAppDispatch();
  const ref = useRef(null);
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);
  const trans = useTrans();
  const orderSlice = useAppSelector((slice) => slice.orderSlice);
  const mainSlice = useAppSelector((slice) => slice.mainSlice);
  const dataOrder = orderSlice.data?.fundingOrder;
  const dataReview = orderSlice.data?.fundingOrder.orderEvaluations?.filter(
    (order) => order.id === Number(router.query.evaluationId)
  )[0];
  const handleBack = () => {
    if (!dataReview && router.pathname === "/orders/[id]/review") {
      setIsOpen(!isOpen);
      return;
    }
    if (router.pathname === "/event-detail/[id]") {
      if (mainSlice.historyBack?.includes("event-detail")) {
        router.push("/");
      } else {
        dispatch(resetMainSlice({}));
        router.back();
        return;
      }
      return;
    }

    router.back();
  };

  const handleOk = () => {
    setIsOpen(!isOpen);
  };
  const handleCancel = () => {
    router.back();
    setIsOpen(!isOpen);
  };

  useEffect(() => {
    if (localStorage.getItem(REVIEWTY_TOKEN)) {
      dispatch(fetchNumberOrderStatistic());
    }
  }, [dispatch]);

  return (
    <div
      className="max-w-[600px] w-full pl-0 top-0 sticky bg-white z-50"
      ref={ref}
    >
      <ModalDownloadApp />

      <MainModal
        isDangerOk={true}
        handleCancel={handleCancel}
        handleOk={handleOk}
        isOpen={isOpen}
        title="Bạn muốn rời trang này ?"
      >
        Nếu bạn rời trang, những thay đổi của bạn không được lưu
        {/* Reviewty sẽ liên hệ với bạn qua địa chỉ mặc định Bánh bèo | 0333818484 */}
      </MainModal>
      {type === "main" && (
        <>
          <div className="flex p-4 px-2 justify-between font-medium items-center max-w-screen-xl m-auto">
            <div className={clsx("", styles.mainImg)}>
              <Image
                onClick={() => {
                  router.push("/");
                }}
                width={160}
                height={45}
                priority
                src={IMAGES.mainLogo}
                alt="main-logo"
              />
              {/* <span className="text-[1.5rem] font-semibold pl-2">Sự kiện</span> */}
              <div
                className="flex w-[34px] h-6"
                style={{ boxSizing: "border-box" }}
              >
                <NavigationMenu />
              </div>
            </div>

            <div className="block md:hidden pb-2"></div>
          </div>
        </>
      )}

      {type !== "main" && (
        <>
          <div className="container flex p-4 justify-between m-auto  font-medium items-center">
            <div className={clsx("cursor-pointer", {})}>
              {router.pathname !== "/checkout/results" && (
                <span onClick={handleBack}>
                  <BackIcon />
                </span>
              )}
            </div>
            {title}

            <Link href="/" className="cursor-pointer">
              <HomeIcon />
            </Link>
          </div>
        </>
      )}
    </div>
  );
};

export default Header;
