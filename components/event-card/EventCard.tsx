import { DeviceDetectContext } from "@/context/DeviceContext";
import { GetEventDetailQuery } from "@/graphql/reviewty/graphql";
import { Col, Row } from "antd";
import clsx from "clsx";
import Image from "next/image";
import { useRouter } from "next/router";
import { useCallback, useContext } from "react";
import RatingStars from "../rating-stars/RatingStars";
interface IEventCard {
  data: GetEventDetailQuery["event"];
}

const EventCard = ({ data }: IEventCard) => {
  const devices = useContext(DeviceDetectContext);

  const router = useRouter();
  const handleClick = useCallback(
    (product: any, data: GetEventDetailQuery["event"]) => {
      router.push({
        pathname: "[id]/product-event",
        query: {
          id: router.query["id"],
          productId: product.id,
        },
      });
    },
    [router]
  );

  return (
    <Row className={clsx("relative overflow-hidden md:col-span-6")}>
      {data?.products.map((product, index) => (
        <Col
          onClick={() => handleClick(product, data)}
          span={8}
          className="cursor-pointer text-center bg-white"
          key={product.id}
        >
          <div className="pb-3">
            <Image
              src={product?.thumbnail?.url!}
              width="0"
              height="0"
              sizes="100vw"
              className="w-full h-auto block "
              alt="img"
            />
          </div>
          <label className="text-elipsis cursor-pointer text-[0.875rem] pb-2">
            {product.translations[0].name}
          </label>
          <div>
            <RatingStars
              ratingNumber={product.reviewsConnection.aggregate.avg.rate}
            />
            <label className="relative top-1 right-0 pl-1 text-[#222222] text-[0.875rem]">
              {product.reviewsConnection.aggregate.avg.rate.toFixed(1)}
            </label>
            <label className="relative top-1 right-0 pl-1 text-[#9B9B9B] text-[0.875rem]">
              ({product.reviewsConnection.aggregate.count || 0})
            </label>
          </div>
        </Col>
      ))}
    </Row>
  );
};

export default EventCard;
