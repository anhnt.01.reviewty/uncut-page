import IMAGES from "@/assets/img/images";
import { Col, Row } from "antd";
import Image from "next/legacy/image";
import Link from "next/link";
import type { CollapsePanelProps } from "antd";
import Collapse from "../collapse";
import useTrans from "@/hooks/useTrans";
import { DeviceDetectContext } from "@/context/DeviceContext";
import { useContext } from "react";
import { useRouter } from "next/router";

const appleStoreLink =
  "https://apps.apple.com/vn/app/reviewty-m%E1%BB%B9-ph%E1%BA%A9m-l%C3%A0m-%C4%91%E1%BA%B9p/id1468579853";
const playStoreLink =
  "https://play.google.com/store/apps/details?id=com.reviewty.reviewty";

const Footer = () => {
  const router = useRouter();
  const devices = useContext(DeviceDetectContext);

  const trans = useTrans();
  const termOfUseAndPolicyLinks = [
    {
      href: "/documents/thong-tin-san-pham",
      label: "Thông tin sản phẩm",
    },
    {
      href: "/documents/chinh-sach-bao-mat",
      label: "Chính sách bảo mật",
    },
    {
      href: "/documents/chinh-sach-kiem-hang",
      label: "Chính sách kiểm hàng",
    },
    {
      href: "/documents/chinh-sach-doi-tra-hang-va-hoan-tien",
      label: "Chính sách đổi trả hàng và hoàn tiền",
    },
    {
      href: "/documents/chinh-sach-thanh-toan",
      label: "Chính sách thanh toán",
    },
    {
      href: "/documents/chinh-sach-van-chuyen-va-giao-nhan",
      label: "Chính sách vận chuyển và giao nhận",
    },
    {
      href: "/documents/nghia-vu-nguoi-ban-va-nguoi-mua",
      label: "Nghĩa vụ người bán và người mua",
    },
    {
      href: "/documents/chinh-sach-dat-hang-pre-order-tren-uncut",
      label: "Chính sách đặt hàng PRE-ORDER trên Uncut",
    },
  ];

  const handleClick = () => {
    router.push("faq");
  };

  return (
    <div className="p-4 pt-12">
      <div className="flex items-center justify-between">
        <Link href="/" className="flex pb-8 items-center">
          <Image
            width={160}
            height={45}
            priority
            src={IMAGES.mainLogo}
            alt="main-logo"
          />
        </Link>
        <Link
          className="flex pb-8 items-center"
          href="http://online.gov.vn/Home/WebDetails/108014"
          target="_blank"
        >
          <Image
            width={190}
            height={70}
            // layout="fill"
            src={IMAGES.confirmedMinistryOfIndustryAndTrade}
            alt="alt"
          />
        </Link>
      </div>

      <div className="flex flex-col items-center justify-center py-4">
        <Collapse
          title={trans.footer.bussinessInformation}
          className="border-b border-b-[#e5e5e5] w-full"
        >
          <div className="mt-2">
            <span className="font-semibold mr-2">
              {trans.footer.bussinessName}:
            </span>
            <span>Công ty TNHH Reviewty</span>
          </div>

          <div className="mt-2">
            <span className="font-semibold mr-2">{trans.common.address}:</span>
            <span>
              Tầng 13, tòa văn phòng 1 – Dự án Sun Square, Số 21 đường Lê Đức
              Thọ, Phường Mỹ Đình 2, Quận Nam Từ Liêm, Thành phố Hà Nội, Việt
              Nam
            </span>
          </div>
          <div className="mt-2">
            <span className="font-semibold mr-2">Email:</span>
            <span>support@review-ty.com</span>
          </div>
          <div className="mt-2">
            <span className="font-semibold mr-2">
              {trans.footer.bussinessID}:
            </span>
            <span>0108694593</span>
          </div>
          <div className="mt-2">
            <span className="font-semibold mr-2">Ngày cấp:</span>
            <span>10/04/2019</span>
          </div>
          <div className="mt-2">
            <span className="font-semibold mr-2">Nơi cấp:</span>
            <span>Phòng đăng ký KD thành phố Hà Nội</span>
          </div>
          <div className="mt-2">
            <span className="font-semibold mr-2">
              {trans.address.phoneNumber}:
            </span>
            <span>(+84) 961-554-388</span>
          </div>
        </Collapse>

        <Collapse
          title={trans.footer.aboutMe}
          className="border-b border-b-[#e5e5e5] w-full"
        >
          <ul className="text-[1rem] text-[#222] pb-8 font-medium">
            <li className="text-[#8d8d8d] mt-4">
              <Link
                href="https://review-ty.com/about/introduction"
                target="_blank"
              >
                {trans.footer.aboutCompany}
              </Link>
            </li>
          </ul>
        </Collapse>

        <Collapse
          title={trans.footer.termAndPolicy}
          className="border-b border-b-[#e5e5e5] w-full"
        >
          <ul className="text-[1rem] text-[#222] font-medium">
            {termOfUseAndPolicyLinks.map((link) => {
              return (
                <li key={link.href} className="pt-4">
                  <Link
                    href={link.href}
                    target="_blank"
                    className="text-[#8d8d8d] "
                  >
                    {link.label}
                  </Link>
                </li>
              );
            })}
          </ul>
        </Collapse>
      </div>

      <ul
        className="text-[1rem] cursor-pointer font-semibold text-[#222]"
        onClick={handleClick}
      >
        FAQ
      </ul>
      <ul className="text-[1rem] font-semibold text-[#222] py-8 ">
        {trans.footer.followUs}
        <li className="text-[#8d8d8d] flex  items-center pt-4">
          <Link
            href="https://www.facebook.com/globalreviewty?mibextid=LQQJ4d"
            className="mr-4"
          >
            <Image src={IMAGES.fbIconFillBlack} alt="logo-fb" />
          </Link>
          <Link href="https://www.instagram.com/reviewty.vn/?igshid=YzcxN2Q2NzY0OA%3D%3D">
            <Image src={IMAGES.instaIconFillBlack} alt="logo-insta" />
          </Link>
        </li>
      </ul>
      <p className="font-bold text-[1rem] text-[#404040] pb-4">
        Download the Review app
      </p>

      {(!devices.isWebViewAndroid || !devices.isWebViewIOS) && (
        <div className="flex pb-3">
          <div
            className="mr-2"
            onClick={() => {
              window.open(playStoreLink);
            }}
          >
            <Image
              width={140}
              height={40}
              src={IMAGES.GGPlay}
              className="mr-2"
              alt="google-play"
            />
          </div>
          <div
            onClick={() => {
              window.open(appleStoreLink);
            }}
          >
            <Image
              width={140}
              height={40}
              src={IMAGES.AplleStore}
              alt="apple-store"
            />
          </div>
        </div>
      )}
      <div>
        <Image src={IMAGES.QRCode} alt="qr-code" />
      </div>

      <p className="">ⓒ2023 Uncut. All Rights Reserved.</p>

      {/* <Row className="items-center my-16" gutter={[20, 40]}>
        <Col span={12}>
          <Link
            href="https://apps.apple.com/vn/app/reviewty-m%E1%BB%B9-ph%E1%BA%A9m-l%C3%A0m-%C4%91%E1%BA%B9p/id1468579853"
            target="_blank"
          >
            <Image src={IMAGES.appStore} alt="app-store" />
          </Link>
        </Col>
        <Col span={12}>
          <Link href="https://play.google.com/store/apps/details?id=com.reviewty.reviewty">
            <Image src={IMAGES.googlePlay} alt="google-play" />
          </Link>
        </Col>
      </Row> */}
    </div>
  );
};

export default Footer;
