import clsx from "clsx";
import React, { useCallback, useEffect, useState } from "react";

import useWindowScroll from "@/hooks/useWindowsScroll";
import { ArrowUp, BackIcon } from "@/assets/icon/icons";
import { useRouter } from "next/router";

const BackButton = () => {
  const { y } = useWindowScroll();
  const [display, setDisplay] = useState(false);
  const router = useRouter();
  const isShow = y >= 600;

  useEffect(() => {
    let timeId: NodeJS.Timeout;

    if (!isShow) {
      timeId = setTimeout(() => {
        setDisplay(false);
      }, 250);
    } else {
      setDisplay(true);
    }

    return () => {
      clearTimeout(timeId);
    };
  }, [isShow]);

  return (
    <button
      onClick={() => {
        router.back();
      }}
      className={clsx(
        "fixed z-action Button left-5 bottom-20 p-0 cursor-pointer transition-opacity rounded-full bg-[white]/80 border-2",
        {
          "flex opacity-1": display,
          "hidden opacity-0": !display,
        }
      )}
    >
      <div className="flex items-center justify-center h-10 w-10">
        <BackIcon />
      </div>
    </button>
  );
};

export default BackButton;
