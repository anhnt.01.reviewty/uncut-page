import useTrans from "@/hooks/useTrans";
import clsx from "clsx";
import { useRouter } from "next/router";
import { FC, ReactElement, ReactNode, useRef } from "react";
import BackButton from "../back-button";
import Footer from "../footer";
import GoTopButton from "../go-top-button";
import Header from "../Header";
import styles from "./main-layout.module.scss";

interface Props {
  hasFooter?: boolean;
  header?: ReactElement;
  children: ReactNode;
}

const Layout: FC<Props> = ({ children, header, hasFooter }) => {
  const router = useRouter();

  return (
    <div className="max-w-[600px] w-full m-auto bg-white min-h-[100vh]">
      <div>
        <div>{children}</div>
        {hasFooter && <Footer />}
        <GoTopButton />
        {router.pathname !== "/" && <BackButton />}
      </div>
    </div>
  );
};
export default Layout;
