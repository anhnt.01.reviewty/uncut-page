import IMAGES from "@/assets/img/images";
import { Col, Row } from "antd";
import Image from "next/legacy/image";

const PageTwo = () => {
  const waysPayment = [
    {
      id: 1,
      text: "Tải về mã VNPay QR -> Vào app ngân hàng bạn có và tải mã lên để thanh toán",
      img: (
        <Image
          priority
          className="rounded-md"
          src={IMAGES.FirstWay}
          alt="img-login"
        />
      ),
    },
    // {
    //   id: 2,
    //   text: "Nhập thông tin thẻ để thanh toán trực tiếp ",
    //   img: (
    //     <Image
    //       priority
    //       className="rounded-md"
    //       src={IMAGES.SecondWay1}
    //       alt="img-login"
    //     />
    //   ),
    // },
  ];

  return (
    <div>
      <p className="text-[#222] pb-4">
        Không cần bắt buộc phải có ví VNPAY bạn vẫn có thể mua hàng. Chỉ cần bạn
        có sử dụng 1 tài khoản ngân hàng là có thể mua hàng bình thường.
      </p>

      <p className="text-[#222]">Có 2 cách thanh toán:</p>
      <div
        style={{ boxShadow: "0px 0px 12px 0px rgba(0, 0, 0, 0.12)" }}
        className="flex m-auto mt-4 mb-6 border border-[#ECECEC] rounded-md w-1/2 h-1/2"
      >
        <Image
          priority
          className="rounded-md "
          src={IMAGES.TwoWaysPayment}
          alt="img-login"
        />
      </div>

      {waysPayment.map((item, index) => {
        return (
          <div key={index}>
            <p className="text-[#222]">
              <span className="text-[#BA92D9]">Cách {index + 1}: </span>
              {item.text}
            </p>
            <div
              style={{ boxShadow: "0px 0px 12px 0px rgba(0, 0, 0, 0.12)" }}
              className="flex m-auto mt-4 mb-6 border border-[#ECECEC] rounded-md w-1/2 h-1/2"
            >
              {item.img}
            </div>
          </div>
        );
      })}

      <div>
        <p className="text-[#222]">
          <span className="text-[#BA92D9]">Cách 2: </span> Nhập thông tin thẻ để
          thanh toán trực tiếp
        </p>

        <Row gutter={[30, 30]}>
          <Col span={12}>
            <div>
              <div
                style={{ boxShadow: "0px 0px 12px 0px rgba(0, 0, 0, 0.12)" }}
                className="flex m-auto mt-4 mb-6 border border-[#ECECEC] rounded-md"
              >
                <Image
                  priority
                  className="rounded-md"
                  src={IMAGES.SecondWay1}
                  alt="img-login"
                />
              </div>
              <p className="text-center text-[#222]">
                Chọn một ngân hàng bạn sử dụng và nhập thông tin thẻ để hoàn tất
                thanh toán
              </p>
            </div>
          </Col>
          <Col span={12}>
            <div
              style={{ boxShadow: "0px 0px 12px 0px rgba(0, 0, 0, 0.12)" }}
              className="flex m-auto mt-4 mb-6 border border-[#ECECEC] rounded-md "
            >
              <Image
                priority
                className="rounded-md"
                src={IMAGES.SecondWay2}
                alt="img-login"
              />
            </div>
            <p className="text-center text-[#222]">
              Nhập thông tin số thẻ của ngân hàng bạn chọn
            </p>
          </Col>

          <Col span={24} className="pb-4">
            <Image
              priority
              className="rounded-md"
              src={IMAGES.SecondWay3}
              alt="img-login"
            />
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default PageTwo;
