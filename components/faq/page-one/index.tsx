import IMAGES from "@/assets/img/images";
import Image from "next/legacy/image";

const PageOne = () => {
  const howToBuy = [
    {
      id: 1,
      text: "Nếu là người dùng mới: bạn vui lòng tải app Reviewty và tạo 1 tài khoản trên app để có thể dễ dàng mua sắm trên Uncut và trải nghiệm các tính năng khác trên app",
      img: (
        <Image
          priority
          className="rounded-md "
          src={IMAGES.ImgLogin}
          alt="img-login"
        />
      ),
    },
    {
      id: 2,
      text: "Click tính năng Uncut",
      img: (
        <Image
          priority
          className="rounded-md "
          src={IMAGES.ImgHomePage}
          alt="img-login"
        />
      ),
    },
    {
      id: 3,
      text: "Chọn mua sản phẩm trên Uncut",
      img: (
        <Image
          priority
          className="rounded-md "
          src={IMAGES.ImgMenu}
          alt="img-login"
        />
      ),
    },
  ];
  return (
    <div>
      <p className="pb-4 text-[1rem] text-[#222]">
        Để có thể mua hàng trên Uncut
      </p>
      {howToBuy.map((item, index) => {
        return (
          <div key={item.id} className="text-[1rem] text-[#222]">
            <div className="flex items-center">
              <div className="min-w-[2.25rem] min-h-[2.25rem] rounded-full relative bg-[#BA92D9] ">
                <span className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 text-white">
                  {index + 1}
                </span>
              </div>
              <div className="ml-2">
                <p className="text-[#222]">{item.text}</p>
              </div>
            </div>

            <div
              style={{ boxShadow: "0px 0px 12px 0px rgba(0, 0, 0, 0.12)" }}
              className="flex m-auto mt-4 mb-6 border border-[#ECECEC] rounded-md w-1/2 h-1/2"
            >
              {item.img}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default PageOne;
