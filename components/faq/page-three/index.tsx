const PageThree = () => {
  return (
    <p className="text-[1rem] text-[#222]">
      Bạn không dùng thẻ ngân hàng mà chỉ dùng internet banking vẫn mua hàng
      trên Uncut. Bạn vui lòng tham khảo cách thanh toán đơn hàng bằng mã thanh
      toán VNPAY
    </p>
  );
};

export default PageThree;
