import IMAGES from "@/assets/img/images";
import { Col, Row } from "antd";
import Image from "next/legacy/image";

const PageFive = () => {
  return (
    <div className="text-[1rem] text-[#222]">
      <p className="text-[#222] pb-4">
        Bạn có thể theo dõi trạng thái đơn hàng ngay tại Uncut bằng các thao tác
        sau:
      </p>

      <div className="text-[1rem] text-[#222]">
        <div className="flex items-center">
          <div className="min-w-[2.25rem] min-h-[2.25rem] rounded-full relative bg-[#BA92D9] ">
            <span className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 text-white">
              1
            </span>
          </div>
          <div className="ml-2">
            <p className="text-[#222]">{`Vào trang Uncut >> Chọn menu góc phải màn hình`}</p>
          </div>
        </div>

        <div
          style={{ boxShadow: "0px 0px 12px 0px rgba(0, 0, 0, 0.12)" }}
          className="flex m-auto mt-4 mb-6 border border-[#ECECEC] rounded-md w-1/2 h-1/2"
        >
          <Image
            priority
            className="rounded-md "
            src={IMAGES.MainPageImg}
            alt="img-login"
          />
        </div>
        <div className="flex items-center pb-4">
          <div className="min-w-[2.25rem] min-h-[2.25rem] rounded-full relative bg-[#BA92D9] ">
            <span className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 text-white">
              2
            </span>
          </div>
          <div className="ml-2">
            <p className="">{` Xem lịch sử chi tiết các đơn hàng tại đây`}</p>
          </div>
        </div>
        <div className="px-2">
          <Row gutter={[40, 60]}>
            <Col span={12}>
              <div
                style={{ boxShadow: "0px 0px 12px 0px rgba(0, 0, 0, 0.12)" }}
                className="flex  m-auto mt-4 mb-6 border border-[#ECECEC] rounded-md "
              >
                <Image
                  priority
                  className="rounded-md "
                  src={IMAGES.OrderHistory}
                  alt="img-login"
                />
              </div>
            </Col>
            <Col span={12}>
              <div
                style={{ boxShadow: "0px 0px 12px 0px rgba(0, 0, 0, 0.12)" }}
                className="flex m-auto mt-4 mb-6 border border-[#ECECEC] rounded-md "
              >
                <Image
                  priority
                  className="rounded-md "
                  src={IMAGES.OrderHistory2}
                  alt="img-login"
                />
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};

export default PageFive;
