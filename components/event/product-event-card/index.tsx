import {
  ErrorIcon,
  ShareIconProductDetail,
  SuccessIcon,
} from "@/assets/icon/icons";
import IMAGES from "@/assets/img/images";
import {
  formatNumber,
  gTagGGAnalysis,
  handleCheckAuth,
  handleClickViewMobileApp,
  REVIEWTY_TOKEN,
} from "@/common/contains";
import {
  action,
  counter,
  EAction,
  setIsOpen,
} from "@/components/main/mainSlice";
import RatingStars from "@/components/rating-stars/RatingStars";
import ReviewSection from "@/components/reviews/review-section";
import { DeviceDetectContext } from "@/context/DeviceContext";
import {
  EventCommentCreateError,
  EventCommentCreateErrorCode,
  useCreateEventCommentV3Mutation,
} from "@/graphql/reviewty-user/graphql";
import {
  FundingProductQuery,
  GetEventDetailQuery,
  ReadEventCommentQuery,
  useReadEventCommentLazyQuery,
} from "@/graphql/reviewty/graphql";
import { useActiveCustomer } from "@/hooks/useActiveCustomer";
import { useGetAddress } from "@/hooks/useGetAddress";
import useTrans from "@/hooks/useTrans";
import { renderErrorMessage } from "@/pages/event-detail/[id]";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { handleDiffDate } from "@/ultis/ultis";
import { Button, Input, message, Modal, Space, Skeleton } from "antd";
import clsx from "clsx";
import Image from "next/legacy/image";
import Link from "next/link";
import { useRouter } from "next/router";
import {
  MouseEventHandler,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";

import styles from "./product-event-card.module.scss";

const ProductEventCard = ({
  data,
  loading,
  callback,
}: {
  data: GetEventDetailQuery["event"];
  loading: boolean;
  callback: Function;
}) => {
  const notStartedYet =
    new Date().getTime() < new Date(data?.startedAt).getTime();
  const { isWebViewIOS } = useContext(DeviceDetectContext);
  const router = useRouter();
  useActiveCustomer();
  const numberId = Number(router.query.id);
  const { default: defaultAddress } = useGetAddress();
  const [isSubmit, setIsSubmit] = useState(false);
  const activeCustomer = useAppSelector((state) => state.authSlice);
  const [count, setCount] = useState(0);
  const [isOpenModal, setIsOpenModal] = useState(false);

  const [open, setOpen] = useState(false);
  const [getComments, { data: readComments }] = useReadEventCommentLazyQuery({
    variables: {
      eventId: numberId,
      first: 20,
      skip: 0,
    },
    fetchPolicy: "no-cache",
  });
  const dispatch = useAppDispatch();
  const [createEvent] = useCreateEventCommentV3Mutation();

  const trans = useTrans();
  const copyToClipboard = () => {
    message.open({
      type: "success",
      content: trans.message.copyToClipboard,
    });

    navigator?.clipboard?.writeText(
      `${process.env.NEXT_PUBLIC_BASE_URL}${router.asPath}`
    );
    // Clipboard API is only supported for pages served over HTTPS or localhost.
  };
  const memoRender = useMemo(() => {
    return (
      <div
        className="pb-20"
        dangerouslySetInnerHTML={{
          __html: data?.content || "",
        }}
      ></div>
    );
  }, [data?.content]);

  const handleClick = useCallback(
    async (usePoint?: "usePoint") => {
      const checkDate = handleDiffDate(data?.endedAt);

      const isExpired = checkDate === "Hết hạn";
      if (isExpired && data?.referred_url) {
        if (isWebViewIOS) {
          router.push(data?.referred_url);
          return;
        }
        window.open(data?.referred_url, "_blank");
        return;
      }
      const token = localStorage.getItem(REVIEWTY_TOKEN);

      if (!token) {
        setIsOpenModal(!isOpenModal);
      }

      if (isExpired) {
        return;
      }

      if (!activeCustomer.me) return;

      if (activeCustomer.me && readComments) {
        if (!defaultAddress.length) {
          Modal.confirm({
            centered: true,
            okType: "primary",
            okText: "Thêm địa chỉ",
            title: "Chưa có địa chỉ",
            content: (
              <p className="">
                {renderErrorMessage(
                  EventCommentCreateErrorCode.ShippingAddressNotExisted
                )}
              </p>
            ),
            cancelText: "Hủy",
            onOk: () => {
              router.push("/profile/pick-address");
            },
            onCancel: () => {
              setCount(0);
            },
          });
          return;
        }
        const { id, fullName, address, ward, province, district, phoneNumber } =
          defaultAddress[0];
        // if (readComments!.eventComments!.length > 0) return;

        const { data } = await createEvent({
          variables: {
            data: {
              event: {
                id: numberId,
              },
              content: usePoint ? "Đổi điểm" : "Tôi tham gia",
              usePoints: usePoint ? true : false,
            },
          },
        });

        if (
          data?.createEventCommentV3_UncheckCondition.__typename ===
          "EventCommentCreateError"
        ) {
          Modal.error({
            icon: <ErrorIcon />,
            type: "error",
            centered: true,
            okType: "primary",
            okText: "Xác nhận",
            title: "Thất bại",
            content: (
              <p>
                {renderErrorMessage(
                  data.createEventCommentV3_UncheckCondition.code,
                  data.createEventCommentV3_UncheckCondition as EventCommentCreateError
                )}
              </p>
            ),
            onOk: async () => {
              setCount(0);
            },
          });
          // setCount(0);
          return;
        }

        if (
          data?.createEventCommentV3_UncheckCondition.__typename ===
          "EventComment"
        ) {
          Modal.confirm({
            icon: <SuccessIcon />,
            type: "success",
            centered: true,
            okType: "primary",
            okText: "Xác nhận",
            cancelText: "Thay đổi",
            title: "Thành công",
            content: (
              <p className="">
                Reviewty sẽ liên hệ với bạn qua địa chỉ mặc định{" "}
                <span className="font-bold text-[#222]">
                  {fullName} | {phoneNumber}, {address}, {ward}, {district},{" "}
                  {province}
                </span>{" "}
                . Bạn có đồng ý không?
              </p>
            ),
            onCancel: () => {
              dispatch(action(EAction.pickAddressBeforeJoinEvent));
              router.push("/profile/address-list");
            },
            onOk: async () => {},
          });
          getComments({
            variables: {
              eventId: numberId,
              userId: activeCustomer.me?.id,
            },
          });

          dispatch(counter({}));
        }
      }
    },
    [
      activeCustomer.me,
      createEvent,
      data?.endedAt,
      data?.referred_url,
      defaultAddress,
      dispatch,
      getComments,
      isOpenModal,
      isWebViewIOS,
      numberId,
      readComments,
      router,
    ]
  );

  const handleExchangePoints = useCallback(() => {
    Modal.confirm({
      centered: true,
      okType: "primary",
      okText: "Có",
      title: "Xác nhận đổi điểm ?",
      content: (
        <p className="">
          Bạn có muốn dùng{" "}
          <span className="text-[red] font-bold ">{data?.points}</span> điểm để
          đổi lấy sản phẩm này ?
        </p>
      ),
      cancelText: "Hủy",
      onOk: () => {
        setIsSubmit(true);
        handleClick("usePoint");
        setTimeout(() => {
          setIsSubmit(false);
        }, 0);
      },
      onCancel: () => {},
    });
    return;
  }, [data?.points, handleClick]);

  const renderBtn = useCallback(() => {
    const checkDate = handleDiffDate(data?.endedAt);
    const isExpired = checkDate === "Hết hạn";

    if (data) {
      if (isExpired && data.referred_url) {
        return (
          <div
            className={clsx(
              "fixed py-1 text-center px-2 w-[100%] bg-white max-w-[600px] bottom-0"
            )}
            style={{
              boxShadow: "0px -4px 4px 0px rgba(0, 0, 0, 0.08)",
            }}
          >
            <button
              onClick={() => handleClick()}
              className="rounded-lg z-50 py-3 w-full bg-[#ba92d9]"
            >
              Mua ngay
            </button>
          </div>
        );
      } else if ((isExpired && !data.referred_url) || notStartedYet) {
        return (
          <div
            className={clsx(
              "fixed py-1 text-center px-2 w-[100%] bg-white max-w-[600px] bottom-0"
            )}
            style={{
              boxShadow: "0px -4px 4px 0px rgba(0, 0, 0, 0.08)",
            }}
          >
            <button disabled className="rounded-lg z-50 py-3 w-full bg-[gray]">
              {notStartedYet ? "Sự kiện chưa bắt đầu" : "Hết hạn sự kiện"}
            </button>
          </div>
        );
      }
      return (
        <div
          className={clsx(
            "fixed py-1 text-center px-4 w-[100%] bg-white max-w-[600px] bottom-0"
          )}
          style={{
            boxShadow: "0px -4px 4px 0px rgba(0, 0, 0, 0.08)",
          }}
        >
          <button
            onClick={() => handleClick()}
            className={clsx("rounded-lg z-50 py-3 w-[48%] mr-[2%]", {
              "bg-[gray] cursor-default":
                readComments?.eventComments?.length || isExpired || count > 0,
              "bg-[#ba92d9] cursor-pointer":
                !readComments?.eventComments?.length,
              "w-full": !data.points,
            })}
          >
            {readComments?.eventComments?.filter((comment) =>
              comment.content.includes("Tôi tham gia")
            ).length
              ? "Bạn đã tham gia"
              : "Tôi tham gia"}
          </button>

          <button
            disabled={isSubmit}
            onClick={handleExchangePoints}
            className={clsx("rounded-lg z-50 py-3 w-[48%] ", {
              "bg-[gray] cursor-default":
                readComments?.eventComments?.length ||
                isExpired ||
                count > 0 ||
                data.currentNumberOfWinners === data.numberOfWinners,

              "bg-[#ba92d9] cursor-pointer":
                !readComments?.eventComments?.length,
              hidden: !data.points,
            })}
          >
            {readComments?.eventComments?.filter((comment) =>
              comment.content.includes("Đổi điểm")
            ).length
              ? "Bạn đã đổi điểm"
              : "Đổi điểm"}
          </button>
        </div>
      );
    }
  }, [
    count,
    data,
    handleClick,
    handleExchangePoints,
    notStartedYet,
    readComments?.eventComments,
  ]);

  const showModal = () => {
    setOpen(!open);
  };

  const hideModal = () => {
    setOpen(false);
  };

  const handleOk = async () => {
    handleCheckAuth();
    setIsOpenModal(!isOpenModal);
  };
  const handleCancel = () => {
    setCount(0);
    setIsOpenModal(!isOpenModal);
  };

  const handleClickBrand = () => {
    handleClickViewMobileApp(
      `https://review-ty.com/brand/${data?.products[0].brand.id}`,
      () => {
        dispatch(setIsOpen({}));
      }
    );
  };

  useEffect(() => {
    if (numberId && activeCustomer.me?.id) {
      getComments({
        variables: {
          eventId: numberId,
          userId: activeCustomer.me?.id,
        },
      });
    }
  }, [activeCustomer.me?.id, getComments, numberId]);

  return (
    <>
      <Modal
        centered
        okType="danger"
        open={isOpenModal}
        title="Cần đăng nhập để tham gia sự kiện"
        onOk={handleOk}
        onCancel={handleCancel}
      >
        Bạn cần phải đăng nhập để sử dụng chức năng này.
      </Modal>
      <Modal
        className="text-center"
        centered
        title={<div className="text-center font-bold py-2">Share</div>}
        open={open}
        onOk={hideModal}
        onCancel={hideModal}
        footer={false}
      >
        <div>
          <Space.Compact className="w-full">
            <Input
              disabled
              className="w-full"
              defaultValue={`${process.env.NEXT_PUBLIC_BASE_URL}${router.asPath}`}
            />
            <Button onClick={copyToClipboard}>Copy</Button>
          </Space.Compact>

          {/* <div className="py-4">
          Social Share
          <div className="py-2">
            <FacebookShareButton
              url={`${process.env.NEXT_PUBLIC_BASE_URL}${router.asPath}`}
              quote={"title"}
            >
              <FacebookIcon size={32} round />
            </FacebookShareButton>
          </div>
        </div> */}
        </div>
      </Modal>
      <div className="relative grid grid-cols-12 gap-4 pt-4">
        <div className="col-span-12 overflow-hidden">
          <div className="relative">
            {data?.products.map((content) => {
              return (
                <div key={Math.random()} className="product product-md">
                  <div className="m-auto w-[275px] h-[275px]">
                    {content?.thumbnail?.url === "" ? (
                      <Skeleton.Image
                        style={{ width: "275px", height: "275px" }}
                      />
                    ) : content?.thumbnail?.url!.includes(".mp4") ? (
                      <video
                        autoPlay
                        playsInline
                        muted
                        loop
                        className="w-full   h-auto block "
                      >
                        <source src={content?.thumbnail?.url!} />
                      </video>
                    ) : content?.thumbnail?.url ? (
                      <Image
                        src={content?.thumbnail?.url}
                        width={275}
                        height={275}
                        className="w-full m-auto h-auto block "
                        alt="img"
                      />
                    ) : (
                      <Skeleton.Image
                        style={{ width: "275px", height: "275px" }}
                        active
                      />
                    )}
                  </div>
                </div>
              );
            })}
            <div className="relative col-span-12  h-[100%] ">
              <div className="flex flex-col sticky p-4  top-20 col-span-12">
                <h2 className="text-[1.25rem] font-medium pb-1 text-[#222222] mr-10">
                  {data?.name || (
                    <Skeleton
                      title={false}
                      active
                      paragraph={{ rows: 2 }}
                      className="pb-4"
                    />
                  )}
                </h2>
                <div className="flex items-end mb-5">
                  {loading ? (
                    <Skeleton title={false} active paragraph={{ rows: 1 }} />
                  ) : (
                    <div className="flex justify-between items-center w-full">
                      <div>
                        <RatingStars
                          ratingNumber={
                            data?.products[0]?.reviewsConnection.aggregate.avg
                              .rate || 0
                          }
                        />
                        <label className="relative top-1 right-0 pl-1 text-[#222222] text-[0.875rem]">
                          {data?.products[0]?.reviewsConnection.aggregate.avg.rate.toFixed(
                            1
                          )}
                        </label>
                        <label className="relative top-1 right-0 pl-1 text-[#9B9B9B] text-[0.875rem]">
                          (
                          {data?.products[0]?.reviewsConnection.aggregate
                            .count || 0}
                          )
                        </label>
                      </div>
                      <div>
                        <div className="cursor-pointer" onClick={showModal}>
                          <ShareIconProductDetail />
                        </div>
                      </div>
                    </div>
                  )}
                </div>

                <div className="flex justify-between text-[#005A53]">
                  <div>
                    <label className="text-[#005A53]">
                      <span className="text-[1.125rem] text-[#005A53] font-bold">
                        {data?.commentsConnection.aggregate.count}
                      </span>{" "}
                      người tham gia
                    </label>
                  </div>
                  <div className="pb-2">
                    <label
                      className=" ml-6 rounded-md px-2 py-1  text-[#005A53] "
                      style={{ background: "rgba(0,90,83,0.06)" }}
                    >
                      {handleDiffDate(data?.endedAt)}
                    </label>
                  </div>
                </div>

                <div></div>
              </div>
            </div>

            <div className="block  w-full  col-span-12">
              <div className={styles.brandName}>
                <div className="flex justify-center items-center">
                  {data?.products[0]?.brand.logoUrl ? (
                    <div
                      onClick={handleClickBrand}
                      className="p-1 cursor-pointer"
                    >
                      <div
                        style={{
                          background: `url(${data?.products[0].brand
                            .logoUrl!}) no-repeat right center`,
                          backgroundPosition: "center",
                          backgroundSize: "contain",
                          backgroundRepeat: "no-repeat",
                          borderRadius: "50%",
                          border: "1px solid #ECECEC",
                          height: "56px",
                          width: "56px",
                        }}
                      />
                    </div>
                  ) : (
                    <Space>
                      <div className="items-center flex">
                        <Skeleton.Avatar
                          className="mr-4"
                          active
                          shape="circle"
                          style={{ width: "56px", height: "56px" }}
                        />
                        <Skeleton
                          active
                          title={false}
                          paragraph={{ rows: 1 }}
                          className="w-[250px] mr-6"
                        />
                        <Skeleton
                          active
                          title={false}
                          paragraph={{ rows: 2 }}
                          className="w-[50px] mr-2"
                        />
                      </div>
                    </Space>
                  )}

                  <span onClick={handleClickBrand} className="pl-4 flex">
                    <span className="text-[1rem] cursor-pointer mr-1 font-medium capitalize text-[#000]">
                      {data?.products[0]?.brand.translations[0].name}
                    </span>

                    <Image src={IMAGES.arrowRight} alt="arrow-right" />
                  </span>
                </div>
                <div className="flex flex-col justify-center items-center text-center">
                  {loading ? (
                    <Skeleton title={false} active paragraph={{ rows: 1 }} />
                  ) : (
                    <>
                      <Image src={IMAGES.Follower} alt="follower-icon" />
                      <div className="text-[0.875rem]  w-full text-[#8D8D8D] text-left">
                        {formatNumber(
                          data?.products[0]?.brand.nbFollowing!,
                          "."
                        )}{" "}
                        followers
                      </div>
                    </>
                  )}
                </div>
              </div>
            </div>

            <div className="information-md block mt-9 md:p-0 col-span-12  ">
              <p
                // id="information"
                className={clsx(
                  " text-[#222222] pl-4 font-medium text-[1.25rem] pb-4",
                  styles.skeletonMain
                )}
              >
                Project Story
              </p>
              {!data?.content ? (
                <Skeleton.Image
                  style={{ width: "600px", height: "100vh" }}
                  active
                />
              ) : (
                memoRender
              )}
            </div>

            {renderBtn()}
          </div>
        </div>
      </div>
    </>
  );
};

export default ProductEventCard;
