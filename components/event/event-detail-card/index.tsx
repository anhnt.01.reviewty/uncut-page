import IMAGES from "@/assets/img/images";
import toCurrency from "@/common/toCurrency";
import { FundingsQuery } from "@/graphql/reviewty/graphql";
import { formatDate } from "@/ultis/ultis";
import clsx from "clsx";
import Image from "next/image";
import { useRouter } from "next/router";
import { MouseEvent } from "react";

interface IEventDetailCard {
  data: FundingsQuery["fundings"][0]["event"];
  index: number;
  type?: "collections";
}

const EventDetailCard = ({ data, index, type }: IEventDetailCard) => {
  const router = useRouter();

  const handleClick = (event: MouseEvent<HTMLDivElement>) => {
    event.preventDefault();
    if (data?.id) {
      router.push(`/event-detail/${data.id}`);
    }
  };

  const handleClickEventDetail = (event: IEventDetailCard["data"]) => {
    if (event?.id) {
      router.push(
        `/event-detail/${event.id}/product-event/?productId=${event.products[0].id}`
      );
    }
  };

  return (
    <>
      {data && (
        <div
          className={clsx("col-span-12 cursor-pointer  relative", {
            "border-none": index === 0,
            "border-t-8": type !== "collections",
          })}
        >
          <div
            className={clsx("", {
              "pb-3": type === "collections",
            })}
            onClick={handleClick}
          >
            <div className="relative">
              <Image
                loading="lazy"
                src={
                  type === "collections"
                    ? data?.coverUrl
                    : data?.coverUrlFunding!
                }
                alt={data?.name!}
                height="0"
                width="0"
                sizes="100vw"
                style={{ objectFit: "contain" }}
                className={clsx(" w-full ", {
                  " rounded-lg": type === "collections",
                })}
              />
              {type !== "collections" && data.isParticipantOfCurrentUser && (
                <div className="bottom-0 right-0 absolute">
                  <div className="relative">
                    <Image src={IMAGES.Joined} alt="joined" />
                    <span className="absolute top-1/2 -translate-y-1/2 right-[10px] flex items-center justify-center">
                      <span className="pr-1">
                        <Image src={IMAGES.CheckedJoin} alt="checked-joining" />
                      </span>
                      <span className="text-[12px] py-1 text-[#4069E2]">
                        Đã tham gia
                      </span>
                    </span>
                  </div>
                </div>
              )}
            </div>

            {type !== "collections" && (
              <div className="top-0 left-2 absolute">
                <Image src={IMAGES.EventTag} alt="event-tag" />
              </div>
            )}

            {type !== "collections" && (
              <div className="p-4 grid grid-cols-12 gap-2">
                <span
                  dangerouslySetInnerHTML={{
                    __html: data!.name.replace("/break", "<br>"),
                  }}
                  className="text-[24px] col-span-12 ellipsis-description  leading-[2rem]  mb-2 font-medium  text-[#222222]"
                ></span>
                <div className="col-span-12 flex justify-start text-[#7a7a7a">
                  <p className="text-[#747474] pb-4  text-[1rem]">
                    {formatDate(data?.startedAt)} ~ {formatDate(data?.endedAt)}
                  </p>
                </div>
              </div>
            )}
          </div>

          <div
            className={clsx("grid grid-cols-12  px-4 ", {
              "pb-4": type !== "collections",
            })}
            onClick={() => handleClickEventDetail(data)}
          >
            <div className="col-span-3 mr-1">
              <Image
                priority
                src={data?.products[0]!.thumbnail?.url!}
                width={72}
                height={72}
                sizes="100vw"
                alt="img"
                // loading="lazy"
              />
            </div>
            <div className="col-span-9 flex flex-col justify-between">
              <div
                className={clsx(
                  "text-[#8d8d8d] text-[1rem] ellipsis-description-2 "
                )}
              >
                <span className="text-[#222]">
                  [{data?.products[0]!.brand.translations[0].name}]
                </span>{" "}
                {data?.products[0]!.translations[0].name}
              </div>

              <div className="flex items-center">
                <span className="mr-1 text-[#D10017] font-medium">
                  {" "}
                  Sự kiện 0đ
                </span>
                <p className="mr-1 line-through text-[#CCCCCC]  text-[12px]">
                  {toCurrency(data?.products[0]!.price!)}
                </p>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default EventDetailCard;
