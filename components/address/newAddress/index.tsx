import IMAGES from "@/assets/img/images";
import useTrans from "@/hooks/useTrans";
import Image from "next/legacy/image";
import { useRouter } from "next/router";

const NewAddress = ({ fundingId }: { fundingId?: string }) => {
  const router = useRouter();
  const trans = useTrans();
  const handleAddNewAddress = () => {
    if (fundingId) {
      router.push({
        pathname: "/profile/pick-address",
        query: {
          fundingId: fundingId,
        },
      });
      return;
    }

    router.push("/profile/pick-address");

    return;
  };
  return (
    <div
      onClick={handleAddNewAddress}
      className="p-4 border-b-4  border-b-[#EFEFEF] flex justify-between items-center"
    >
      <label>{trans.address.addNewAddress}</label>

      <span className="cursor-pointer">
        <Image src={IMAGES.add} alt="add-address" />
      </span>
    </div>
  );
};

export default NewAddress;
