import IMAGES from "@/assets/img/images";
import { GetOneFundingOrderQuery } from "@/graphql/reviewty-user/graphql";
import Image from "next/image";

interface IFundingOder {
  data: GetOneFundingOrderQuery["fundingOrder"];
}

const AddressCard = ({ data }: IFundingOder) => {
  const copyToClipboard = () => {
    navigator?.clipboard?.writeText(`${data.deliveryAddress}`);
  };

  return (
    <>
      {data && (
        <div className="m-4 ">
          <div className="flex items-center justify-between pb-3">
            <div className="flex items-center">
              <span className="mr-2">
                <Image src={IMAGES.pickAddressIcon} alt="pick-address" />
              </span>
              <span className="font-medium">Địa chỉ nhận hàng</span>
            </div>

            <div></div>
          </div>

          <div className="">
            <span className="text-[1rem] text-[#404040] ">
              {data.userName} | {data.phoneNumber}
            </span>

            <p className="text-[#9b9b9b]">{data.deliveryAddress}</p>
          </div>
        </div>
      )}
    </>
  );
};

export default AddressCard;
