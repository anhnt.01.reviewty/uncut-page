import IMAGES from "@/assets/img/images";
import {
  deleteAddress,
  fetchAddresses,
  selectedAddress,
} from "@/components/auth/authSlice";
import {
  GetShippingAddressesQuery,
  ShippingAddress,
  useDeleteShippingAddressMutation,
} from "@/graphql/reviewty-user/graphql";
import { useActiveCustomer } from "@/hooks/useActiveCustomer";
import { REVIEWTY_CLIENT } from "@/hooks/useApollo";
import { useGetAddress } from "@/hooks/useGetAddress";
import useTrans from "@/hooks/useTrans";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { message, Modal, Skeleton } from "antd";
import clsx from "clsx";
import { GetServerSideProps } from "next";
import Image from "next/legacy/image";
import { useRouter } from "next/router";
import { MouseEvent } from "react";
import AddressListCard from "../addressList/addressListCard";
import NewAddress from "../newAddress";

interface IAddressOverView {
  type?: "operation" | "default";
  fundingId?: string;
}

const AddressOverview = ({ type = "default", fundingId }: IAddressOverView) => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const trans = useTrans();
  const addressSlice = useGetAddress();

  return (
    <>
      {addressSlice.status === "loading" ? (
        <div className="p-4">
          <Skeleton title={false} active paragraph={{ rows: 3 }} />
        </div>
      ) : addressSlice[type === "operation" ? "data" : "default"].length > 0 ? (
        <>
          {addressSlice[type === "operation" ? "data" : "default"].map(
            (shippingAddress) => {
              return (
                <AddressListCard
                  data={shippingAddress}
                  key={shippingAddress.id}
                  type={type}
                  fundingId={fundingId}
                />
              );
            }
          )}
          {type === "operation" && <NewAddress />}
        </>
      ) : (
        <NewAddress fundingId={router.query.id as string} />
      )}
    </>
  );
};

export default AddressOverview;

export const getServerSideProps: GetServerSideProps = async (context) => {
  return {
    props: {
      context: context.req.headers?.referer || "",
    },
  };
};
