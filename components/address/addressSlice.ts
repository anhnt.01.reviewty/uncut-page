import {
  Exact,
  GetMeDocument,
  GetMeQuery,
  GetMeQueryVariables,
  GetShippingAddressesDocument,
  GetShippingAddressesQuery,
  GetShippingAddressesQueryVariables,
  GetShippingFeeDocument,
  GetShippingFeeQuery,
  GetShippingFeeQueryVariables,
  ShippingFeeInput,
} from "@/graphql/reviewty-user/graphql";
import {
  AUTH_TOKEN,
  initializeApollo,
  REVIEWTY_CLIENT,
} from "@/hooks/useApollo";
import { AppState, AppThunkStatus } from "@/store/store";
import { LazyQueryHookOptions } from "@apollo/client";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import Router from "next/router";

interface AuthState {
  shippingFee?: GetShippingFeeQuery["shippingFee"];
  status: AppThunkStatus;
  error: any;
}

const initialState: AuthState = {
  status: "idle",
  error: null,
  shippingFee: undefined,
};

export const fetchShippingFee = createAsyncThunk(
  "auth/fetchShippingFee",
  async (
    options?: Partial<
      LazyQueryHookOptions<
        GetShippingFeeQuery,
        Exact<{
          data: ShippingFeeInput;
        }>
      >
    >
  ) => {
    const apolloClient = initializeApollo();
    const { data, error, errors } = await apolloClient.query<
      GetShippingFeeQuery,
      GetShippingFeeQueryVariables
    >({
      query: GetShippingFeeDocument,
      context: {
        clientName: REVIEWTY_CLIENT,
        headers: {
          Authorization: `Bearer ${localStorage.getItem(AUTH_TOKEN)}`,
        },
      },
      fetchPolicy: "no-cache",
    });

    if (error || errors) {
      throw error || errors;
    }

    return {
      shippingFee: data.shippingFee,
    };
  }
);

const addressSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    resetAddressSlice: (state) => initialState,
  },
  extraReducers(builder) {
    builder
      .addCase(fetchShippingFee.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchShippingFee.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.shippingFee = action.payload.shippingFee;
      })
      .addCase(fetchShippingFee.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error;
      });
  },
});

export const selectAddresses = (state: AppState) => state.authSlice.addresses;

export const { resetAddressSlice } = addressSlice.actions;

export default addressSlice.reducer;
