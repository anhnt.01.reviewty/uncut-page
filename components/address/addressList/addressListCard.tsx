import IMAGES from "@/assets/img/images";
import {
  deleteAddress,
  fetchAddresses,
  selectedAddress,
} from "@/components/auth/authSlice";
import {
  GetShippingAddressesQuery,
  useDeleteShippingAddressMutation,
} from "@/graphql/reviewty-user/graphql";
import useTrans from "@/hooks/useTrans";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { Modal, Skeleton } from "antd";
import clsx from "clsx";
import Image from "next/legacy/image";
import { useRouter } from "next/router";
import { MouseEvent } from "react";

interface IAddressListCard {
  data: GetShippingAddressesQuery["shippingAddresses"][0];
  type?: "operation" | "default";
  fundingId?: string;
}

const AddressListCard = ({ data, type, fundingId }: IAddressListCard) => {
  const [deleteShippingAddress] = useDeleteShippingAddressMutation({});
  const trans = useTrans();

  const dispatch = useAppDispatch();
  const addressSlice = useAppSelector((slice) => slice.authSlice);

  const router = useRouter();
  const handleClick = (
    address: GetShippingAddressesQuery["shippingAddresses"][0]
  ) => {
    if (
      router.pathname === "/profile/address-list" &&
      router.query.type === "default" &&
      router.query.id
    ) {
      dispatch(selectedAddress([data]));
      router.push(
        {
          pathname: `/checkout/[id]`,
          query: {
            id: router.query.id,
          },
        },
        undefined,
        { shallow: true }
      );
      return;
    }

    router.push(`/profile/address-list?type=${type}&id=${fundingId}`);
  };

  const handleEditAddress = (
    e: MouseEvent<HTMLSpanElement>,
    address: GetShippingAddressesQuery["shippingAddresses"][0]
  ) => {
    e.preventDefault();
    router.push(`/profile/address-list/${address.id}`);
    return;
  };
  const handleDeleteAddress = async (
    e: MouseEvent<HTMLSpanElement>,
    address: GetShippingAddressesQuery["shippingAddresses"][0]
  ) => {
    if (addressSlice.addresses.data.length <= 1) {
      Modal.confirm({
        title: trans.address.confirm.deleteOneLeftAddress,
        content: trans.address.content.deleteOneLeftAddressContent,
        okType: "primary",
        okText: trans.address.addNewAddress,
        cancelText: trans.common.cancel,
        okButtonProps: {
          style: {
            background: "#BA92D9",
          },
        },
        onOk: () => {
          router.push("/profile/pick-address");
        },
      });

      return;
    }

    Modal.confirm({
      title: trans.address.confirm.delete,
      content: "",
      okText: trans.common.delete,
      cancelText: trans.common.cancel,
      okType: "danger",
      onOk: async () => {
        const { data } = await deleteShippingAddress({
          variables: {
            where: {
              id: address.id,
            },
          },
        });
        if (data?.deleteShippingAddress.__typename === "ShippingAddress") {
          await dispatch(deleteAddress(data.deleteShippingAddress.id));
          await dispatch(fetchAddresses());
        }
      },
    });
    return;
  };

  return (
    <div className="p-4 relative cursor-pointer border-b-4" key={data.id}>
      <p
        onClick={() => handleClick(data)}
        className={clsx("font-medium text-[1.125rem] pb-3")}
      >
        {data.fullName}
        <span className="text-[#8D8D8D]">|</span> {data.phoneNumber}
      </p>
      <p
        onClick={() => handleClick(data)}
        className="text-[#8D8D8D] text-[1rem] mr-10 break-words"
      >
        {data.address}, {data.ward}, {data.district}, {data.province}
      </p>

      {data.default && <p className="text-[#8579CC]">{trans.common.default}</p>}

      {type === "operation" && (
        <div className="pt-3">
          <button
            className="cursor-pointer text-[#404040] relative z-10"
            onClick={(e) => handleEditAddress(e, data)}
          >
            {trans.common.edit}
          </button>{" "}
          |{" "}
          <span
            className="cursor-pointer"
            onClick={(e) => handleDeleteAddress(e, data)}
          >
            {trans.common.delete}
          </span>
        </div>
      )}

      {type !== "operation" && (
        <div
          onClick={() => handleClick(data)}
          className="absolute right-4 top-1/2 -translate-y-1/2 cursor-pointer"
        >
          <Image src={IMAGES.arrowRight} alt="arrow-right" />
        </div>
      )}
    </div>
  );
};

export default AddressListCard;
