import useGetOrders from "@/hooks/useGetOrders";
import { formatDate } from "@/ultis/ultis";
import { Steps } from "antd";
import clsx from "clsx";
import { DetailedHTMLProps, HTMLAttributes, useMemo } from "react";

// interface TrackingLogListProps
//   extends  {
//   logs: OrderTrackingLog[];
//   limit?: number;
// }
interface OrderTrackingLog {
  statusText: string;
  createdAt: any;
}

interface TrackingLogListProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  logs: OrderTrackingLog[];
  limit?: number;
}

const ShipmentHistory = ({ logs, limit, ...props }: TrackingLogListProps) => {
  const _logs = useMemo(() => {
    if (limit) {
      return [...logs].reverse().slice(0, limit);
    }
    return [...logs].reverse();
  }, [limit, logs]);

  return (
    <div {...props} className="p-4">
      <div className="flex justify-between">
        <span className="text-[1rem] text-[#404040] font-medium">
          Thông tin vận chuyển
        </span>
        <span onClick={props?.onClick} className="cursor-pointer">
          Xem
        </span>
      </div>

      <p className="text-[#404040] text-[1rem] pb-2">
        Giao hàng tiết kiệm - VN219454111879D
      </p>

      {_logs.map((log, index) => (
        <div
          key={index}
          className={clsx(
            "flex items-stretch font-normal gap-2",
            index === 0 ? "text-primary" : "text-gray-spanish"
          )}
        >
          <div className="flex flex-col items-end text-xs">
            {/* <span>{formatDate(new Date(log.createdAt), "HH:mm")}</span> */}
          </div>
          <div className="relative w-[9px]">
            {<div className="absolute h-full bg-[#ececec] w-px ml-1"></div>}
            <div
              className={clsx(
                "w-[9px] h-[9px] bg-[#404040] rounded-full relative z-10"
              )}
            ></div>
          </div>
          <div className="text-sm flex flex-col">
            <span>{formatDate(new Date(log.createdAt), "HH:mm")}</span>

            {log.statusText}
          </div>
        </div>
      ))}
    </div>
  );
};

export default ShipmentHistory;
