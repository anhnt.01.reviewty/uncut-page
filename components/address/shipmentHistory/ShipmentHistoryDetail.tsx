import IMAGES from "@/assets/img/images";
import {
  GetOneFundingOrderQuery,
  ShipmentHistory,
} from "@/graphql/reviewty-user/graphql";
import useGetOrders from "@/hooks/useGetOrders";
import { formatDate } from "@/ultis/ultis";
import { Steps } from "antd";
import clsx from "clsx";
import Image from "next/legacy/image";
import { DetailedHTMLProps, HTMLAttributes, useMemo } from "react";

// interface TrackingLogListProps
//   extends  {
//   logs: OrderTrackingLog[];
//   limit?: number;
// }
interface OrderTrackingLog {
  statusText: string;
  createdAt: any;
}

interface TrackingLogListProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  logs: ShipmentHistory[];
  data: GetOneFundingOrderQuery["fundingOrder"];
  limit?: number;
}

const ShipmentHistoryDetail = ({
  logs,
  data,
  limit = 1,
  ...props
}: TrackingLogListProps) => {
  const _logs = useMemo(() => {
    if (!logs) return [];
    if (limit && logs.length && logs) {
      return [...logs].slice(0, limit);
    }
    return [...logs].reverse();
  }, [limit, logs]);

  return (
    <div {...props} className="p-4">
      <div className="flex justify-between">
        <span className="text-[1rem] text-[#404040] font-medium flex items-center justify-center">
          <span className="mr-2 flex items-center justify-center">
            <Image src={IMAGES.ShippingIcon} alt="shipping-icon" />
          </span>
          <p className="text-[#404040] font-medium text-[1rem]">
            Thông tin vận chuyển
          </p>
        </span>
        {limit === 1 && (
          <span
            onClick={props?.onClick}
            className="cursor-pointer text-[#005A53] text-[1rem] font-medium"
          >
            Xem
          </span>
        )}
      </div>

      <div className="pl-6 pt-3">
        <p className="text-[#8d8d8d] font-medium pb-4 text-[1rem] ">
          Giao hàng tiết kiệm - VN219454111879D
        </p>

        {_logs.map((log, index) => (
          <div
            key={index}
            className={clsx(
              "flex items-start font-normal gap-2",
              index === 0 ? "text-primary" : "text-gray-spanish"
            )}
          >
            {limit !== 1 && (
              <div className="flex flex-col  items-end text-xs pb-4 pr-4">
                <span
                  className={clsx(
                    "w-[100px] text-end leading-[1.125rem] text-[#222] text-[1rem]",
                    {
                      "text-[#8d8d8d]": log.status !== data.shipmentStatus,
                    }
                  )}
                >
                  {formatDate(new Date(log.createdAt), "HH:mm")}
                </span>
              </div>
            )}
            <div className="relative w-[9px] h-[9px]">
              <div className="absolute h-[50px] bg-[#E5E5E5] w-px ml-1"></div>
              <div
                className={clsx(
                  "w-[9px] h-[9px] bg-[#404040] rounded-full relative z-10",
                  {
                    "bg-[#BA92D9]": log.status === data.shipmentStatus,
                    "bg-[#8d8d8d]": log.status !== data.shipmentStatus,
                  }
                )}
              ></div>
            </div>
            <div className="flex justify-between flex-col items-start">
              <div
                className={clsx(" flex flex-col text-[1rem] ", {
                  "text-[#8d8d8d]": log.status !== data.shipmentStatus,
                  "text-[#BA92D9]": log.status === data.shipmentStatus,
                })}
              >
                {log.statusText}
              </div>
              <span
                className={clsx(" text-end text-[#9b9b9b]", {
                  // "text-[#8d8d8d]": log.status !== data.shipmentStatus,
                })}
              >
                {formatDate(new Date(log.createdAt), "HH:mm")}
              </span>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ShipmentHistoryDetail;
