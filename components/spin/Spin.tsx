import { useAppDispatch, useAppSelector } from "@/store/store";
import clsx from "clsx";
import { Spin } from "antd";

const SpinLoader = () => {
  const authSlice = useAppSelector((slice) => slice.authSlice);
  return (
    <div
      className={clsx(
        "fixed top-0 bottom-0 left-0 right-0 bg-[rgba(0,0,0,0.25)] z-[999]",
        {
          hidden: !authSlice.spinning,
        }
      )}
    >
      <Spin
        spinning={authSlice.spinning}
        className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 z-[51]"
      />
    </div>
  );
};

export default SpinLoader;
