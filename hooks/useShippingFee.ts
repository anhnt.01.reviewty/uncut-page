import { REVIEWTY_TOKEN } from "@/common/contains";
import { fetchShippingFee } from "@/components/address/addressSlice";
import {
  fetchActiveCustomer,
  fetchAddresses,
  selectAddresses,
} from "@/components/auth/authSlice";
import {
  DeliverOption,
  useGetMeLazyQuery,
} from "@/graphql/reviewty-user/graphql";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { useEffect } from "react";
import { REVIEWTY_CLIENT } from "./useApollo";

export const useShippingFee = () => {
  const dispatch = useAppDispatch();
  const getSelectAddress = useAppSelector(selectAddresses);
  const addressSlice = useAppSelector((state) => state.addressSlice);

  useEffect(() => {
    if (
      addressSlice.status === "idle" &&
      localStorage.getItem(REVIEWTY_TOKEN)
    ) {
      if (getSelectAddress.default && getSelectAddress.default[0]?.address) {
        const { address, province, district } = getSelectAddress?.default[0];

        dispatch(
          fetchShippingFee({
            variables: {
              data: {
                address,
                province,
                district,
                weight: 1000,
                deliverOption: DeliverOption.None,
              },
            },
          })
        );
      }
    }
  }, [addressSlice.status, dispatch, getSelectAddress.default]);

  return {
    fee: addressSlice.shippingFee,
    status: addressSlice.status,
  };
};
