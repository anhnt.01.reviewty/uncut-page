import { setIsSpin } from "@/components/auth/authSlice";
import {
  clearOrder,
  fetchOrder,
  resetOrderSlice,
} from "@/components/orders/oderSlice";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { useRouter } from "next/router";
import { useEffect } from "react";

const useGetOrders = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const orderSlice = useAppSelector((slice) => slice.orderSlice);
  const { data } = orderSlice;
  const getId = Number(router.query.id);

  useEffect(() => {
    if (getId) {
      (async () => {
        dispatch(clearOrder({}));
        dispatch(setIsSpin(true));
        await dispatch(fetchOrder(getId));
        dispatch(setIsSpin(false));
      })();
    }

    return () => {};
  }, [dispatch, getId]);
  return { dataOrder: data?.fundingOrder };
};

export default useGetOrders;
