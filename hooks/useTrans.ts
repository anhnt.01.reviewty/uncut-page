import { useRouter } from "next/router";
import { en } from "@public/lang/en";
import { vi } from "@public/lang/vi";
import { useEffect, useMemo, useState } from "react";
import { REVIEWTY_LOCALE } from "@/common/contains";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { setDefaultLang } from "@/components/auth/authSlice";

const useTrans = () => {
  const dispatch = useAppDispatch();
  const authSlice = useAppSelector((slice) => slice.authSlice);

  // useEffect(() => {
  //   if (localStorage.getItem(REVIEWTY_LOCALE)) {
  //     dispatch(setDefaultLang(localStorage.getItem(REVIEWTY_LOCALE) || "vi"));
  //   }
  // }, [dispatch]);

  return vi;
};

export default useTrans;
