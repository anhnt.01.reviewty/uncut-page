import { useState, useEffect } from "react";

interface IUseDebounce {
  callback: Function;
  delaySec: number;
}

const useDebounce = ({ callback, delaySec = 0.35 }: IUseDebounce) => {
  const [debouncedCallback, setDebouncedCallback] = useState(callback);

  useEffect(() => {
    const timer = setTimeout(() => {
      setDebouncedCallback(() => callback);
    }, delaySec);

    return () => {
      clearTimeout(timer);
    };
  }, [callback, delaySec]);

  return debouncedCallback;
};

export default useDebounce;
