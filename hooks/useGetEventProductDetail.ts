import {
  GetEventDetailQuery,
  useGetEventDetailLazyQuery,
} from "@/graphql/reviewty/graphql";
import { useAppSelector } from "@/store/store";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export const useGetEventProductDetail = () => {
  const router = useRouter();
  const [dataEvent, setDataEvent] = useState<GetEventDetailQuery["event"]>();
  const mainSlice = useAppSelector((slice) => slice.mainSlice);
  const productId = Number(router.query.productId);
  const eventId = Number(router.query.id);
  const [getEventDetail, { loading }] = useGetEventDetailLazyQuery();

  useEffect(() => {
    if (eventId && productId && mainSlice.counter)
      getEventDetail({
        variables: {
          id: eventId,
        },
        fetchPolicy: "no-cache",
        onCompleted(data) {
          if (data.event) {
            const normalizeData: GetEventDetailQuery = {
              ...data,
              event: {
                ...data.event,
                products: data.event?.products.filter(
                  (p) => p.id === Number(productId)
                ),
              },
            };

            setDataEvent(normalizeData!.event!);
          }
        },
      });
  }, [eventId, getEventDetail, mainSlice.counter, productId, router]);
  return {
    getEventDetail,
    dataEvent,
    loading,
  };
};
