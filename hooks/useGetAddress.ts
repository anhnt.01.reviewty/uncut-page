import { REVIEWTY_TOKEN } from "@/common/contains";
import {
  fetchActiveCustomer,
  fetchAddresses,
  selectAddresses,
} from "@/components/auth/authSlice";
import { useGetMeLazyQuery } from "@/graphql/reviewty-user/graphql";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { useEffect } from "react";
import { REVIEWTY_CLIENT } from "./useApollo";

export const useGetAddress = () => {
  const dispatch = useAppDispatch();
  const address = useAppSelector(selectAddresses);

  useEffect(() => {
    if (address.status === "idle" && localStorage.getItem(REVIEWTY_TOKEN)) {
      dispatch(fetchAddresses());
    }
  }, [address.status, dispatch]);

  return {
    data: address.data,
    status: address.status,
    default: address.default,
  };
};
