export const isMobile = (userAgent: string) => /mobi/gi.test(userAgent);

export const isNativeApp = (userAgent?: string) =>
  /reviewty/gi.test(
    typeof window !== "undefined" ? window.navigator?.userAgent : ""
  );

export const isIOS = (userAgent?: string) => {
  return /ipad|iphone|ipod/gi.test(userAgent ?? window.navigator?.userAgent);
};
export const isWebViewAndroid = (userAgent?: string) =>
  /; wv/gi.test(window.navigator?.userAgent);
export const isWebViewIOS = (userAgent?: string) =>
  /WebViewApp/gi.test(window.navigator?.userAgent);

export const checkUserAgent = (userAgent: string) => {
  const arrWishListOrigin = JSON.parse(
    process.env.NEXT_PUBLIC_WISHLIST_ORIGIN_URL as any
  );
  const isOriginWishList = arrWishListOrigin?.some((url: string) =>
    url.includes(document?.referrer)
  );
  const device = {
    isWebViewAndroid: false,
    isWebViewIOS: false,
    isMobile: false,
    isAndroid: false,
    isIOS: false,
    isWebView: false,
  };

  if (/mobi/gi.test(userAgent)) {
    device.isMobile = true;
  }

  if (/; wv/gi.test(userAgent)) {
    device.isWebViewAndroid = true;
  }

  if (/WebViewApp/gi.test(userAgent)) {
    device.isWebViewIOS = true;
  }
  if (isOriginWishList) {
    device.isWebView = true;
  }

  if (/Android/.test(userAgent)) {
    device.isAndroid = true;
  }

  if (/iPhone/.test(userAgent)) {
    device.isIOS = true;
  }
  return device;
};
