import {
  ApolloClient,
  ApolloLink,
  HttpLink,
  InMemoryCache,
  NormalizedCache,
  ApolloError,
  from,
  concat,
} from "@apollo/client";
import { ApolloErrorOptions } from "@apollo/client/errors";
import merge from "deepmerge";
import isEqual from "lodash.isequal";
import { useRouter } from "next/router";
import { useMemo } from "react";
import { onError } from "@apollo/client/link/error";

export const AUTH_TOKEN = "rvt_tk";
// const router = useRouter();

// import { AUTH_TOKEN, VENDURE_HEADER_TOKEN } from "../components/auth/authSlice";

let apolloClient: ApolloClient<NormalizedCache>;

const authMiddleware = new ApolloLink((operation, forward) => {
  // if (!localStorage.getItem("rvt_tk")) {
  //   router.push("https://review-ty.com/auth");
  // }
  operation.setContext(({ headers = {} }) => ({
    headers: {
      ...headers,
      // "Access-Control-Allow-Headers":'',
      Authorization:
        typeof window === "undefined"
          ? null
          : localStorage.getItem(AUTH_TOKEN)
          ? `Bearer ${localStorage.getItem(AUTH_TOKEN)}`
          : null,
    },
  }));
  return forward(operation);
});

// const mallLink = new HttpLink({
//   uri: process.env.NEXT_PUBLIC_MALL_GRAPHQL_API_ENDPOINT,
//   credentials: "same-origin",
// });
const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors)
    graphQLErrors.forEach(({ message, locations, path }) =>
      console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
      )
    );
  if (networkError) console.log(`[Network error]: ${networkError}`);
});

const reviewtyLink = new HttpLink({
  uri: process.env.NEXT_PUBLIC_REVIEWTY_USER_GRAPHQL_API_ENDPOINT,
  credentials: "same-origin",
});

export const REVIEWTY_CLIENT = "reviewty";

function createApolloClient() {
  return new ApolloClient({
    ssrMode: typeof window === "undefined",
    link: concat(authMiddleware, reviewtyLink),
    cache: new InMemoryCache({
      typePolicies: {
        Query: {
          fields: {
            // fundingOrders: {
            //   keyArgs: false,
            //   merge(existing = [], incoming) {
            //     if (!existing) return existing;
            //     if (!incoming) return incoming;
            //     return [...existing, ...incoming];
            //   },
            // },

            eventComments: {
              keyArgs: false,
              merge(existing = [], incoming) {
                return [...existing, ...incoming];
              },
            },
            products: {
              keyArgs: false,
              merge(existing, incoming) {
                if (!incoming) return existing;
                if (!existing) return incoming;

                const { items, ...rest } = incoming;
                let result = rest;
                result.items = [...existing.items, ...items];
                return result;
              },
            },
          },
        },
      },
    }),
  });
}

export function initializeApollo(initialState = null) {
  const _apolloClient = apolloClient ?? createApolloClient();

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // get hydrated here
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract();

    // Merge the existing cache into data passed from getStaticProps/getServerSideProps
    const data = merge(initialState, existingCache, {
      // combine arrays using object equality (like in sets)
      arrayMerge: (destinationArray, sourceArray) => [
        ...sourceArray,
        ...destinationArray.filter((d) =>
          sourceArray.every((s) => !isEqual(d, s))
        ),
      ],
    });

    // Restore the cache with the merged data
    _apolloClient.cache.restore(data);
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === "undefined") return _apolloClient;
  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient;

  return _apolloClient;
}

export function useApollo(initialState = null) {
  const store = useMemo(() => initializeApollo(initialState), [initialState]);
  return store;
}
