import { REVIEWTY_UUID } from "@/common/contains";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect } from "react";

interface IUseViewGGANL {
  eventName: string;
  eventCategory: string;
  eventLabel: "VIEW" | "CLICK";
}

export const useViewGGAnalysis = ({
  eventCategory,
  eventLabel,
  eventName,
}: IUseViewGGANL) => {
  const router = useRouter();

  useEffect(() => {
    if (
      typeof window.gtag !== "undefined" &&
      process.env.NEXT_PUBLIC_TYPE === "production"
    ) {
      try {
        axios.post(
          process.env.NEXT_PUBLIC_REVIEWTY_USER_GRAPHQL_API_ENDPOINT!,
          {
            operationName: `${eventLabel}_${eventName}_funding`,
            variables: {
              data: {
                name: router.asPath,
                message: JSON.stringify({
                  date: new Date(),
                  "device-token": localStorage.getItem(REVIEWTY_UUID),
                }),
              },
            },
            query: `mutation ${eventLabel}_${eventName}_funding($data: ActivityLogCreateInput!) {\n  actitvityLogs(data: $data)\n}\n`,
          }
        );
      } catch (error) {}
      window.gtag("event", "page_view", {
        event_category: `CLICK_ENTER_${eventCategory}`,
        event_label: eventLabel,
        value: localStorage.getItem(REVIEWTY_UUID) ?? "",
      });
    }
  }, [eventCategory, eventLabel, eventName, router.asPath]);

  return {};
};
