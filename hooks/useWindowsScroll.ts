import { useEffect, useState } from "react";

export interface State {
  x: number;
  y: number;
}

const useWindowScroll = (): State => {
  const [state, setState] = useState<State>(() => ({
    x: typeof window !== "undefined" ? window.pageXOffset : 0,
    y: typeof window !== "undefined" ? window.pageYOffset : 0,
  }));

  useEffect(() => {
    const handler = () => {
      setState((prevState) => {
        const { pageXOffset, pageYOffset } = window;
        return prevState.x !== pageXOffset || prevState.y !== pageYOffset
          ? {
              x: pageXOffset,
              y: pageYOffset,
            }
          : prevState;
      });
    };

    //We have to update window scroll at mount, before subscription.
    //Window scroll may be changed between render and effect handler.
    handler();

    window.addEventListener("scroll", handler, {
      capture: true,
      passive: true,
    });

    return () => {
      window.removeEventListener("scroll", handler);
    };
  }, []);

  return state;
};

export default useWindowScroll;
