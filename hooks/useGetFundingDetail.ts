import { useFundingProductLazyQuery } from "@/graphql/reviewty/graphql";
import { useRouter } from "next/router";
import { useEffect } from "react";

export const useGetFundingDetail = () => {
  const router = useRouter();

  const productId = Number(router.query.productId);
  const fundingId = Number(router.query.fundingId);
  const [
    getDetailsFundingProduct,
    { data: dataProduct, loading: loadingProduct, error: errorProduct },
  ] = useFundingProductLazyQuery({
    variables: {
      where: {
        productId: productId,
        id: fundingId,
      },
    },
  });

  useEffect(() => {
    if (productId && fundingId) {
      getDetailsFundingProduct({
        variables: {
          where: {
            productId: productId,
            id: fundingId,
          },
        },
      });
    }
  }, [fundingId, getDetailsFundingProduct, productId]);
  return {
    dataProduct,
    loadingProduct,
  };
};
