import { REVIEWTY_TOKEN } from "@/common/contains";
import { counter, fetchActiveCustomer } from "@/components/auth/authSlice";
import { useGetMeLazyQuery } from "@/graphql/reviewty-user/graphql";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { useEffect } from "react";
import { REVIEWTY_CLIENT } from "./useApollo";

export const useActiveCustomer = () => {
  const dispatch = useAppDispatch();
  const authSlice = useAppSelector((store) => store.authSlice);

  useEffect(() => {
    if (!authSlice?.me?.id) {
      dispatch(fetchActiveCustomer());
    }
  }, [authSlice?.me?.id, dispatch]);

  return {
    data: authSlice.me,
    status: authSlice.status,
  };
};
