import { convertToText, increaseDate } from "@/common/contains";
import toCurrency from "@/common/toCurrency";
import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import {
  FundingOrdersQuery,
  FundingOrderStatus,
  OrderBy,
  useFundingOrdersLazyQuery,
  usePayBackFundingOrderMutation,
} from "@/graphql/reviewty-user/graphql";
import clsx from "clsx";
import Image from "next/legacy/image";
import { useRouter } from "next/router";
import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { NextPageWithLayout } from "../_app";
import LoadingProduct from "@/components/skeleton/loading-product";
import IMAGES from "@/assets/img/images";
import useTrans from "@/hooks/useTrans";
import { setIsSpin } from "@/components/auth/authSlice";
import { useAppDispatch, useAppSelector } from "@/store/store";
import OrderCard from "@/components/orders/orders-card";
import DragScroll from "@/components/drag-scroll";

const OrderOverview: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const mainSlice = useAppSelector((slice) => slice.mainSlice);
  const { NumberOrderStatistic } = mainSlice;
  const [ref, setRef] = useState<HTMLSpanElement | null>(null);
  const router = useRouter();
  const trans = useTrans();
  const [data, setData] = useState<FundingOrdersQuery>();
  const [orders, { data: dataOrders, error, loading, fetchMore, variables }] =
    useFundingOrdersLazyQuery({
      variables: {
        first: 20,
        where: {
          status:
            (router.query["type"] as FundingOrderStatus) ||
            FundingOrderStatus.InProcessing,
        },
        orderBy: {
          id: OrderBy.Desc,
        },
      },
      fetchPolicy: "no-cache",
      onCompleted(data) {
        setData(data);
      },
    });
  useEffect(() => {
    orders({
      variables: {
        first: 20,
        where: {
          status:
            (router.query["type"] as FundingOrderStatus) ||
            FundingOrderStatus.InProcessing,
        },
        orderBy: {
          id: OrderBy.Desc,
        },
      },
    });
  }, [orders, router.query]);

  const handleFetchMore = useCallback(async () => {
    dispatch(setIsSpin(true));

    const { data: dataOrders } = await fetchMore({
      variables: {
        first: 20,
        skip: data?.fundingOrders.length || 0,
        where: {
          status:
            (router.query["type"] as FundingOrderStatus) ||
            FundingOrderStatus.InProcessing,
        },
        orderBy: {
          id: OrderBy.Desc,
        },
      },
    });
    if (dataOrders && typeof data !== "undefined") {
      setData((prv) => {
        if (prv) {
          return {
            ...prv,
            fundingOrders: [...prv.fundingOrders, ...dataOrders.fundingOrders],
          };
        }
        return;
      });
    }
    dispatch(setIsSpin(false));
  }, [dispatch, fetchMore, data, router.query]);

  const barItems = useMemo(() => {
    return [
      {
        id: "inprocess",
        label: trans.home.navigationMenu.waitForPay,
        status: FundingOrderStatus.InProcessing,
        counter: NumberOrderStatistic?.nbOrderStatisticOfUser.filter(
          (numberOrderStatistic) =>
            numberOrderStatistic.orderStatus === FundingOrderStatus.InProcessing
        ),
      },
      {
        id: "wait-delivering",
        label: trans.home.navigationMenu.waitDelivering,
        status: FundingOrderStatus.WaitDelivering,
        counter: NumberOrderStatistic?.nbOrderStatisticOfUser.filter(
          (numberOrderStatistic) =>
            numberOrderStatistic.orderStatus ===
            FundingOrderStatus.WaitDelivering
        ),
      },
      {
        id: "delivering",
        label: trans.home.navigationMenu.delivering,
        status: FundingOrderStatus.Delivering,
        counter: NumberOrderStatistic?.nbOrderStatisticOfUser.filter(
          (numberOrderStatistic) =>
            numberOrderStatistic.orderStatus === FundingOrderStatus.Delivering
        ),
      },
      {
        id: "completed",
        label: trans.common.completed,
        status: FundingOrderStatus.Completed,
        counter: NumberOrderStatistic?.nbOrderStatisticOfUser.filter(
          (numberOrderStatistic) =>
            numberOrderStatistic.orderStatus === FundingOrderStatus.Completed
        ),
      },
      {
        id: "cancelled",
        label: trans.common.cancelled,
        status: FundingOrderStatus.Cancelled,
        counter: NumberOrderStatistic?.nbOrderStatisticOfUser.filter(
          (numberOrderStatistic) =>
            numberOrderStatistic.orderStatus === FundingOrderStatus.Cancelled
        ),
      },
    ];
  }, [
    NumberOrderStatistic?.nbOrderStatisticOfUser,
    trans.common.cancelled,
    trans.common.completed,
    trans.home.navigationMenu.delivering,
    trans.home.navigationMenu.waitDelivering,
    trans.home.navigationMenu.waitForPay,
  ]);

  const handleClickBarITems = useCallback(
    async (statusFilter?: FundingOrderStatus) => {
      dispatch(setIsSpin(true));

      if (!loading) {
        await orders({
          variables: {
            first: 20,
            skip: 0,
            where: {
              status: statusFilter,
            },
            orderBy: {
              id: OrderBy.Desc,
            },
          },
        });
        router.push(
          {
            pathname: router.pathname,
            query: {
              type: statusFilter,
            },
          },
          undefined,
          { shallow: false }
        );
      }
      dispatch(setIsSpin(false));

      return;
    },
    [dispatch, loading, orders, router]
  );

  useEffect(() => {
    if (!ref) {
      return;
    }
    ref.scrollIntoView({
      behavior: "smooth",
      block: "end",
      inline: "end",
    });
  }, [ref]);

  return (
    <div className="">
      <DragScroll>
        <div className="flex cursor-pointer ">
          {barItems.map((item, index) => (
            <span
              ref={(ref) => {
                if (item.status === router.query["type"]) {
                  setRef(ref);
                }
              }}
              key={item.id}
              onClick={() => {
                handleClickBarITems(item.status);
              }}
              className={clsx(
                "w-[120px] cursor-pointer whitespace-nowrap text-[1rem] border-b-2 scrollbar p-4 py-3 col-span-3 text-center ",
                {
                  "text-[#BA92D9]  border-b-[#BA92D9] ":
                    item.status === router.query["type"],
                  "border-b-[#ececec]": item.status !== router.query["type"],
                }
              )}
            >
              {item.label}
              <p
                className={clsx(
                  " cursor-pointer whitespace-nowrap text-[0.85rem]  scrollbar   text-center ",
                  {
                    "text-[#BA92D9] ": item.status === router.query["type"],
                  }
                )}
              >
                ( {item.counter?.[0]?.orderTotal} )
              </p>
            </span>
          ))}
        </div>
      </DragScroll>

      <InfiniteScroll
        dataLength={data?.fundingOrders?.length! || 0}
        next={handleFetchMore}
        hasMore={
          data?.fundingOrders?.length! ===
          data?.fundingOrderConnection.aggregate.countFundingOrder
            ? false
            : true
        }
        loader={<LoadingProduct />}
      >
        {data?.fundingOrders.length! > 0 ? (
          data?.fundingOrders.map((order, index) => {
            return (
              <div
                key={order.id}
                className={clsx("p-4 pb-0 border-b-8 border-b-[#F3F6FA]", {
                  "border-b-0": data?.fundingOrders.length - 1 === index,
                })}
              >
                <OrderCard order={order} />
              </div>
            );
          })
        ) : loading ? (
          <LoadingProduct />
        ) : (
          <div className="h-[400px] flex justify-center items-center">
            <div className="flex flex-col">
              <Image src={IMAGES.noOrder} alt="no-order" />
              <p className="text-[1rem] pt-4 text-[#8D8D8D]">
                Chưa có đơn hàng
              </p>
            </div>
          </div>
        )}
      </InfiniteScroll>
    </div>
  );
};

export default OrderOverview;

OrderOverview.getLayout = function (page) {
  return (
    <Layout>
      <Header></Header>
      {page}
    </Layout>
  );
};
