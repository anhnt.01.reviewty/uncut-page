import ShipmentHistory from "@/components/address/shipmentHistory/ShipmentHistory";
import ShipmentHistoryDetail from "@/components/address/shipmentHistory/ShipmentHistoryDetail";
import { setIsSpin } from "@/components/auth/authSlice";
import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import { clearOrder, fetchOrder } from "@/components/orders/oderSlice";
import useGetOrders from "@/hooks/useGetOrders";
import { NextPageWithLayout } from "@/pages/_app";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const ShipmentHistoryPage: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const orderSlice = useAppSelector((slice) => slice.orderSlice);
  const dataShipmentHistory = orderSlice.data?.fundingOrder.shipmentHistory;
  const router = useRouter();

  useEffect(() => {
    const getId = Number(router.query.id);

    if (!orderSlice.data && getId) {
      (async () => {
        dispatch(clearOrder({}));
        dispatch(setIsSpin(true));
        await dispatch(fetchOrder(getId));
        dispatch(setIsSpin(false));
      })();
    }
  }, [dispatch, orderSlice.data, router.query.id]);
  return (
    <>
      {dataShipmentHistory && orderSlice.data?.fundingOrder && (
        <ShipmentHistoryDetail
          data={orderSlice.data?.fundingOrder}
          limit={dataShipmentHistory.length}
          // onClick={() => {
          //   setLimit(limit !== items.length ? items.length : 1);
          // }}
          logs={dataShipmentHistory}
          // limit={limit}
        />
      )}
    </>
  );
};

export default ShipmentHistoryPage;

ShipmentHistoryPage.getLayout = function (page) {
  return (
    <>
      <Layout>
        <Header title={"Lịch sử giao hàng"} />
        {page}
      </Layout>
    </>
  );
};
