import IMAGES from "@/assets/img/images";
import { setIsSpin } from "@/components/auth/authSlice";
import ButtonComponent from "@/components/button";

import FundingProductCard from "@/components/funding-product/fundingProductCard";
import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import { resetOrderSlice } from "@/components/orders/oderSlice";
import {
  useCreateEvaluationMutation,
  useUpdateEvaluationMutation,
} from "@/graphql/reviewty-user/graphql";
import useGetOrders from "@/hooks/useGetOrders";
import { NextPageWithLayout } from "@/pages/_app";
import { useAppDispatch, useAppSelector } from "@/store/store";
import {
  Form,
  Image as ImageAntd,
  Input,
  message,
  Rate,
  Upload,
  UploadFile,
} from "antd";
import { RcFile, UploadProps } from "antd/es/upload";
import axios from "axios";
import clsx from "clsx";
import Image from "next/legacy/image";
import { useRouter } from "next/router";
import { ReactElement, useCallback, useEffect, useMemo, useState } from "react";
import { uuid } from "uuidv4";

enum EDesc {
  BAD = "Tệ!",
  NOT_GOOD = "Không tốt!",
  NORMAL = "Bình thường !",
  GOOD = "Tốt !",
  VERY_GOOD = "Rất tốt !",
}

export const desc = [
  EDesc.BAD,
  EDesc.NOT_GOOD,
  EDesc.NORMAL,
  EDesc.GOOD,
  EDesc.VERY_GOOD,
];

interface IUpload extends UploadFile {
  videoPreview?: string;
}
interface IRcFile extends RcFile {
  videoPreview?: string;
}

const uploadButton = (
  <div>
    <Image src={IMAGES.uploadImg} alt="upload-icon" />
  </div>
);

export const renderDescRating = (value: EDesc) => {
  switch (value) {
    case EDesc.BAD:
      return (
        <>
          <Image priority src={IMAGES.badIcon} alt="bad-icon" />
          <span className="pl-2 ant-rate-text text-[1rem] text-[#404040]">
            {EDesc.BAD}
          </span>
        </>
      );
    case EDesc.NOT_GOOD:
      return (
        <>
          <Image priority src={IMAGES.notGoodIcon} alt="bad-icon" />
          <span className="pl-2 ant-rate-text text-[1rem] text-[#404040]">
            {EDesc.NOT_GOOD}
          </span>
        </>
      );
    case EDesc.NORMAL:
      return (
        <>
          <Image priority src={IMAGES.normallyIcon} alt="bad-icon" />
          <span className="pl-2 ant-rate-text text-[1rem] text-[#404040]">
            {EDesc.NORMAL}
          </span>
        </>
      );
    case EDesc.GOOD:
      return (
        <>
          <Image priority src={IMAGES.goodIcon} alt="bad-icon" />
          <span className="pl-2 ant-rate-text text-[1rem] text-[#404040]">
            {EDesc.GOOD}
          </span>
        </>
      );
    case EDesc.VERY_GOOD:
      return (
        <>
          <Image priority src={IMAGES.veryGoodIcon} alt="bad-icon" />
          <span className="pl-2 ant-rate-text text-[1rem] text-[#404040]">
            {EDesc.VERY_GOOD}
          </span>
        </>
      );
  }
};

////////////////////////

const Review: NextPageWithLayout = () => {
  const router = useRouter();
  const { dataOrder } = useGetOrders();
  const [isLoad, setIsLoad] = useState(false);

  const orderSlice = useAppSelector((slice) => slice.orderSlice);
  const dataReview = useMemo(() => {
    return dataOrder?.orderEvaluations?.filter(
      (order) => order.id === Number(router.query.evaluationId)
    )[0];
  }, [dataOrder?.orderEvaluations, router.query.evaluationId]);
  // debugger;
  const dispatch = useAppDispatch();
  const [form] = Form.useForm();

  const [value, setValue] = useState(dataReview?.rate || 0);

  const [updateReview] = useUpdateEvaluationMutation();
  const [createReview] = useCreateEvaluationMutation();

  const getBase64 = async (file: RcFile): Promise<string> => {
    if (!file) {
      return "";
    }
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader?.readAsDataURL(file);
      reader.onload = () => resolve(reader.result as string);
      reader.onerror = (error) => reject(error);
    });
  };

  const handleUploadImages = async (fileListFilter: any, formData: any) => {
    if (fileListFilter.length === 0) {
      return [];
    }
    const modifiedUrl =
      process.env.NEXT_PUBLIC_REVIEWTY_USER_GRAPHQL_API_ENDPOINT;
    const splitUrl = modifiedUrl?.split("/graphql");
    return await axios.post<
      {
        name: string;
        url: string;
      }[],
      any
    >(`${splitUrl![0]}/admin-api/multiple-upload`, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  };

  const handleSubmitForm = async (values: any) => {
    if (value === 0 || !value) {
      return;
    }

    dispatch(setIsSpin(true));
    let files = [];
    let images:
      | {
          data: {
            name: string;
            url: string;
          }[];
        }
      | undefined = undefined;
    const formData = new FormData();
    if (values?.fileList?.fileList) {
      const fileListFilter = values.fileList.fileList?.filter(
        (item: UploadFile) => item?.originFileObj
      );
      files = values.fileList.fileList
        ?.filter((file: UploadFile) => !file.originFileObj)
        .map((d: UploadFile) => {
          return {
            name: d?.name,
            url: d?.url,
          };
        });
      fileListFilter.forEach((file: UploadFile) => {
        if (file?.originFileObj) {
          formData.append("file", file?.originFileObj);
        }
      });

      images = await handleUploadImages(fileListFilter, formData);
    }

    if (dataReview) {
      if (dataReview.updatedAt) {
        message.error("Chỉ có thể sửa bình luận được một lần");
        return;
      }

      try {
        const { data, errors } = await updateReview({
          variables: {
            data: {
              rate: value,
              comment: values.comment,
              images: [...files, ...(images?.data || [])] || [],
            },
            where: {
              id: dataReview.id,
            },
          },
        });

        if (errors) {
          message.error(errors[0].message);
        }

        if (data) {
          message.success("Sửa đánh giá thành công");
          router.back();
        }
      } catch (error: any) {
        message.error(error?.message);
      }
      dispatch(setIsSpin(false));

      return;
    }

    const { data, errors } = await createReview({
      variables: {
        data: {
          orderId: dataOrder?.id!,
          productId: dataOrder?.productId!,
          rate: value,
          comment: values.comment,
          images: [...files, ...(images?.data || [])] || [],
        },
      },
    });

    if (data?.createEvaluation) {
      message.success("Viêt đánh giá thành công");
      router.back();
    }
    dispatch(setIsSpin(false));
  };

  const [fileList, setFileList] = useState<IUpload[]>([]);

  const handleDeleteFile = (file: IUpload, fileDelete: IUpload[]) => {
    const newFileList = fileDelete.filter((items) => items.uid !== file.uid);
    setFileList(newFileList);
  };

  const customItemRender = useCallback(
    (originNode: ReactElement, file: IUpload, fileListt: IUpload[]) => {
      if (file.videoPreview) {
        return (
          <div className="relative">
            <div
              className="absolute cursor-pointer right-1  bg-[rgba(64,64,64,0.5)] rounded-full flex justify-center items-center p-2 z-20"
              onClick={() => handleDeleteFile(file, fileListt)}
            >
              <Image src={IMAGES.closeWhite} alt="close-white" />
            </div>
            <ImageAntd
              height={103}
              width={100}
              preview={{
                imageRender: () => (
                  <video muted width="100%" controls src={file.videoPreview} />
                ),
                toolbarRender: () => null,
              }}
              src={IMAGES.VideoDefault.src}
            />
          </div>
        );
      }

      // You can customize the rendering of each uploaded item here
      return (
        <div className="relative">
          <div
            className="absolute cursor-pointer right-1  bg-[rgba(64,64,64,0.5)] rounded-full flex justify-center items-center p-2 z-20"
            onClick={() => handleDeleteFile(file, fileListt)}
          >
            <Image src={IMAGES.closeWhite} alt="close-white" />
          </div>
          {/* <GalleryCard images={fileListt! as any} /> */}
          {file.thumbUrl && (
            <Image
              height={103}
              width={100}
              src={file.thumbUrl!}
              alt={file.name}
            />
          )}

          {file.url?.includes(".mp4") ? (
            <ImageAntd
              height={103}
              width={100}
              preview={{
                imageRender: () => (
                  <video muted width="100%" controls src={file.url} />
                ),
                toolbarRender: () => null,
              }}
              src={IMAGES.VideoDefault.src}
            />
          ) : (
            <ImageAntd
              height={103}
              width={100}
              src={file.url!}
              alt={file.name}
            />
          )}
        </div>
      );
    },
    []
  );
  const handleChange: UploadProps["onChange"] = async ({
    fileList: newFileList,
    file,
    ...props
  }) => {
    const getTotalSizeFile = newFileList.reduce((accumulator, currentValue) => {
      return accumulator + currentValue.size!;
    }, 0);
    const isSmt5M = getTotalSizeFile! / 1024 / 1024 > 5;
    if (isSmt5M) {
      message.error("Tổng dung lượng ảnh tải lên không vượt quá 5Mb");
      return;
    }

    if (
      !file.thumbUrl &&
      (file.type === "video/mp4" || file.type === "video/quicktime")
    ) {
      const searchIndex = newFileList.findIndex(
        (item) => item.uid === file.uid
      );
      (newFileList[searchIndex] as any).videoPreview = await getBase64(
        file.originFileObj!
      );
    }

    return setFileList(newFileList);
  };

  useEffect(() => {
    if (dataReview) {
      form.setFieldsValue({
        rate: dataReview?.rate,
        comment: dataReview?.comment,
        fileList: {
          fileList: dataReview?.images,
        },
      });
      const translateData = dataReview.images?.map((img) => {
        return {
          url: img.url,
          name: img.name,
          uid: uuid(),
          status: "done",
        };
      });
      setValue(dataReview.rate!);
      setFileList(translateData! as any);
    }
    setIsLoad(true);

    return () => {
      // dispatch(resetOrderSlice());
    };
  }, [
    dataReview,
    fileList.length,
    form,
    orderSlice,
    router.query.evaluationId,
    value,
  ]);

  return (
    <div className="p-4 bg-white">
      {orderSlice.data && (
        <FundingProductCard isHidePrice={false} data={orderSlice.data} />
      )}
      <Form form={form} layout="vertical" onFinish={handleSubmitForm}>
        <Form.Item name="rate" className="pb-2">
          <div className="flex flex-col justify-center items-center">
            <p className="pt-6 font-medium text-[1rem] text-[#404040]">
              Đánh giá sản phẩm
            </p>
            <div className="py-3">
              <Rate
                disabled={dataReview?.updatedAt}
                className={clsx("text-[2.25rem] text-[#F7B500] ", {
                  "opacity-0": !isLoad,
                })}
                // tooltips={desac}
                value={value}
                onChange={setValue}
              />
            </div>
            {value ? (
              <div className="flex items-center justify-center">
                {renderDescRating(desc[value - 1])}
              </div>
            ) : (
              ""
            )}
          </div>
        </Form.Item>
        <Form.Item
          name="comment"
          label={
            <span className="font-medium text-[1rem] text-[#404040]">
              Cảm nhận của bạn về sản phẩm
            </span>
          }
        >
          <Input.TextArea
            className="bg-[#f7f7f7] text-[1rem] text-[#404040]"
            rows={5}
            placeholder="Nêu cảm nhận của bạn về sản phẩm, Tối đa 200 kí tự"
            maxLength={200}
          />
        </Form.Item>

        <Form.Item
          name="fileList"
          label={
            <span className="font-medium text-[1rem] text-[#404040]">
              Một vài hình ảnh của sản phẩm
            </span>
          }
        >
          <Upload
            // beforeUpload={handleBeforeUpload}
            multiple
            accept="*"
            listType="picture-card"
            fileList={fileList}
            onChange={handleChange}
            itemRender={customItemRender}
          >
            <>{fileList?.length >= 8 ? null : uploadButton}</>
          </Upload>
        </Form.Item>
        <ButtonComponent
          disabled={dataReview?.updatedAt || value === 0}
          // disabled={true}
          htmlType="submit"
          type="primary"
          className={clsx("w-full py-3 ", {
            "bg-[#BA92D9]": !dataReview?.updatedAt,
            "bg-[#C1C1C1]": dataReview?.updatedAt || value === 0,
          })}
        >
          {" "}
          {dataReview?.updatedAt || dataReview?.id
            ? "Sửa đánh giá"
            : "Hoàn thành đánh giá"}
        </ButtonComponent>
      </Form>
    </div>
  );
};

export default Review;

Review.getLayout = function (page) {
  return (
    <Layout>
      <Header title="Đánh giá" />
      {page}
    </Layout>
  );
};
