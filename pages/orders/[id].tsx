import IMAGES from "@/assets/img/images";
import toCurrency from "@/common/toCurrency";
import AddressCard from "@/components/address/addressCard";
import ShipmentHistoryDetail from "@/components/address/shipmentHistory/ShipmentHistoryDetail";
import { selectAddresses, setIsSpin } from "@/components/auth/authSlice";
import FundingProductCard from "@/components/funding-product/fundingProductCard";
import Header from "@/components/Header";
import Layout from "@/components/layout";
import RenderButtonOrderDetail from "@/components/orders/button-order";
import { clearOrder, fetchOrder } from "@/components/orders/oderSlice";
import StatusOrder from "@/components/orders/status-order";
import {
  FundingOrderStatus,
  useGetOneFundingOrderLazyQuery,
  usePayBackFundingOrderMutation,
} from "@/graphql/reviewty-user/graphql";
import { REVIEWTY_CLIENT } from "@/hooks/useApollo";
import useTrans from "@/hooks/useTrans";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { formatDate } from "@/ultis/ultis";
import { message, Skeleton } from "antd";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { NextPageWithLayout } from "../_app";

const OrderDetail: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const orderSlice = useAppSelector((slice) => slice.orderSlice);

  const router = useRouter();
  const trans = useTrans();
  const { data } = orderSlice;

  useEffect(() => {
    const getId = Number(router.query.id);
    if (getId && getId !== orderSlice.data?.fundingOrder.id) {
      (async () => {
        dispatch(clearOrder({}));
        dispatch(setIsSpin(true));
        await dispatch(fetchOrder(getId));
        dispatch(setIsSpin(false));
      })();
    }
  }, [dispatch, orderSlice.data?.fundingOrder.id, router.query.id]);

  const handleClick = () => {
    router.push({
      pathname: "/orders/[id]/shipment-history",
      query: {
        id: data?.fundingOrder.id,
      },
    });
  };

  return (
    <div>
      {data?.fundingOrder.createdAt ? (
        <StatusOrder data={data} />
      ) : (
        <Skeleton className="m-4" active paragraph={{ rows: 2 }} />
      )}

      {data?.fundingOrder &&
        data?.fundingOrder?.shipmentHistory?.length! > 0 && (
          <ShipmentHistoryDetail
            data={data?.fundingOrder}
            limit={1}
            onClick={handleClick}
            logs={data?.fundingOrder.shipmentHistory!}
            // limit={limit}
          />
        )}

      <AddressCard data={data?.fundingOrder!} />

      <div className="m-4">
        {/* <div className="pt-4">{data && <ShipmentHis data={data} />}</div> */}

        <label className="font-medium text-[1.125rem]">
          {trans.common.product}
        </label>
        <div className="pt-4">{data && <FundingProductCard data={data} />}</div>
      </div>
      <div className="p-4 border-y-[#F4F5F9] border-y-8">
        <label className="font-medium text-[1.25rem]">
          {trans.common.deliveryMethod}
        </label>

        <div className="pt-4">
          {data?.fundingOrder.deliveryFee ? (
            <div className="flex justify-between">
              <label className="text-[#222] text-[1rem]">
                Giao hàng tiết kiệm{" "}
              </label>
              <span className="text-[#222] text-[1rem] font-medium">
                {toCurrency(data?.fundingOrder.deliveryFee)}
              </span>
            </div>
          ) : (
            <Skeleton
              title={false}
              className="w-full"
              active
              paragraph={{ rows: 1 }}
            />
          )}
        </div>
      </div>

      <div className="p-4">
        <label className="font-medium text-[1.25rem] ">
          {trans.common.paymentMethod}
        </label>
        <div className="pt-4">
          <p className="text-[#222] text-[1rem]">
            {data?.fundingOrder.paymentMethod}
          </p>
        </div>
      </div>

      <div className="p-4 pt-0 border-b-[#F4F5F9] border-b-8">
        <div>
          <div className="py-4  mb-4 border-y-2 border-y-[#ECECEC]">
            <div className="flex py-2 justify-between">
              <label className="text-[#8D8D8D] text-[1rem]">
                {trans.common.provisionalFee}
              </label>
              <span className="text-[#404040]">
                {!data?.fundingOrder.totalAmount ? (
                  <Skeleton title={false} paragraph={{ rows: 1 }} />
                ) : (
                  toCurrency(
                    data?.fundingOrder.discount! * data?.fundingOrder.quantity!
                  )
                )}
              </span>
            </div>
            {data?.fundingOrder.deliveryFee ? (
              <div className="flex py-2 justify-between">
                <label className="text-[#8D8D8D] text-[1rem]">
                  {trans.common.shippingFee}
                </label>
                <span className="text-[#222] text-[1rem]">
                  {toCurrency(data?.fundingOrder.deliveryFee!)}
                </span>
              </div>
            ) : (
              <Skeleton
                active
                className="py-2"
                title={false}
                paragraph={{ rows: 1 }}
              />
            )}
          </div>

          {data?.fundingOrder.deliveryFee ? (
            <div className="flex justify-between">
              <label className="text-[#222] text-[1.125rem] ">
                {trans.common.totalMoney}
              </label>
              <span className="text-[#222] text-[1.125rem] font-semibold">
                {toCurrency(data?.fundingOrder.totalAmount!)}
              </span>
            </div>
          ) : (
            <Skeleton
              active
              className="py-2"
              title={false}
              paragraph={{ rows: 1 }}
            />
          )}
        </div>
      </div>

      <div className="p-4 pt-0 pb-[25%]">
        {data?.fundingOrder.transportId && (
          <div className="flex py-2 justify-between">
            <label className="text-[#222] text-[1rem]">
              {trans.common.orderId}
            </label>
            <span className="text-[#404040] text-[1rem] text-right flex">
              {data.fundingOrder.transportId}
              <Image
                onClick={() => {
                  navigator?.clipboard?.writeText(
                    `${data.fundingOrder.transportId}`
                  );

                  message.success({
                    content: trans.message.copyToClipboard,
                  });
                }}
                className="ml-1 cursor-pointer"
                src={IMAGES.copyIcon}
                alt="copy-icon"
              />
            </span>
          </div>
        )}
        {data?.fundingOrder.createdAt ? (
          <div className="flex pt-4 justify-between">
            <label className="text-[#8D8D8D]">
              {trans.common.orderCreatedAt}
            </label>
            <span className="text-[#8D8D8D]">
              {formatDate(data.fundingOrder.createdAt)}
            </span>
          </div>
        ) : (
          <Skeleton
            active
            className="py-2"
            title={false}
            paragraph={{ rows: 1 }}
          />
        )}
      </div>
      <div className="flex">
        <div className="fixed max-w-[600px] left-1/2 -translate-x-1/2 z-[1] shadow-[0_5px_25px_rgba(0,0,0,0.35)] md:shadow-none  px-4  bottom-0 w-full flex items-center bg-white py-2 ">
          <div className="grid grid-cols-12 w-full">
            {data?.fundingOrder.status && (
              <RenderButtonOrderDetail data={data} />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default OrderDetail;

OrderDetail.getLayout = function (page) {
  return (
    <Layout>
      <Header title="Thông tin đơn hàng" />
      {page}
    </Layout>
  );
};
