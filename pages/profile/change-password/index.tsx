import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import { NextPageWithLayout } from "@/pages/_app";

const ChangePassword: NextPageWithLayout = () => {
  return <div>change</div>;
};

export default ChangePassword;

ChangePassword.getLayout = function (page) {
  return (
    <Layout>
      <Header title="Đổi mật khẩu"></Header>
      {page}
    </Layout>
  );
};
