import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import { useRouter } from "next/router";
import { useEffect } from "react";
import PickAddress from "../pick-address";
import { NextPageWithLayout } from "../../_app";

const EditAddressOverview: NextPageWithLayout = () => {
  const router = useRouter();

  return <PickAddress id={Number(router.query.id)}></PickAddress>;
};

export default EditAddressOverview;

EditAddressOverview.getLayout = function (page) {
  return (
    <Layout>
      <Header title="Chỉnh sửa địa chỉ" />
      {page}
    </Layout>
  );
};
