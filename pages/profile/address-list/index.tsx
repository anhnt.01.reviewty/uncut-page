import AddressOverview from "@/components/address/addressOverview";
import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import useTrans from "@/hooks/useTrans";
import { useRouter } from "next/router";
import { NextPageWithLayout } from "../../_app";

const AddressList: NextPageWithLayout = () => {
  const router = useRouter();

  return (
    <div>
      <AddressOverview type="operation" />
    </div>
  );
};

export default AddressList;

AddressList.getLayout = function (page) {
  return (
    <Layout>
      <Header title="Địa chỉ của tôi" />
      {page}
    </Layout>
  );
};
