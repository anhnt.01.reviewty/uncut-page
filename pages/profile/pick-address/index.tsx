import {
  handleCheckAuth,
  IS_MOBILE,
  REGEX_PHONE_NUMBER,
} from "@/common/contains";
import { fetchAddresses } from "@/components/auth/authSlice";
import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import { clearAction, EAction } from "@/components/main/mainSlice";
import {
  useCreateShippingAddressMutation,
  useGetDistrictsLazyQuery,
  useGetProvincesLazyQuery,
  useGetWardsLazyQuery,
  useShippingAddressLazyQuery,
  useUpdateShippingAddressMutation,
} from "@/graphql/reviewty-user/graphql";
import { useActiveCustomer } from "@/hooks/useActiveCustomer";
import { REVIEWTY_CLIENT } from "@/hooks/useApollo";
import { useGetAddress } from "@/hooks/useGetAddress";
import useTrans from "@/hooks/useTrans";
import { NextPageWithLayout } from "@/pages/_app";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { Checkbox, Form, Input, message, Select } from "antd";
import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";

export interface IProvinces {
  code: number;
  codename: string;
  districts: IDistricts[];
  division_type: string;
  name: string;
}

export interface IDistricts {
  code: number;
  codename: string;
  wards: IWards[];
  division_type: string;
  name: string;
}

export interface IWards {
  code: number;
  codename: string;
  district_code: number;
  division_type: string;
  name: string;
}

export interface IAddress {
  provinces: ISelectOptions[];
  districts: ISelectOptionsDistricts[];
  wards: ISelectOptionsDistricts[];
}
export interface ISelectOptions {
  value: number;
  label: string;
}
export interface ISelectOptionsDistricts {
  value: string;
  label: string;
}

export enum ENameSelect {
  province = "province",
  district = "district",
  ward = "ward",
}

interface IPickAddress {
  id?: number;
}

const PickAddress: NextPageWithLayout<IPickAddress> = ({
  id,
}: IPickAddress) => {
  const ref = useRef();
  const mainSlice = useAppSelector((slice) => slice.mainSlice);
  const dispatch = useAppDispatch();
  const addressList = useGetAddress();
  const trans = useTrans();
  const router = useRouter();
  const [form] = Form.useForm();
  const { data: dataUser } = useActiveCustomer();

  const [address, setAddress] = useState<IAddress>({
    provinces: [],
    districts: [],
    wards: [],
  });

  const [getProvinces] = useGetProvincesLazyQuery();
  const [getDistricts] = useGetDistrictsLazyQuery();
  const [getwards] = useGetWardsLazyQuery();
  const [createShippingAddress] = useCreateShippingAddressMutation({
    context: {
      clientName: REVIEWTY_CLIENT,
    },
  });
  const [updateShippingAddress] = useUpdateShippingAddressMutation({
    context: {
      clientName: REVIEWTY_CLIENT,
    },
  });

  const [getAddress] = useShippingAddressLazyQuery({
    context: {
      clientName: REVIEWTY_CLIENT,
    },
  });
  const handleBackTwoTimes = () => {
    if (mainSlice.action === EAction.pickAddressBeforeJoinEvent) {
      dispatch(clearAction({}));
      router.back();
      // router.back();
      return;
    }
  };

  const handleSubmitForm = async (values: {
    fullName: string;
    address: string;
    province: string;
    district: string;
    ward: string;
  }) => {
    if (!dataUser?.id) {
      handleCheckAuth();
      return;
    }

    if (id) {
      const { data, ...errors } = await updateShippingAddress({
        variables: {
          data: {
            ...values,
            address: values["address"],
            country: "Việt Nam",
          },
          where: {
            id: id,
          },
        },
      });

      if (data?.updateShippingAddress.__typename === "ShippingAddress") {
        message.success("Cập nhật địa chỉ thành công");
        dispatch(fetchAddresses());
      }

      await dispatch(fetchAddresses());
      handleBackTwoTimes();
      router.back();

      return;
    }
    const { data } = await createShippingAddress({
      variables: {
        data: {
          ...values,
          country: "Việt Nam",
        },
      },
    });
    if (data?.createShippingAddress.__typename === "ShippingAddress") {
      message.success("Lưu địa chỉ thành công");
      dispatch(fetchAddresses());

      if (router.query.fundingId) {
        router.push(
          {
            pathname: "/checkout/[id]",
            query: {
              id: router.query.fundingId,
            },
          },
          undefined,
          {
            shallow: true,
          }
        );

        return;
      }
      handleBackTwoTimes();
      router.back();
      return;
    }
  };

  useEffect(() => {
    (async () => {
      const { data } = await getProvinces();

      if (data?.provinces) {
        const translateData = data?.provinces.map((provinces) => {
          return {
            value: provinces.id,
            label: provinces.name,
          };
        });
        setAddress((prev) => ({ ...prev, provinces: translateData }));
      }
      if (id) {
        const { data } = await getAddress({
          variables: {
            where: {
              id: id,
            },
          },
          fetchPolicy: "no-cache",
        });

        if (data?.shippingAddress.__typename === "ShippingAddress") {
          form.setFieldsValue({
            ...data.shippingAddress,
          });
        }
      }
    })();

    return () => {};
  }, [dispatch, form, getAddress, getProvinces, id]);

  const onChangeSelect = async (
    value: number,
    options: any,
    name: ENameSelect | string
  ) => {
    if (name === ENameSelect.province) {
      const { data } = await getDistricts({
        variables: {
          where: {
            provinceId: value,
          },
        },
      });
      form.setFieldsValue({
        district: "",
        ward: "",
      });
      if (data?.districts) {
        const translateData = data?.districts.map((district) => {
          return {
            value: district.code,
            label: district.name,
          };
        });

        setAddress({ ...address, districts: translateData, wards: [] });
      }
    } else if (name === ENameSelect.district) {
      const { data } = await getwards({
        variables: {
          where: {
            districtCode: value as any as string,
          },
        },
      });
      if (data?.wards) {
        const translateData = data?.wards.map((ward) => {
          return {
            value: ward.ward_code,
            label: ward.ward_name,
          };
        });
        setAddress({ ...address, wards: translateData });
      }
    }

    form.setFieldsValue({
      [name]: options.label,
    });
  };
  return (
    <div className="container grid grid-cols-12 m-auto">
      <Form
        className="col-span-12 p-4"
        form={form}
        onFinish={handleSubmitForm}
        layout="vertical"
      >
        <Form.Item
          name="province"
          label={trans.address.province}
          rules={[{ required: true, message: trans.input.errors.required }]}
        >
          <Select
            placeholder={trans.address.placeholder.nameProvince}
            // optionFilterProp="children"
            // filterOption={(input, option) =>
            //   (option?.label.toLowerCase() ?? "").includes(input.toLowerCase())
            // }
            filterSort={(optionA, optionB) =>
              (optionA?.label ?? "")
                .toLowerCase()
                .localeCompare((optionB?.label ?? "").toLowerCase())
            }
            options={address.provinces}
            onChange={(value: number, options: any) =>
              onChangeSelect(value, options, ENameSelect.province)
            }
          />
        </Form.Item>

        <Form.Item
          name="district"
          label={trans.address.district}
          rules={[{ required: true, message: trans.input.errors.required }]}
        >
          <Select
            // allowClear
            disabled={address.districts.length <= 0}
            // showSearch
            placeholder={trans.address.placeholder.nameDistrict}
            // filterOption={(input, option) =>
            //   (option?.label.toLowerCase() ?? "").includes(input.toLowerCase())
            // }
            filterSort={(optionA, optionB) =>
              (optionA?.label ?? "")
                .toLowerCase()
                .localeCompare((optionB?.label ?? "").toLowerCase())
            }
            options={address.districts}
            onChange={(value: number, options: any) =>
              onChangeSelect(value, options, ENameSelect.district)
            }
          />
        </Form.Item>
        <Form.Item
          name="ward"
          label={trans.address.ward}
          rules={[{ required: true, message: trans.input.errors.required }]}
        >
          <Select
            disabled={address.wards.length <= 0}
            placeholder={trans.address.placeholder.nameWard}
            filterSort={(optionA, optionB) =>
              (optionA?.label ?? "")
                .toLowerCase()
                .localeCompare((optionB?.label ?? "").toLowerCase())
            }
            options={address.wards}
            onChange={(value: number, options: any) =>
              onChangeSelect(value, options, ENameSelect.ward)
            }
          />
        </Form.Item>

        <Form.Item
          rules={[
            {
              required: true,
              message: trans.input.errors.minMaxCharacter,
              min: 1,
              max: 100,
            },
          ]}
          name="address"
          label={trans.address.specificAddress}
        >
          <Input placeholder={trans.address.specificAddress} />
        </Form.Item>
        <Form.Item
          rules={[{ required: true, message: trans.input.errors.required }]}
          name="fullName"
          label={trans.address.fullName}
        >
          <Input type="text" placeholder={trans.address.fullName} />
        </Form.Item>
        <Form.Item
          rules={[
            {
              required: true,
              message: trans.input.errors.required,
            },
            {
              pattern: REGEX_PHONE_NUMBER,
              message: trans.input.errors.phoneNumberInvalid,
            },
          ]}
          name="phoneNumber"
          label={trans.address.phoneNumber}
        >
          <Input
            inputMode="numeric"
            type="number"
            placeholder={trans.address.phoneNumber}
          />
        </Form.Item>

        <Form.Item name="default" valuePropName="checked">
          <Checkbox disabled={id ? addressList.data.length <= 1 : false}>
            {trans.address.setIsDefaultAddress}
          </Checkbox>
        </Form.Item>

        <div className="flex">
          <Form.Item>
            <button
              onClick={() => {
                router.back();
              }}
              className="border border-[#1D1D1B] text-[#404040] py-3 px-12 rounded-md mr-2"
              type="button"
            >
              {trans.common.cancel}
            </button>
          </Form.Item>
          <Form.Item>
            <button
              className="border text-white border-[#1D1D1B] py-3 px-12 rounded-md bg-[#1D1D1B]"
              type="submit"
            >
              {id ? trans.common.update : trans.common.save}
            </button>
          </Form.Item>
        </div>
      </Form>
    </div>
  );
};

export default PickAddress;

PickAddress.getLayout = function (page) {
  return (
    <Layout>
      <Header title="Thêm mới địa chỉ" />
      {page}
    </Layout>
  );
};
