import React, { useMemo, useState } from "react";

import DynamicOGImage from "@/components/dynamic-og-image/DynamicOGImage";
import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import { useGetFundingDetail } from "@/hooks/useGetFundingDetail";
import useTrans from "@/hooks/useTrans";
import { useViewGGAnalysis } from "@/hooks/useViewGGANL";
import { NextPageWithLayout } from "@/pages/_app";
import clsx from "clsx";
import { useRouter } from "next/router";
import "swiper/css";
import "swiper/css/pagination";
import { SlugProduct } from "../../../components/slug-product/SlugProduct";

export enum Tab {
  Product = "PRODUCT",
  Information = "INFORMATION",
  Policy = "POLICY",
  Comments = "COMMENTS",
}

interface IDataSSG {
  data: any;
}

const FundingDetails: NextPageWithLayout<IDataSSG> = ({ data }) => {
  useViewGGAnalysis({
    eventName: "user_view_funding_detail",
    eventCategory: "FUNDING_DETAIL",
    eventLabel: "VIEW",
  });
  const { dataProduct, loadingProduct } = useGetFundingDetail();

  const t = useTrans();
  const router = useRouter();

  const [tabKey, setTabKey] = useState<Tab | undefined>(Tab.Product);
  const barItems = useMemo(() => {
    return [
      {
        id: "product",
        label: `${t.home.common.product}`,
        status: Tab.Product,
      },
      {
        id: "information",
        label: `${t.home.common.information}`,
        status: Tab.Information,
      },

      {
        id: "comments",
        label: `${t.home.common.comment}`,
        status: Tab.Comments,
      },
    ];
  }, [t.home.common.comment, t.home.common.information, t.home.common.product]);

  const handleClickBarITems = (status?: Tab, id?: string) => {
    document
      .querySelector(`.${id}-md`)
      ?.scrollIntoView({ behavior: "smooth", block: "start" });

    setTabKey(status);
    return;
  };

  return (
    <React.Fragment>
      {dataProduct && (
        <DynamicOGImage
          data={{
            url: dataProduct?.fundingProduct.product?.thumbnail?.url!,
            title: dataProduct?.fundingProduct.funding.shortDescription!,
          }}
        />
      )}
      <div className="max-w-screen-xl relative m-auto h-[100%]">
        <div className="grid grid-cols-12 sticky top-0 z-[2] bg-white">
          {barItems.map((item, index) => (
            <span
              key={item.id}
              onClick={() => {
                handleClickBarITems(item.status, item.id);
              }}
              className={clsx(
                "col-span-4 p-2 pt-1 text-center text-[0.875rem] leading-8 cursor-pointer  text-[#8d8d8d] border-b-2 ",
                {
                  "text-[black] border-b-[black] font-medium":
                    item.status === tabKey,
                }
              )}
            >
              {item.label}
            </span>
          ))}
        </div>

        <SlugProduct
          dataProduct={dataProduct!}
          loading={loadingProduct}
        ></SlugProduct>
      </div>
    </React.Fragment>
  );
};

export default FundingDetails;

FundingDetails.getLayout = function (page) {
  return (
    <Layout>
      <Header></Header>
      {page}
    </Layout>
  );
};
