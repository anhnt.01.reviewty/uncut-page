import { BackIcon, HomeIcon } from "@/assets/icon/icons";
import CardProduct from "@/components/card-product";
import Layout from "@/components/layout/layout";
import {
  FundingDetailQuery,
  useFundingDetailLazyQuery,
  useFundingDetailQuery,
} from "@/graphql/reviewty/graphql";
import useTrans from "@/hooks/useTrans";
import { useViewGGAnalysis } from "@/hooks/useViewGGANL";
import { useAppSelector } from "@/store/store";
import { formatDate } from "@/ultis/ultis";
import { DefaultSeo } from "next-seo";
import Image from "next/legacy/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { NextPageWithLayout } from "../_app";

interface IProductDetail {
  data: FundingDetailQuery;
}

const ProductDetail: NextPageWithLayout<IProductDetail> = ({
  data,
}: IProductDetail) => {
  // const disabledBtn =
  //   new Date().getTime() <
  //   new Date(dataProduct?.fundingProduct.funding.startDate).getTime();
  const router = useRouter();
  const idQuery = Number(router.query.id);
  const trans = useTrans();
  const mainSlice = useAppSelector((slice) => slice.mainSlice);
  const [productDetail, setProductDetail] =
    useState<FundingDetailQuery["funding"]>();

  useViewGGAnalysis({
    eventName: "user_view_funding_detail",
    eventCategory: "FUNDING_DETAIL",
    eventLabel: "VIEW",
  });

  const [getDetails, { data: dataDetails }] = useFundingDetailLazyQuery();

  useEffect(() => {
    if (mainSlice.selectedItem) {
      setProductDetail(mainSlice.selectedItem);
    } else {
      if (idQuery) {
        getDetails({
          variables: {
            where: {
              id: idQuery,
            },
          },
          onCompleted(data) {
            setProductDetail(data.funding);
          },
        });
      }
    }
  }, [getDetails, idQuery, mainSlice.selectedItem]);

  return (
    <React.Fragment>
      {productDetail && (
        <div className="container m-auto overflow-hidden ">
          <DefaultSeo
            title={productDetail?.shortDescription!}
            openGraph={{
              images: [
                {
                  url: JSON.parse(productDetail?.coverUrl!)?.[0]?.url!,
                  alt: "img",
                },
              ],
            }}
          />
          <div className="flex p-4 justify-between font-medium items-center">
            <div
              className="cursor-pointer"
              onClick={() => {
                router.back();
              }}
            >
              <BackIcon />
            </div>

            <div className="truncate max-w-[60%] font-medium">
              {productDetail?.shortDescription}
            </div>
            <Link href="/" className="cursor-pointer">
              <HomeIcon />
            </Link>
          </div>

          <div className="w-full  m-auto bg-[#e4e3e3]">
            {productDetail.coverUrl &&
            JSON.parse(productDetail.coverUrl)?.[0]?.url.includes("mp4") ? (
              <video
                autoPlay
                // playsInline
                // controls={true}
                muted
                loop
                className="w-full h-auto block "
              >
                <source src={JSON.parse(productDetail.coverUrl)?.[0]?.url!} />
              </video>
            ) : (
              <Image
                priority
                src={JSON.parse(productDetail.coverUrl!)?.[0]?.url}
                width="0"
                height="0"
                sizes="100vw"
                className="w-full h-auto block "
                alt="img"
                layout="responsive"
              />
            )}

            <div className="flex bg-[white] flex-col  m-auto justify-center items-start p-4  w-full">
              <h3 className="border-b-[#ececec] w-full border-b-2  pt-0 text-[1.25rem] font-medium pl-4 pb-3 bg-white">
                {trans.fundingDetail.productInformation}
              </h3>

              <ul className="pl-4 pt-4 " style={{ listStyleType: "disc" }}>
                <li className="text-[#222222] text-[1rem]">
                  {trans.fundingDetail.participationTime}
                </li>
                <p className="text-[#747474] pb-4  text-[1rem]">
                  {formatDate(productDetail?.startDate)} ~{" "}
                  {formatDate(productDetail?.endDate)}
                </p>
              </ul>
            </div>
            <div
              className="w-full  m-auto pb-4 bg-[#e4e3e3]"
              dangerouslySetInnerHTML={{
                __html: productDetail?.longDescription || "",
              }}
            ></div>
          </div>
          <div className="grid p-4 text-[1.125rem] font-medium">
            {trans.fundingDetail.relatedProducts}
          </div>
          <div className="container p-4 pb-12 m-auto grid gap-2  pb-12 grid-cols-12">
            <CardProduct data={productDetail} />
          </div>
        </div>
      )}
    </React.Fragment>
  );
};

ProductDetail.getLayout = function (page) {
  return <Layout>{page}</Layout>;
};
export default ProductDetail;
