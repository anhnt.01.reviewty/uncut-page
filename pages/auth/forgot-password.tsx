import IMAGES from "@/assets/img/images";
import { handleCheckAuth, IS_MOBILE } from "@/common/contains";
import { useForgetPasswordMutation } from "@/graphql/reviewty-user/graphql";
import { REVIEWTY_CLIENT } from "@/hooks/useApollo";
import useTrans from "@/hooks/useTrans";
import { useAppDispatch } from "@/store/store";
import { Form, Input, message } from "antd";
import { useRouter } from "next/router";
import { useState } from "react";

const ForgotPassword = () => {
  const router = useRouter();
  const [form] = Form.useForm();
  const trans = useTrans();

  const [fogetPassword] = useForgetPasswordMutation();

  const handlesubmitForm = async (values: { [key: string]: string }) => {
    if (values) {
      try {
        const { data } = await fogetPassword({
          context: {
            clientName: REVIEWTY_CLIENT,
          },
          variables: {
            email: values.email,
          },
        });

        if (data) {
          message.success({
            content: "Mật khẩu của bạn đã được gửi tới email",
          });
        }
      } catch (error: any) {
        form.setFields([
          {
            name: "email",
            errors: ["Email không tồn tại"],
          },
        ]);
      }
    }
  };
  return (
    <div
      className=""
      style={{
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        height: "100vh",
      }}
    >
      <div className="overflow-y-auto container m-auto px-4  h-[100%]">
        {/* <Router> */}
        <div className="grid grid-cols-12 bg-white rounded-lg max-w-[500px] px-4 py-8 md:p-16 relative  left-1/2 -translate-x-1/2 justify-center items-center ">
          <div className="col-span-12">
            <div className="flex pb-6 justify-center items-center">
              {/* eslint-disable-next-line @next/next/no-img-element */}
              <img src={IMAGES.logoInline.src} alt="main-logo" />
            </div>
            <h3 className="text-[1.25rem] fomt-medium text-[#222] mb-6  text-center">
              {trans.forgotPassword.heading}
            </h3>

            <p className="text-center text-[#8D8D8D]  pb-16">
              {trans.forgotPassword.paragraph}
            </p>

            <Form form={form} onFinish={handlesubmitForm} layout="vertical">
              <Form.Item
                name="email"
                rules={[
                  {
                    type: "email",
                    required: true,
                    message: `${trans.input.errors.required} or ${trans.input.errors.formatEmailIncorrect}`,
                  },
                ]}
              >
                <Input
                  placeholder="Email*"
                  className="border ease-in-out duration-300 rounded border-[#DBE0E6] hover:border-[#8592ab] p-4 w-full "
                />
              </Form.Item>

              <div className="text-center">
                <Form.Item>
                  <button
                    type="submit"
                    className="w-full rounded-lg mb-6 px-8 py-2 text-white bg-[#BA92D9] text-[1.125rem] "
                  >
                    {trans.forgotPassword.resetPassword}
                  </button>
                  <div
                    onClick={handleCheckAuth}
                    className="text-center cursor-pointer w-full rounded-lg px-8 py-2 text-[#8D8D8D] bg-none mb-6"
                  >
                    {trans.forgotPassword.back}
                  </div>
                </Form.Item>
              </div>
            </Form>
          </div>
        </div>
      </div>
      <div className="bg-[#F7F7F7] w-full fixed bottom-0 p-4 text-center">
        © 2023 Reviewty Uncut
      </div>
    </div>
  );
};

export default ForgotPassword;
