import IMAGES from "@/assets/img/images";
import { handleCheckAuth, IS_MOBILE, REVIEWTY_TOKEN } from "@/common/contains";
import { fetchActiveCustomer } from "@/components/auth/authSlice";
import FacebookLogin from "@/components/facebook-login";
import Layout from "@/components/layout/layout";
import {
  useCheckUserExistsLazyQuery,
  useRegisterCustomerAccountMutation,
  useUserLoginMutation,
} from "@/graphql/reviewty-user/graphql";
import { REVIEWTY_CLIENT } from "@/hooks/useApollo";
import { NextPageWithLayout } from "@/pages/_app";
import { useAppDispatch } from "@/store/store";
import logoImg from "@public/logo-inline.png";
import { Form, Input } from "antd";
import Image from "next/image";
import { useRouter } from "next/router";

import clsx from "clsx";
import { useEffect, useState } from "react";
import styles from "./signup.module.scss";
import InputPassword from "@/components/input/input-password";

const Signup: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const [form] = Form.useForm();

  const [isOpen, setIsOpen] = useState(false);

  const [login] = useUserLoginMutation({
    context: {
      clientName: REVIEWTY_CLIENT,
    },
  });

  const [register] = useRegisterCustomerAccountMutation();

  const [checkExists] = useCheckUserExistsLazyQuery();

  useEffect(() => {}, []);

  const forgotPasswordHandler = () => {
    router.push("forgot-password");
  };

  const handlesubmitForm = async (values: { [key: string]: string }) => {
    if (values) {
      const { data: userData } = await checkExists({
        context: {
          clientName: REVIEWTY_CLIENT,
        },

        variables: {
          where: {
            email: values.email,
          },
        },
      });

      if (userData?.user?.id) {
        form.setFields([
          {
            name: "email",
            errors: ["Email này đã tồn tại"],
          },
        ]);
        return;
      }

      const { data: registerData } = await register({
        context: {
          clientName: REVIEWTY_CLIENT,
        },
        variables: {
          data: {
            email: values.email,
            password: values.password,
          },
        },
      });

      if (registerData && registerData?.signUp.__typename === "AuthPayload") {
        localStorage.setItem(REVIEWTY_TOKEN, registerData.signUp.token);
        dispatch(fetchActiveCustomer());
        router.back();
      }
    }
  };

  return (
    <div className="min-h-screen">
      <div className="container m-auto p-4 h-screen overflow-y-auto">
        <div className="grid grid-cols-12 bg-white rounded-lg max-w-[500px]  md:p-20 relative  left-1/2 -translate-x-1/2  justify-center items-center ">
          <div className="col-span-12">
            <div className="flex pb-6 justify-center items-center">
              {/* eslint-disable-next-line @next/next/no-img-element */}
              <img src={logoImg.src} alt="main-logo" />
            </div>

            <h3 className="text-[1.25rem] text-[#222] mb-8 font-medium text-center">
              Đăng ký
            </h3>

            <p className="text-[#8d8d8d] text-center mb-12">
              Trở thành thành viên của Reviewty Uncut
            </p>
            <Form
              form={form}
              onFinish={handlesubmitForm}
              layout="vertical"
              initialValues={{
                email: "",
                password: "",
                confirm: "",
              }}
            >
              <Form.Item
                label={<label className="text-[#8D8D8D]">Email</label>}
                name="email"
                rules={[
                  {
                    type: "email",
                    message: "Chưa đúng định dạng email",
                  },
                  {
                    required: true,
                    message: "Trường này là bắt buộc",
                  },
                ]}
              >
                <Input
                  placeholder="Email"
                  className="border ease-in-out duration-300 rounded border-[#ECECEC] hover:border-[#8592ab] p-2 px-4 w-full focus:border-[#222]"
                />
              </Form.Item>
              <Form.Item
                shouldUpdate
                label={<label className="text-[#8D8D8D]">Mật khẩu</label>}
                name="password"
                rules={[{ required: true, message: "Trường này là bắt buộc" }]}
              >
                <InputPassword />
              </Form.Item>
              <Form.Item
                label={
                  <label className="text-[#8D8D8D]">Xác nhận mật khẩu</label>
                }
                name="confirm"
                dependencies={["password"]}
                hasFeedback
                rules={[
                  { required: true, message: "Trường này là bắt buộc" },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue("password") === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(
                        new Error("Mật khẩu xác nhận không giống mật khẩu")
                      );
                    },
                  }),
                ]}
              >
                <InputPassword />
              </Form.Item>

              <div className="flex justify-end">
                <label
                  onClick={forgotPasswordHandler}
                  // href="/auth/forgot-password"
                  className="cursor-pointer text-[#005A53]"
                >
                  Quên mật khẩu ?
                </label>
              </div>

              <div className="text-center flex justify-between mt-2">
                <Form.Item className="w-full">
                  <button
                    type="submit"
                    className="w-full rounded-lg px-8 py-2 text-white bg-[#BA92D9] text-[1.125rem] "
                  >
                    Đăng ký
                  </button>
                </Form.Item>
              </div>
              <div className="text-center w-full rounded-lg px-8 py-2 text-[#8D8D8D] bg-none mb-6">
                Hoặc đăng ký với
              </div>

              <FacebookLogin />

              <div
                className={clsx(
                  "flex cursor-pointer my-9 justify-center text-center text-[#5E51D3] text-[1rem] "
                )}
                onClick={() => handleCheckAuth()}
              >
                Đăng nhập
              </div>
            </Form>
          </div>
        </div>
      </div>
      <div className="bg-[#F7F7F7] w-full fixed bottom-0 p-4 text-center">
        © 2023 Reviewty Uncut
      </div>
    </div>
  );
};

export default Signup;

Signup.getLayout = function (page) {
  return <Layout>{page}</Layout>;
};
