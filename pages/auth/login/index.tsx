import { REGEX_EMAIL, REVIEWTY_TOKEN } from "@/common/contains";
import { fetchActiveCustomer } from "@/components/auth/authSlice";
import FacebookLogin from "@/components/facebook-login";
import { useUserLoginMutation } from "@/graphql/reviewty-user/graphql";
import { REVIEWTY_CLIENT } from "@/hooks/useApollo";
import { NextPageWithLayout } from "@/pages/_app";
import { useAppDispatch } from "@/store/store";
import logoImg from "@public/logo-inline.png";
import { Form, Input, message } from "antd";
import { useRouter } from "next/router";

import InputPassword from "@/components/input/input-password";
import { useEffect, useState } from "react";
import Image from "next/legacy/image";
import IMAGES from "@/assets/img/images";
import { GetServerSideProps } from "next";
import useTrans from "@/hooks/useTrans";
import Link from "next/link";

const Login: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const [form] = Form.useForm();
  const trans = useTrans();

  const [login] = useUserLoginMutation();

  const forgotPasswordHandler = () => {
    router.push("forgot-password");
  };

  const handlesubmitForm = async (values: { [key: string]: string }) => {
    if (values) {
      const { data } = await login({
        variables: {
          email: values.email.trim(),
          password: values.password.trim(),
        },
      });

      if (data?.loginV2.__typename === "AuthError") {
        form.setFields([
          {
            name: "email",
            errors: [trans.input.errors.accountOrPasswordIncorrect],
          },
          {
            name: "password",
            errors: [trans.input.errors.accountOrPasswordIncorrect],
          },
        ]);

        message.open({
          type: "error",
          content: trans.input.errors.accountOrPasswordIncorrect,
        });

        return;
      }
      if (data?.loginV2.__typename === "AuthPayload") {
        localStorage.setItem(REVIEWTY_TOKEN, data?.loginV2.token);
        dispatch(fetchActiveCustomer());

        router.back();
      }
    }
  };

  useEffect(() => {
    localStorage.removeItem(REVIEWTY_TOKEN);
  }, []);

  return (
    <div className="min-h-[110vh]">
      <div className="container m-auto p-4 overflow-y-auto h-[110vh]">
        <div className="grid grid-cols-12 p-4 bg-white rounded-lg max-w-[500px]  md:p-20 relative  left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2 justify-center items-center ">
          <div className="col-span-12">
            <div className="flex pb-6 justify-center items-center cursor-pointer">
              <Link href="/">
                <Image
                  src={IMAGES.mainLogo}
                  width={160}
                  height={45}
                  alt="main-logo"
                />
              </Link>
            </div>

            <h3 className="text-[1.25rem] text-[#222] mb-2 font-medium text-center">
              {trans.common.signIn}
            </h3>

            <p className=" w-full text-[0.875rem] text-center   text-[#9b9b9b] pb-8">
              {trans.login.canSigninWithReviewty}
            </p>
            <Form form={form} onFinish={handlesubmitForm} layout="vertical">
              <Form.Item
                label={<label className="text-[#8D8D8D]">Email</label>}
                name="email"
                rules={[
                  {
                    validator(_, value) {
                      if (value.trim() <= 0) {
                        return Promise.reject(trans.input.errors.required);
                      }

                      // if (!REGEX_EMAIL.test(value.trim())) {
                      //   return Promise.reject(
                      //     trans.input.errors.formatEmailIncorrect
                      //   );
                      // }

                      return Promise.resolve();
                    },
                  },
                ]}
              >
                <Input
                  placeholder="Email"
                  className="border ease-in-out duration-300 rounded border-[#ECECEC] hover:border-[#8592ab] p-2 px-4 w-full focus:border-[#222]"
                />
              </Form.Item>

              <Form.Item
                label={
                  <label className="text-[#8D8D8D]">
                    {trans.common.password}
                  </label>
                }
                name="password"
                rules={[
                  {
                    validator(rule, value, callback) {
                      if (value.trim().length <= 0) {
                        return Promise.reject(trans.input.errors.required);
                      }
                      return Promise.resolve();
                    },
                  },
                ]}
              >
                <InputPassword />
              </Form.Item>

              {/* <div className="flex justify-end">
                <label
                  onClick={forgotPasswordHandler}
                  className="cursor-pointer text-[#005A53]"
                >
                  {trans.common.forgotPassword} ?
                </label>
              </div> */}

              <div className="text-center flex justify-between mt-2">
                <Form.Item className="w-full">
                  <button
                    type="submit"
                    className="w-full rounded-lg px-8 py-2 text-white bg-[#BA92D9] text-[1.125rem] "
                  >
                    {trans.common.signIn}
                  </button>
                </Form.Item>
              </div>
              <div className="text-center w-full rounded-lg px-8 py-2 text-[#8D8D8D] bg-none mb-6">
                {trans.login.orSigninWith}
              </div>

              <FacebookLogin />
            </Form>
          </div>
        </div>
      </div>
      <div className="bg-[#F7F7F7] w-full fixed bottom-0 p-4 text-center">
        © 2023 Reviewty Uncut
      </div>
    </div>
  );
};

export default Login;
