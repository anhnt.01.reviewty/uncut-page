import React, { useEffect, useState } from "react";
import crypto from "crypto";
import { useRouter } from "next/router";
import { REVIEWTY_TOKEN, sentEventLog } from "@/common/contains";
import Main from "@/components/main";
import { NextPageWithLayout } from "@/pages/_app";
import Layout from "@/components/layout/layout";
import Header from "@/components/Header";
import { clearMe, fetchActiveCustomer } from "@/components/auth/authSlice";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { useActiveCustomer } from "@/hooks/useActiveCustomer";
import { Spin } from "antd";
import CryptoJS from "crypto-js";

const RsaDecoder: NextPageWithLayout = () => {
  const dispatch = useAppDispatch();
  const router = useRouter();

  const activeCustomer = useAppSelector((slice) => slice.authSlice);
  const [decryptedMessage, setDecryptedMessage] = useState("");

  useEffect(() => {
    (async () => {
      localStorage.removeItem(REVIEWTY_TOKEN);
      dispatch(clearMe({}));

      if (process.env.NEXT_PUBLIC_KEY_DECODE && router.query.partner) {
        try {
          const decrypted = CryptoJS.AES.decrypt(
            (router.query.partner as string).replaceAll(" ", "+"),
            CryptoJS.enc.Utf8.parse(process.env.NEXT_PUBLIC_KEY_DECODE),
            {
              mode: CryptoJS.mode.ECB,
              padding: CryptoJS.pad.Pkcs7,
            }
          ).toString(CryptoJS.enc.Utf8);

          if (decrypted) {
            setDecryptedMessage(decrypted);
            localStorage.setItem(REVIEWTY_TOKEN, decrypted);
          } else {
            localStorage.removeItem(REVIEWTY_TOKEN);
          }
          if (router.query?.redirect_url) {
            await dispatch(fetchActiveCustomer());
            router.push(router.query?.redirect_url as string);
          } else {
            await dispatch(fetchActiveCustomer());
            router.push("/");
          }
        } catch (error) {
          sentEventLog({
            eventName: "Liwrvttk",
            type: "Errors",
            asPath: router.pathname,
            userId: activeCustomer.me?.id,
            errors: JSON.stringify(error),
            encrpyt: router.query?.partner as string,
          });
          setDecryptedMessage(error as any);
          localStorage.removeItem(REVIEWTY_TOKEN);
          router.push("/");
        }
      }
    })();
  }, [router, dispatch, router.query.partner, activeCustomer.me?.id]);

  return (
    <div className="h-[100vh] flex justify-center items-center">
      <Spin></Spin>
    </div>
  );
};

export default RsaDecoder;

RsaDecoder.getLayout = function (page) {
  return (
    <Layout>
      {/* <Header type="main" /> */}
      {page}
    </Layout>
  );
};

// export async function getServerSideProps() {
//   // Fetch data from external API
//   const res = await fetch(`https://.../data`);
//   const data = await res.json();

//   // Pass data to the page via props
//   return { props: { data } };
// }
