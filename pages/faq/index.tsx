import Collapse from "@/components/collapse";
import PageFive from "@/components/faq/page-five";
import PageFour from "@/components/faq/page-four";
import PageOne from "@/components/faq/page-one";
import PageThree from "@/components/faq/page-three";
import PageTwo from "@/components/faq/page-two";
import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import { NodeElement } from "rc-tree/lib/interface";
import { ComponentElement } from "react";
import { NextPageWithLayout } from "../_app";

const FAQPage: NextPageWithLayout = () => {
  const faq: {
    id: number;
    text: string;
    children: JSX.Element;
  }[] = [
    {
      id: 1,
      text: "Làm thế nào để tôi có thể mua hàng trên Uncut?",
      children: <PageOne />,
    },
    {
      id: 1,
      text: "Bắt buộc phải có ví VNPAY thì tôi mới mua hàng được?",
      children: <PageTwo />,
    },
    {
      id: 1,
      text: "Không có thẻ ngân hàng có thể mua hàng không?",
      children: <PageThree />,
    },
    {
      id: 1,
      text: "Mua hàng trên Uncut có được mua với hình thức COD không?",
      children: <PageFour />,
    },
    {
      id: 1,
      text: "Tôi có thể xem lịch sử trạng thái đơn hàng ở đâu",
      children: <PageFive />,
    },
  ];
  return (
    <div className="p-4">
      {faq.map((item, index) => {
        return (
          <Collapse
            className="border-b border-b-[#e5e5e5] w-full bg-white"
            title={`${index + 1}. ${item.text}`}
            key={index}
          >
            {item.children}
          </Collapse>
        );
      })}
    </div>
  );
};

export default FAQPage;

FAQPage.getLayout = function (page) {
  return (
    <Layout>
      <Header title="FAQ" />
      {page}
    </Layout>
  );
};
