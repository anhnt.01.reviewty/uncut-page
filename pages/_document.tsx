import Document, { Head, Html, Main, NextScript } from "next/document";
import Script from "next/script";
// import scriptTs from "@public/script";
class MyDocument extends Document {
  static async getInitialProps(ctx: any) {
    const originalRenderPage = ctx.renderPage;
    // Run the React rendering logic synchronously
    ctx.renderPage = () =>
      originalRenderPage({
        // Useful for wrapping the whole react tree
        enhanceApp: (App: any) => App,
        // Useful for wrapping in a per-page basis
        enhanceComponent: (Component: any) => Component,
      });

    // Run the parent `getInitialProps`, it now includes the custom `renderPage`
    const initialProps = await Document.getInitialProps(ctx);

    return initialProps;
  }

  render() {
    return (
      <Html>
        <Head>
          <script
            async
            src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS_ID}`}
          />
          <script
            id="google-analysis"
            dangerouslySetInnerHTML={{
              __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
          
            gtag('config', '${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS_ID}');
            `,
            }}
          ></script>

          {/* eslint-disable-next-line */}
          <script
            id="gg-analysis"
            dangerouslySetInnerHTML={{
              __html: `
            const url = "${process.env.NEXT_PUBLIC_REVIEWTY_USER_GRAPHQL_API_ENDPOINT}";

            async function confirmExit() {
              const data = {
                operationName: "uncutActivities",
                variables: {
                  data: {
                    name: "uncut-logs",
                    message: JSON.stringify({
                      date: new Date(),
                      "device-token": localStorage.getItem("uuidv4"),
                      status: "ondestroy",
                    }),
                  },
                },
                query:
                  "mutation uncutActivities($data: ActivityLogCreateInput!) { actitvityLogs(data: $data) }",
              };

              fetch(url, {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify(data),
              });
              var start = new Date().getTime();
              while (new Date().getTime() < start + 1000);
              return ""
            }
            

if (typeof window !== "undefined") {
  window.onunload = confirmExit;

  window.onfocus = function () {

    const data = {
      operationName: "uncutActivities",
      variables: {
        data: {
          name: "uncut-logs",
          message: JSON.stringify({
            date: new Date(),
            "device-token": localStorage.getItem("uuidv4"),
            status: "onfocus",
          }),
        },
      },
      query:
        "mutation uncutActivities($data: ActivityLogCreateInput!) { actitvityLogs(data: $data)}",
    }
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
  };

  window.onblur = function () {
    const data =  {
      operationName: "uncutActivities",
      variables: {
        data: {
          name: "uncut-logs",
          message: JSON.stringify({
            date: new Date(),
            "device-token": localStorage.getItem("uuidv4"),
            status: "onblur",
          }),
        },
      },
      query:
        "mutation uncutActivities($data: ActivityLogCreateInput!) { actitvityLogs(data: $data)}",
    }
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
  };

  window.onload = function () {
    const data = {
      operationName: "uncutActivities",
      variables: {
        data: {
          name: "uncut-logs",
          message: JSON.stringify({
            date: new Date(),
            "device-token": localStorage.getItem("uuidv4"),
            status: "oninit",
          }),
        },
      },
      query:
        "mutation uncutActivities($data: ActivityLogCreateInput!) {  actitvityLogs(data: $data)}",
    }
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
  };
}
            `,
            }}
          />
        </Head>

        <body>
          <source />
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

// MyDocument.getInitialProps = async (ctx:DocumentContext) => {
//   return {} as DocumentInitialProps;
// };

export default MyDocument;
