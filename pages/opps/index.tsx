import IMAGES from "@/assets/img/images";
import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import Image from "next/legacy/image";
import { useRouter } from "next/router";
import { NextPageWithLayout } from "../_app";

const Opps: NextPageWithLayout = () => {
  const router = useRouter();
  return (
    <div className="flex flex-col justify-center items-center">
      <div className="pt-20">
        <Image src={IMAGES.opps} alt="opps" />
      </div>

      <h3 className="pb-2 pt-4 text-[1.25rem] font-medium">Opps! Sorry</h3>
      <p className="pb-20 px-20 text-center text-[#8D8D8D] text-[1rem]">
        Tính năng này đang được phát triển, chúng tôi sẽ cố gắng hoàn thiện
        trong thời gian sớm nhất
      </p>

      <button
        onClick={() => {
          router.push("/");
        }}
        className="text-white w-[90%] py-3  rounded-lg bg-[#BA92D9]"
      >
        Quay lại trang chủ
      </button>
    </div>
  );
};

export default Opps;

Opps.getLayout = function (page) {
  return (
    <Layout>
      <Header />
      {page}
    </Layout>
  );
};
