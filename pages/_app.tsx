import { uuid } from "uuidv4";
import "./globals.scss";
import "../public/fonts/styles.css";

import { Provider } from "react-redux";

import { REVIEWTY_UUID } from "@/common/contains";
import HeadMeta from "@/components/head-meta";
import SpinLoader from "@/components/spin/Spin";
import { DeviceContext } from "@/context/DeviceContext";
import { useApollo } from "@/hooks/useApollo";
import { useStore } from "@/store/store";
import { ApolloProvider } from "@apollo/client";
import { ConfigProvider } from "antd";
import { NextComponentType, NextPageContext } from "next";
import { AppProps } from "next/app";
import { ReactElement, ReactNode, useEffect } from "react";

export type NextPageWithLayout<P = {}> = NextComponentType<
  NextPageContext,
  any,
  P
> & {
  getLayout?: (page: ReactElement) => ReactNode;
};

export type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  const apolloClient = useApollo(pageProps.initialApolloState);
  const store = useStore(pageProps.initialReduxState);

  const getLayout =
    Component.getLayout ??
    ((page) => {
      return page;
    });

  useEffect(() => {
    const uuidLocalStorage = localStorage.getItem(REVIEWTY_UUID);
    if (!uuidLocalStorage) {
      localStorage.setItem(REVIEWTY_UUID, uuid());
    }
  }, []);

  return (
    <Provider store={store}>
      <ApolloProvider client={apolloClient}>
        <ConfigProvider
          theme={{
            token: {
              colorPrimary: "#7003BB",
              colorPrimaryBgHover: "#7003BB",
            },
          }}
        >
          <DeviceContext>
            <SpinLoader />
            <HeadMeta></HeadMeta>
            <div className="bg-[#F6F6F6] ">
              {getLayout(<Component {...pageProps} />)}
            </div>
          </DeviceContext>
        </ConfigProvider>
      </ApolloProvider>
    </Provider>
  );
}

export default MyApp;
