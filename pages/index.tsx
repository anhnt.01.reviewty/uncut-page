import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import Main from "../components/main";
import { NextPageWithLayout } from "./_app";

const Home: NextPageWithLayout = () => {
  return (
    <div className="">
      <Main></Main>
    </div>
  );
};

export default Home;

Home.getLayout = function (page) {
  return (
    <Layout hasFooter={true}>
      <Header type="main" />

      {page}
    </Layout>
  );
};
