import ProductEventCard from "@/components/event/product-event-card";
import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import { useGetEventProductDetail } from "@/hooks/useGetEventProductDetail";
import useTrans from "@/hooks/useTrans";
import { Tab } from "@/pages/product-detail/funding/[slug]";
import { NextPageWithLayout } from "@/pages/_app";
import clsx from "clsx";
import { useRouter } from "next/router";
import { useMemo, useState } from "react";

const EventProduct: NextPageWithLayout = () => {
  const router = useRouter();
  const [tabKey, setTabKey] = useState<Tab | undefined>(Tab.Product);
  const t = useTrans();
  const { dataEvent, getEventDetail, loading } = useGetEventProductDetail();
  const barItems = useMemo(() => {
    return [
      {
        id: "product",
        label: `${t.home.common.product}`,
        status: Tab.Product,
      },
      {
        id: "information",
        label: `${t.home.common.information}`,
        status: Tab.Information,
      },
    ];
  }, [t.home.common.information, t.home.common.product]);

  const handleClickBarITems = (status?: Tab, id?: string) => {
    document
      .querySelector(`.${id}-md`)
      ?.scrollIntoView({ behavior: "smooth", block: "start" });

    setTabKey(status);
    return;
  };

  return (
    <div className="max-w-screen-xl relative m-auto h-[100%]">
      <div className="grid grid-cols-12 sticky top-0 z-[2] bg-white">
        {barItems.map((item, index) => (
          <span
            key={item.id}
            onClick={() => {
              handleClickBarITems(item.status, item.id);
            }}
            className={clsx(
              `col-span-${
                12 / barItems.length
              } p-2 pt-1 text-center text-[0.875rem] leading-8 cursor-pointer  text-[#8d8d8d] border-b-2`,
              {
                "text-[black] border-b-[black] font-medium":
                  item.status === tabKey,
              }
            )}
          >
            {item.label}
          </span>
        ))}
      </div>

      <ProductEventCard
        data={dataEvent}
        loading={loading}
        callback={getEventDetail}
      />
    </div>
  );
};

export default EventProduct;

EventProduct.getLayout = function (page) {
  return (
    <Layout>
      <Header></Header>
      {page}
    </Layout>
  );
};
