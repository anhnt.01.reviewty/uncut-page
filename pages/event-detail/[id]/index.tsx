import { ErrorIcon, SuccessIcon } from "@/assets/icon/icons";
import { handleCheckAuth, REVIEWTY_TOKEN } from "@/common/contains";
import EventCard from "@/components/event-card/EventCard";
import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import { action, addHistoryBack, EAction } from "@/components/main/mainSlice";
import { DeviceDetectContext } from "@/context/DeviceContext";
import {
  EventCommentCreateError,
  EventCommentCreateErrorCode,
  useCreateEventCommentV3Mutation,
} from "@/graphql/reviewty-user/graphql";
import {
  GetEventDetailQuery,
  useGetEventDetailLazyQuery,
  useReadEventCommentLazyQuery,
} from "@/graphql/reviewty/graphql";
import { useActiveCustomer } from "@/hooks/useActiveCustomer";
import { useGetAddress } from "@/hooks/useGetAddress";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { formatDate, handleDiffDate } from "@/ultis/ultis";
import { Modal } from "antd";
import clsx from "clsx";
import { GetServerSideProps } from "next";
import { AppContextType } from "next/dist/shared/lib/utils";
import Image from "next/image";
import { useRouter } from "next/router";
import { useCallback, useContext, useEffect, useMemo, useState } from "react";
import { NextPageWithLayout } from "../../_app";

export const renderErrorMessage = (
  code: EventCommentCreateErrorCode,
  error?: EventCommentCreateError
) => {
  switch (code) {
    case EventCommentCreateErrorCode.NotEnoughPoints:
      return `Bạn không đủ điểm để tham gia sự kiện`;
    case EventCommentCreateErrorCode.ShippingAddressNotExisted:
      return (
        <p className="">
          Địa chỉ mặc định chưa tồn tại, bạn thêm địa chỉ để tham gia sự kiện
          nhé.
        </p>
      );
    case EventCommentCreateErrorCode.ExceedMaxParticipants:
      return `Số lượng đổi điểm đã đạt đến giới hạn`;
    case EventCommentCreateErrorCode.AlreadyParticipatedEvent:
      return <p className="">Bạn đã tham gia sự kiện rồi.</p>;
    case EventCommentCreateErrorCode.NotEnoughReviews:
      return `Sự kiện này chỉ dành cho các thành viên có ít nhất ${error?.expectedValue!} đánh giá chi tiết được duyệt. Bạn cần hoàn thành ${
        error?.expectedValue! - error?.currentValue!
      } đánh giá nữa để tham gia sự kiện.`;
    default:
      return "Đã có lỗi gì đó xảy ra, liên hệ với admin để được hỗ trợ";
  }
};

const EventDetail: NextPageWithLayout<{ referer: string }> = ({
  referer = "",
}) => {
  const { isWebViewIOS } = useContext(DeviceDetectContext);

  const dispatch = useAppDispatch();
  const router = useRouter();
  const mainSlice = useAppSelector((slice) => slice.mainSlice);
  const [count, setCount] = useState(0);
  const [isSubmit, setIsSubmit] = useState(false);
  const numberId = Number(router.query.id);
  useActiveCustomer();
  const { default: defaultAddress } = useGetAddress();
  const activeCustomer = useAppSelector((state) => state.authSlice);
  const [isOpen, setIsOpen] = useState(false);
  const [dataEvent, setDataEvent] = useState<GetEventDetailQuery["event"]>();
  const notStartedYet =
    new Date().getTime() < new Date(dataEvent?.startedAt).getTime();
  const [getEventDetail] = useGetEventDetailLazyQuery();
  const [getComments, { data: readComments }] = useReadEventCommentLazyQuery({
    variables: {
      eventId: numberId,
      first: 20,
      skip: 0,
    },
    fetchPolicy: "no-cache",
  });

  const [createEvent] = useCreateEventCommentV3Mutation();

  useEffect(() => {
    if (referer) {
      dispatch(addHistoryBack(referer));
    }
  }, [dispatch, referer]);

  useEffect(() => {
    if (Number(router.query.id)) {
      getEventDetail({
        variables: {
          id: Number(router.query.id),
        },
        fetchPolicy: "no-cache",
        onCompleted(data) {
          setDataEvent(data.event);
        },
      });
    }

    if (numberId && activeCustomer.me?.id) {
      getComments({
        variables: {
          eventId: numberId,
          userId: activeCustomer.me?.id,
        },
      });
    }

    return () => {};
  }, [
    activeCustomer.me?.id,
    getComments,
    getEventDetail,
    numberId,
    router.query.id,
  ]);

  const memoRender = useMemo(() => {
    return (
      <div
        className="w-full  m-auto  bg-[#e4e3e3]"
        dangerouslySetInnerHTML={{
          __html: dataEvent?.content || "",
        }}
      ></div>
    );
  }, [dataEvent?.content]);

  const handleClick = useCallback(
    async (usePoint?: "usePoint") => {
      const checkDate = handleDiffDate(dataEvent?.endedAt);

      const isExpired = checkDate === "Hết hạn";

      if (isExpired && dataEvent?.referred_url) {
        if (isWebViewIOS) {
          router.push(dataEvent?.referred_url);
          return;
        }
        window.open(dataEvent?.referred_url, "_blank");
        return;
      }
      const token = localStorage.getItem(REVIEWTY_TOKEN);

      if (!token) {
        setIsOpen(!isOpen);
        return;
      }

      if (isExpired) {
        return;
      }

      if (!activeCustomer.me) return;

      if (activeCustomer.me && readComments) {
        if (count === 0) {
          if (!defaultAddress.length) {
            Modal.confirm({
              centered: true,
              okType: "primary",
              okText: "Thêm địa chỉ",
              title: "Chưa có địa chỉ",
              content: (
                <p className="">
                  {renderErrorMessage(
                    EventCommentCreateErrorCode.ShippingAddressNotExisted
                  )}
                </p>
              ),
              cancelText: "Hủy",
              onOk: () => {
                dispatch(action(EAction.pickAddressBeforeJoinEvent));
                router.push("/profile/pick-address");
              },
              onCancel: () => {
                setCount(0);
              },
            });
            return;
          }
          const {
            id,
            fullName,
            address,
            ward,
            province,
            district,
            phoneNumber,
          } = defaultAddress[0];

          const { data } = await createEvent({
            variables: {
              data: {
                event: {
                  id: numberId,
                },
                content: usePoint ? "Đổi điểm" : "Tôi tham gia",
                usePoints: usePoint ? true : false,
              },
            },
          });

          if (
            data?.createEventCommentV3_UncheckCondition.__typename ===
            "EventCommentCreateError"
          ) {
            Modal.error({
              icon: <ErrorIcon />,
              type: "error",
              centered: true,
              okType: "primary",
              okText: "Xác nhận",
              title: "Thất bại",
              content: (
                <p>
                  {renderErrorMessage(
                    data.createEventCommentV3_UncheckCondition.code,
                    data.createEventCommentV3_UncheckCondition as EventCommentCreateError
                  )}
                </p>
              ),
              onOk: async () => {
                setCount(0);
              },
            });

            return;
          }

          if (
            data?.createEventCommentV3_UncheckCondition.__typename ===
            "EventComment"
          ) {
            Modal.confirm({
              icon: <SuccessIcon />,
              type: "success",
              centered: true,
              okType: "primary",
              okText: "Xác nhận",
              cancelText: "Thay đổi",
              title: "Thành công",
              content: (
                <p className="">
                  Reviewty sẽ liên hệ với bạn qua địa chỉ mặc định{" "}
                  <span className="font-bold text-[#222]">
                    {fullName} | {phoneNumber}, {address}, {ward}, {district},{" "}
                    {province}
                  </span>{" "}
                  . Bạn có đồng ý không?
                </p>
              ),
              onCancel: () => {
                dispatch(action(EAction.pickAddressBeforeJoinEvent));
                router.push("/profile/address-list");
              },
              onOk: async () => {},
            });
            getComments({
              variables: {
                eventId: numberId,
                userId: activeCustomer.me?.id,
              },
            });
          }
        }
      }
    },
    [
      activeCustomer.me,
      count,
      createEvent,
      dataEvent?.endedAt,
      dataEvent?.referred_url,
      defaultAddress,
      dispatch,
      getComments,
      isOpen,
      isWebViewIOS,
      numberId,
      readComments,
      router,
    ]
  );

  const handleExchangePoints = useCallback(() => {
    // if(dataEvent?.currentNumberOfWinners === dataEvent?.numberOfWinners){

    //   return
    // }
    Modal.confirm({
      centered: true,
      okType: "primary",
      okText: "Có",
      title: "Xác nhận đổi điểm ?",
      content: (
        <p className="">
          Bạn có muốn dùng{" "}
          <span className="text-[red] font-bold ">{dataEvent?.points}</span>{" "}
          điểm để đổi lấy sản phẩm này ?
        </p>
      ),
      cancelText: "Hủy",
      onOk: () => {
        setIsSubmit(true);
        handleClick("usePoint");
        setTimeout(() => {
          setIsSubmit(false);
        }, 0);
      },
      onCancel: () => {},
    });
    return;
  }, [dataEvent?.points, handleClick]);

  const renderBtn = useCallback(() => {
    const checkDate = handleDiffDate(dataEvent?.endedAt);
    const isExpired = checkDate === "Hết hạn";

    if (dataEvent) {
      if (isExpired && dataEvent.referred_url) {
        return (
          <div
            className={clsx(
              "fixed py-1 text-center px-2 w-[100%] bg-white max-w-[600px] bottom-0"
            )}
            style={{
              boxShadow: "0px -4px 4px 0px rgba(0, 0, 0, 0.08)",
            }}
          >
            <button
              onClick={() => handleClick()}
              className="rounded-lg z-50 py-3 w-full bg-[#ba92d9]"
            >
              Mua ngay
            </button>
          </div>
        );
      } else if ((isExpired && !dataEvent.referred_url) || notStartedYet) {
        return (
          <div
            className={clsx(
              "fixed py-1 text-center px-2 w-[100%] bg-white max-w-[600px] bottom-0"
            )}
            style={{
              boxShadow: "0px -4px 4px 0px rgba(0, 0, 0, 0.08)",
            }}
          >
            <button className="rounded-lg z-50 py-3 w-full bg-[gray]">
              {notStartedYet ? "Sự kiện chưa bắt đầu" : "Hết hạn sự kiện"}
            </button>
          </div>
        );
      }
      return (
        <div
          className={clsx(
            "fixed py-1 text-center px-4 w-[100%] bg-white max-w-[600px] bottom-0"
          )}
          style={{
            boxShadow: "0px -4px 4px 0px rgba(0, 0, 0, 0.08)",
          }}
        >
          <button
            onClick={() => handleClick()}
            className={clsx("rounded-lg z-50 py-3 w-[48%] mr-[2%]", {
              "bg-[gray] cursor-default":
                readComments?.eventComments?.length || isExpired || count > 0,
              "bg-[#ba92d9] cursor-pointer":
                !readComments?.eventComments?.length,
              "w-full": !dataEvent.points,
            })}
          >
            {readComments?.eventComments?.filter((comment) =>
              comment.content.includes("Tôi tham gia")
            ).length
              ? "Bạn đã tham gia"
              : "Tôi tham gia"}
          </button>

          <button
            disabled={isSubmit}
            onClick={handleExchangePoints}
            className={clsx("rounded-lg z-50 py-3 w-[48%] ", {
              "bg-[gray] cursor-default":
                readComments?.eventComments?.length ||
                isExpired ||
                count > 0 ||
                dataEvent.currentNumberOfWinners === dataEvent.numberOfWinners,
              "bg-[#ba92d9] cursor-pointer":
                !readComments?.eventComments?.length,
              hidden: !dataEvent.points,
            })}
          >
            {readComments?.eventComments?.filter((comment) =>
              comment.content.includes("Đổi điểm")
            ).length
              ? "Bạn đã đổi điểm"
              : "Đổi điểm"}
          </button>
        </div>
      );
    }
  }, [
    count,
    dataEvent,
    handleClick,
    handleExchangePoints,
    notStartedYet,
    readComments?.eventComments,
  ]);

  const handleOk = async () => {
    handleCheckAuth();
    setIsOpen(!isOpen);
  };
  const handleCancel = () => {
    setCount(0);
    setIsOpen(!isOpen);
  };

  return (
    <>
      <Modal
        centered
        okType="danger"
        open={isOpen}
        title="Cần đăng nhập để tham gia sự kiện"
        onOk={handleOk}
        onCancel={handleCancel}
      >
        Bạn cần phải đăng nhập để sử dụng chức năng này.
      </Modal>
      {dataEvent && (
        <div className="container m-auto relative">
          <div className="fixed bottom-0 flex z-10 container m-auto"></div>
          <div
            key={dataEvent?.id}
            className={"col-span-12  relative overflow-hidden md:col-span-6"}
          >
            <div>
              <Image
                src={dataEvent?.coverUrlFunding!}
                alt={dataEvent?.name!}
                width="0"
                height="0"
                sizes="100vw"
                className="w-full block"
              />

              {/* <div className="p-4">
                <Progressbar
                  completed={dataEvent?.currentNumberOfWinners!}
                  limit={dataEvent?.numberOfWinners!}
                  bgcolor={"red"}
                />
              </div> */}
            </div>
            <div className="px-4">
              <ul className="p-4" style={{ listStyleType: "disc" }}>
                <li className="text-[#222] font-medium text-[1rem] lib-2">
                  Tên sự kiện
                </li>

                <p className="text-[#747474] pb-4 font-medium text-[1rem]">
                  {dataEvent?.name}
                </p>

                <li className="text-[#222]  font-medium pb-2 text-[1rem]">
                  Thời gian tham gia
                </li>

                <p className="text-[#747474] pb-4 font-medium text-[1rem]">
                  {formatDate(dataEvent?.startedAt)} ~{" "}
                  {formatDate(dataEvent?.endedAt)}
                </p>

                <li className="text-[#222] pb-2 font-medium text-[1rem]">
                  Điều kiện tham gia
                </li>
                <p className="text-[#747474] pb-4 font-medium text-[1rem]">
                  {dataEvent?.condition}
                </p>
              </ul>
            </div>
            {memoRender}

            <div className="pt-4  border-t-[20px] border-[#e4e3e3]">
              <p className="pl-4 pb-8 font-medium text-[#222222] text-[1.25rem]">
                Sản phẩm liên quan
              </p>
            </div>
            <div className="p-4 pb-20">
              <EventCard data={dataEvent!} />
            </div>

            {renderBtn()}
          </div>
        </div>
      )}
    </>
  );
};

export default EventDetail;

EventDetail.getLayout = function (page) {
  return (
    <Layout>
      <Header />
      {page}
    </Layout>
  );
};

export const getServerSideProps = async (ctx: any) => {
  return {
    props: {
      referer: ctx.req.headers?.referer || "",
    },
  };
};
