import { GetServerSideProps } from "next";
import crypto from "crypto";
import qs from "qs";
import { useEffect, useMemo } from "react";
import { Button } from "antd";
import { useRouter } from "next/router";
import Image from "next/image";
import IMAGES from "@/assets/img/images";
import axios from "axios";
import useTrans from "@/hooks/useTrans";
import {
  FundingOrderStatus,
  usePayBackFundingOrderMutation,
} from "@/graphql/reviewty-user/graphql";
import { NextPageWithLayout } from "@/pages/_app";
import Layout from "@/components/layout/layout";
import Header from "@/components/Header";
import { sentEventLog } from "@/common/contains";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { useActiveCustomer } from "@/hooks/useActiveCustomer";
// const fetch = require("node-fetch");
interface IResult {
  isValidChecksum: boolean;
  orderId: string;
  responseCode: string;
  data: any;
}

const Result: NextPageWithLayout<IResult> = ({
  isValidChecksum,
  orderId,
  responseCode,
  data,
}: IResult) => {
  const router = useRouter();
  const trans = useTrans();
  useActiveCustomer();

  const getMe = useAppSelector((slice) => slice.authSlice);

  const [payback] = usePayBackFundingOrderMutation();

  const handlePayBack = async (id: number) => {
    const { data, errors } = await payback({
      variables: {
        where: {
          id: id,
        },
      },
    });

    if (errors) {
    }
    if (data?.payBackFundingOrder) {
      router.push(data.payBackFundingOrder);
    }
  };

  const errorMessage = useMemo(() => {
    switch (responseCode) {
      case "00":
        return "Giao dịch thành công";
      case "07":
        return "Trừ tiền thành công. Giao dịch bị nghi ngờ (liên quan tới lừa đảo, giao dịch bất thường).";
      case "09":
        return "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ InternetBanking tại ngân hàng.";
      case "10":
        return "Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản không đúng quá 3 lần";
      case "11":
        return "Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng thực hiện lại giao dịch.";
      case "12":
        return "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa.";
      case "13":
        return "Giao dịch không thành công do: Quý khách nhập sai mật khẩu xác thực giao dịch (OTP). Xin quý khách vui lòng thực hiện lại giao dịch.";
      case "24":
        return "Giao dịch không thành công do: Khách hàng hủy giao dịch";
      case "51":
        return "Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư để thực hiện giao dịch.";
      case "65":
        return "Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức giao dịch trong ngày.";
      case "75":
        return "Giao dịch không thành công do: Ngân hàng thanh toán đang bảo trì.";
      case "79":
        return "Giao dịch không thành công do: Khách hàng nhập sai mật khẩu thanh toán quá số lần quy định. Xin quý khách vui lòng thực hiện lại giao dịch";
      case "99":
      default:
        return "Giao dịch không thành công do: Lỗi không xác định.";
    }
  }, [responseCode]);

  useEffect(() => {
    if (getMe) {
      sentEventLog({
        eventName: "Result",
        type: "view",
        asPath: router.pathname,
        userId: getMe.me?.id,
        orderId: orderId,
        orderStatus: responseCode,
      });
    }
  }, [getMe, orderId, responseCode, router.pathname]);
  return (
    <div className="max-w-[600px] h-screen w-full p-4 text-center m-auto flex justify-center flex-col items-center">
      {responseCode ? (
        <>
          <div>
            <Image
              src={
                errorMessage.includes("Giao dịch không thành công do:")
                  ? IMAGES.buyErrors
                  : IMAGES.buySuccess
              }
              alt="buy-success"
            />
          </div>
          <p className="py-4 font-medium text-[1.25rem]">{errorMessage}</p>

          {!errorMessage.includes("Giao dịch không thành công do:") && (
            <p className="text-[#8D8D8D] pb-4">
              Cảm ơn bạn đã ủng hộ thương hiệu thông qua việc đặt mua trước sản
              phẩm. Reviewty sẽ kiểm tra và chuyển hàng cho bạn sớm nhất có thể.
            </p>
          )}

          {(responseCode === "00" || responseCode === "07") && (
            <button
              className="w-full py-3 rounded-lg text-white bg-[#BA92D9]"
              onClick={() => {
                router.push({
                  pathname: "/orders",
                  query: {
                    type: FundingOrderStatus.WaitDelivering,
                  },
                });
              }}
            >
              {trans.common.viewOrders}
            </button>
          )}

          <button
            className="w-full py-3 rounded-lg mt-4 text-white bg-[#BA92D9]"
            onClick={() => {
              router.push("/");
            }}
          >
            {trans.common.backToEvent}
          </button>
        </>
      ) : (
        errorMessage
      )}
    </div>
  );
};

export default Result;

Result.getLayout = function (page) {
  return (
    <Layout>
      <Header />
      {page}
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  // ;
  let vnp_Params = context.query;
  const secureHash = vnp_Params["vnp_SecureHash"];
  delete vnp_Params["vnp_SecureHash"];
  delete vnp_Params["vnp_SecureHashType"];
  vnp_Params = sortObject(vnp_Params);

  const secretKey = process.env.VNP_HASHSECRET || "";
  const signData = qs.stringify(vnp_Params, { encode: false });
  const hmac = crypto.createHmac("sha512", secretKey);
  const signed = hmac.update(Buffer.from(signData, "utf-8")).digest("hex");
  let data = {};
  if (process.env.NEXT_PUBLIC_BASE_URL === "https://r-uncut.vercel.app") {
    data = fetch("https://api-admin-test.review-ty.com/order/delivery-test", {
      method: "POST",
      body: JSON.stringify({
        orderId: vnp_Params["vnp_TxnRef"]?.toString().split(".")[0],
      }),
      headers: {
        "Content-Type": "application/json",
        // "Content-Type": "application/x-www-form-urlencoded",
      },
    })
      .then((data) => data.json())
      .catch((err) => console.log(err));
  }

  return {
    props: {
      isValidChecksum: secureHash === signed,
      orderId: vnp_Params["vnp_TxnRef"]?.toString().split(".")[0] || "",
      responseCode: vnp_Params["vnp_ResponseCode"] || "",
      // data: data,
    },
  };
};

function sortObject(obj: { [key: string]: any }) {
  const sorted: { [key: string]: any } = {};
  const str = [];
  let key: string;
  for (key in obj) {
    str.push(encodeURIComponent(key));
  }
  str.sort();
  str.forEach((key) => {
    sorted[key] = encodeURIComponent(obj[key]).replace(/%20/g, "+");
  });
  return sorted;
}
