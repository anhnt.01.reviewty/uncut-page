import { DecreeIcon, IncreeIcon } from "@/assets/icon/icons";
import {
  gTagGGAnalysis,
  handleCheckAuth,
  IS_MOBILE,
  REVIEWTY_TOKEN,
  sentEventLog,
} from "@/common/contains";
import toCurrency from "@/common/toCurrency";
import AddressOverview from "@/components/address/addressOverview";
import { selectAddresses, setIsSpin } from "@/components/auth/authSlice";
import ButtonComponent from "@/components/button";
import {
  decreement,
  increement,
  infoOrders,
} from "@/components/checkout/checkoutSlice";
import Header from "@/components/Header";
import Layout from "@/components/layout/layout";
import {
  DeliverOption,
  PaymentMethod,
  useCreateFundingOrderMutation,
  useGetShippingFeeLazyQuery,
} from "@/graphql/reviewty-user/graphql";
import {
  FundingDetailQuery,
  useFundingDetailLazyQuery,
} from "@/graphql/reviewty/graphql";
import { useActiveCustomer } from "@/hooks/useActiveCustomer";
import useTrans from "@/hooks/useTrans";
import { useAppDispatch, useAppSelector } from "@/store/store";
import { handleDiffDate } from "@/ultis/ultis";
import { Button, message, Modal, Radio, Skeleton, Space, Spin } from "antd";
import clsx from "clsx";
import Image from "next/legacy/image";
import { useRouter } from "next/router";
import { useEffect, useMemo, useState } from "react";
import { NextPageWithLayout } from "../_app";
import styles from "./checkout.module.scss";

enum ETypeButton {
  ADD_NEW = "ADD_NEW",
  EDIT = "EDIT",
}

const Checkout: NextPageWithLayout = () => {
  const getMe = useAppSelector((slice) => slice.authSlice);
  useActiveCustomer();

  const dispatch = useAppDispatch();
  const router = useRouter();
  const trans = useTrans();
  const cardSlice = useAppSelector((store) => store.checkoutSlice);
  const [shippingFee, setShippingFee] = useState<any>();
  const addressSlice = useAppSelector(selectAddresses);

  const [products, setProducts] = useState<FundingDetailQuery>();
  const [getDetails, { data: dataDetails, error }] = useFundingDetailLazyQuery({
    variables: {
      where: {
        id: Number(router.query.id),
      },
    },
    onCompleted(data) {
      dispatch(
        infoOrders({ data: data, quantity: Number(router.query.quantity) })
      );
    },
  });

  const checkDate = handleDiffDate(cardSlice.info?.funding.endDate);

  const isExpired = checkDate === "Hết hạn";

  useEffect(() => {
    if (cardSlice.info) {
      setProducts(cardSlice.info);
    }
  }, [cardSlice.info]);
  const [
    getShippingFee,
    { data: dataShippingFee, loading: loadingShippingFee },
  ] = useGetShippingFeeLazyQuery({});

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalEditOpen, setIsModalEditOpen] = useState(false);

  const [createFundingOrder] = useCreateFundingOrderMutation({});

  const total = useMemo(() => {
    return products?.funding.fundingProducts?.reduce(
      (accumulator, currentValue) =>
        accumulator + currentValue.discount! * currentValue.quantity!,
      0
    );
  }, [products?.funding.fundingProducts]);

  useEffect(() => {
    (async () => {
      if (router.query.id) {
        const { data, error } = await getDetails();
        if (error) {
        }
      }
    })();
    if (addressSlice.default && addressSlice.default[0]?.address) {
      const { address, province, district } = addressSlice?.default[0];

      getShippingFee({
        variables: {
          data: {
            address,
            province,
            district,
            weight:
              products?.funding.fundingProducts?.[0].product?.attributes
                ?.netWeightPerPackage || 500,
            deliverOption: DeliverOption.None,
          },
        },
      });

      if (dataShippingFee) {
        const parseData = JSON.parse(dataShippingFee.shippingFee);
        setShippingFee(parseData?.fee || 0);
        sentEventLog({
          eventName: "Price",
          type: "view",
          asPath: router.pathname,
          userId: getMe.me?.id,
          productPrice: products?.funding.fundingProducts?.[0]?.product?.price!,
          shippingPrice: parseData?.fee?.fee!,
          productId: products?.funding.fundingProducts?.[0]?.product?.id!,
          fundingId: products?.funding.id!,
        });
      }
    }

    return () => {};
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    addressSlice.default,
    dataShippingFee,
    getDetails,
    getShippingFee,
    // products?.funding.fundingProducts, // need disabled line this
    router.query.id,
  ]);

  const handleBuyNow = async () => {
    gTagGGAnalysis({
      eventName: "purchase",
      eventCategory: "CLICK_CHECKOUT_BUYNOW",
      eventLabel: "CLICK",
      asPath: router.asPath,
    });
    if (!localStorage.getItem(REVIEWTY_TOKEN)) {
      handleCheckAuth();

      return;
    }
    if (!addressSlice.default.length) {
      setIsModalOpen(true);
      return;
    }

    if (
      !addressSlice.default[0].fullName ||
      !addressSlice.default[0].phoneNumber
    ) {
      setIsModalEditOpen(true);
      return;
    }

    if (!addressSlice.default[0]) {
      return;
    }
    if (error) {
      router.push("/");
    }

    if (!shippingFee?.fee || shippingFee.fee <= 0) {
      message.error(
        "Đã xảy ra sự cố, vui lòng liên hệ với admin để được hỗ trợ"
      );
      return;
    }

    if (!total && !shippingFee?.fee) {
      return;
    }

    const {
      address,
      district,
      province,
      ward,
      phoneNumber,
      fullName,
      ...rest
    } = addressSlice.default[0]!;
    dispatch(setIsSpin(true));

    const { data, errors } = await createFundingOrder({
      variables: {
        data: {
          discount: products?.funding.fundingProducts?.[0]?.discount!,
          deliveryFee: shippingFee.fee,
          insuranceFee: shippingFee.insuranceFee,
          fundingId: Number(router.query.id),
          productId: products?.funding.fundingProducts?.[0]?.product?.id!,
          quantity: products?.funding.fundingProducts?.[0]?.quantity!,
          paymentMethod: PaymentMethod.Vnpay,
          deliveryAddress: `${address}, ${ward}, ${district}, ${province}`,
          telephone: phoneNumber?.toString()!,
          userName: fullName!,
          freeShip: products?.funding.freeShip,
          preOrder: products?.funding.preOrder,
        },
      },
    });

    if (data) {
      router.push(data!.createFundingOrder);
    }
    dispatch(setIsSpin(false));

    return;
  };

  const decreeQuantityHandler = () => {
    dispatch(decreement({}));
  };

  const increeQuantityHandler = (index: number) => {
    dispatch(
      increement({
        quantity: cardSlice.root?.funding.fundingProducts?.[index].quantity!,
      })
    );
  };

  const handleOk = (type?: ETypeButton) => {
    if (type === ETypeButton.EDIT) {
      router.push({
        pathname: "/profile/address-list/[id]",
        query: {
          id: addressSlice.default[0].id,
        },
      });
      return;
    }
    router.push("/profile/pick-address");
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    setIsModalEditOpen(false);
  };

  return (
    <div>
      <Modal
        okText={trans.address.addNewAddress}
        title={trans.address.confirm.noOrderAddressAddedYet}
        open={isModalOpen}
        onOk={() => handleOk()}
        onCancel={handleCancel}
        footer={false}
      >
        <p>{trans.address.content.noAddedAddressYet}.</p>

        <div className="flex justify-end pt-4">
          <Button onClick={handleCancel} type="dashed" className="mr-2">
            {trans.common.cancel}
          </Button>

          <ButtonComponent onClick={() => handleOk()}>
            {trans.address.addNewAddress}
          </ButtonComponent>
        </div>
      </Modal>
      <Modal
        title={"Thông tin địa chỉ chưa hợp lệ"}
        open={isModalEditOpen}
        onOk={() => handleOk()}
        onCancel={handleCancel}
        footer={false}
      >
        <p>Có vẻ như thông tên người nhận hoặc số điện thoại chưa hợp lệ.</p>

        <div className="flex justify-end pt-4">
          <Button onClick={handleCancel} type="dashed" className="mr-2">
            {trans.common.cancel}
          </Button>

          <ButtonComponent onClick={() => handleOk(ETypeButton.EDIT)}>
            Chỉnh sửa thông tin
          </ButtonComponent>
        </div>
      </Modal>

      <AddressOverview fundingId={router.query.id! as string} />
      <div className="m-4">
        {products?.funding.fundingProducts?.length ? (
          <div>
            <label className="font-medium text-[1.25rem]">
              {trans.common.product}
            </label>
            <div className="pt-4">
              {products?.funding.fundingProducts.map((fundingProduct, i) => {
                return (
                  <div className="grid grid-cols-12" key={Math.random()}>
                    <div className="col-span-3">
                      <Image
                        // sizes="100vw"
                        // className="w-full"
                        width={70}
                        height={70}
                        src={fundingProduct.product?.thumbnail?.url!}
                        alt={fundingProduct.product?.translations[0].name}
                      />
                    </div>
                    <label className="col-span-6">
                      <span className="ellipsis-description">
                        {fundingProduct.product?.translations[0].name}{" "}
                      </span>
                      {products.funding.preOrder && (
                        <div className="py-1">
                          <span className="text-[#D10017] rounded-[3.75rem] text-[0.75rem] border border-[#D10017] px-3 py-1">
                            Pre-Order
                          </span>
                        </div>
                      )}

                      <div className="flex pt-2">
                        <span
                          className={clsx("cursor-pointer", {
                            "cursor-not-allowed":
                              products.funding.fundingProducts?.[0]
                                ?.quantity! <= 1,
                          })}
                          onClick={decreeQuantityHandler}
                        >
                          <DecreeIcon />
                        </span>
                        <label className="px-3">
                          {" "}
                          {fundingProduct.quantity}
                        </label>
                        <span
                          className="cursor-pointer"
                          onClick={() => increeQuantityHandler(i)}
                        >
                          <IncreeIcon />
                        </span>
                      </div>
                    </label>
                    <label className="col-span-3 flex justify-end">
                      <span className="ellipsis-description text-[#BE2827]">
                        {toCurrency(
                          fundingProduct.discount! * fundingProduct.quantity!
                        )}
                      </span>
                    </label>
                  </div>
                );
              })}
            </div>
          </div>
        ) : (
          <Skeleton active paragraph={{ rows: 2 }} />
        )}
      </div>
      <div className="p-4">
        <span className="font-medium mb-2 text-[1.25rem]">
          {trans.common.deliveryMethod}
        </span>
        {loadingShippingFee ? (
          <Skeleton
            title={false}
            className="w-full"
            active
            paragraph={{ rows: 1 }}
          />
        ) : (
          <div className="flex justify-between pt-2">
            <label className="text-[#9b9b9b]">Giao hàng tiết kiệm </label>
            <span>{toCurrency(shippingFee?.fee || 0)}</span>
          </div>
        )}
      </div>

      <div className="p-4">
        <label className="font-medium text-[1.25rem]">
          {trans.common.paymentMethod}
        </label>
        <div className="pt-4">
          <Radio.Group defaultValue={1}>
            <Space direction="vertical">
              <Radio value={1} className="text-[1rem]">
                <span className="text-[1rem] text-[#9b9b9b]">VNPay</span>
              </Radio>
            </Space>
          </Radio.Group>
        </div>
      </div>
      <div className="p-4 pb-20">
        <label className="font-medium text-[1.25rem]">
          {trans.common.order}
        </label>

        {products?.funding.fundingProducts?.length ? (
          <p className="text-[#8D8D8D] ">
            {products?.funding.fundingProducts?.[0]?.quantity}{" "}
            {trans.common.product.toLowerCase()}
          </p>
        ) : (
          <Skeleton active paragraph={{ rows: 1 }} />
        )}

        <div className="">
          <div className="py-4 my-4 mb-12 border-y-2 border-y-[#ECECEC]">
            <div className="flex py-2 justify-between">
              <label className="text-[#8D8D8D]">
                {trans.common.provisionalFee}
              </label>
              <span>
                {toCurrency(
                  total +
                    (shippingFee?.fee || 0) -
                    (products?.funding.preOrder ? shippingFee?.fee || 0 : 0)
                )}
              </span>
            </div>
            <div className="flex py-2 justify-between">
              <label className="text-[#8D8D8D]">
                {trans.common.shippingFee}
              </label>
              <span>{toCurrency(shippingFee?.fee! || 0)}</span>
            </div>
            {products?.funding.freeShip && (
              <div className="flex py-2 justify-between">
                <label className="text-[#8D8D8D]">Giảm giá vận chuyển</label>
                <span className="text-[#005A53]">
                  {"- "}
                  {toCurrency(shippingFee?.fee! || 0)}
                </span>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="fixed max-w-[600px] left-1/2 -translate-x-1/2 z-[1] shadow-[0_5px_25px_rgba(0,0,0,0.35)] md:shadow-none  px-4  bottom-0 w-full flex items-center bg-white py-2 ">
        <div className="grid grid-cols-12 w-full">
          <div
            className={clsx(
              "col-span-5 cursor-pointer bg-white flex flex-col pr-2"
            )}
          >
            <p className="text-[#404040] text-[1rem]">
              {trans.common.totalMoney}
            </p>
            <div className="text-[#D10017] font-bold text-[1.5rem]">
              {total ? (
                toCurrency(
                  total! +
                    (shippingFee?.fee || 0) -
                    (products?.funding.freeShip ? shippingFee?.fee || 0 : 0)
                )
              ) : (
                <Skeleton title={false} active paragraph={{ rows: 1 }} />
              )}
            </div>
          </div>
          <button
            disabled={
              // !addressSlice.default[0]?.address ||

              !total ||
              new Date().getTime() <
                new Date(products?.funding.startDate).getTime() ||
              (isExpired! && products?.funding.preOrder!)
            }
            onClick={handleBuyNow}
            className={clsx(
              "col-span-7 w-full bg-[#ba92d9] ",
              {
                "cursor-pointer bg-[#ba92d9]": total && shippingFee?.fee,
                "cursor-default bg-[gray]":
                  // !shippingFee?.fee ||
                  !total ||
                  new Date().getTime() <
                    new Date(products?.funding.startDate).getTime() ||
                  (isExpired && products?.funding.preOrder),
              },
              styles.buyNow
            )}
          >
            {isExpired && products?.funding.preOrder
              ? "Hết hạn đặt trước"
              : products?.funding.preOrder
              ? "Đặt hàng"
              : trans.common.buyNow}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Checkout;

Checkout.getLayout = function (page) {
  return (
    <Layout>
      <Header />
      {page}
    </Layout>
  );
};
