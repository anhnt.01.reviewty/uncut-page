import gql from 'graphql-tag';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
};

export enum ApiMethod {
  Delete = 'DELETE',
  Get = 'GET',
  Patch = 'PATCH',
  Post = 'POST',
  Put = 'PUT'
}

export enum AccountType {
  Organization = 'ORGANIZATION',
  Person = 'PERSON'
}

export type ActivityLogCreateInput = {
  message?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
};

export type Address = {
  __typename?: 'Address';
  address: Scalars['String'];
  country: Scalars['String'];
  district: Scalars['String'];
  fullName: Scalars['String'];
  phoneNumber: Scalars['String'];
  province: Scalars['String'];
  ward: Scalars['String'];
};

export type AddressIdInput = {
  id: Scalars['Float'];
};

export type AdultRdi = {
  __typename?: 'AdultRdi';
  maxAge?: Maybe<Scalars['Int']>;
  minAge?: Maybe<Scalars['Int']>;
  rdi?: Maybe<Scalars['Float']>;
  rdiUnit?: Maybe<Unit>;
  ul?: Maybe<Scalars['Float']>;
  ulUnit?: Maybe<Unit>;
};

export type Attribute = {
  __typename?: 'Attribute';
  color?: Maybe<Scalars['String']>;
  ingredients?: Maybe<Scalars['String']>;
  store: Scalars['String'];
  weight: Scalars['Float'];
};

export type AttributeInputType = {
  color?: InputMaybe<Scalars['String']>;
  ingredients?: InputMaybe<Scalars['String']>;
  store?: InputMaybe<Scalars['String']>;
  weight?: InputMaybe<Scalars['Float']>;
};

export type AttributeUpsertInput = {
  upsert?: InputMaybe<Array<AttributeInputType>>;
};

export type AuthError = Error & {
  __typename?: 'AuthError';
  code: AuthErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum AuthErrorCode {
  AccountAlreadyInUse = 'ACCOUNT_ALREADY_IN_USE',
  AccountIsBlocked = 'ACCOUNT_IS_BLOCKED',
  AppleAlreadyInUse = 'APPLE_ALREADY_IN_USE',
  AppleNotExists = 'APPLE_NOT_EXISTS',
  EmailAlreadyInUse = 'EMAIL_ALREADY_IN_USE',
  FacebookAlreadyInUse = 'FACEBOOK_ALREADY_IN_USE',
  FacebookNotExists = 'FACEBOOK_NOT_EXISTS',
  InvalidReferralCode = 'INVALID_REFERRAL_CODE',
  OtpNotValid = 'OTP_NOT_VALID',
  ReferralCodeInvalid = 'REFERRAL_CODE_INVALID',
  Unauthorized = 'UNAUTHORIZED'
}

export type AuthPayload = {
  __typename?: 'AuthPayload';
  token: Scalars['String'];
  user: User;
};

export type AuthPayloadOrError = AuthError | AuthPayload | CommonError;

export type BabyRdi = {
  __typename?: 'BabyRdi';
  maxMonth?: Maybe<Scalars['Int']>;
  minMonth?: Maybe<Scalars['Int']>;
  rdi?: Maybe<Scalars['Float']>;
  rdiUnit?: Maybe<Unit>;
  ul?: Maybe<Scalars['Float']>;
  ulUnit?: Maybe<Unit>;
};

export type BankCard = {
  __typename?: 'BankCard';
  bankId: Scalars['String'];
  bankInfo?: Maybe<VnBank>;
  cardName?: Maybe<Scalars['String']>;
  cardNumber: Scalars['String'];
  id?: Maybe<Scalars['Int']>;
  shopId?: Maybe<Scalars['Int']>;
};

export type BankShopCreateInput = {
  bankId: Scalars['String'];
  cardName: Scalars['String'];
  cardNumber: Scalars['String'];
};

export type BankWhereInput = {
  bankName?: InputMaybe<Scalars['String']>;
  brandName?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['Int']>;
};

export type Banner = {
  __typename?: 'Banner';
  liveStreamId: Scalars['Int'];
  thumbnailUrl: Scalars['String'];
};

export enum BannerCategoriesType {
  BannerOnBar = 'BANNER_ON_BAR',
  BannerOnCommunity = 'BANNER_ON_COMMUNITY',
  BannerOnEvent = 'BANNER_ON_EVENT',
  BannerOnHomePage = 'BANNER_ON_HOME_PAGE',
  BannerOnProduct = 'BANNER_ON_PRODUCT'
}

export type BannerImage = {
  __typename?: 'BannerImage';
  bannerType: BannerCategoriesType;
  content?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  createdUid?: Maybe<Scalars['String']>;
  height?: Maybe<Scalars['Float']>;
  id: Scalars['Int'];
  imageUrl?: Maybe<Scalars['String']>;
  redirectLink?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['Boolean']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  width?: Maybe<Scalars['Float']>;
};

export type BannerImageConnection = {
  __typename?: 'BannerImageConnection';
  aggregate: BannerImageConnectionAggregate;
};

export type BannerImageConnectionAggregate = {
  __typename?: 'BannerImageConnectionAggregate';
  count: Scalars['Int'];
};

export type BannerImageInput = {
  bannerType: Scalars['String'];
  content?: InputMaybe<Scalars['String']>;
  height?: InputMaybe<Scalars['Float']>;
  imageUrl: Scalars['String'];
  redirectLink?: InputMaybe<Scalars['String']>;
  width?: InputMaybe<Scalars['Float']>;
};

export type BannerImageOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type BannerImageUpdateInput = {
  bannerType: Scalars['String'];
  content?: InputMaybe<Scalars['String']>;
  imageUrl: Scalars['String'];
  redirectLink?: InputMaybe<Scalars['String']>;
  status: Scalars['Boolean'];
};

export type BannerImageWhereInput = {
  banner_type?: InputMaybe<Scalars['String']>;
  created_uid?: InputMaybe<Scalars['String']>;
  fromDate?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<Scalars['Boolean']>;
  toDate?: InputMaybe<Scalars['DateTime']>;
};

export type Barcode = {
  __typename?: 'Barcode';
  id: Scalars['Int'];
  value: Scalars['String'];
};

export type BarcodeOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type BarcodeWhereInput = {
  product?: InputMaybe<ProductWhereInput>;
};

export type BarcodesAggregate = {
  __typename?: 'BarcodesAggregate';
  count: Scalars['Int'];
};

export type BarcodesConnection = {
  __typename?: 'BarcodesConnection';
  aggregate: BarcodesAggregate;
};

export type BaseOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type BaumannAnswer = {
  __typename?: 'BaumannAnswer';
  questionId: Scalars['ID'];
  value: BaumannOptionValue;
};

export type BaumannAnswerCreateInput = {
  question: BaumannQuestionCreateNestedOneWithoutAnswersInput;
  value: BaumannAnswerValueInput;
};

export type BaumannAnswerValueInput = {
  booleanValue?: InputMaybe<Scalars['Boolean']>;
  stringValue?: InputMaybe<Scalars['String']>;
};

export type BaumannOption = {
  __typename?: 'BaumannOption';
  content: Scalars['String'];
  value: BaumannOptionValue;
};

export type BaumannOptionValue = BooleanBox | StringBox;

export type BaumannQuestion = {
  __typename?: 'BaumannQuestion';
  content: Scalars['String'];
  id: Scalars['ID'];
  options: Array<BaumannOption>;
};

export type BaumannQuestionCreateNestedOneWithoutAnswersInput = {
  connect: BaumannQuestionWhereUniqueInput;
};

export type BaumannQuestionWhereUniqueInput = {
  id: Scalars['ID'];
};

export enum BaumannSkinType {
  Drnt = 'DRNT',
  Drnw = 'DRNW',
  Drpt = 'DRPT',
  Drpw = 'DRPW',
  Dsnt = 'DSNT',
  Dsnw = 'DSNW',
  Dspt = 'DSPT',
  Dspw = 'DSPW',
  Ornt = 'ORNT',
  Ornw = 'ORNW',
  Orpt = 'ORPT',
  Orpw = 'ORPW',
  Osnt = 'OSNT',
  Osnw = 'OSNW',
  Ospt = 'OSPT',
  Ospw = 'OSPW'
}

export type BaumannSkinTypeFilter = {
  equals?: InputMaybe<BaumannSkinType>;
  in?: InputMaybe<Array<BaumannSkinType>>;
};

export type BooleanBox = {
  __typename?: 'BooleanBox';
  value: Scalars['Boolean'];
};

export type BooleanFilter = {
  equals?: InputMaybe<Scalars['Boolean']>;
  not?: InputMaybe<Scalars['Boolean']>;
};

export type Brand = {
  __typename?: 'Brand';
  brandAdmins?: Maybe<Array<BrandAdmin>>;
  coo?: Maybe<Scalars['String']>;
  fixedLogoUrl?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isFavouriteBrandOfViewer?: Maybe<Scalars['Boolean']>;
  isFollowed?: Maybe<Scalars['Boolean']>;
  logoUrl: Scalars['String'];
  mallId?: Maybe<Scalars['ID']>;
  nbFollowing: Scalars['Int'];
  status: BrandStatus;
  translations: Array<BrandTranslation>;
  types: Array<BrandType>;
  /** @deprecated should be removed in the future, just use id instead */
  uid: Scalars['ID'];
};


export type BrandBrandAdminsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandAdminWhereInput>;
};


export type BrandFixedLogoUrlArgs = {
  width: FixedSize;
};


export type BrandTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandTranslationWhereInput>;
};

export type BrandAdmin = {
  __typename?: 'BrandAdmin';
  bannerUrl?: Maybe<Scalars['String']>;
  brandId?: Maybe<Scalars['ID']>;
  coverUrl?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  facebookUrl?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  instagramUrl?: Maybe<Scalars['String']>;
  isFollowedByViewer?: Maybe<Scalars['Boolean']>;
  logoUrl?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  owner: User;
  ownerId: Scalars['ID'];
  promotionalProducts?: Maybe<Array<BrandPromotionalProduct>>;
  slogan?: Maybe<Scalars['String']>;
  status: BrandAdminStatus;
  storyUrl?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
  webSite?: Maybe<Scalars['String']>;
};

export enum BrandAdminStatus {
  Approved = 'APPROVED',
  Pending = 'PENDING',
  Rejected = 'REJECTED'
}

export type BrandAdminUpdateInput = {
  bannerUrl?: InputMaybe<Scalars['String']>;
  coverUrl?: InputMaybe<Scalars['String']>;
  facbookUrl?: InputMaybe<Scalars['String']>;
  instagramUrl?: InputMaybe<Scalars['String']>;
  logoUrl?: InputMaybe<Scalars['String']>;
  slogan?: InputMaybe<Scalars['String']>;
  storyUrl?: InputMaybe<Scalars['String']>;
  webSite?: InputMaybe<Scalars['String']>;
};

export type BrandAdminWhereInput = {
  brandId?: InputMaybe<Scalars['ID']>;
  id?: InputMaybe<Scalars['ID']>;
  ownerId?: InputMaybe<Scalars['ID']>;
  status?: InputMaybe<BrandAdminStatus>;
};

export type BrandAdminWhereUniqueInput = {
  id: Scalars['ID'];
};

export type BrandOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type BrandPromotionalProducWhereInput = {
  productId: Scalars['Int'];
};

export type BrandPromotionalProduct = {
  __typename?: 'BrandPromotionalProduct';
  brandAdminId: Scalars['ID'];
  product?: Maybe<Product>;
  productId: Scalars['ID'];
  productName?: Maybe<Scalars['String']>;
};

export type BrandPromotionalProductInput = {
  productId?: InputMaybe<Scalars['ID']>;
  productName?: InputMaybe<Scalars['String']>;
};

export type BrandSearchResult = {
  __typename?: 'BrandSearchResult';
  brands: Array<Brand>;
  total: Scalars['Int'];
};

export enum BrandStatus {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export type BrandStore = {
  __typename?: 'BrandStore';
  address: Scalars['String'];
  brandCode: Scalars['String'];
  code: Scalars['String'];
  id: Scalars['Int'];
  mapCoord: Scalars['String'];
  name: Scalars['String'];
  telephone: Scalars['String'];
};

export type BrandTranslation = {
  __typename?: 'BrandTranslation';
  brand: Brand;
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isOriginal: Scalars['Boolean'];
  language: LanguageCode;
  name: Scalars['String'];
  slug?: Maybe<Scalars['String']>;
};

export type BrandTranslationOrderByInput = {
  id?: InputMaybe<OrderBy>;
  name?: InputMaybe<OrderBy>;
};

export type BrandTranslationWhereInput = {
  brand?: InputMaybe<BrandWhereInput>;
  language?: InputMaybe<LanguageCode>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type BrandType = {
  __typename?: 'BrandType';
  id: Scalars['ID'];
  value: BrandTypeValue;
};

export enum BrandTypeValue {
  DepartmentStore = 'DEPARTMENT_STORE',
  DrugStore = 'DRUG_STORE',
  OnlineOther = 'ONLINE_OTHER',
  RoadStore = 'ROAD_STORE'
}

export type BrandTypeWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
  value?: InputMaybe<BrandTypeValue>;
};

export type BrandWhereInput = {
  brandIdFilters?: InputMaybe<Array<Scalars['Int']>>;
  coo?: InputMaybe<Scalars['String']>;
  followedByUser?: InputMaybe<UserWhereUniqueInput>;
  followersSome?: InputMaybe<UserWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  mallId?: InputMaybe<StringFilter>;
  productsSome?: InputMaybe<ProductWhereInput>;
  status?: InputMaybe<BrandStatus>;
  text?: InputMaybe<Scalars['String']>;
  translationsSome?: InputMaybe<BrandTranslationWhereInput>;
  type?: InputMaybe<BrandTypeWhereUniqueInput>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type BrandWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  mallId?: InputMaybe<Scalars['ID']>;
};

export type BrandsAggregate = {
  __typename?: 'BrandsAggregate';
  count: Scalars['Int'];
  following: Scalars['Int'];
};

export type BrandsConnection = {
  __typename?: 'BrandsConnection';
  aggregate: BrandsAggregate;
};

export type BrandsFollowingConnection = {
  __typename?: 'BrandsFollowingConnection';
  aggregate: BrandsFollowingsAggregate;
};

export type BrandsFollowingsAggregate = {
  __typename?: 'BrandsFollowingsAggregate';
  count: Scalars['Int'];
};

export type CartItem = {
  __typename?: 'CartItem';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  liveStreamProduct: LiveStreamProduct;
  note?: Maybe<Scalars['String']>;
  order?: Maybe<Order>;
  quantity: Scalars['Int'];
  updatedAt: Scalars['DateTime'];
};

export type CartItemCreateInput = {
  liveStreamProduct: LiveStreamProductCreateOneWithoutCartItemsInput;
  note?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<OrderCreateOneWithoutCartItemInput>;
  quantity: Scalars['Int'];
};

export type CartItemCreateManyWithoutOrderInput = {
  connect: Array<CartItemWhereUniqueInput>;
};

export type CartItemOrError = CartItem | CommonError | OrderError;

export type CartItemOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type CartItemUpdateInput = {
  note?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<OrderUpdateOneWithoutCartItemsInput>;
  quantity?: InputMaybe<Scalars['Int']>;
};

export type CartItemUpdateManyWithoutOrderInput = {
  set: Array<CartItemWhereUniqueInput>;
};

export type CartItemWhereInput = {
  liveStreamProduct?: InputMaybe<LiveStreamProductWhereInput>;
  order?: InputMaybe<OrderWhereInput>;
  user?: InputMaybe<UserWhereInput>;
};

export type CartItemWhereUniqueInput = {
  id: Scalars['Int'];
};

export type CartItemsAggregate = {
  __typename?: 'CartItemsAggregate';
  count: Scalars['Int'];
  sum: CartItemsSum;
};

export type CartItemsConnection = {
  __typename?: 'CartItemsConnection';
  aggregate: CartItemsAggregate;
};

export type CartItemsSum = {
  __typename?: 'CartItemsSum';
  price: Scalars['Int'];
};

export type CartProduct = {
  __typename?: 'CartProduct';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  quantity: Scalars['Int'];
  shop: Shop;
  shopId: Scalars['Int'];
  updatedAt?: Maybe<Scalars['DateTime']>;
  user: User;
  userId: Scalars['Int'];
  variant: ProductVariant;
  variantId: Scalars['Int'];
};

export type CartProductInsertInput = {
  quantity: Scalars['Int'];
  shopId: Scalars['Int'];
  variantId: Scalars['Int'];
};

export type CartProductOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type CartProductUpdateInput = {
  quantity?: InputMaybe<Scalars['Int']>;
};

export type CartProductWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  ids?: InputMaybe<Array<Scalars['Int']>>;
  userId?: InputMaybe<Scalars['Int']>;
  variantId?: InputMaybe<Scalars['Int']>;
};

export type CartProductsOfShop = {
  __typename?: 'CartProductsOfShop';
  cartProducts: Array<CartProduct>;
  shop: Shop;
  shopId: Scalars['Int'];
};

export type CartProductsOfShopAggregate = {
  __typename?: 'CartProductsOfShopAggregate';
  countProductsOfCart: Scalars['Int'];
};

export type CartProductsOfShopConnection = {
  __typename?: 'CartProductsOfShopConnection';
  aggregate: CartProductsOfShopAggregate;
};

export type Cast = {
  __typename?: 'Cast';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  description?: Maybe<Scalars['String']>;
  /** @deprecated use uid instead */
  id: Scalars['Int'];
  name: Scalars['String'];
  products: Array<Product>;
  savedByViewer: Scalars['Boolean'];
  tags: Array<Tag>;
  thumbnailUrl: Scalars['String'];
  uid: Scalars['ID'];
  updatedAt: Scalars['DateTime'];
};


export type CastProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};

export type CastAggregate = {
  __typename?: 'CastAggregate';
  count: Scalars['Int'];
};

export type CastComment = {
  __typename?: 'CastComment';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type CastCommentCreateInput = {
  cast: CastCreateOneWithoutCommentsInput;
  content: Scalars['String'];
};

export type CastCommentOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type CastCommentWhereInput = {
  cast: CastWhereUniqueInput;
};

export type CastCommentsAggregate = {
  __typename?: 'CastCommentsAggregate';
  count: Scalars['Int'];
};

export type CastCommentsConnection = {
  __typename?: 'CastCommentsConnection';
  aggregate: CastCommentsAggregate;
};

export type CastConnection = {
  __typename?: 'CastConnection';
  aggregate: CastAggregate;
};

export type CastCreateOneWithoutCommentsInput = {
  connect: CastWhereUniqueInput;
};

export type CastOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type CastWhereInput = {
  nameContains?: InputMaybe<Scalars['String']>;
};

export type CastWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type CategoriesAggregate = {
  __typename?: 'CategoriesAggregate';
  count: Scalars['Int'];
};

export type CategoriesConnection = {
  __typename?: 'CategoriesConnection';
  aggregate: CategoriesAggregate;
};

export type Category = {
  __typename?: 'Category';
  activeLogo?: Maybe<Image>;
  children?: Maybe<Array<Category>>;
  id: Scalars['Int'];
  inactiveLogo?: Maybe<Image>;
  parent?: Maybe<Category>;
  productRankings?: Maybe<Array<ProductRanking>>;
  productRankingsConnection: ProductRankingsConnection;
  productRankingsV2?: Maybe<Array<ProductRanking>>;
  reviewQuestionSetId?: Maybe<Scalars['ID']>;
  status: CategoryStatus;
  translations: Array<CategoryTranslation>;
  uid: Scalars['ID'];
};


export type CategoryChildrenArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  idFilters?: InputMaybe<Array<Scalars['Int']>>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CategoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CategoryWhereInput>;
};


export type CategoryProductRankingsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type CategoryProductRankingsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type CategoryProductRankingsV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type CategoryTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CategoryTranslationWhereInput>;
};

export type CategoryOrderByInput = {
  id?: InputMaybe<OrderBy>;
  mixedRows?: InputMaybe<Scalars['Boolean']>;
};

export type CategorySearchResult = {
  __typename?: 'CategorySearchResult';
  categories: Array<Category>;
  total: Scalars['Int'];
};

export enum CategoryStatus {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export type CategoryTranslation = {
  __typename?: 'CategoryTranslation';
  id: Scalars['Int'];
  isOriginal: Scalars['Boolean'];
  language: LanguageCode;
  name: Scalars['String'];
  slug?: Maybe<Scalars['String']>;
};

export type CategoryTranslationWhereInput = {
  language?: InputMaybe<LanguageCode>;
  name?: InputMaybe<StringFilter>;
  slug?: InputMaybe<Scalars['String']>;
};

export type CategoryWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  idNot?: InputMaybe<Scalars['Int']>;
  isLeaf?: InputMaybe<Scalars['Boolean']>;
  parent?: InputMaybe<CategoryWhereUniqueInput>;
  productType?: InputMaybe<ProductType>;
  status?: InputMaybe<CategoryStatus>;
  translationsSome?: InputMaybe<CategoryTranslationWhereInput>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type CategoryWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  idFilters?: InputMaybe<Array<Scalars['Int']>>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type CautionWhereInput = {
  id?: InputMaybe<IntFilter>;
  productType?: InputMaybe<ProductType>;
  productsSome?: InputMaybe<ProductWhereInput>;
  translationsSome?: InputMaybe<IngredientCautionTranslationWhereInput>;
};

export type CautionWhereUniqueInput = {
  id: Scalars['Int'];
};

export type Channel = {
  __typename?: 'Channel';
  id: Scalars['ID'];
};

export enum ChannelCommand {
  Start = 'START',
  Status = 'STATUS',
  Stop = 'STOP'
}

export type ChannelError = Error & {
  __typename?: 'ChannelError';
  command: ChannelCommand;
  id?: Maybe<Scalars['String']>;
  message: Scalars['String'];
  status: ErrorStatus;
};

export type ChannelOrError = Channel | ChannelError | CommonError;

export type ChatNotificationInput = {
  message: Scalars['String'];
  receiver: UserWhereUniqueInput;
};

export type CheckInPayload = {
  __typename?: 'CheckInPayload';
  checkInList: Array<Scalars['Int']>;
  isCheckedInToday: Scalars['Boolean'];
  todayIndex: Scalars['Int'];
};

export type CommonError = Error & {
  __typename?: 'CommonError';
  code: CommonErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum CommonErrorCode {
  Forbidden = 'FORBIDDEN',
  InternalServerError = 'INTERNAL_SERVER_ERROR',
  NotFound = 'NOT_FOUND'
}

export enum Currency {
  Vnd = 'VND'
}

export type DateTimeFilter = {
  equals?: InputMaybe<Scalars['DateTime']>;
  gt?: InputMaybe<Scalars['DateTime']>;
  gte?: InputMaybe<Scalars['DateTime']>;
  in?: InputMaybe<Array<Scalars['DateTime']>>;
  lt?: InputMaybe<Scalars['DateTime']>;
  lte?: InputMaybe<Scalars['DateTime']>;
  not?: InputMaybe<Scalars['DateTime']>;
  notIn?: InputMaybe<Array<Scalars['DateTime']>>;
};

export enum DeliverOption {
  None = 'NONE',
  Xteam = 'XTEAM'
}

export type DeliveryHistory = {
  __typename?: 'DeliveryHistory';
  createdAt: Scalars['DateTime'];
  detail?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  orderId: Scalars['Int'];
  status: Scalars['Int'];
  statusText: Scalars['String'];
};

export type DeliveryHistoryOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type DeliveryHistoryWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  order_id?: InputMaybe<Scalars['Int']>;
};

export type Device = {
  __typename?: 'Device';
  deviceType: DeviceType;
  id: Scalars['Int'];
  token: Scalars['String'];
};

export type DeviceCreateInput = {
  deviceType: DeviceType;
  token: Scalars['String'];
};

export enum DeviceType {
  Android = 'ANDROID',
  Ios = 'IOS'
}

export type Districts = {
  __typename?: 'Districts';
  code: Scalars['String'];
  id: Scalars['Int'];
  name: Scalars['String'];
  province_id: Scalars['Int'];
};

export type DistrictsWhereInput = {
  provinceId: Scalars['Int'];
};

export type EcommOrderCreateError = Error & {
  __typename?: 'EcommOrderCreateError';
  code: EcommOrderCreateErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum EcommOrderCreateErrorCode {
  AddressOfShopOrShopNotExist = 'ADDRESS_OF_SHOP_OR_SHOP_NOT_EXIST',
  DeliveryAddressError = 'DELIVERY_ADDRESS_ERROR',
  NotEnoughProduct = 'NOT_ENOUGH_PRODUCT',
  TotalAmountIsLess_10000OrMore_20000000 = 'TOTAL_AMOUNT_IS_LESS_10000_OR_MORE_20000000'
}

export type EcommerceOrder = {
  __typename?: 'EcommerceOrder';
  allowPayingForShop?: Maybe<Scalars['Boolean']>;
  buyer: User;
  buyerAddress: Scalars['String'];
  buyerId: Scalars['Int'];
  buyerName: Scalars['String'];
  buyerPhone: Scalars['String'];
  completedDate?: Maybe<Scalars['DateTime']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  deliveryHistory?: Maybe<Array<DeliveryHistory>>;
  ecommFee?: Maybe<Scalars['Float']>;
  estimatedDeliverTime?: Maybe<Scalars['String']>;
  estimatedPickTime?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isPaid?: Maybe<Scalars['Boolean']>;
  orderCode: Scalars['String'];
  paymentMethod?: Maybe<Scalars['String']>;
  productVariantsByOrderId?: Maybe<Array<EcommerceOrderProduct>>;
  reviewtyDiscount?: Maybe<Scalars['Float']>;
  sellerAddress?: Maybe<Scalars['String']>;
  shippingFee?: Maybe<Scalars['Float']>;
  shop: Shop;
  shopDiscount?: Maybe<Scalars['Float']>;
  shopId: Scalars['Int'];
  status?: Maybe<EcommerceOrderStatus>;
  statusText?: Maybe<Scalars['String']>;
  totalAmount?: Maybe<Scalars['Float']>;
  transportationCode?: Maybe<Scalars['String']>;
  vnpayUrl?: Maybe<Scalars['String']>;
};


export type EcommerceOrderDeliveryHistoryArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<DeliveryHistoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<DeliveryHistoryWhereInput>;
};


export type EcommerceOrderProductVariantsByOrderIdArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderProductWhereInput>;
};

export type EcommerceOrderAggregate = {
  __typename?: 'EcommerceOrderAggregate';
  countEcommerceOrder: Scalars['Int'];
};

export type EcommerceOrderCancelInput = {
  id: Scalars['Int'];
  reason?: InputMaybe<Scalars['String']>;
};

export type EcommerceOrderConfirmInput = {
  ids: Array<Scalars['Int']>;
};

export type EcommerceOrderConnection = {
  __typename?: 'EcommerceOrderConnection';
  aggregate: EcommerceOrderAggregate;
};

export type EcommerceOrderInput = {
  bankCode?: InputMaybe<Scalars['String']>;
  buyerAddress: Scalars['String'];
  buyerName: Scalars['String'];
  buyerPhone: Scalars['String'];
  freeshipPromotions?: InputMaybe<Array<Scalars['Int']>>;
  paymentMethod: PaymentMethod;
  reviewtyPromotions?: InputMaybe<Array<Scalars['Int']>>;
  shopOrders: Array<ShopOrderInput>;
};

export type EcommerceOrderOrderByInput = {
  id?: InputMaybe<OrderBy>;
  shopId?: InputMaybe<OrderBy>;
};

export type EcommerceOrderProduct = {
  __typename?: 'EcommerceOrderProduct';
  id: Scalars['Int'];
  orderId: Scalars['Int'];
  price: Scalars['Float'];
  quantity: Scalars['Int'];
  review?: Maybe<EcommerceReview>;
  totalPrice: Scalars['Float'];
  variant?: Maybe<ProductVariant>;
  variantId: Scalars['Int'];
};


export type EcommerceOrderProductReviewArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceReviewWhereInput>;
};

export type EcommerceOrderProductInput = {
  price: Scalars['Float'];
  quantity: Scalars['Int'];
  variantId: Scalars['Int'];
};

export type EcommerceOrderProductOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type EcommerceOrderProductWhereInput = {
  shopId?: InputMaybe<Scalars['Int']>;
  variantId?: InputMaybe<Scalars['Int']>;
};

export enum EcommerceOrderStatus {
  Cancelled = 'CANCELLED',
  Completed = 'COMPLETED',
  Delivering = 'DELIVERING',
  InProcessing = 'IN_PROCESSING',
  PaymentError = 'PAYMENT_ERROR',
  Refunded = 'REFUNDED',
  Returns = 'RETURNS',
  SystemError = 'SYSTEM_ERROR',
  WaitConfirming = 'WAIT_CONFIRMING',
  WaitDelivering = 'WAIT_DELIVERING',
  WaitRefunding = 'WAIT_REFUNDING'
}

export enum EcommerceOrderStatusFilter {
  Cancelled = 'CANCELLED',
  Completed = 'COMPLETED',
  Delivering = 'DELIVERING',
  ReturnsOrRefund = 'RETURNS_OR_REFUND',
  WaitConfirming = 'WAIT_CONFIRMING',
  WaitDelivering = 'WAIT_DELIVERING'
}

export type EcommerceOrderWhereInput = {
  allowPayingForShop?: InputMaybe<Scalars['Boolean']>;
  buyerId?: InputMaybe<Scalars['Int']>;
  buyerName?: InputMaybe<Scalars['String']>;
  fromDate?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['Int']>;
  isFinance?: InputMaybe<Scalars['Boolean']>;
  orderCode?: InputMaybe<Scalars['String']>;
  paymentMethod?: InputMaybe<PaymentMethod>;
  periodType?: InputMaybe<PeriodType>;
  productName?: InputMaybe<Scalars['String']>;
  shopId?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<EcommerceOrderStatusFilter>;
  textSearch?: InputMaybe<Scalars['String']>;
  toDate?: InputMaybe<Scalars['DateTime']>;
  userId?: InputMaybe<Scalars['Int']>;
  variantId?: InputMaybe<Scalars['Int']>;
};

export type EcommerceProductIdsWhereInput = {
  ids?: InputMaybe<Array<Scalars['Int']>>;
};

export type EcommercePromotion = {
  __typename?: 'EcommercePromotion';
  amount?: Maybe<Scalars['Float']>;
  amountUnit?: Maybe<PromotionAmountUnitType>;
  code?: Maybe<Scalars['String']>;
  content?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  createdBy?: Maybe<PromotionCreatedByType>;
  entity?: Maybe<Scalars['Int']>;
  entityObject?: Maybe<EcommercePromotionEntityUnion>;
  entityType?: Maybe<PromotionEntityType>;
  fromTime?: Maybe<Scalars['DateTime']>;
  id: Scalars['Int'];
  images?: Maybe<Array<Scalars['String']>>;
  maxUsingTimes?: Maybe<Scalars['Float']>;
  maximumPrice?: Maybe<Scalars['Float']>;
  minimumPrice?: Maybe<Scalars['Float']>;
  quantity?: Maybe<Scalars['Float']>;
  redirectUrl?: Maybe<Scalars['String']>;
  shopId?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  toTime?: Maybe<Scalars['DateTime']>;
  type: PromotionType;
  usedTimes?: Maybe<Scalars['Float']>;
  userId?: Maybe<Scalars['Int']>;
};

export type EcommercePromotionEntityUnion = Shop | ShopProduct;

export type EcommercePromotionFilterWhereInput = {
  createdBy?: InputMaybe<PromotionCreatedByType>;
  id?: InputMaybe<Scalars['Int']>;
  isExpired?: InputMaybe<Scalars['Boolean']>;
};

export type EcommercePromotionInsertInput = {
  amount?: InputMaybe<Scalars['Float']>;
  amountUnit?: InputMaybe<PromotionAmountUnitType>;
  code?: InputMaybe<Scalars['String']>;
  content?: InputMaybe<Scalars['String']>;
  entity?: InputMaybe<Scalars['Int']>;
  entityType: PromotionEntityType;
  fromTime?: InputMaybe<Scalars['DateTime']>;
  images?: InputMaybe<Array<Scalars['String']>>;
  maxUsingTimes?: InputMaybe<Scalars['Float']>;
  maximumPrice?: InputMaybe<Scalars['Float']>;
  minimumPrice?: InputMaybe<Scalars['Float']>;
  quantity?: InputMaybe<Scalars['Float']>;
  redirectUrl?: InputMaybe<Scalars['String']>;
  shopId?: InputMaybe<Scalars['Int']>;
  title?: InputMaybe<Scalars['String']>;
  toTime?: InputMaybe<Scalars['DateTime']>;
  type: PromotionType;
  userId?: InputMaybe<Scalars['Int']>;
};

export type EcommercePromotionUpdateInput = {
  amount?: InputMaybe<Scalars['Float']>;
  amountUnit?: InputMaybe<PromotionAmountUnitType>;
  code?: InputMaybe<Scalars['String']>;
  content?: InputMaybe<Scalars['String']>;
  entity?: InputMaybe<Scalars['Int']>;
  entityType: PromotionEntityType;
  fromTime?: InputMaybe<Scalars['DateTime']>;
  id: Scalars['Int'];
  images?: InputMaybe<Array<Scalars['String']>>;
  maxUsingTimes?: InputMaybe<Scalars['Float']>;
  maximumPrice?: InputMaybe<Scalars['Float']>;
  minimumPrice?: InputMaybe<Scalars['Float']>;
  quantity?: InputMaybe<Scalars['Float']>;
  redirectUrl?: InputMaybe<Scalars['String']>;
  shopId?: InputMaybe<Scalars['Int']>;
  title?: InputMaybe<Scalars['String']>;
  toTime?: InputMaybe<Scalars['DateTime']>;
  type: PromotionType;
  userId?: InputMaybe<Scalars['Int']>;
};

export type EcommercePromotionWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
};

export type EcommerceReview = {
  __typename?: 'EcommerceReview';
  comment?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  id: Scalars['Int'];
  images?: Maybe<Array<Image>>;
  orderId?: Maybe<Scalars['Int']>;
  productId?: Maybe<Scalars['Int']>;
  rate?: Maybe<Scalars['Int']>;
  reviewer: User;
  updatedAt?: Maybe<Scalars['DateTime']>;
  userId?: Maybe<Scalars['Int']>;
  variant?: Maybe<ProductVariant>;
  variantId?: Maybe<Scalars['Int']>;
};

export type EcommerceReviewAggregate = {
  __typename?: 'EcommerceReviewAggregate';
  countEcommerceReview: Scalars['Int'];
};

export type EcommerceReviewConnection = {
  __typename?: 'EcommerceReviewConnection';
  aggregate: EcommerceReviewAggregate;
};

export type EcommerceReviewInsertInput = {
  orderId: Scalars['Int'];
  variantReviewInfo: Array<VariantReviewInsertInput>;
};

export type EcommerceReviewUpdateInput = {
  comment?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<Array<ImageReviewInput>>;
  rate?: InputMaybe<Scalars['Int']>;
};

export type EcommerceReviewWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  orderId?: InputMaybe<Scalars['Int']>;
  productId?: InputMaybe<Scalars['Int']>;
  rate?: InputMaybe<IntFilter>;
  variantId?: InputMaybe<Scalars['Int']>;
};

export type EcommerceShippingFee = {
  __typename?: 'EcommerceShippingFee';
  fee: Scalars['Float'];
  shopId: Scalars['Int'];
};

export enum EcommerceSite {
  Lazada = 'LAZADA',
  Shopee = 'SHOPEE',
  Tiki = 'TIKI'
}

export type EmailError = Error & {
  __typename?: 'EmailError';
  code: EmailErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum EmailErrorCode {
  EmailAddressAlreadyBeingUsed = 'EMAIL_ADDRESS_ALREADY_BEING_USED'
}

export type EmailOrError = CommonError | EmailError | EmailPayload;

export type EmailPayload = {
  __typename?: 'EmailPayload';
  email: Scalars['String'];
};

export type Error = {
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum ErrorStatus {
  BadRequest = 'BAD_REQUEST',
  Forbidden = 'FORBIDDEN',
  InternalServerError = 'INTERNAL_SERVER_ERROR',
  NotFound = 'NOT_FOUND',
  Unauthorized = 'UNAUTHORIZED',
  Unprocessable = 'UNPROCESSABLE'
}

export type EvaluationAggregate = {
  __typename?: 'EvaluationAggregate';
  countEvaluation: Scalars['Int'];
};

export type EvaluationConnection = {
  __typename?: 'EvaluationConnection';
  aggregate: EvaluationAggregate;
};

export type Event = {
  __typename?: 'Event';
  commentsConnection: EventCommentConnection;
  condition: Scalars['String'];
  content: Scalars['String'];
  coverUrl: Scalars['String'];
  coverUrlFunding?: Maybe<Scalars['String']>;
  currentNumberOfWinners: Scalars['Int'];
  endedAt: Scalars['DateTime'];
  eventToProducts: Array<EventToProduct>;
  id: Scalars['Int'];
  isParticipantOfCurrentUser?: Maybe<Scalars['Boolean']>;
  /** @deprecated use maximumNumberOfWinners instead */
  maximumNumberOfParticipants?: Maybe<Scalars['Int']>;
  /** @deprecated use numberOfWinner instead */
  maximumNumberOfWinners?: Maybe<Scalars['Int']>;
  minimumNumberOfReviews?: Maybe<Scalars['Int']>;
  name: Scalars['String'];
  notifications: Array<EventNotification>;
  numberOfWinners?: Maybe<Scalars['Int']>;
  points?: Maybe<Scalars['Int']>;
  products: Array<Product>;
  productsCount: Scalars['Int'];
  referred_url?: Maybe<Scalars['String']>;
  reminderDates?: Maybe<Array<Scalars['DateTime']>>;
  reviewDeadline?: Maybe<Scalars['DateTime']>;
  startedAt: Scalars['DateTime'];
  tags: Array<Tag>;
  type: EventType;
  /** @deprecated please use id instead */
  uid: Scalars['ID'];
  visible: Scalars['Boolean'];
};


export type EventCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventCommentWhereInput>;
};


export type EventProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};

export type EventAggregate = {
  __typename?: 'EventAggregate';
  count: Scalars['Int'];
};

export type EventComment = {
  __typename?: 'EventComment';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  event: Event;
  id: Scalars['Int'];
  product?: Maybe<Product>;
  shippingAddress: ShippingAddress;
  status: EventCommentStatus;
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type EventCommentAggregate = {
  __typename?: 'EventCommentAggregate';
  count: Scalars['Int'];
};

export type EventCommentConnection = {
  __typename?: 'EventCommentConnection';
  aggregate: EventCommentAggregate;
};

export type EventCommentCreateError = Error & {
  __typename?: 'EventCommentCreateError';
  code: EventCommentCreateErrorCode;
  comments?: Maybe<Array<EventComment>>;
  currentValue?: Maybe<Scalars['Int']>;
  expectedValue?: Maybe<Scalars['Int']>;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum EventCommentCreateErrorCode {
  AlreadyParticipatedEvent = 'ALREADY_PARTICIPATED_EVENT',
  ExceedMaxParticipants = 'EXCEED_MAX_PARTICIPANTS',
  HaveNotLinkedSocialMedia = 'HAVE_NOT_LINKED_SOCIAL_MEDIA',
  HaveNotWrittenReviewForTrialProducts = 'HAVE_NOT_WRITTEN_REVIEW_FOR_TRIAL_PRODUCTS',
  NotEnoughPoints = 'NOT_ENOUGH_POINTS',
  NotEnoughReviews = 'NOT_ENOUGH_REVIEWS',
  ShippingAddressNotExisted = 'SHIPPING_ADDRESS_NOT_EXISTED'
}

export type EventCommentCreateInput = {
  content: Scalars['String'];
  event: EventWhereUniqueInput;
  isNewApi?: InputMaybe<Scalars['Boolean']>;
  usePoints?: InputMaybe<Scalars['Boolean']>;
};

export type EventCommentOrError = CommonError | EventComment | EventCommentCreateError;

export type EventCommentOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export enum EventCommentStatus {
  Cancelled = 'CANCELLED',
  Confirmed = 'CONFIRMED',
  Created = 'CREATED'
}

export type EventCommentWhereInput = {
  event?: InputMaybe<EventWhereInput>;
  status?: InputMaybe<EventCommentStatus>;
  user?: InputMaybe<UserWhereInput>;
};

export type EventCommentWhereUniqueInput = {
  id: Scalars['Int'];
};

export type EventConnection = {
  __typename?: 'EventConnection';
  aggregate: EventAggregate;
};

export type EventNotification = {
  __typename?: 'EventNotification';
  content: Scalars['String'];
  id: Scalars['String'];
  notifiedAt?: Maybe<Scalars['DateTime']>;
  title: Scalars['String'];
};

export type EventOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type EventToProduct = {
  __typename?: 'EventToProduct';
  quantity?: Maybe<Scalars['Int']>;
};

export enum EventType {
  Sale = 'SALE',
  Trial = 'TRIAL'
}

export type EventWhereInput = {
  conditionContains?: InputMaybe<Scalars['String']>;
  hasReferredUrl?: InputMaybe<Scalars['Boolean']>;
  id?: InputMaybe<Scalars['Int']>;
  isExpired?: InputMaybe<Scalars['Boolean']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  nameContains?: InputMaybe<Scalars['String']>;
  productsSome?: InputMaybe<ProductWhereInput>;
  type?: InputMaybe<EventType>;
  visible?: InputMaybe<Scalars['Boolean']>;
};

export type EventWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export enum EwgRiskType {
  High = 'HIGH',
  Low = 'LOW',
  Medium = 'MEDIUM',
  Unknown = 'UNKNOWN'
}

export type ExternalLink = {
  __typename?: 'ExternalLink';
  affiliateUrl?: Maybe<Scalars['String']>;
  image?: Maybe<Image>;
  title: Scalars['String'];
  url: Scalars['String'];
};

export type ExternalLinkCreateManyWithoutShopInput = {
  create: Array<ExternalLinkCreateWithoutShopInput>;
};

export type ExternalLinkCreateManyWithoutShopToProductInput = {
  create: Array<ExternalLinkCreateWithoutShopToProductInput>;
};

export type ExternalLinkCreateWithoutShopInput = {
  affiliateUrl?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageCreateOneWithoutExternalLinkInput>;
  title: Scalars['String'];
  url: Scalars['String'];
};

export type ExternalLinkCreateWithoutShopToProductInput = {
  affiliateUrl?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageCreateOneWithoutExternalLinkInput>;
  title: Scalars['String'];
  url: Scalars['String'];
};

export type ExternalLinkUpdateManyWithoutShopInput = {
  create: Array<ExternalLinkCreateWithoutShopInput>;
};

export type ExternalLinkUpdateManyWithoutShopToProductInput = {
  create: Array<ExternalLinkCreateWithoutShopToProductInput>;
};

export enum ExternalProvider {
  Lazada = 'LAZADA',
  Other = 'OTHER',
  Sendo = 'SENDO',
  Shopee = 'SHOPEE',
  Tiki = 'TIKI'
}

export type Feature = {
  __typename?: 'Feature';
  liveStream?: Maybe<Scalars['Boolean']>;
  video?: Maybe<Scalars['Boolean']>;
};

export enum FixedSize {
  Large = 'LARGE',
  Medium = 'MEDIUM',
  Small = 'SMALL'
}

export type FollowersAggregate = {
  __typename?: 'FollowersAggregate';
  count: Scalars['Int'];
};

export type FollowersConnection = {
  __typename?: 'FollowersConnection';
  aggregate: FollowersAggregate;
};

export type FollowingsAggregate = {
  __typename?: 'FollowingsAggregate';
  count: Scalars['Int'];
};

export type FollowingsConnection = {
  __typename?: 'FollowingsConnection';
  aggregate: FollowingsAggregate;
};

export type Function = {
  __typename?: 'Function';
  id: Scalars['Int'];
  productType: ProductType;
  symbolUrl?: Maybe<Scalars['String']>;
  translations: Array<FunctionTranslation>;
  type: FunctionType;
  uid: Scalars['ID'];
};


export type FunctionTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionTranslationWhereInput>;
};

export type FunctionOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type FunctionProductUpsertInput = {
  ids?: InputMaybe<Array<FuntionProductInputType>>;
};

export type FunctionProducts = {
  __typename?: 'FunctionProducts';
  ids?: Maybe<Array<Function>>;
};

export type FunctionTranslation = {
  __typename?: 'FunctionTranslation';
  description?: Maybe<Scalars['String']>;
  function: Function;
  id: Scalars['Int'];
  language: LanguageCode;
  name: Scalars['String'];
};

export type FunctionTranslationWhereInput = {
  function?: InputMaybe<FunctionWhereInput>;
  language?: InputMaybe<LanguageCode>;
  name?: InputMaybe<StringFilter>;
};

export enum FunctionType {
  Advantage = 'ADVANTAGE',
  Disadvantage = 'DISADVANTAGE',
  Recommendation = 'RECOMMENDATION'
}

export type FunctionWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  ingredientToFunctionsSome?: InputMaybe<IngredientToFunctionWhereInput>;
  ingredientsSome?: InputMaybe<IngredientWhereInput>;
  productType?: InputMaybe<ProductType>;
  translationsSome?: InputMaybe<FunctionTranslationWhereInput>;
  type?: InputMaybe<FunctionType>;
};

export type FunctionWhereUniqueInput = {
  id: Scalars['Int'];
};

export type FunctionsAggregate = {
  __typename?: 'FunctionsAggregate';
  count: Scalars['Int'];
};

export type FunctionsConnection = {
  __typename?: 'FunctionsConnection';
  aggregate: FunctionsAggregate;
};

export type Funding = {
  __typename?: 'Funding';
  condition?: Maybe<Scalars['String']>;
  coverUrl?: Maybe<Scalars['String']>;
  endDate?: Maybe<Scalars['DateTime']>;
  event?: Maybe<Event>;
  eventId?: Maybe<Scalars['Int']>;
  freeShip?: Maybe<Scalars['Boolean']>;
  fundingProducts: Array<FundingProduct>;
  id: Scalars['Int'];
  isVisible?: Maybe<Scalars['Boolean']>;
  longDescription?: Maybe<Scalars['String']>;
  minimumNumberOfPoints?: Maybe<Scalars['Int']>;
  minimumNumberOfReviews?: Maybe<Scalars['Int']>;
  nbOfPreOrder?: Maybe<Scalars['Int']>;
  preOrder?: Maybe<Scalars['Boolean']>;
  preOrderQuantity?: Maybe<Scalars['Int']>;
  productNbOfPreOrder?: Maybe<Scalars['Int']>;
  shortDescription?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  subTitle?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
};

export type FundingCreateInput = {
  condition?: InputMaybe<Scalars['String']>;
  coverUrl: Scalars['String'];
  endDate: Scalars['DateTime'];
  freeShip?: InputMaybe<Scalars['Boolean']>;
  fundingProducts?: InputMaybe<Array<FundingProductUpsertInput>>;
  longDescription: Scalars['String'];
  minimumNumberOfPoints?: InputMaybe<Scalars['Float']>;
  minimumNumberOfReviews?: InputMaybe<Scalars['Float']>;
  preOrder?: InputMaybe<Scalars['Boolean']>;
  preOrderQuantity?: InputMaybe<Scalars['Int']>;
  shortDescription: Scalars['String'];
  startDate: Scalars['DateTime'];
  subTitle?: InputMaybe<Scalars['String']>;
  title: Scalars['String'];
};

export type FundingOrder = {
  __typename?: 'FundingOrder';
  createdAt?: Maybe<Scalars['DateTime']>;
  deliveryAddress?: Maybe<Scalars['String']>;
  deliveryFee?: Maybe<Scalars['Float']>;
  deliveryFeeInfo?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['Float']>;
  estimatedDeliverTime?: Maybe<Scalars['String']>;
  estimatedPickTime?: Maybe<Scalars['String']>;
  freeShip?: Maybe<Scalars['Boolean']>;
  fundingId: Scalars['Int'];
  fundingProducts: Array<FundingProduct>;
  id: Scalars['Int'];
  insuranceFee?: Maybe<Scalars['Float']>;
  isPaid?: Maybe<Scalars['Boolean']>;
  orderEvaluations?: Maybe<Array<FundingOrderEvaluation>>;
  paymentMethod?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
  pickAddress?: Maybe<Scalars['String']>;
  preOrder?: Maybe<Scalars['Boolean']>;
  productId: Scalars['Int'];
  quantity?: Maybe<Scalars['Int']>;
  shipmentHistory?: Maybe<Array<ShipmentHistory>>;
  shipmentStatus?: Maybe<Scalars['Int']>;
  shipmentStatusText?: Maybe<Scalars['String']>;
  status?: Maybe<FundingOrderStatus>;
  statusText?: Maybe<Scalars['String']>;
  totalAmount?: Maybe<Scalars['Float']>;
  transportId?: Maybe<Scalars['String']>;
  userId: Scalars['Int'];
  userName?: Maybe<Scalars['String']>;
  userOrder: User;
};


export type FundingOrderShipmentHistoryArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShipmentHistoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShipmentHistoryWhereInput>;
};

export type FundingOrderAggregate = {
  __typename?: 'FundingOrderAggregate';
  countFundingOrder: Scalars['Int'];
};

export type FundingOrderByInput = {
  id?: InputMaybe<OrderBy>;
  startDate?: InputMaybe<OrderBy>;
};

export type FundingOrderConnection = {
  __typename?: 'FundingOrderConnection';
  aggregate: FundingOrderAggregate;
};

export type FundingOrderEvaluation = {
  __typename?: 'FundingOrderEvaluation';
  comment?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  fundingOrder: FundingOrder;
  id: Scalars['Int'];
  images?: Maybe<Array<Image>>;
  orderId?: Maybe<Scalars['Int']>;
  productId?: Maybe<Scalars['Int']>;
  rate?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type FundingOrderEvaluationInput = {
  comment?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<Array<ImageInput>>;
  orderId: Scalars['Int'];
  productId: Scalars['Int'];
  rate?: InputMaybe<Scalars['Int']>;
};

export type FundingOrderEvaluationUpdateInput = {
  comment?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<Array<ImageInput>>;
  rate?: InputMaybe<Scalars['Int']>;
};

export type FundingOrderEvaluationWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  orderId?: InputMaybe<Scalars['Int']>;
  productId?: InputMaybe<Scalars['Int']>;
  rate?: InputMaybe<IntFilter>;
  userId?: InputMaybe<Scalars['Int']>;
};

export type FundingOrderInput = {
  bankCode?: InputMaybe<Scalars['String']>;
  deliveryAddress: Scalars['String'];
  deliveryFee: Scalars['Float'];
  deliveryFeeInfo?: InputMaybe<Scalars['String']>;
  discount: Scalars['Float'];
  freeShip?: InputMaybe<Scalars['Boolean']>;
  fundingId: Scalars['Int'];
  insuranceFee?: InputMaybe<Scalars['Float']>;
  paymentMethod: PaymentMethod;
  preOrder?: InputMaybe<Scalars['Boolean']>;
  productId: Scalars['Int'];
  quantity: Scalars['Int'];
  telephone: Scalars['String'];
  userName: Scalars['String'];
};

export type FundingOrderOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type FundingOrderStatisticWhereInput = {
  fromDate?: InputMaybe<Scalars['DateTime']>;
  fundingId?: InputMaybe<Scalars['Int']>;
  groupBy?: InputMaybe<GroupByType>;
  keyword?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<FundingOrderType>;
  toDate?: InputMaybe<Scalars['DateTime']>;
  userId?: InputMaybe<Scalars['Int']>;
};

export enum FundingOrderStatus {
  Cancelled = 'CANCELLED',
  Completed = 'COMPLETED',
  Delivering = 'DELIVERING',
  InProcessing = 'IN_PROCESSING',
  PaymentError = 'PAYMENT_ERROR',
  Refunded = 'REFUNDED',
  SystemError = 'SYSTEM_ERROR',
  WaitDelivering = 'WAIT_DELIVERING',
  WaitRefunding = 'WAIT_REFUNDING'
}

export enum FundingOrderType {
  Cancel = 'CANCEL',
  Completed = 'COMPLETED',
  Delivering = 'DELIVERING',
  InProcessing = 'IN_PROCESSING',
  WaitDelivering = 'WAIT_DELIVERING'
}

export type FundingOrderWhereInput = {
  fundingId?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['Int']>;
  preOrder?: InputMaybe<Scalars['Boolean']>;
  productId?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<FundingOrderStatus>;
  userId?: InputMaybe<Scalars['Int']>;
};

export type FundingProduct = {
  __typename?: 'FundingProduct';
  discount?: Maybe<Scalars['Float']>;
  funding: Funding;
  fundingID?: Maybe<Scalars['Int']>;
  id: Scalars['Int'];
  product?: Maybe<Product>;
  productEvaluation?: Maybe<Array<FundingOrderEvaluation>>;
  productId?: Maybe<Scalars['Int']>;
  quantity?: Maybe<Scalars['Int']>;
  totalQuantity?: Maybe<Scalars['Int']>;
};

export type FundingProductUpsertInput = {
  discount?: InputMaybe<Scalars['Float']>;
  productId?: InputMaybe<Scalars['Int']>;
  quantity?: InputMaybe<Scalars['Int']>;
};

export enum FundingStatus {
  Created = 'CREATED',
  Running = 'RUNNING',
  Stopped = 'STOPPED'
}

export type FundingUpdateInput = {
  condition?: InputMaybe<Scalars['String']>;
  coverUrl?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Scalars['DateTime']>;
  freeShip?: InputMaybe<Scalars['Boolean']>;
  fundingProducts?: InputMaybe<Array<FundingProductUpsertInput>>;
  longDescription?: InputMaybe<Scalars['String']>;
  minimumNumberOfPoints?: InputMaybe<Scalars['Float']>;
  minimumNumberOfReviews?: InputMaybe<Scalars['Float']>;
  preOrder?: InputMaybe<Scalars['Boolean']>;
  preOrderQuantity?: InputMaybe<Scalars['Int']>;
  shortDescription?: InputMaybe<Scalars['String']>;
  startDate?: InputMaybe<Scalars['DateTime']>;
  subTitle?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
};

export type FundingWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  objectType?: InputMaybe<ObjectType>;
  productId?: InputMaybe<Scalars['Int']>;
  statusFilter?: InputMaybe<FundingStatus>;
  textSearch?: InputMaybe<Scalars['String']>;
};

export type FundingsAggregate = {
  __typename?: 'FundingsAggregate';
  countFunding: Scalars['Int'];
};

export type FundingsConnection = {
  __typename?: 'FundingsConnection';
  aggregate: FundingsAggregate;
};

export type FuntionProductInputType = {
  id: Scalars['Int'];
};

export enum Gender {
  Female = 'FEMALE',
  Male = 'MALE',
  Other = 'OTHER'
}

export type GoodsList = {
  __typename?: 'GoodsList';
  brandCode: Scalars['String'];
  brandLogo?: Maybe<Scalars['String']>;
  brandName: Scalars['String'];
  cateCode: Scalars['String'];
  commtGuide: Scalars['String'];
  commtGuideEn: Scalars['String'];
  commtGuideKr: Scalars['String'];
  commtProduct: Scalars['String'];
  commtProductEn: Scalars['String'];
  commtProductKr: Scalars['String'];
  enGoodsName: Scalars['String'];
  expiryDate: Scalars['String'];
  expiryDateEn: Scalars['String'];
  expiryDateKr: Scalars['String'];
  goodsId: Scalars['String'];
  goodsName: Scalars['String'];
  goodsType: Scalars['String'];
  id: Scalars['Int'];
  krGoodsName: Scalars['String'];
  listPrice: Scalars['Float'];
  modDate: Scalars['String'];
  originImg: Scalars['String'];
  rpPoint: Scalars['Int'];
  saleFeeType: Scalars['String'];
  salePrice: Scalars['Float'];
  useYn: Scalars['String'];
  vendorId?: Maybe<Scalars['Int']>;
  vendorLogo?: Maybe<Scalars['String']>;
  vendorName?: Maybe<Scalars['String']>;
  voucherBrand: VoucherBrand;
  voucherCategories?: Maybe<VoucherCategories>;
  voucherVendor: VoucherVendor;
};

export type GoodsListArgsOrderByInput = {
  id?: InputMaybe<OrderBy>;
  rp_point?: InputMaybe<OrderBy>;
};

export type GoodsListWhereInput = {
  categories?: InputMaybe<Array<Scalars['String']>>;
  id?: InputMaybe<Scalars['Int']>;
};

export type GoogleProduct = {
  __typename?: 'GoogleProduct';
  googleUrl: Scalars['String'];
  id: Scalars['Int'];
  image: Scalars['String'];
  name: Scalars['String'];
  price: Scalars['String'];
  productId: Scalars['Int'];
  rating?: Maybe<Scalars['Float']>;
  searchOrder: Scalars['Int'];
  shippingPrivacy: Scalars['String'];
  shopName: Scalars['String'];
  updatedTime: Scalars['Int'];
};

export type GoogleProductOrderInput = {
  googleOrder?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
  rating?: InputMaybe<OrderBy>;
};

export type GoogleProductWhereInput = {
  productId: Scalars['Int'];
  shopName?: InputMaybe<ExternalProvider>;
};

export enum GroupByType {
  Product = 'PRODUCT',
  User = 'USER'
}

export type Hashtag = {
  __typename?: 'Hashtag';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  isBookmarkedByViewer?: Maybe<Scalars['Boolean']>;
  postCount: Scalars['Int'];
  reviewCount: Scalars['Int'];
  value: Scalars['String'];
};

export type HashtagBookmarkPayload = CommonError | Hashtag;

export type HashtagInput = {
  id?: InputMaybe<Scalars['ID']>;
  value: Scalars['String'];
};

export type HashtagWhereInput = {
  value: StringFilter;
};

export type HashtagWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
  value?: InputMaybe<Scalars['String']>;
};

export type HashtagsBookmarkedByUserOrderByInput = {
  bookmarkedAt?: InputMaybe<OrderBy>;
};

export type HighlightProduct = {
  __typename?: 'HighlightProduct';
  product: Product;
  ranking?: Maybe<Scalars['Int']>;
  rankingChange?: Maybe<Scalars['Int']>;
};

export type HotKeywordRanking = {
  __typename?: 'HotKeywordRanking';
  isNew?: Maybe<Scalars['Boolean']>;
  keywordList?: Maybe<Array<KeywordList>>;
  name: Scalars['String'];
  nbEntries?: Maybe<Scalars['Int']>;
  rank: Scalars['Int'];
};

export type IamOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type Image = {
  __typename?: 'Image';
  createdAt: Scalars['DateTime'];
  fixed: Image;
  height?: Maybe<Scalars['Float']>;
  id: Scalars['Int'];
  name: Scalars['String'];
  url: Scalars['String'];
  width?: Maybe<Scalars['Float']>;
};


export type ImageFixedArgs = {
  width: FixedSize;
};

export type ImageCreateManyWithoutPostCommentInput = {
  create?: InputMaybe<Array<ImageCreateWithoutPostCommentInput>>;
};

export type ImageCreateManyWithoutPostInput = {
  create?: InputMaybe<Array<ImageCreateWithoutPostInput>>;
};

export type ImageCreateManyWithoutReviewInput = {
  create?: InputMaybe<Array<ImageCreateWithoutReviewInput>>;
};

export type ImageCreateOneWithoutExternalLinkInput = {
  create?: InputMaybe<ImageCreateWithoutExternalLinkInput>;
};

export type ImageCreateOneWithoutLiveStreamInput = {
  create: ImageCreateWithoutLiveStreamInput;
};

export type ImageCreateOneWithoutShopInput = {
  create: ImageCreateWithoutShopInput;
};

export type ImageCreateWithoutExternalLinkInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutLiveStreamInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutPostCommentInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutPostInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutReviewInput = {
  height: Scalars['Int'];
  name: Scalars['String'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type ImageCreateWithoutShopInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutUserInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageInput = {
  height?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  url?: InputMaybe<Scalars['String']>;
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageInputType = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  height?: InputMaybe<Scalars['Float']>;
  id?: InputMaybe<Scalars['Int']>;
  name?: InputMaybe<Scalars['String']>;
  url?: InputMaybe<Scalars['String']>;
  width?: InputMaybe<Scalars['Float']>;
};

export type ImageOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type ImageReviewInput = {
  height?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  url?: InputMaybe<Scalars['String']>;
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageUpdateManyWithoutPostCommentInput = {
  create?: InputMaybe<Array<ImageCreateWithoutPostCommentInput>>;
  disconnect?: InputMaybe<Array<ImageWhereUniqueInput>>;
  set?: InputMaybe<Array<ImageWhereUniqueInput>>;
};

export type ImageUpdateManyWithoutPostInput = {
  create?: InputMaybe<Array<ImageCreateWithoutPostInput>>;
  set?: InputMaybe<Array<ImageWhereUniqueInput>>;
};

export type ImageUpdateManyWithoutReviewInput = {
  create?: InputMaybe<Array<ImageCreateWithoutReviewInput>>;
  set?: InputMaybe<Array<ImageWhereUniqueInput>>;
};

export type ImageUpdateManyWithoutUserInput = {
  create?: InputMaybe<ImageCreateWithoutUserInput>;
};

export type ImageUpdateOneWithoutShopInput = {
  create: ImageCreateWithoutShopInput;
};

export type ImageUpsertInput = {
  urls?: InputMaybe<Array<ImageInputType>>;
};

export type ImageWhereInput = {
  review?: InputMaybe<ReviewWhereInput>;
};

export type ImageWhereUniqueInput = {
  id: Scalars['Int'];
};

export type Images = {
  __typename?: 'Images';
  urls?: Maybe<Array<Image>>;
};

export type ImagesAggregate = {
  __typename?: 'ImagesAggregate';
  count: Scalars['Int'];
};

export type ImagesConnection = {
  __typename?: 'ImagesConnection';
  aggregate: ImagesAggregate;
};

export type Ingredient = {
  __typename?: 'Ingredient';
  attributes?: Maybe<IngredientAttribute>;
  /** @deprecated please use cosmeticCautions or functionalFoodCautions */
  cautions?: Maybe<Array<IngredientCaution>>;
  cosmeticCautions?: Maybe<Array<IngredientCaution>>;
  cosmeticFunctions?: Maybe<Array<Function>>;
  /** @deprecated please use description of translations instead */
  description?: Maybe<Scalars['String']>;
  ewg?: Maybe<Scalars['String']>;
  ewgRiskType?: Maybe<EwgRiskType>;
  functionalFoodCautions?: Maybe<Array<IngredientCaution>>;
  functionalFoodFunctions?: Maybe<Array<Function>>;
  id: Scalars['Int'];
  /** @deprecated please use cosmeticFunctions or functionalFoodFunctions */
  specialFunctions?: Maybe<Array<SpecialIngredientFunction>>;
  translations: Array<IngredientTranslation>;
  uid: Scalars['ID'];
};


export type IngredientCautionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type IngredientCosmeticCautionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type IngredientCosmeticFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type IngredientFunctionalFoodCautionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type IngredientFunctionalFoodFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type IngredientSpecialFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SpecialIngredientFunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SpecialIngredientFunctionWhereInput>;
};


export type IngredientTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientTranslationWhereInput>;
};

export type IngredientAttribute = {
  __typename?: 'IngredientAttribute';
  demographic?: Maybe<IngredientDemographic>;
  rdi?: Maybe<Scalars['Float']>;
  rdiUnit?: Maybe<Unit>;
  ul?: Maybe<Scalars['Float']>;
  ulUnit?: Maybe<Unit>;
};

export type IngredientCaution = {
  __typename?: 'IngredientCaution';
  icon?: Maybe<Image>;
  id: Scalars['Int'];
  productType: ProductType;
  translations: Array<IngredientCautionTranslation>;
  uid: Scalars['ID'];
};


export type IngredientCautionTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientCautionTranslationWhereInput>;
};

export type IngredientCautionTranslation = {
  __typename?: 'IngredientCautionTranslation';
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  language: LanguageCode;
  name: Scalars['String'];
};

export type IngredientCautionTranslationWhereInput = {
  caution?: InputMaybe<CautionWhereInput>;
  language?: InputMaybe<LanguageCode>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type IngredientCautionsAggregate = {
  __typename?: 'IngredientCautionsAggregate';
  count: Scalars['Int'];
};

export type IngredientCautionsConnection = {
  __typename?: 'IngredientCautionsConnection';
  aggregate: IngredientCautionsAggregate;
};

export type IngredientCosmeticAttribute = {
  __typename?: 'IngredientCosmeticAttribute';
  description?: Maybe<Scalars['String']>;
  effect?: Maybe<Scalars['String']>;
};

export type IngredientDemographic = {
  __typename?: 'IngredientDemographic';
  baby?: Maybe<Array<BabyRdi>>;
  breastfeedingWoman?: Maybe<AdultRdi>;
  man?: Maybe<Array<AdultRdi>>;
  pregnantWoman?: Maybe<AdultRdi>;
  woman?: Maybe<Array<AdultRdi>>;
};

export type IngredientFunctionFoodAttribute = {
  __typename?: 'IngredientFunctionFoodAttribute';
  attention?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  dose?: Maybe<Scalars['String']>;
};

export type IngredientOrderByInput = {
  id?: InputMaybe<OrderBy>;
  priority?: InputMaybe<OrderBy>;
};

export type IngredientSearchResult = {
  __typename?: 'IngredientSearchResult';
  ingredients: Array<Maybe<Ingredient>>;
  total: Scalars['Int'];
};

export enum IngredientToFunctionLevel {
  Major = 'MAJOR',
  Minor = 'MINOR'
}

export type IngredientToFunctionWhereInput = {
  functionId?: InputMaybe<IntFilter>;
  functionType?: InputMaybe<FunctionType>;
  ingredient?: InputMaybe<IngredientWhereInput>;
  level?: InputMaybe<IngredientToFunctionLevel>;
  productType?: InputMaybe<ProductType>;
};

export type IngredientTranslation = {
  __typename?: 'IngredientTranslation';
  cosmeticAttributes?: Maybe<IngredientCosmeticAttribute>;
  /** @deprecated please use functionalFoodAttributes.description or cosmeticAttributes.description */
  description?: Maybe<Scalars['String']>;
  functionalFoodAttributes?: Maybe<IngredientFunctionFoodAttribute>;
  id: Scalars['Int'];
  language: LanguageCode;
  name: Scalars['String'];
  /** @deprecated use description instead */
  purpose?: Maybe<Scalars['String']>;
};

export type IngredientTranslationOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type IngredientTranslationWhereInput = {
  ingredient?: InputMaybe<IngredientWhereInput>;
  language?: InputMaybe<LanguageCode>;
  name?: InputMaybe<StringFilter>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type IngredientUsage = {
  __typename?: 'IngredientUsage';
  attributes?: Maybe<ProductToIngredientAttribute>;
  ingredient: Ingredient;
  ingredientId: Scalars['ID'];
  level?: Maybe<ProductToIngredientLevel>;
};

export type IngredientWhereInput = {
  cautionsSome?: InputMaybe<CautionWhereInput>;
  ewg?: InputMaybe<Scalars['String']>;
  ewgRiskType?: InputMaybe<EwgRiskType>;
  functionsSome?: InputMaybe<SpecialIngredientFunctionWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  ingredientToFunctionsSome?: InputMaybe<IngredientToFunctionWhereInput>;
  isCosmetic?: InputMaybe<BooleanFilter>;
  isFunctionalFood?: InputMaybe<BooleanFilter>;
  productsSome?: InputMaybe<ProductWhereInput>;
  translationsSome?: InputMaybe<IngredientTranslationWhereInput>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type IngredientWhereUniqueInput = {
  id: Scalars['Int'];
};

export type IngredientsAggregate = {
  __typename?: 'IngredientsAggregate';
  count: Scalars['Int'];
};

export type IngredientsConnection = {
  __typename?: 'IngredientsConnection';
  aggregate: IngredientsAggregate;
};

export type IntFilter = {
  equals?: InputMaybe<Scalars['Int']>;
  gt?: InputMaybe<Scalars['Int']>;
  gte?: InputMaybe<Scalars['Int']>;
  in?: InputMaybe<Array<Scalars['Int']>>;
  lt?: InputMaybe<Scalars['Int']>;
  lte?: InputMaybe<Scalars['Int']>;
  ne?: InputMaybe<Scalars['Int']>;
  notIn?: InputMaybe<Array<Scalars['Int']>>;
};

export type KeywordList = {
  __typename?: 'KeywordList';
  key: Scalars['String'];
  value: Scalars['Int'];
};

export enum LanguageCode {
  En = 'EN',
  Ko = 'KO',
  Vi = 'VI'
}

export type LanguageCodeFilter = {
  equals?: InputMaybe<LanguageCode>;
  in?: InputMaybe<Array<LanguageCode>>;
};

export type Level = {
  __typename?: 'Level';
  id: Scalars['Int'];
  maxPoints?: Maybe<Scalars['Int']>;
  minPoints: Scalars['Int'];
};

export type LiveStream = {
  __typename?: 'LiveStream';
  background?: Maybe<Image>;
  channelId?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  description?: Maybe<Scalars['String']>;
  host: User;
  id: Scalars['Int'];
  liveUrl?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  roomId?: Maybe<Scalars['String']>;
  savedByViewer?: Maybe<Scalars['Boolean']>;
  savedDateByViewer?: Maybe<Scalars['DateTime']>;
  startedAt?: Maybe<Scalars['DateTime']>;
  status: LiveStreamStatus;
  streamUrl?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
  vodUrl?: Maybe<Scalars['String']>;
};

export type LiveStreamCreateInput = {
  background?: InputMaybe<ImageCreateOneWithoutLiveStreamInput>;
  description?: InputMaybe<Scalars['String']>;
  lsProducts?: InputMaybe<LiveStreamProductCreateManyWithoutLiveStreamInput>;
  name: Scalars['String'];
  startedAt?: InputMaybe<Scalars['DateTime']>;
};

export type LiveStreamError = Error & {
  __typename?: 'LiveStreamError';
  code: LiveStreamErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum LiveStreamErrorCode {
  HaveNotCreatedShop = 'HAVE_NOT_CREATED_SHOP',
  NotEnoughChannels = 'NOT_ENOUGH_CHANNELS',
  ShopIsBlocked = 'SHOP_IS_BLOCKED',
  ShopNotApprovedYet = 'SHOP_NOT_APPROVED_YET'
}

export type LiveStreamOrError = CommonError | LiveStream | LiveStreamError;

export type LiveStreamOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type LiveStreamProduct = {
  __typename?: 'LiveStreamProduct';
  code: Scalars['String'];
  createdAt: Scalars['DateTime'];
  currency: Currency;
  id: Scalars['Int'];
  name?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Int']>;
  product?: Maybe<Product>;
  updatedAt: Scalars['DateTime'];
};

export type LiveStreamProductCreateManyWithoutLiveStreamInput = {
  create?: InputMaybe<Array<LiveStreamProductCreateWithoutLiveStreamInput>>;
};

export type LiveStreamProductCreateOneWithoutCartItemsInput = {
  connect: LiveStreamProductWhereUniqueInput;
};

export type LiveStreamProductCreateWithoutLiveStreamInput = {
  code: Scalars['String'];
  currency?: InputMaybe<Currency>;
  name?: InputMaybe<Scalars['String']>;
  price?: InputMaybe<Scalars['Int']>;
  product?: InputMaybe<ProductCreateOneWithoutLiveStreamProductInput>;
};

export type LiveStreamProductOrError = CommonError | LiveStreamProduct;

export type LiveStreamProductOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type LiveStreamProductUpdateInput = {
  name?: InputMaybe<Scalars['String']>;
  price?: InputMaybe<Scalars['Int']>;
  product?: InputMaybe<ProductUpdateOneWithoutLiveStreamProductInput>;
};

export type LiveStreamProductUpdateManyWithoutLiveStreamInput = {
  create?: InputMaybe<Array<LiveStreamProductCreateWithoutLiveStreamInput>>;
  set?: InputMaybe<Array<LiveStreamProductWhereUniqueInput>>;
  update?: InputMaybe<Array<LiveStreamProductUpdateWithoutLiveStreamInput>>;
};

export type LiveStreamProductUpdateWithoutLiveStreamInput = {
  code: Scalars['String'];
  currency?: InputMaybe<Currency>;
  id: Scalars['Int'];
  name?: InputMaybe<Scalars['String']>;
  price?: InputMaybe<Scalars['Int']>;
  product?: InputMaybe<ProductUpdateOneWithoutLiveStreamProductInput>;
};

export type LiveStreamProductWhereInput = {
  liveStream: LiveStreamWhereInput;
};

export type LiveStreamProductWhereUniqueInput = {
  id: Scalars['Int'];
};

export type LiveStreamStartInput = {
  roomId: Scalars['String'];
};

export enum LiveStreamStatus {
  Created = 'CREATED',
  Running = 'RUNNING',
  Stopped = 'STOPPED'
}

export type LiveStreamUpdateInput = {
  background?: InputMaybe<ImageCreateOneWithoutLiveStreamInput>;
  description?: InputMaybe<Scalars['String']>;
  lsProducts?: InputMaybe<LiveStreamProductUpdateManyWithoutLiveStreamInput>;
  name: Scalars['String'];
  startedAt?: InputMaybe<Scalars['DateTime']>;
};

export type LiveStreamWhereInput = {
  host?: InputMaybe<UserWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<LiveStreamStatus>;
  statusIn?: InputMaybe<Array<LiveStreamStatus>>;
};

export type LiveStreamWhereUniqueInput = {
  id: Scalars['Int'];
};

export type Location = {
  __typename?: 'Location';
  latitude: Scalars['Float'];
  longitude: Scalars['Float'];
};

export type LocationInput = {
  latitude: Scalars['Float'];
  longitude: Scalars['Float'];
};

export type M_LikeReviewResult = {
  __typename?: 'M_LikeReviewResult';
  count: Scalars['Int'];
  message: Scalars['String'];
  status: Scalars['Boolean'];
};

export type MediaEntityUnion = LiveStream | Post;

export type MediaOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
};

export enum MediaType {
  LiveStream = 'LIVE_STREAM',
  Video = 'VIDEO'
}

export type MediaWhereInput = {
  createdAt?: InputMaybe<DateTimeFilter>;
  type?: InputMaybe<MediaType>;
  user: UserWhereUniqueInput;
};

export type MediaWhereUniqueInput = {
  liveStream?: InputMaybe<LiveStreamWhereUniqueInput>;
  post?: InputMaybe<PostWhereUniqueInput>;
};

export type MediasAggregate = {
  __typename?: 'MediasAggregate';
  count: Scalars['Int'];
};

export type MediasConnection = {
  __typename?: 'MediasConnection';
  aggregate: MediasAggregate;
};

export type Mutation = {
  __typename?: 'Mutation';
  actitvityLogs: Scalars['Boolean'];
  activityLogs: Scalars['Boolean'];
  addBankCard: Scalars['Boolean'];
  approveShop: Shop;
  blockShop: Shop;
  blockUser: Scalars['Boolean'];
  bookmarkPost: Scalars['Boolean'];
  bookmarkProduct: Scalars['Boolean'];
  bookmarkReview: Scalars['Boolean'];
  brandAdminUpdate: BrandAdmin;
  brandPromotionalProductCreate?: Maybe<BrandPromotionalProduct>;
  brandPromotionalProductDelete?: Maybe<BrandPromotionalProduct>;
  brandPromotionalProductUpdate?: Maybe<BrandPromotionalProduct>;
  cancelEcommerceOrders: Scalars['String'];
  cancelEventComment: EventComment;
  cancelOrder: OrderOrError;
  changeEmail: EmailOrError;
  changePassword: AuthPayload;
  changePhone: Scalars['Boolean'];
  changePickupAddress: Scalars['Boolean'];
  checkIn: Scalars['Boolean'];
  confirmEmail: AuthPayloadOrError;
  confirmOrder: OrderOrError;
  confirmOrders: Scalars['Boolean'];
  createBannerImage: BannerImage;
  createBaumannAnswers: BaumannSkinType;
  createCartItem: CartItemOrError;
  createCartProduct: CartProduct;
  createCastComment: CastComment;
  createChannel: ChannelOrError;
  createChatNotification: Scalars['Boolean'];
  createDevice: Device;
  createEcommerceOrders: Scalars['String'];
  createEcommerceOrdersV2: SucessfullEcommOrderOrError;
  createEcommercePromotion: Scalars['Boolean'];
  createEcommerceReview: Array<EcommerceReview>;
  createEvaluation: FundingOrderEvaluation;
  createEventComment: EventComment;
  createEventCommentV2: EventCommentOrError;
  createEventCommentV3: EventCommentOrError;
  createEventCommentV3_UncheckCondition: EventCommentOrError;
  createFunding: Funding;
  createFundingOrder: Scalars['String'];
  createLiveStream: LiveStreamOrError;
  createOrder: OrderOrError;
  createPost: PostOrError;
  createPostComment: PostComment;
  /** @deprecated please use createPost instead */
  createPostV2: PostOrError;
  createProductEdit: ProductEdit;
  createProductRequest: ProductRequest;
  createProductUsageLog: ProductUsageLog;
  /** @deprecated please use reviewCreate instead, this mutation will be removed on 1/4/2022 */
  createReview: Review;
  createReviewReport: ReviewReport;
  createShippingAddress: ShippingAddressOrError;
  /** @deprecated use createShopV2 instead */
  createShop: Shop;
  createShopProduct: ShopProduct;
  createShopToProducts: Array<ShopToProduct>;
  createShopV2: ShopOrMutationError;
  createSlot: PostReservedSlot;
  createSystemConfig: SystemConfig;
  deleteBannerImage: BannerImage;
  deleteCartItem: CartItemOrError;
  deleteCartProduct: CartProduct;
  deleteEcommercePromotion: Scalars['Boolean'];
  deleteEcommerceReview: EcommerceReview;
  deleteEvaluation: FundingOrderEvaluation;
  deleteEventComment: EventComment;
  deleteFunding: Funding;
  deleteLiveStream: LiveStreamOrError;
  deleteLiveStreamProduct: LiveStreamProductOrError;
  deleteNotification: NotificationMutatePayload;
  deleteNotifications: NotificationMutatePayload;
  deletePost: Post;
  deletePostComment: PostComment;
  deletePostReservedSlot: PostReservedSlot;
  deleteProductUsageLogs: Scalars['Int'];
  deleteReview: Review;
  deleteShippingAddress: ShippingAddressOrError;
  deleteShopProduct: ShopProduct;
  deleteShopToProduct: ShopToProduct;
  deleteSystemConfig: SystemConfig;
  deleteUser: User;
  dislikeReview: Scalars['Boolean'];
  followBrand: Scalars['Boolean'];
  followUser: User;
  forgetPassword: Scalars['Boolean'];
  generateCurrentEmailOTP: Scalars['Boolean'];
  generateOTP: Scalars['Boolean'];
  generatePresignedUrl: PresignedUrl;
  generateRegisterMultiProductForms?: Maybe<Scalars['Boolean']>;
  generateSmsOTP: Scalars['Boolean'];
  /** @deprecated use checkIn instead */
  getActivePoints: Scalars['Boolean'];
  /** @deprecated use checkIn instead */
  getDailyActivePoints: Scalars['Boolean'];
  ghtkApi: Scalars['String'];
  handleVisibleProductVariant: ProductVariant;
  handleVisibleProductViewStore: ProductViewStore;
  handleVisibleShopProduct: ShopProduct;
  hashtagBookmark: HashtagBookmarkPayload;
  hashtagUnbookmark: UnbookmarkHashtagPayload;
  hidePostComment: PostComment;
  likeDislikeReview: M_LikeReviewResult;
  likeReview: M_LikeReviewResult;
  linkFacebook: SocialMediaOrError;
  linkInstagram: SocialMediaOrError;
  /** @deprecated please use loginByAppleV2 instead. This mutation will be removed on 01/03/2022 */
  loginByApple: AuthPayload;
  loginByAppleV2: AuthPayloadOrError;
  /** @deprecated please use loginByFacebookV2 instead. This mutation will be removed on 01/03/2022 */
  loginByFacebook: AuthPayload;
  loginByFacebookV2: AuthPayloadOrError;
  loginV2: AuthPayloadOrError;
  logout: Scalars['Boolean'];
  markBestAnswer: Scalars['Boolean'];
  notifyLiveStream: Scalars['Boolean'];
  payBackEcommerceOrder: Scalars['String'];
  payBackFundingOrder: Scalars['String'];
  pinReview: Scalars['String'];
  purchaseVouchers: Scalars['Boolean'];
  reactPost: Post;
  reactPostComment: PostComment;
  readNotification: NotificationMutatePayload;
  readNotifications: NotificationMutatePayload;
  reportPost: PostReport;
  reportPostComment: PostCommentReport;
  returnsEcommerceOrders: Scalars['String'];
  reviewCreate: ReviewCreatePayload;
  reviewUpdate: ReviewUpdatePayload;
  saveFavouriteBrands: Scalars['Boolean'];
  saveMedia: Scalars['Boolean'];
  /** @deprecated please use bookmarkProduct instead, this field will be remove from 1/11/2021 */
  saveProduct: Scalars['Boolean'];
  sendNotification?: Maybe<Scalars['Boolean']>;
  signUp: AuthPayloadOrError;
  startLiveStream: LiveStreamOrError;
  stopChannel: ChannelOrError;
  stopLiveStream: LiveStreamOrError;
  subscribePost: Scalars['Boolean'];
  terminateEcommercePromotion: Scalars['Boolean'];
  unBookmarkPost: Scalars['Boolean'];
  unBookmarkProduct: Scalars['Boolean'];
  unBookmarkReview: Scalars['Boolean'];
  unblockUser: Scalars['Boolean'];
  unfollowBrand: Brand;
  unfollowUser: User;
  unhidePostComment: PostComment;
  unlinkFacebook: Scalars['Boolean'];
  unlinkInstagram: Scalars['Boolean'];
  unsaveMedia: Scalars['Boolean'];
  /** @deprecated please use unBookmarkProduct instead, this field will be remove from 1/11/2021 */
  unsaveProduct: Scalars['Boolean'];
  unsubscribePost: Scalars['Boolean'];
  updateBannerImage: BannerImage;
  updateCartItem: CartItemOrError;
  updateCartProduct: CartProduct;
  updateEcommercePromotion: Scalars['Boolean'];
  updateEcommerceReview: EcommerceReview;
  updateEvaluation: FundingOrderEvaluation;
  updateFunding: Funding;
  updateLiveStream: LiveStreamOrError;
  updateLiveStreamProduct: LiveStreamProductOrError;
  updateOrder: OrderOrError;
  updatePost: Post;
  updatePostComment: PostComment;
  updateProductUserRequestByAdmin?: Maybe<UserRequestUpdateResponse>;
  updateProductUserStatus?: Maybe<UserRequestUpdateResponse>;
  updateProductVariant: ProductVariant;
  /** @deprecated please use reviewUpdate instead, this mutation will be removed on 1/4/2022 */
  updateReview: Review;
  updateShippingAddress: ShippingAddressOrError;
  /** @deprecated use updateShopV2 instead */
  updateShop: Shop;
  updateShopProduct: ShopProduct;
  updateShopToProduct: ShopToProduct;
  updateShopV2: ShopOrMutationError;
  updateSlot: PostReservedSlot;
  updateSystemConfig: SystemConfig;
  updateUser: User;
  updateUserNotificationConfig: Scalars['Boolean'];
  updateVoucherStatus: Scalars['Boolean'];
  userRequestCreate: UserRequest;
  viewPost: Scalars['Boolean'];
  viewedPost: Scalars['Boolean'];
  visitedContentCreate: Scalars['Boolean'];
  vnpayUrl: Scalars['String'];
  voucherBrand: Scalars['Boolean'];
};


export type MutationActitvityLogsArgs = {
  data: ActivityLogCreateInput;
};


export type MutationActivityLogsArgs = {
  data: ActivityLogCreateInput;
};


export type MutationAddBankCardArgs = {
  data: BankShopCreateInput;
};


export type MutationApproveShopArgs = {
  where: ShopWhereUniqueInput;
};


export type MutationBlockShopArgs = {
  reason: Scalars['String'];
  status: ShopStatus;
  where: ShopWhereUniqueInput;
};


export type MutationBlockUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationBookmarkPostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationBookmarkProductArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationBookmarkReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationBrandAdminUpdateArgs = {
  data: BrandAdminUpdateInput;
  where: BrandAdminWhereUniqueInput;
};


export type MutationBrandPromotionalProductCreateArgs = {
  data: BrandPromotionalProductInput;
};


export type MutationBrandPromotionalProductDeleteArgs = {
  where: BrandPromotionalProducWhereInput;
};


export type MutationBrandPromotionalProductUpdateArgs = {
  data: BrandPromotionalProductInput;
  where: BrandPromotionalProducWhereInput;
};


export type MutationCancelEcommerceOrdersArgs = {
  data: EcommerceOrderCancelInput;
};


export type MutationCancelEventCommentArgs = {
  where: EventCommentWhereUniqueInput;
};


export type MutationCancelOrderArgs = {
  where: OrderWhereUniqueInput;
};


export type MutationChangeEmailArgs = {
  email: Scalars['String'];
};


export type MutationChangePasswordArgs = {
  currentPassword: Scalars['String'];
  newPassword: Scalars['String'];
};


export type MutationChangePhoneArgs = {
  phone: Scalars['String'];
};


export type MutationChangePickupAddressArgs = {
  data: AddressIdInput;
};


export type MutationConfirmEmailArgs = {
  otp: Scalars['Int'];
};


export type MutationConfirmOrderArgs = {
  where: OrderWhereUniqueInput;
};


export type MutationConfirmOrdersArgs = {
  data: EcommerceOrderConfirmInput;
};


export type MutationCreateBannerImageArgs = {
  data: BannerImageInput;
};


export type MutationCreateBaumannAnswersArgs = {
  data: Array<BaumannAnswerCreateInput>;
};


export type MutationCreateCartItemArgs = {
  data: CartItemCreateInput;
};


export type MutationCreateCartProductArgs = {
  data: CartProductInsertInput;
};


export type MutationCreateCastCommentArgs = {
  data: CastCommentCreateInput;
};


export type MutationCreateChatNotificationArgs = {
  data: ChatNotificationInput;
};


export type MutationCreateDeviceArgs = {
  data: DeviceCreateInput;
};


export type MutationCreateEcommerceOrdersArgs = {
  data: EcommerceOrderInput;
};


export type MutationCreateEcommerceOrdersV2Args = {
  data: EcommerceOrderInput;
};


export type MutationCreateEcommercePromotionArgs = {
  data: EcommercePromotionInsertInput;
};


export type MutationCreateEcommerceReviewArgs = {
  data: EcommerceReviewInsertInput;
};


export type MutationCreateEvaluationArgs = {
  data: FundingOrderEvaluationInput;
};


export type MutationCreateEventCommentArgs = {
  data: EventCommentCreateInput;
};


export type MutationCreateEventCommentV2Args = {
  data: EventCommentCreateInput;
};


export type MutationCreateEventCommentV3Args = {
  data: EventCommentCreateInput;
};


export type MutationCreateEventCommentV3_UncheckConditionArgs = {
  data: EventCommentCreateInput;
};


export type MutationCreateFundingArgs = {
  data: FundingCreateInput;
};


export type MutationCreateFundingOrderArgs = {
  data: FundingOrderInput;
};


export type MutationCreateLiveStreamArgs = {
  data: LiveStreamCreateInput;
};


export type MutationCreateOrderArgs = {
  data: OrderCreateInput;
};


export type MutationCreatePostArgs = {
  data: PostCreateInput;
};


export type MutationCreatePostCommentArgs = {
  data: PostCommentCreateInput;
};


export type MutationCreatePostV2Args = {
  data: PostCreateInput;
};


export type MutationCreateProductEditArgs = {
  data: ProductEditCreateInput;
};


export type MutationCreateProductRequestArgs = {
  data: ProductRequestCreateInput;
};


export type MutationCreateProductUsageLogArgs = {
  data: ProductUsageLogCreateInput;
};


export type MutationCreateReviewArgs = {
  data: ReviewCreateInput;
};


export type MutationCreateReviewReportArgs = {
  data: ReviewReportCreateInput;
};


export type MutationCreateShippingAddressArgs = {
  data: ShippingAddressCreateInput;
};


export type MutationCreateShopArgs = {
  data: ShopCreateInput;
};


export type MutationCreateShopProductArgs = {
  data: ShopProductInsertInput;
};


export type MutationCreateShopToProductsArgs = {
  data: Array<ShopToProductCreateInput>;
};


export type MutationCreateShopV2Args = {
  data: ShopCreateInput;
};


export type MutationCreateSlotArgs = {
  data: PostReservedSlotCreateInput;
};


export type MutationCreateSystemConfigArgs = {
  data: SystemConfigCreateInput;
};


export type MutationDeleteBannerImageArgs = {
  where: BannerImageWhereInput;
};


export type MutationDeleteCartItemArgs = {
  where: CartItemWhereUniqueInput;
};


export type MutationDeleteCartProductArgs = {
  where: CartProductWhereInput;
};


export type MutationDeleteEcommercePromotionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionWhereInput>;
};


export type MutationDeleteEcommerceReviewArgs = {
  where: EcommerceReviewWhereInput;
};


export type MutationDeleteEvaluationArgs = {
  where: FundingOrderEvaluationWhereInput;
};


export type MutationDeleteEventCommentArgs = {
  where: EventCommentWhereUniqueInput;
};


export type MutationDeleteFundingArgs = {
  where: FundingWhereInput;
};


export type MutationDeleteLiveStreamArgs = {
  where: LiveStreamWhereUniqueInput;
};


export type MutationDeleteLiveStreamProductArgs = {
  where: LiveStreamProductWhereUniqueInput;
};


export type MutationDeleteNotificationArgs = {
  where: NotificationWhereUniqueInput;
};


export type MutationDeletePostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationDeletePostCommentArgs = {
  where: PostCommentWhereUniqueInput;
};


export type MutationDeletePostReservedSlotArgs = {
  where: PostReservedSlotWhereInput;
};


export type MutationDeleteProductUsageLogsArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationDeleteReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationDeleteShippingAddressArgs = {
  where: ShippingAddressWhereUniqueInput;
};


export type MutationDeleteShopProductArgs = {
  where: ShopProductWhereInput;
};


export type MutationDeleteShopToProductArgs = {
  where: ShopToProductWhereUniqueInput;
};


export type MutationDeleteSystemConfigArgs = {
  where: SystemConfigWhereInput;
};


export type MutationDeleteUserArgs = {
  data: UserDeleteInput;
  where: UserWhereUniqueInput;
};


export type MutationDislikeReviewArgs = {
  reviewId: Scalars['Int'];
};


export type MutationFollowBrandArgs = {
  where: BrandWhereUniqueInput;
};


export type MutationFollowUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationForgetPasswordArgs = {
  email: Scalars['String'];
};


export type MutationGenerateOtpArgs = {
  email: Scalars['String'];
};


export type MutationGenerateSmsOtpArgs = {
  phone: Scalars['String'];
};


export type MutationGhtkApiArgs = {
  params: Scalars['String'];
};


export type MutationHandleVisibleProductVariantArgs = {
  data: ProductVariantUpdateInput;
  where: ProductVariantWhereInput;
};


export type MutationHandleVisibleProductViewStoreArgs = {
  isVisible: Scalars['Boolean'];
  where: ProductViewStoreWhereInput;
};


export type MutationHandleVisibleShopProductArgs = {
  data: ShopProductUpdateInput;
  where: ShopProductWhereInput;
};


export type MutationHashtagBookmarkArgs = {
  id: Scalars['ID'];
};


export type MutationHashtagUnbookmarkArgs = {
  id: Scalars['ID'];
};


export type MutationHidePostCommentArgs = {
  where: PostCommentWhereUniqueInput;
};


export type MutationLikeDislikeReviewArgs = {
  authorId?: InputMaybe<Scalars['Int']>;
  productId: Scalars['Int'];
  productName?: InputMaybe<Scalars['String']>;
  reactStatus?: InputMaybe<ReactStatus>;
  reviewId: Scalars['Int'];
};


export type MutationLikeReviewArgs = {
  authorId?: InputMaybe<Scalars['Int']>;
  productId: Scalars['Int'];
  productName?: InputMaybe<Scalars['String']>;
  reviewId: Scalars['Int'];
};


export type MutationLinkFacebookArgs = {
  accessToken: Scalars['String'];
};


export type MutationLinkInstagramArgs = {
  token: Scalars['String'];
};


export type MutationLoginByAppleArgs = {
  accessToken: Scalars['String'];
};


export type MutationLoginByAppleV2Args = {
  accessToken: Scalars['String'];
};


export type MutationLoginByFacebookArgs = {
  accessToken: Scalars['String'];
};


export type MutationLoginByFacebookV2Args = {
  accessToken: Scalars['String'];
};


export type MutationLoginV2Args = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationLogoutArgs = {
  token?: InputMaybe<Scalars['String']>;
};


export type MutationMarkBestAnswerArgs = {
  where: PostCommentWhereUniqueInput;
};


export type MutationNotifyLiveStreamArgs = {
  where: LiveStreamWhereUniqueInput;
};


export type MutationPayBackEcommerceOrderArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type MutationPayBackFundingOrderArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderWhereInput>;
};


export type MutationPinReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationPurchaseVouchersArgs = {
  data: VoucherIssuesInput;
};


export type MutationReactPostArgs = {
  status: ReactStatus;
  where: PostWhereUniqueInput;
};


export type MutationReactPostCommentArgs = {
  status: ReactStatus;
  where: PostCommentWhereUniqueInput;
};


export type MutationReadNotificationArgs = {
  where: NotificationWhereUniqueInput;
};


export type MutationReportPostArgs = {
  data: PostReportCreateInput;
};


export type MutationReportPostCommentArgs = {
  data: PostCommentReportCreateInput;
};


export type MutationReturnsEcommerceOrdersArgs = {
  data: EcommerceOrderCancelInput;
};


export type MutationReviewCreateArgs = {
  data: ReviewInput;
};


export type MutationReviewUpdateArgs = {
  data: ReviewInput;
};


export type MutationSaveFavouriteBrandsArgs = {
  data: Array<BrandWhereUniqueInput>;
};


export type MutationSaveMediaArgs = {
  where: MediaWhereUniqueInput;
};


export type MutationSaveProductArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationSendNotificationArgs = {
  where: UserRequestUpdateInput;
};


export type MutationSignUpArgs = {
  data: SignUpInput;
};


export type MutationStartLiveStreamArgs = {
  data: LiveStreamStartInput;
  where: LiveStreamWhereUniqueInput;
};


export type MutationStopChannelArgs = {
  id: Scalars['ID'];
};


export type MutationStopLiveStreamArgs = {
  where: LiveStreamWhereUniqueInput;
};


export type MutationSubscribePostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationTerminateEcommercePromotionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionWhereInput>;
};


export type MutationUnBookmarkPostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationUnBookmarkProductArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationUnBookmarkReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationUnblockUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationUnfollowBrandArgs = {
  where: BrandWhereUniqueInput;
};


export type MutationUnfollowUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationUnhidePostCommentArgs = {
  where: PostCommentWhereUniqueInput;
};


export type MutationUnsaveMediaArgs = {
  where: MediaWhereUniqueInput;
};


export type MutationUnsaveProductArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationUnsubscribePostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationUpdateBannerImageArgs = {
  data: BannerImageUpdateInput;
  where: BannerImageWhereInput;
};


export type MutationUpdateCartItemArgs = {
  data: CartItemUpdateInput;
  where: CartItemWhereUniqueInput;
};


export type MutationUpdateCartProductArgs = {
  data: CartProductUpdateInput;
  where: CartProductWhereInput;
};


export type MutationUpdateEcommercePromotionArgs = {
  data: EcommercePromotionUpdateInput;
};


export type MutationUpdateEcommerceReviewArgs = {
  data: EcommerceReviewUpdateInput;
  where: EcommerceReviewWhereInput;
};


export type MutationUpdateEvaluationArgs = {
  data: FundingOrderEvaluationUpdateInput;
  where: FundingOrderEvaluationWhereInput;
};


export type MutationUpdateFundingArgs = {
  data: FundingUpdateInput;
  where: FundingWhereInput;
};


export type MutationUpdateLiveStreamArgs = {
  data: LiveStreamUpdateInput;
  where: LiveStreamWhereUniqueInput;
};


export type MutationUpdateLiveStreamProductArgs = {
  data: LiveStreamProductUpdateInput;
  where: LiveStreamProductWhereUniqueInput;
};


export type MutationUpdateOrderArgs = {
  data: OrderUpdateInput;
  where: OrderWhereUniqueInput;
};


export type MutationUpdatePostArgs = {
  data: PostUpdateInput;
  where: PostWhereUniqueInput;
};


export type MutationUpdatePostCommentArgs = {
  data: PostCommentUpdateInput;
  where: PostCommentWhereUniqueInput;
};


export type MutationUpdateProductUserRequestByAdminArgs = {
  data: UserRequestUpdateInput;
  where: UserRequestWhereUniqueInput;
};


export type MutationUpdateProductUserStatusArgs = {
  data: UserRequestUpdateStatusInput;
  where: UserRequestUpdateInput;
};


export type MutationUpdateProductVariantArgs = {
  data: ProductVariantUpdateInput;
  where: ProductVariantWhereInput;
};


export type MutationUpdateReviewArgs = {
  data: ReviewUpdateInput;
  where: ReviewWhereUniqueInput;
};


export type MutationUpdateShippingAddressArgs = {
  data: ShippingAddressUpdateInput;
  where: ShippingAddressWhereUniqueInput;
};


export type MutationUpdateShopArgs = {
  data: ShopUpdateInput;
  where: ShopWhereUniqueInput;
};


export type MutationUpdateShopProductArgs = {
  data: ShopProductUpdateInput;
  where: ShopProductWhereInput;
};


export type MutationUpdateShopToProductArgs = {
  data: ShopToProductUpdateInput;
  where: ShopToProductWhereUniqueInput;
};


export type MutationUpdateShopV2Args = {
  data: ShopUpdateInput;
  where: ShopWhereUniqueInput;
};


export type MutationUpdateSlotArgs = {
  data: PostReservedSlotInput;
  where: PostReservedSlotWhereInput;
};


export type MutationUpdateSystemConfigArgs = {
  data: SystemConfigUpdateInput;
  where: SystemConfigWhereInput;
};


export type MutationUpdateUserArgs = {
  data: UserUpdateInput;
  where: UserWhereUniqueInput;
};


export type MutationUpdateUserNotificationConfigArgs = {
  data: UserNotificationConfigUpdateInput;
};


export type MutationUpdateVoucherStatusArgs = {
  data: VoucherGiftpopListUpdateInput;
};


export type MutationUserRequestCreateArgs = {
  data: UserRequestInput;
};


export type MutationViewPostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationViewedPostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationVisitedContentCreateArgs = {
  data: VisitedContentCreateInput;
};


export type MutationVnpayUrlArgs = {
  amount: Scalars['Float'];
  orderInfo: Scalars['String'];
};

export type NbEcommerceOrderStatistic = {
  __typename?: 'NbEcommerceOrderStatistic';
  billTotal?: Maybe<Scalars['Int']>;
  orderStatus?: Maybe<Scalars['String']>;
  orderTotal?: Maybe<Scalars['Int']>;
  productTotal?: Maybe<Scalars['Int']>;
};

export type NbOrderStatistic = {
  __typename?: 'NbOrderStatistic';
  billTotal?: Maybe<Scalars['Int']>;
  evaluationTotal?: Maybe<Scalars['Int']>;
  orderStatus?: Maybe<Scalars['String']>;
  orderTotal?: Maybe<Scalars['Int']>;
  productTotal?: Maybe<Scalars['Int']>;
};

export type NearByInput = {
  distance: Scalars['Float'];
  latitude: Scalars['Float'];
  longitude: Scalars['Float'];
};

export type Notification = {
  __typename?: 'Notification';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  isViewed: Scalars['Boolean'];
  notificationObject: NotificationObject;
};

export type NotificationChange = {
  __typename?: 'NotificationChange';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  user: User;
};

export type NotificationEntityUnion = EcommerceOrder | EcommerceReview | Event | NotificationEvent | PointHistory | Post | PostComment | Product | Review | User;

export type NotificationEvent = {
  __typename?: 'NotificationEvent';
  active?: Maybe<Scalars['Boolean']>;
  alarmTime?: Maybe<Scalars['DateTime']>;
  attachedLink?: Maybe<Scalars['String']>;
  buttonName?: Maybe<Scalars['String']>;
  changeBy?: Maybe<Scalars['String']>;
  content?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  creatorId?: Maybe<Scalars['String']>;
  creatorName?: Maybe<Scalars['String']>;
  dataExcelFile?: Maybe<NotificationEventAttribute>;
  detail?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  nbUserSuccess?: Maybe<Scalars['Int']>;
  nbUserTotal?: Maybe<Scalars['Int']>;
  noEventType?: Maybe<Scalars['String']>;
  noId?: Maybe<Scalars['Int']>;
  receivedObjectData?: Maybe<NotificationObjectByUserInfo>;
  receivedObjectType?: Maybe<Scalars['String']>;
  status?: Maybe<NotificationEventStatus>;
  title?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type NotificationEventAttribute = {
  __typename?: 'NotificationEventAttribute';
  name?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
};

export enum NotificationEventStatus {
  Pending = 'PENDING',
  Ready = 'READY',
  Success = 'SUCCESS'
}

export type NotificationMutatePayload = {
  __typename?: 'NotificationMutatePayload';
  affected?: Maybe<Scalars['Int']>;
};

export type NotificationObject = {
  __typename?: 'NotificationObject';
  createdAt: Scalars['DateTime'];
  entity?: Maybe<NotificationEntityUnion>;
  id: Scalars['Int'];
  mainContent?: Maybe<Scalars['String']>;
  notificationChanges: Array<NotificationChange>;
  subContent?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  type: NotificationObjectType;
};

export type NotificationObjectByUserInfo = {
  __typename?: 'NotificationObjectByUserInfo';
  fromAge?: Maybe<Scalars['Int']>;
  skinType?: Maybe<SkinTypeObject>;
  toAge?: Maybe<Scalars['Float']>;
  userLevel?: Maybe<UserLevelObject>;
};

export enum NotificationObjectType {
  CompletedEcommerceOrder = 'COMPLETED_ECOMMERCE_ORDER',
  ConfirmedEcommerceOrder = 'CONFIRMED_ECOMMERCE_ORDER',
  CreatePost = 'CREATE_POST',
  EventAdmin = 'EVENT_ADMIN',
  EventRemindWriteReview = 'EVENT_REMIND_WRITE_REVIEW',
  FollowUser = 'FOLLOW_USER',
  MentionInComment = 'MENTION_IN_COMMENT',
  MentionInPost = 'MENTION_IN_POST',
  NewsUpgrade = 'NEWS_UPGRADE',
  NewEcommerceOrder = 'NEW_ECOMMERCE_ORDER',
  NewOnlineEcommerceOrder = 'NEW_ONLINE_ECOMMERCE_ORDER',
  PointApprovedReviewReport = 'POINT_APPROVED_REVIEW_REPORT',
  PointApprovedVideo = 'POINT_APPROVED_VIDEO',
  PointBestAnswer = 'POINT_BEST_ANSWER',
  PointDailyActive = 'POINT_DAILY_ACTIVE',
  PointEditProduct = 'POINT_EDIT_PRODUCT',
  PointEventPoints = 'POINT_EVENT_POINTS',
  PointExchangePointsForCoupons = 'POINT_EXCHANGE_POINTS_FOR_COUPONS',
  PointFirstApprovedVideo = 'POINT_FIRST_APPROVED_VIDEO',
  PointFirstComment = 'POINT_FIRST_COMMENT',
  PointFirstPost = 'POINT_FIRST_POST',
  PointFirstReview = 'POINT_FIRST_REVIEW',
  PointGift = 'POINT_GIFT',
  PointHave_20Followers = 'POINT_HAVE_20_FOLLOWERS',
  PointMonthlyActive = 'POINT_MONTHLY_ACTIVE',
  PointNormalComment = 'POINT_NORMAL_COMMENT',
  PointNormalReview = 'POINT_NORMAL_REVIEW',
  PointPostHasManyLikes = 'POINT_POST_HAS_MANY_LIKES',
  PointPostWithHighReactions = 'POINT_POST_WITH_HIGH_REACTIONS',
  PointReferee = 'POINT_REFEREE',
  PointReferral = 'POINT_REFERRAL',
  PointReferralBonus = 'POINT_REFERRAL_BONUS',
  PointReportedComment = 'POINT_REPORTED_COMMENT',
  PointRequestProduct = 'POINT_REQUEST_PRODUCT',
  PointSignUp = 'POINT_SIGN_UP',
  PointVideoLikes = 'POINT_VIDEO_LIKES',
  PointWeeklyActive = 'POINT_WEEKLY_ACTIVE',
  ReactComment = 'REACT_COMMENT',
  ReactPost = 'REACT_POST',
  ReplyComment = 'REPLY_COMMENT',
  ReplyPost = 'REPLY_POST',
  ReviewedEcommerceProduct = 'REVIEWED_ECOMMERCE_PRODUCT',
  ReviewApproved = 'REVIEW_APPROVED',
  ReviewRejected = 'REVIEW_REJECTED',
  ReviewReported = 'REVIEW_REPORTED',
  ShopCancelledEcommerceOrder = 'SHOP_CANCELLED_ECOMMERCE_ORDER',
  SystemCancelledEcommerceOrder = 'SYSTEM_CANCELLED_ECOMMERCE_ORDER',
  TrialEvent = 'TRIAL_EVENT',
  UserCancelledEcommerceOrder = 'USER_CANCELLED_ECOMMERCE_ORDER',
  UserRequest = 'USER_REQUEST'
}

export type NotificationOrderByInput = {
  created_at?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type NotificationWhereInput = {
  eventAdminType?: InputMaybe<Scalars['Float']>;
  id?: InputMaybe<Scalars['ID']>;
  idGt?: InputMaybe<Scalars['ID']>;
  idGte?: InputMaybe<Scalars['ID']>;
  idIn?: InputMaybe<Array<Scalars['ID']>>;
  idLt?: InputMaybe<Scalars['ID']>;
  idLte?: InputMaybe<Scalars['ID']>;
  idNot?: InputMaybe<Scalars['ID']>;
  idNotIn?: InputMaybe<Array<Scalars['ID']>>;
  isViewed?: InputMaybe<Scalars['Boolean']>;
  noObjectId?: InputMaybe<Scalars['Int']>;
  user?: InputMaybe<UserWhereUniqueInput>;
};

export type NotificationWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  noObjectId?: InputMaybe<Scalars['Int']>;
};

export type NotificationsAggregate = {
  __typename?: 'NotificationsAggregate';
  count: Scalars['Int'];
};

export type NotificationsConnection = {
  __typename?: 'NotificationsConnection';
  aggregate: NotificationsAggregate;
};

export enum OauthProvider {
  Apple = 'Apple',
  Facebook = 'Facebook',
  Instagram = 'Instagram'
}

export enum ObjectType {
  Event = 'EVENT',
  Funding = 'FUNDING',
  PreOrder = 'PRE_ORDER'
}

export type Order = {
  __typename?: 'Order';
  address?: Maybe<Address>;
  cartItems: Array<CartItem>;
  cartItemsConnection: CartItemsConnection;
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  shop: Shop;
  status: OrderStatus;
  updatedAt: Scalars['DateTime'];
  user: User;
};


export type OrderCartItemsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CartItemOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CartItemWhereInput>;
};


export type OrderCartItemsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CartItemOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CartItemWhereInput>;
};

export enum OrderBy {
  Asc = 'ASC',
  AscNullsFirst = 'ASC_NULLS_FIRST',
  AscNullsLast = 'ASC_NULLS_LAST',
  Desc = 'DESC',
  DescNullsFirst = 'DESC_NULLS_FIRST',
  DescNullsLast = 'DESC_NULLS_LAST'
}

export type OrderCreateInput = {
  cartItems: CartItemCreateManyWithoutOrderInput;
  shippingAddress: ShippingAddressCreateOneWithoutOrderInput;
  shop: ShopCreateOneWithoutOrderInput;
};

export type OrderCreateOneWithoutCartItemInput = {
  connect: OrderWhereUniqueInput;
};

export type OrderError = Error & {
  __typename?: 'OrderError';
  code: OrderErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum OrderErrorCode {
  OrderCancelled = 'ORDER_CANCELLED',
  OrderConfirmed = 'ORDER_CONFIRMED'
}

export type OrderOrError = CommonError | Order | OrderError;

export type OrderOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type OrderShippingFeeInput = {
  address?: InputMaybe<Scalars['String']>;
  deliverOption: DeliverOption;
  district: Scalars['String'];
  orderShopInfo: Array<OrderShopInput>;
  province: Scalars['String'];
  ward?: InputMaybe<Scalars['String']>;
};

export type OrderShopInput = {
  shopId: Scalars['Int'];
  weight: Scalars['Float'];
};

export type OrderStatisticOfShop = {
  __typename?: 'OrderStatisticOfShop';
  billTotal?: Maybe<Scalars['Int']>;
  ecommFeeTotal?: Maybe<Scalars['Int']>;
  orderTotal?: Maybe<Scalars['Int']>;
  revenueTotal?: Maybe<Scalars['Int']>;
  shippingFeeTotal?: Maybe<Scalars['Int']>;
};

export type OrderStatisticWithPeriod = {
  __typename?: 'OrderStatisticWithPeriod';
  orderTotal?: Maybe<Array<Scalars['Int']>>;
  revenueTotal?: Maybe<Array<Scalars['Int']>>;
  time?: Maybe<Array<Scalars['String']>>;
};

export enum OrderStatus {
  Cancelled = 'CANCELLED',
  Confirmed = 'CONFIRMED',
  Created = 'CREATED'
}

export type OrderUpdateInput = {
  cartItems?: InputMaybe<CartItemUpdateManyWithoutOrderInput>;
  shippingAddress?: InputMaybe<ShippingAddressCreateOneWithoutOrderInput>;
};

export type OrderUpdateOneWithoutCartItemsInput = {
  connect: OrderWhereUniqueInput;
};

export type OrderWhereInput = {
  cartItemsSome?: InputMaybe<CartItemWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  shop?: InputMaybe<ShopWhereInput>;
  status?: InputMaybe<OrderStatus>;
  user?: InputMaybe<UserWhereInput>;
};

export type OrderWhereUniqueInput = {
  id: Scalars['Int'];
};

export type OrdersAggregate = {
  __typename?: 'OrdersAggregate';
  count: Scalars['Int'];
};

export type OrdersConnection = {
  __typename?: 'OrdersConnection';
  aggregate: OrdersAggregate;
};

export enum PackagingtUnit {
  Box = 'BOX',
  Pack = 'PACK',
  Pill = 'PILL',
  Tbsp = 'TBSP',
  Tube = 'TUBE'
}

export enum PaymentMethod {
  Cod = 'COD',
  Vnpay = 'VNPAY'
}

export enum PeriodType {
  Day = 'DAY',
  Month = 'MONTH',
  Quarter = 'QUARTER',
  Week = 'WEEK'
}

export type PointEntityUnion = Post | PostComment | Review | User | VoucherOrder;

export type PointHistoriesAggregate = {
  __typename?: 'PointHistoriesAggregate';
  count: Scalars['Int'];
  sum: Scalars['Int'];
};

export type PointHistoriesConnection = {
  __typename?: 'PointHistoriesConnection';
  aggregate: PointHistoriesAggregate;
};

export type PointHistory = {
  __typename?: 'PointHistory';
  createdAt: Scalars['DateTime'];
  entity?: Maybe<PointEntityUnion>;
  id: Scalars['Int'];
  point: Scalars['Int'];
  status: PointStatus;
  type: PointType;
};

export type PointHistoryOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type PointHistoryStatistic = {
  __typename?: 'PointHistoryStatistic';
  pointTotal?: Maybe<Array<Scalars['Int']>>;
  time?: Maybe<Array<Scalars['String']>>;
};

export type PointHistoryStatisticWhereInput = {
  fromDate?: InputMaybe<Scalars['DateTime']>;
  status?: InputMaybe<Scalars['Boolean']>;
  toDate?: InputMaybe<Scalars['DateTime']>;
};

export type PointHistoryWhereInput = {
  pointGt?: InputMaybe<Scalars['Int']>;
  redeemable?: InputMaybe<Scalars['Boolean']>;
  review?: InputMaybe<ReviewWhereInput>;
  typeNot?: InputMaybe<PointType>;
  user?: InputMaybe<UserWhereInput>;
};

export enum PointStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Rejected = 'REJECTED'
}

export enum PointType {
  ApprovedReviewReport = 'APPROVED_REVIEW_REPORT',
  ApprovedVideo = 'APPROVED_VIDEO',
  Attend_5TrialEvent = 'ATTEND_5_TRIAL_EVENT',
  Attend_10TrialEvent = 'ATTEND_10_TRIAL_EVENT',
  BestAnswer = 'BEST_ANSWER',
  DailyActive = 'DAILY_ACTIVE',
  EditProduct = 'EDIT_PRODUCT',
  EventPoints = 'EVENT_POINTS',
  ExchangePointsForCoupons = 'EXCHANGE_POINTS_FOR_COUPONS',
  First_5ApprovedReviews = 'FIRST_5_APPROVED_REVIEWS',
  First_10ApprovedReviews = 'FIRST_10_APPROVED_REVIEWS',
  First_50ApprovedReviews = 'FIRST_50_APPROVED_REVIEWS',
  FirstApprovedVideo = 'FIRST_APPROVED_VIDEO',
  FirstComment = 'FIRST_COMMENT',
  FirstPost = 'FIRST_POST',
  FirstReview = 'FIRST_REVIEW',
  Follow_20Users = 'FOLLOW_20_USERS',
  Gift = 'GIFT',
  Have_20Followers = 'HAVE_20_FOLLOWERS',
  Have_50Followers = 'HAVE_50_FOLLOWERS',
  Have_100Followers = 'HAVE_100_FOLLOWERS',
  MonthlyActive = 'MONTHLY_ACTIVE',
  NormalComment = 'NORMAL_COMMENT',
  NormalReview = 'NORMAL_REVIEW',
  PostHasManyLikes = 'POST_HAS_MANY_LIKES',
  PostWithHighReactions = 'POST_WITH_HIGH_REACTIONS',
  Referee = 'REFEREE',
  Referral = 'REFERRAL',
  ReferralBonus = 'REFERRAL_BONUS',
  ReportedComment = 'REPORTED_COMMENT',
  RequestProduct = 'REQUEST_PRODUCT',
  SignUp = 'SIGN_UP',
  VideoLikes = 'VIDEO_LIKES',
  Voucher = 'VOUCHER',
  WeeklyActive = 'WEEKLY_ACTIVE'
}

export type Post = {
  __typename?: 'Post';
  author: User;
  bestAnswer?: Maybe<PostComment>;
  bookMark?: Maybe<Scalars['Boolean']>;
  category?: Maybe<PostCategory>;
  commentedAt?: Maybe<Scalars['DateTime']>;
  comments?: Maybe<Array<PostComment>>;
  commentsConnection: PostCommentsConnection;
  content?: Maybe<Scalars['String']>;
  countPostLoaderViewedByUser?: Maybe<Scalars['Float']>;
  countViewedPostConnection?: Maybe<Scalars['Float']>;
  createdAt: Scalars['DateTime'];
  fakePostView?: Maybe<Scalars['Float']>;
  hashtags?: Maybe<Array<Hashtag>>;
  id: Scalars['Int'];
  images: Array<Image>;
  imagesConnection: PostImagesConnection;
  isBookMark?: Maybe<Scalars['Boolean']>;
  isUnsubscribedByViewer?: Maybe<Scalars['Boolean']>;
  points?: Maybe<Scalars['Int']>;
  postToProducts?: Maybe<Array<PostToProduct>>;
  products: Array<Product>;
  productsConnection: PostProductsConnection;
  /** @deprecated deprecationReason: 'use reactionOfViewer instead', */
  reactStatus: ReactStatus;
  reactionOfViewer: ReactStatus;
  reactionsConnection: PostReactionsConnection;
  realPostView?: Maybe<Scalars['Float']>;
  review?: Maybe<Review>;
  savedByViewer?: Maybe<Scalars['Boolean']>;
  savedDateByViewer?: Maybe<Scalars['DateTime']>;
  shopProductViewStore?: Maybe<ProductViewStore>;
  slug?: Maybe<Scalars['String']>;
  status: PostStatus;
  title?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
  video?: Maybe<Video>;
  videos?: Maybe<Array<VideoObject>>;
  viewedPostStatistic?: Maybe<ViewedPostStatistic>;
};


export type PostCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type PostCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type PostImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type PostProductsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};

export type PostArgsWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type PostCategory = {
  __typename?: 'PostCategory';
  id: Scalars['Int'];
  isCreatable: Scalars['Boolean'];
  menuOrder: Scalars['Int'];
  translations: Array<PostCategoryTranslation>;
};


export type PostCategoryTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCategoryTranslationWhereInput>;
};

export type PostCategoryCreateOneWithoutPostsInput = {
  connect?: InputMaybe<PostCategoryWhereUniqueInput>;
};

export type PostCategoryOrderByInput = {
  id?: InputMaybe<OrderBy>;
  menuOrder?: InputMaybe<OrderBy>;
};

export type PostCategoryTranslation = {
  __typename?: 'PostCategoryTranslation';
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isOriginal: Scalars['Boolean'];
  language: Scalars['String'];
  name: Scalars['String'];
};

export type PostCategoryTranslationWhereInput = {
  language?: InputMaybe<LanguageCode>;
};

export type PostCategoryUpdateOneWithoutPostsInput = {
  connect?: InputMaybe<PostCategoryWhereUniqueInput>;
};

export type PostCategoryWhereInput = {
  id?: InputMaybe<Scalars['ID']>;
  idGt?: InputMaybe<Scalars['ID']>;
  idGte?: InputMaybe<Scalars['ID']>;
  idIn?: InputMaybe<Array<Scalars['ID']>>;
  idLt?: InputMaybe<Scalars['ID']>;
  idLte?: InputMaybe<Scalars['ID']>;
  idNot?: InputMaybe<Scalars['ID']>;
  idNotIn?: InputMaybe<Array<Scalars['ID']>>;
  isCreatable?: InputMaybe<Scalars['Boolean']>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type PostCategoryWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
};

export type PostComment = {
  __typename?: 'PostComment';
  author: User;
  children: Array<PostComment>;
  childrenConnection: PostCommentChildrenConnection;
  content?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  images: Array<Image>;
  imagesConnection: PostCommentImagesConnection;
  parent?: Maybe<PostComment>;
  post: Post;
  /** @deprecated use reactionOfViewer instead */
  reactStatus: ReactStatus;
  reactionConnections: PostCommentReactionsConnection;
  reactionOfViewer: ReactStatus;
  status: PostCommentStatus;
  updatedAt: Scalars['DateTime'];
};


export type PostCommentChildrenArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type PostCommentChildrenConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type PostCommentImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};

export type PostCommentChildrenAggregate = {
  __typename?: 'PostCommentChildrenAggregate';
  count: Scalars['Int'];
};

export type PostCommentChildrenConnection = {
  __typename?: 'PostCommentChildrenConnection';
  aggregate: PostCommentChildrenAggregate;
};

export type PostCommentCreateInput = {
  content?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<ImageCreateManyWithoutPostCommentInput>;
  mentions?: InputMaybe<Array<UserWhereUniqueInput>>;
  parent?: InputMaybe<PostCommentCreateOneWithoutChildrenInput>;
  post: PostCreateOneWithoutPostCommentsInput;
  review?: InputMaybe<ReviewCreateOneWithoutPostCommentsInput>;
};

export type PostCommentCreateManyWithoutPostInput = {
  create?: InputMaybe<Array<PostCommentCreateWithoutPostInput>>;
};

export type PostCommentCreateOneWithoutChildrenInput = {
  connect?: InputMaybe<PostCommentWhereUniqueInput>;
};

export type PostCommentCreateOneWithoutReportsInput = {
  connect?: InputMaybe<PostCommentWhereUniqueInput>;
};

export type PostCommentCreateWithoutPostInput = {
  content?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<ImageCreateManyWithoutPostCommentInput>;
  mentions?: InputMaybe<Array<UserWhereUniqueInput>>;
};

export type PostCommentImagesAggregate = {
  __typename?: 'PostCommentImagesAggregate';
  count: Scalars['Int'];
};

export type PostCommentImagesConnection = {
  __typename?: 'PostCommentImagesConnection';
  aggregate: PostCommentImagesAggregate;
};

export type PostCommentOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type PostCommentReactionsAggregate = {
  __typename?: 'PostCommentReactionsAggregate';
  sum: PostCommentReactionsSum;
};

export type PostCommentReactionsConnection = {
  __typename?: 'PostCommentReactionsConnection';
  aggregate: PostCommentReactionsAggregate;
};

export type PostCommentReactionsSum = {
  __typename?: 'PostCommentReactionsSum';
  value: Scalars['Int'];
};

export type PostCommentReport = {
  __typename?: 'PostCommentReport';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  postComment: PostComment;
  postCommentId: Scalars['Int'];
  reason?: Maybe<Scalars['String']>;
  status: PostCommentReportStatus;
  type: PostCommentReportType;
  updatedAt: Scalars['DateTime'];
  user: User;
  userId: Scalars['Int'];
};

export type PostCommentReportCreateInput = {
  postComment: PostCommentCreateOneWithoutReportsInput;
  reason?: InputMaybe<Scalars['String']>;
  type?: InputMaybe<PostCommentReportType>;
};

export type PostCommentReportOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export enum PostCommentReportStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Rejected = 'REJECTED'
}

export enum PostCommentReportType {
  Advertising = 'ADVERTISING',
  Harass = 'HARASS',
  Other = 'OTHER',
  PersonalInformation = 'PERSONAL_INFORMATION',
  Spam = 'SPAM',
  Swearing = 'SWEARING'
}

export type PostCommentReportWhereInput = {
  comment?: InputMaybe<PostCommentWhereInput>;
  post?: InputMaybe<PostWhereInput>;
  reasonContains?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<PostCommentReportStatus>;
  user?: InputMaybe<UserWhereInput>;
};

export type PostCommentReportsAggregate = {
  __typename?: 'PostCommentReportsAggregate';
  count: Scalars['Int'];
};

export type PostCommentReportsConnection = {
  __typename?: 'PostCommentReportsConnection';
  aggregate: PostCommentReportsAggregate;
};

export enum PostCommentStatus {
  Created = 'CREATED',
  Hidden = 'HIDDEN',
  Reported = 'REPORTED'
}

export type PostCommentUpdateInput = {
  content?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<ImageUpdateManyWithoutPostCommentInput>;
};

export type PostCommentWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  idGt?: InputMaybe<Scalars['Int']>;
  idGte?: InputMaybe<Scalars['Int']>;
  idIsNull?: InputMaybe<Scalars['Boolean']>;
  idLt?: InputMaybe<Scalars['Int']>;
  idLte?: InputMaybe<Scalars['Int']>;
  parent?: InputMaybe<PostCommentWhereInput>;
  post?: InputMaybe<PostWhereInput>;
};

export type PostCommentWhereUniqueInput = {
  id: Scalars['Int'];
};

export type PostCommentWhereUserInput = {
  id: Scalars['Int'];
};

export type PostCommentsAggregate = {
  __typename?: 'PostCommentsAggregate';
  count: Scalars['Int'];
};

export type PostCommentsConnection = {
  __typename?: 'PostCommentsConnection';
  aggregate: PostCommentsAggregate;
};

export type PostCreateError = Error & {
  __typename?: 'PostCreateError';
  code: PostCreateErrorCode;
  currentValue?: Maybe<Scalars['Int']>;
  expectedValue?: Maybe<Scalars['Int']>;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum PostCreateErrorCode {
  ExceedMaxSharedPointsPerDay = 'EXCEED_MAX_SHARED_POINTS_PER_DAY',
  HaveNotVerifiedEmail = 'HAVE_NOT_VERIFIED_EMAIL',
  HaveNotVerifiedPhoneNumber = 'HAVE_NOT_VERIFIED_PHONE_NUMBER',
  NotEnoughPoints = 'NOT_ENOUGH_POINTS'
}

export type PostCreateInput = {
  category?: InputMaybe<PostCategoryCreateOneWithoutPostsInput>;
  comments?: InputMaybe<PostCommentCreateManyWithoutPostInput>;
  content?: InputMaybe<Scalars['String']>;
  hashtags?: InputMaybe<Array<HashtagInput>>;
  images?: InputMaybe<ImageCreateManyWithoutPostInput>;
  isNewApi?: InputMaybe<Scalars['Boolean']>;
  mentions?: InputMaybe<Array<UserWhereUniqueInput>>;
  points?: InputMaybe<Scalars['Int']>;
  products?: InputMaybe<ProductCreateManyWithoutPostInput>;
  review?: InputMaybe<ReviewCreateOneWithoutPostInput>;
  shopProductId?: InputMaybe<Scalars['Int']>;
  slug?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  videoKey?: InputMaybe<Scalars['String']>;
  videos?: InputMaybe<VideoCreateManyWithoutPostInput>;
};

export type PostCreateOneWithoutPostCommentsInput = {
  connect?: InputMaybe<PostWhereUniqueInput>;
  create?: InputMaybe<PostCreateWithoutPostCommentsInput>;
};

export type PostCreateOneWithoutReportsInput = {
  connect?: InputMaybe<PostWhereUniqueInput>;
};

export type PostCreateWithoutPostCommentsInput = {
  review?: InputMaybe<ReviewCreateOneWithoutPostInput>;
};

export type PostImagesAggregate = {
  __typename?: 'PostImagesAggregate';
  count: Scalars['Int'];
};

export type PostImagesConnection = {
  __typename?: 'PostImagesConnection';
  aggregate: PostImagesAggregate;
};

export type PostOrError = CommonError | Post | PostCreateError;

export type PostOrderByInput = {
  clicksAggregate?: InputMaybe<Scalars['String']>;
  commentedAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  likesAggregate?: InputMaybe<Scalars['String']>;
  newest?: InputMaybe<OrderBy>;
};

export type PostProductsAggregate = {
  __typename?: 'PostProductsAggregate';
  count: Scalars['Int'];
};

export type PostProductsConnection = {
  __typename?: 'PostProductsConnection';
  aggregate: PostProductsAggregate;
};

export enum PostQueryType {
  Discovery = 'DISCOVERY',
  Following = 'FOLLOWING',
  Normal = 'NORMAL'
}

export type PostReactionsAggregate = {
  __typename?: 'PostReactionsAggregate';
  sum: PostReactionsSum;
};

export type PostReactionsConnection = {
  __typename?: 'PostReactionsConnection';
  aggregate: PostReactionsAggregate;
};

export type PostReactionsSum = {
  __typename?: 'PostReactionsSum';
  value: Scalars['Int'];
};

export type PostRecommentdationEntityUnion = Post | Review;

export type PostReport = {
  __typename?: 'PostReport';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  post: Post;
  postId: Scalars['Int'];
  reason?: Maybe<Scalars['String']>;
  status?: Maybe<PostReportStatus>;
  type: PostReportType;
  updatedAt: Scalars['DateTime'];
  user: User;
  userId: Scalars['Int'];
};

export type PostReportCreateInput = {
  post: PostCreateOneWithoutReportsInput;
  reason?: InputMaybe<Scalars['String']>;
  type?: InputMaybe<PostReportType>;
};

export enum PostReportStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Rejected = 'REJECTED'
}

export enum PostReportType {
  Advertising = 'ADVERTISING',
  Other = 'OTHER',
  PersonalInformation = 'PERSONAL_INFORMATION',
  Repeat = 'REPEAT',
  Swearing = 'SWEARING'
}

export type PostReservedSlot = {
  __typename?: 'PostReservedSlot';
  id: Scalars['Int'];
  post?: Maybe<Post>;
  post_id: Scalars['String'];
  slot_name: Scalars['String'];
  slot_position: Scalars['String'];
  type: PostReservedSlotType;
};

export type PostReservedSlotConnection = {
  __typename?: 'PostReservedSlotConnection';
  total: Scalars['Int'];
};

export type PostReservedSlotCreateInput = {
  post_id: Scalars['Float'];
  slot_name: Scalars['String'];
  slot_position: Scalars['String'];
  type: Scalars['String'];
};

export type PostReservedSlotHistory = {
  __typename?: 'PostReservedSlotHistory';
  id: Scalars['Int'];
  post?: Maybe<Post>;
  post_id: Scalars['Float'];
  slot_id?: Maybe<Scalars['Float']>;
  updated_at: Scalars['DateTime'];
  updated_user: Scalars['Float'];
  user: User;
};

export type PostReservedSlotInput = {
  post_id: Scalars['Float'];
  slot_name?: InputMaybe<Scalars['String']>;
  slot_position?: InputMaybe<Scalars['String']>;
  type: Scalars['String'];
};

export type PostReservedSlotOrderByInput = {
  id?: InputMaybe<OrderBy>;
  updatedAt?: InputMaybe<OrderBy>;
};

export enum PostReservedSlotType {
  Mobile = 'MOBILE',
  Web = 'WEB'
}

export type PostReservedSlotWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  post_id?: InputMaybe<Scalars['Int']>;
  slot_name?: InputMaybe<Scalars['String']>;
  slot_position?: InputMaybe<Scalars['Int']>;
  type?: InputMaybe<Scalars['String']>;
};

export enum PostStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Hidden = 'HIDDEN'
}

export type PostToProduct = {
  __typename?: 'PostToProduct';
  externalLinks?: Maybe<Array<ExternalLink>>;
  price?: Maybe<Scalars['Int']>;
  product: Product;
  productId: Scalars['Int'];
  shop?: Maybe<Shop>;
  shopId?: Maybe<Scalars['Int']>;
};

export type PostUpdateInput = {
  category?: InputMaybe<PostCategoryUpdateOneWithoutPostsInput>;
  content?: InputMaybe<Scalars['String']>;
  hashtags?: InputMaybe<Array<HashtagInput>>;
  images?: InputMaybe<ImageUpdateManyWithoutPostInput>;
  products?: InputMaybe<ProductUpdateManyWithoutPostInput>;
  shopProductId?: InputMaybe<Scalars['Int']>;
  slug?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  video?: InputMaybe<VideoUpdateOneWithoutPostInput>;
  videos?: InputMaybe<VideoUpdateManyWithoutPostInput>;
};

export type PostWhereInput = {
  author?: InputMaybe<UserWhereInput>;
  category?: InputMaybe<PostCategoryWhereInput>;
  commentedAtLt?: InputMaybe<Scalars['DateTime']>;
  contentContains?: InputMaybe<Scalars['String']>;
  createdAtGte?: InputMaybe<Scalars['DateTime']>;
  createdAtLte?: InputMaybe<Scalars['DateTime']>;
  hashtagsSome?: InputMaybe<HashtagWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  idGt?: InputMaybe<Scalars['Int']>;
  idGte?: InputMaybe<Scalars['Int']>;
  idLt?: InputMaybe<Scalars['Int']>;
  idLte?: InputMaybe<Scalars['Int']>;
  inId?: InputMaybe<Array<Scalars['Int']>>;
  isBookMark?: InputMaybe<Scalars['Boolean']>;
  productsSome?: InputMaybe<ProductWhereInput>;
  queryType?: InputMaybe<PostQueryType>;
  review?: InputMaybe<ReviewWhereInput>;
  status?: InputMaybe<PostStatus>;
  text?: InputMaybe<Scalars['String']>;
  video?: InputMaybe<VideoWhereInput>;
};

export type PostWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type PostsAggregate = {
  __typename?: 'PostsAggregate';
  count: Scalars['Int'];
};

export type PostsConnection = {
  __typename?: 'PostsConnection';
  aggregate: PostsAggregate;
};

export type PresignedUrl = {
  __typename?: 'PresignedUrl';
  key: Scalars['String'];
  url: Scalars['String'];
};

export type Product = {
  __typename?: 'Product';
  alreadySoldByViewer: Scalars['Boolean'];
  amount?: Maybe<Scalars['Float']>;
  amountUnit?: Maybe<Scalars['String']>;
  attributes?: Maybe<ProductAttribute>;
  barcodes: Array<Barcode>;
  barcodesConnection: BarcodesConnection;
  /** @deprecated use numberOfUserBookmarks instead */
  bookmarkedByUser: Scalars['Int'];
  brand: Brand;
  categories: Array<Category>;
  /** @deprecated please use functionsV2 instead */
  functions: Array<SpecialIngredientFunction>;
  /** @deprecated please use functionsConnectionV2 instead, this field will be removed on 01/01/2022 */
  functionsConnection: SpecialIngredientFunctionsConnection;
  functionsConnectionV2: FunctionsConnection;
  functionsV2: Array<Function>;
  highlightProductRanking?: Maybe<HighlightProduct>;
  id: Scalars['Int'];
  images: Array<Image>;
  imagesConnection: ImagesConnection;
  ingredientCautionsConnection: IngredientCautionsConnection;
  ingredients: Array<Ingredient>;
  ingredientsConnection: IngredientsConnection;
  isUsedByViewer?: Maybe<Scalars['Boolean']>;
  manufacturer?: Maybe<Scalars['String']>;
  manufacturerAddress?: Maybe<Scalars['String']>;
  measure: Scalars['String'];
  numberOfUserBookmarks: Scalars['Int'];
  numberOfUsers: Scalars['Int'];
  postsConnection: PostsConnection;
  price?: Maybe<Scalars['Int']>;
  priceUnit: Scalars['String'];
  productToIngredients: Array<ProductToIngredient>;
  productToIngredientsConnection: ProductToIngredientsConnection;
  rankings?: Maybe<Array<ProductRanking>>;
  rankingsV2: Array<ProductRanking>;
  reviewedByViewer: Scalars['Boolean'];
  reviews?: Maybe<Array<Review>>;
  reviewsConnection: ReviewsConnection;
  reviewsCountByRate: ReviewsCountByRate;
  soldByShops: Scalars['Boolean'];
  status: ProductStatus;
  stockInfoOfViewer?: Maybe<ShopToProduct>;
  thumbnail?: Maybe<Image>;
  translations: Array<ProductTranslation>;
  type: ProductType;
  uid: Scalars['ID'];
  updatedAt: Scalars['DateTime'];
  videoTitle?: Maybe<Scalars['String']>;
  videoUrl?: Maybe<Scalars['String']>;
  wishedByViewer: Scalars['Boolean'];
};


export type ProductBarcodesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type ProductBarcodesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BarcodeOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BarcodeWhereInput>;
};


export type ProductFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SpecialIngredientFunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SpecialIngredientFunctionWhereInput>;
};


export type ProductFunctionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SpecialIngredientFunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SpecialIngredientFunctionWhereInput>;
};


export type ProductFunctionsConnectionV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type ProductFunctionsV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type ProductImagesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ImageWhereInput>;
};


export type ProductImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ImageWhereInput>;
};


export type ProductIngredientCautionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type ProductIngredientsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientWhereInput>;
};


export type ProductIngredientsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientWhereInput>;
};


export type ProductPostsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<PostOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostWhereInput>;
};


export type ProductProductToIngredientsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductToIngredientOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductToIngredientWhereInput>;
};


export type ProductProductToIngredientsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductToIngredientOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductToIngredientWhereInput>;
};


export type ProductRankingsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type ProductRankingsV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type ProductReviewsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type ProductReviewsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type ProductTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductTranslationWhereInput>;
};

export type ProductAttribute = {
  __typename?: 'ProductAttribute';
  energyPerPackage?: Maybe<Scalars['Float']>;
  energyUnit?: Maybe<Unit>;
  netVolumePerPackage?: Maybe<Scalars['Float']>;
  netWeightPerPackage?: Maybe<Scalars['Float']>;
  packagingUnit?: Maybe<PackagingtUnit>;
  rdi?: Maybe<Scalars['Float']>;
  volumeUnit?: Maybe<Unit>;
  weightUnit?: Maybe<Unit>;
};

export type ProductCreateManyWithoutPostInput = {
  connect?: InputMaybe<Array<ProductWhereUniqueInput>>;
};

export type ProductCreateNestedOneWithoutProductUsageLogInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductCreateOneWithoutEditInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductCreateOneWithoutLiveStreamProductInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductCreateOneWithoutShopToProductsInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductEdit = {
  __typename?: 'ProductEdit';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  images?: Maybe<Array<Scalars['String']>>;
  status?: Maybe<ProductEditStatus>;
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type ProductEditCreateInput = {
  content: Scalars['String'];
  images?: InputMaybe<Array<Scalars['String']>>;
  product: ProductCreateOneWithoutEditInput;
};

export enum ProductEditStatus {
  Created = 'CREATED'
}

export type ProductOrderByInput = {
  id?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
  productToIngredients?: InputMaybe<ProductToIngredientOrderByInput>;
  reviewsConnection?: InputMaybe<ReviewsAggregateOrderByInput>;
  updatedAt?: InputMaybe<OrderBy>;
};

export type ProductRanking = {
  __typename?: 'ProductRanking';
  category: Category;
  product: Product;
  ranking: Scalars['Int'];
  rankingChange?: Maybe<Scalars['Int']>;
};

export type ProductRankingBrandIds = {
  __typename?: 'ProductRankingBrandIds';
  brandIds: Array<Scalars['Int']>;
};

export type ProductRankingOrderByInput = {
  product?: InputMaybe<ProductOrderByInput>;
  ranking?: InputMaybe<OrderBy>;
};

export type ProductRankingWhereInput = {
  category?: InputMaybe<CategoryWhereInput>;
  product?: InputMaybe<ProductWhereInput>;
};

export type ProductRankingsAggregate = {
  __typename?: 'ProductRankingsAggregate';
  count: Scalars['Int'];
  countV2: Scalars['Int'];
};

export type ProductRankingsConnection = {
  __typename?: 'ProductRankingsConnection';
  aggregate: ProductRankingsAggregate;
};

export type ProductRequest = {
  __typename?: 'ProductRequest';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  images?: Maybe<Array<Scalars['String']>>;
  isDone: Scalars['Boolean'];
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type ProductRequestCreateInput = {
  content: Scalars['String'];
  images?: InputMaybe<Array<Scalars['String']>>;
};

export type ProductRequestOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type ProductRequestWhereInput = {
  contentContains?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['Boolean']>;
};

export type ProductRequestsAggregate = {
  __typename?: 'ProductRequestsAggregate';
  count: Scalars['Int'];
};

export type ProductRequestsConnection = {
  __typename?: 'ProductRequestsConnection';
  aggregate: ProductRequestsAggregate;
};

export type ProductSearchOrderByInput = {
  avg?: InputMaybe<ReviewAvgOrderByInput>;
  count?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
};

export type ProductSearchResult = {
  __typename?: 'ProductSearchResult';
  brandIds: Array<Scalars['Int']>;
  categoryIdParentChildrens: Array<Scalars['Int']>;
  categoryIdRoots: Array<Category>;
  products: Array<Product>;
  total: Scalars['Int'];
};

export enum ProductStatus {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export type ProductToIngredient = {
  __typename?: 'ProductToIngredient';
  attributes?: Maybe<ProductToIngredientAttribute>;
  ingredient: Ingredient;
  ingredientId: Scalars['ID'];
  level?: Maybe<ProductToIngredientLevel>;
  order?: Maybe<Scalars['Int']>;
  productId: Scalars['ID'];
};

export type ProductToIngredientAttribute = {
  __typename?: 'ProductToIngredientAttribute';
  amount?: Maybe<Scalars['Float']>;
  amountUnit?: Maybe<Unit>;
};

export enum ProductToIngredientLevel {
  Major = 'MAJOR',
  Minor = 'MINOR'
}

export type ProductToIngredientOrderByInput = {
  amount?: InputMaybe<OrderBy>;
  ingredient?: InputMaybe<IngredientOrderByInput>;
  level?: InputMaybe<OrderBy>;
  order?: InputMaybe<OrderBy>;
};

export type ProductToIngredientWhereInput = {
  ingredient?: InputMaybe<IngredientWhereInput>;
  level?: InputMaybe<ProductToIngredientLevel>;
  product?: InputMaybe<ProductWhereInput>;
};

export type ProductToIngredientsAggregate = {
  __typename?: 'ProductToIngredientsAggregate';
  count: Scalars['Int'];
};

export type ProductToIngredientsConnection = {
  __typename?: 'ProductToIngredientsConnection';
  aggregate: ProductToIngredientsAggregate;
};

export type ProductTranslation = {
  __typename?: 'ProductTranslation';
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  imageDescription?: Maybe<Scalars['String']>;
  isOriginal: Scalars['Boolean'];
  language: LanguageCode;
  name: Scalars['String'];
  product: Product;
  slug?: Maybe<Scalars['String']>;
};

export type ProductTranslationOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type ProductTranslationWhereInput = {
  language?: InputMaybe<LanguageCode>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export enum ProductType {
  Cosmetic = 'COSMETIC',
  FunctionalFood = 'FUNCTIONAL_FOOD'
}

export type ProductUpdateManyWithoutPostInput = {
  set?: InputMaybe<Array<ProductWhereUniqueInput>>;
};

export type ProductUpdateOneWithoutLiveStreamProductInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductUsageLog = {
  __typename?: 'ProductUsageLog';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  product: Product;
  productId: Scalars['Int'];
  userId: Scalars['Int'];
};

export type ProductUsageLogCreateInput = {
  product: ProductCreateNestedOneWithoutProductUsageLogInput;
};

export type ProductUsageLogOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type ProductUsageLogWhereInput = {
  createdAt?: InputMaybe<DateTimeFilter>;
  productId?: InputMaybe<Scalars['ID']>;
  userId?: InputMaybe<Scalars['ID']>;
};

export type ProductVariant = {
  __typename?: 'ProductVariant';
  attributes?: Maybe<Array<Attribute>>;
  id: Scalars['Int'];
  images?: Maybe<Images>;
  isBlocked?: Maybe<Scalars['Boolean']>;
  isVisible?: Maybe<Scalars['Boolean']>;
  originalPrice: Scalars['Int'];
  price: Scalars['Int'];
  productId: Scalars['Int'];
  quantity: Scalars['Int'];
  shopProduct?: Maybe<ShopProduct>;
  soldQuantity: Scalars['Int'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type ProductVariantConnection = {
  __typename?: 'ProductVariantConnection';
  aggregate: ProductViewstoreAggregate;
};

export type ProductVariantInsertInput = {
  attributes?: InputMaybe<AttributeUpsertInput>;
  images?: InputMaybe<ImageUpsertInput>;
  originalPrice: Scalars['Int'];
  price: Scalars['Int'];
  quantity: Scalars['Int'];
};

export type ProductVariantOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
  soldQuantity?: InputMaybe<OrderBy>;
};

export type ProductVariantUpdateInput = {
  attributes?: InputMaybe<AttributeUpsertInput>;
  images?: InputMaybe<ImageUpsertInput>;
  isBlocked?: InputMaybe<Scalars['Boolean']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  originalPrice?: InputMaybe<Scalars['Int']>;
  price?: InputMaybe<Scalars['Int']>;
  quantity?: InputMaybe<Scalars['Int']>;
  variantId?: InputMaybe<Scalars['Int']>;
};

export type ProductVariantWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  price?: InputMaybe<IntFilter>;
  productId?: InputMaybe<Scalars['Int']>;
  quantity?: InputMaybe<IntFilter>;
  soldQuantity?: InputMaybe<IntFilter>;
};

export type ProductViewStore = {
  __typename?: 'ProductViewStore';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  isBlocked?: Maybe<Scalars['Boolean']>;
  isVisible?: Maybe<Scalars['Boolean']>;
  post?: Maybe<Post>;
  postId?: Maybe<Scalars['Int']>;
  productId?: Maybe<Scalars['Int']>;
  shopId?: Maybe<Scalars['Int']>;
  shopProduct?: Maybe<ShopProduct>;
};

export type ProductViewStoreConnection = {
  __typename?: 'ProductViewStoreConnection';
  aggregate: ProductViewstoreAggregate;
};

export type ProductViewStoreOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type ProductViewStoreWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  isBlocked?: InputMaybe<Scalars['Boolean']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  postId?: InputMaybe<Scalars['Int']>;
  productId?: InputMaybe<Scalars['Int']>;
  productName?: InputMaybe<Scalars['String']>;
  shopId?: InputMaybe<Scalars['Int']>;
};

export type ProductViewstoreAggregate = {
  __typename?: 'ProductViewstoreAggregate';
  count: Scalars['Int'];
};

export type ProductWhereInput = {
  _id?: InputMaybe<IntFilter>;
  brand?: InputMaybe<BrandWhereInput>;
  brandFilter?: InputMaybe<Array<Scalars['Int']>>;
  brandTextFilter?: InputMaybe<Scalars['String']>;
  categoriesSome?: InputMaybe<CategoryWhereInput>;
  categoryFilter?: InputMaybe<Array<Scalars['Int']>>;
  fansSome?: InputMaybe<UserWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  ingredientsSome?: InputMaybe<IngredientWhereInput>;
  isFunctionalFood?: InputMaybe<BooleanFilter>;
  isUsedByViewer?: InputMaybe<BooleanFilter>;
  nameContains?: InputMaybe<Scalars['String']>;
  price?: InputMaybe<IntFilter>;
  productTextsSearch?: InputMaybe<Array<Scalars['String']>>;
  reviewsSome?: InputMaybe<ReviewWhereInput>;
  shopsSome?: InputMaybe<ShopWhereInput>;
  status?: InputMaybe<ProductStatus>;
  type?: InputMaybe<ProductType>;
  uid?: InputMaybe<Scalars['ID']>;
  updatedAtGte?: InputMaybe<Scalars['DateTime']>;
};

export type ProductWhereUniqueInput = {
  barcode?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type ProductsAggregate = {
  __typename?: 'ProductsAggregate';
  count: Scalars['Int'];
  max: ProductsMax;
};

export type ProductsConnection = {
  __typename?: 'ProductsConnection';
  aggregate: ProductsAggregate;
};

export type ProductsMax = {
  __typename?: 'ProductsMax';
  price: Scalars['Float'];
};

export enum PromotionAmountUnitType {
  Percent = 'PERCENT',
  Vnd = 'VND'
}

export enum PromotionCreatedByType {
  Reviewty = 'REVIEWTY',
  Shop = 'SHOP'
}

export enum PromotionEntityType {
  All = 'ALL',
  Product = 'PRODUCT',
  Shop = 'SHOP',
  Variant = 'VARIANT'
}

export enum PromotionType {
  Discount = 'DISCOUNT',
  FlashSale = 'FLASH_SALE',
  Freeship = 'FREESHIP',
  NewUser = 'NEW_USER'
}

export type Provinces = {
  __typename?: 'Provinces';
  code: Scalars['String'];
  country_id: Scalars['Int'];
  id: Scalars['Int'];
  name: Scalars['String'];
  tax: Scalars['String'];
  tax_name: Scalars['String'];
  tax_percentage: Scalars['String'];
  tax_type: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  analyzeIngredients: IngredientSearchResult;
  bannerImageConnectionAggrs: BannerImageConnection;
  banners: Array<Banner>;
  baumannQuestions: Array<BaumannQuestion>;
  brand?: Maybe<Brand>;
  brandAdmin?: Maybe<BrandAdmin>;
  brandAdmins?: Maybe<Array<BrandAdmin>>;
  brandTranslations: Array<BrandTranslation>;
  brands: Array<Brand>;
  brandsConnection: BrandsConnection;
  cartItems: Array<CartItem>;
  cartProductsOfShop: Array<CartProductsOfShop>;
  cartProductsOfShopConnection: CartProductsOfShopConnection;
  cast: Cast;
  castComments: Array<CastComment>;
  castCommentsConnection: CastCommentsConnection;
  casts: Array<Cast>;
  castsConnection: CastConnection;
  categories: Array<Category>;
  categoriesConnection: CategoriesConnection;
  category?: Maybe<Category>;
  categorySearch: CategorySearchResult;
  caution: IngredientCaution;
  cautions: Array<IngredientCaution>;
  cautionsConnection: IngredientCautionsConnection;
  changedPostHistorysConnection: PostReservedSlotConnection;
  checkExistedInfo: Scalars['Boolean'];
  checkInList: Array<Scalars['Int']>;
  checkPointBonusAvailable: Scalars['Float'];
  checkVerifiedInfo: Scalars['Boolean'];
  childrenUsers: Array<User>;
  childrenUsersConnection: UsersConnection;
  countEcommerceShopPromotion: Scalars['Float'];
  districts: Array<Districts>;
  ecommerceFlashSaleProduct: Array<ShopProduct>;
  ecommerceIncentivesForNewUsers: Array<ShopProduct>;
  ecommerceOrder: EcommerceOrder;
  ecommerceOrderConnection: EcommerceOrderConnection;
  ecommerceOrderReviewtyAdmin: EcommerceOrder;
  ecommerceOrderReviwetyAdminConnection: EcommerceOrderConnection;
  ecommerceOrderShopAdminConnection: EcommerceOrderConnection;
  ecommerceOrders: Array<EcommerceOrder>;
  ecommerceOrdersAdmin: Array<EcommerceOrder>;
  ecommerceOrdersShopAdmin: Array<EcommerceOrder>;
  ecommerceReview?: Maybe<EcommerceReview>;
  ecommerceReviewConnection: EcommerceReviewConnection;
  ecommerceReviews: Array<EcommerceReview>;
  ecommerceShopPromotion: Array<EcommercePromotion>;
  evaluation: FundingOrderEvaluation;
  evaluationConnection: EvaluationConnection;
  evaluations: Array<FundingOrderEvaluation>;
  event?: Maybe<Event>;
  eventComments: Array<EventComment>;
  eventCommentsConnection: EventCommentConnection;
  events: Array<Event>;
  eventsConnection: EventConnection;
  feature?: Maybe<Feature>;
  freeshipPromotionsOnCart: Array<EcommercePromotion>;
  function: Function;
  functionTranslations: Array<FunctionTranslation>;
  functions: Array<Function>;
  functionsConnection: FunctionsConnection;
  funding: Funding;
  fundingOrder: FundingOrder;
  fundingOrderConnection: FundingOrderConnection;
  fundingOrders: Array<FundingOrder>;
  fundingProduct: FundingProduct;
  fundings: Array<Funding>;
  fundingsConnection: FundingsConnection;
  getBannerImage?: Maybe<BannerImage>;
  getBannerImages: Array<BannerImage>;
  getBrandStore: Array<BrandStore>;
  getChangedPostHistorys: Array<PostReservedSlotHistory>;
  getCheckInPayload: CheckInPayload;
  getExternalProducts: Array<ExternalProvider>;
  getFeedItems?: Maybe<Array<Recommendation>>;
  getGoodsList: Array<GoodsList>;
  getMyVoucher: Array<Voucher>;
  getPointHistoriesStatistic: PointHistoryStatistic;
  getProductsFromGoogle: Array<GoogleProduct>;
  getUserAPIs: Array<ResourceApi>;
  getUserNotificationConfig: UserNotificationConfig;
  getVoucherCategories: Array<VoucherCategories>;
  hashtag?: Maybe<Hashtag>;
  hashtags?: Maybe<Array<Hashtag>>;
  hashtagsBookmarkedByUser: Array<Hashtag>;
  images: Array<Image>;
  imagesConnection: ImagesConnection;
  ingredient?: Maybe<Ingredient>;
  ingredientUsages: Array<IngredientUsage>;
  ingredients: Array<Ingredient>;
  ingredientsConnection: IngredientsConnection;
  liveStream: LiveStreamOrError;
  liveStreamProduct: LiveStreamProduct;
  liveStreamProducts: Array<LiveStreamProduct>;
  liveStreams: Array<LiveStream>;
  me: User;
  medias: Array<MediaEntityUnion>;
  mediasConnection: MediasConnection;
  myShopProducts: Array<ShopProduct>;
  myShopProductsConnection: ShopProductsConnection;
  myShops: Array<Shop>;
  myTopShopProducts: Array<TopShopProduct>;
  nbEcommerceOrderStatistic: Array<NbEcommerceOrderStatistic>;
  nbOrderStatisticOfUser: Array<NbOrderStatistic>;
  newestViewStore: Array<ProductViewStore>;
  notifications: Array<Notification>;
  notificationsConnection: NotificationsConnection;
  order: OrderOrError;
  orderShippingFee: Array<EcommerceShippingFee>;
  orderStatisticAdmin: OrderStatisticOfShop;
  orderStatisticOfShopAdmin: OrderStatisticOfShop;
  orderStatisticOfShopAdminWithPeriod: OrderStatisticWithPeriod;
  orders: Array<OrderOrError>;
  ordersConnection: OrdersConnection;
  pointHistories: Array<PointHistory>;
  pointHistoriesConnection: PointHistoriesConnection;
  post?: Maybe<Post>;
  postCategories: Array<PostCategory>;
  postCommentReports: Array<PostCommentReport>;
  postCommentReportsConnection: PostCommentReportsConnection;
  postComments: Array<PostComment>;
  postCommentsConnection: PostCommentsConnection;
  postReservedSlot?: Maybe<PostReservedSlot>;
  postReservedSlots: Array<PostReservedSlot>;
  posts: Array<Post>;
  postsConnection: PostsConnection;
  printEcommOrder: Scalars['String'];
  product?: Maybe<Product>;
  productRankingBrandIds: ProductRankingBrandIds;
  productRankings: Array<ProductRanking>;
  productRankingsConnection: ProductRankingsConnection;
  productRankingsV2: Array<ProductRanking>;
  productRequests: Array<ProductRequest>;
  productRequestsConnection: ProductRequestsConnection;
  productTranslations: Array<ProductTranslation>;
  productUsageLogs: Array<ProductUsageLog>;
  productVariant: ProductVariant;
  productVariantConnection: ProductVariantConnection;
  productVariants: Array<ProductVariant>;
  productViewStore: ProductViewStore;
  productViewStoreConnection: ProductViewStoreConnection;
  productViewStores: Array<ProductViewStore>;
  products: Array<Product>;
  productsConnection: ProductsConnection;
  provinces: Array<Provinces>;
  recommendationBackground: Array<RecommendationBackground>;
  relatedProducts: Array<Product>;
  revenueOfShopAdmin: Array<NbEcommerceOrderStatistic>;
  review: Review;
  reviewQuestionSet: ReviewQuestionSet;
  reviewQuestionSets: Array<ReviewQuestionSet>;
  reviewQuestions: Array<ReviewQuestion>;
  reviewQuestionsForProduct: Array<ReviewQuestion>;
  reviewReport: ReviewReport;
  reviewReports: Array<ReviewReport>;
  reviewReportsConnection: ReviewReportsConnection;
  reviews: Array<Review>;
  reviewsConnection: ReviewsConnection;
  reviewtyPromotionsOnCart: Array<EcommercePromotion>;
  savedMedias: Array<MediaEntityUnion>;
  savedMediasConnection: SavedMediasConnection;
  searchBrands: BrandSearchResult;
  searchHotKeywords: Array<HotKeywordRanking>;
  searchProducts: ProductSearchResult;
  searchProductsV2: ProductSearchResult;
  searchShopProducts: ShopProductSearchResult;
  shippingAddress: ShippingAddressOrError;
  shippingAddresses: Array<ShippingAddress>;
  shippingFee: Scalars['String'];
  shop?: Maybe<Shop>;
  shopConditions: ShopCondition;
  shopOnReviewtyByProduct: Array<ShopOnReviewtyByProduct>;
  shopProduct: ShopProduct;
  shopProducts: Array<ShopProduct>;
  shopProductsConnection: ShopProductsConnection;
  shopPromotionsOnCart: Array<EcommercePromotion>;
  shopToProducts: Array<ShopToProduct>;
  shopToProductsConnection: ShopToProductsConnection;
  shops: Array<Shop>;
  shopsConnection: ShopsConnection;
  suggestKeywords: Array<Scalars['String']>;
  systemConfig?: Maybe<SystemConfig>;
  systemConfigs: Array<SystemConfig>;
  topBuyer: Array<TopBuyer>;
  /** @deprecated use topHighlightShopsV2 instead */
  topHighlightShops: Array<Shop>;
  topHighlightShopsV2: Array<ShopRanking>;
  topRankHighlightProducts: Array<HighlightProduct>;
  trendKeywords: Array<HotKeywordRanking>;
  trendingBrands: Array<Brand>;
  trendingViewStore: Array<ProductViewStore>;
  user?: Maybe<User>;
  userByRole: Array<User>;
  userRequests: Array<UserRequest>;
  userRequestsConnection: UserRequestConnection;
  users: Array<User>;
  usersConnection: UsersConnection;
  verifyCurrentEmail: Scalars['Boolean'];
  verifyOTP: Scalars['Boolean'];
  verifyReferralCode: Scalars['Boolean'];
  verifyShopOTP: Scalars['Boolean'];
  videos: Array<VideoObject>;
  viewCountPost: Scalars['Float'];
  visitedContents: Array<VisitedContentEntityUnion>;
  vnBank: Array<VnBank>;
  wards: Array<Wards>;
};


export type QueryAnalyzeIngredientsArgs = {
  productType: ProductType;
  text: Scalars['String'];
};


export type QueryBannerImageConnectionAggrsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BannerImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BannerImageWhereInput>;
};


export type QueryBrandArgs = {
  where: BrandWhereUniqueInput;
};


export type QueryBrandAdminArgs = {
  where: BrandAdminWhereInput;
};


export type QueryBrandAdminsArgs = {
  where: BrandAdminWhereInput;
};


export type QueryBrandTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandTranslationWhereInput>;
};


export type QueryBrandsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandWhereInput>;
};


export type QueryBrandsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandWhereInput>;
};


export type QueryCartItemsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CartItemOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CartItemWhereInput>;
};


export type QueryCartProductsOfShopArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CartProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CartProductWhereInput>;
};


export type QueryCartProductsOfShopConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CartProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CartProductWhereInput>;
};


export type QueryCastArgs = {
  where: CastWhereUniqueInput;
};


export type QueryCastCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CastCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: CastCommentWhereInput;
};


export type QueryCastCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CastCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: CastCommentWhereInput;
};


export type QueryCastsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CastOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CastWhereInput>;
};


export type QueryCastsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CastOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CastWhereInput>;
};


export type QueryCategoriesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  idFilters?: InputMaybe<Array<Scalars['Int']>>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CategoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CategoryWhereInput>;
};


export type QueryCategoriesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  idFilters?: InputMaybe<Array<Scalars['Int']>>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CategoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CategoryWhereInput>;
};


export type QueryCategoryArgs = {
  idFilters: Array<Scalars['Int']>;
  where: CategoryWhereUniqueInput;
};


export type QueryCategorySearchArgs = {
  first?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  text: Scalars['String'];
};


export type QueryCautionArgs = {
  where: CautionWhereUniqueInput;
};


export type QueryCautionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type QueryCautionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type QueryChangedPostHistorysConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostReservedSlotOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostReservedSlotWhereInput>;
};


export type QueryCheckExistedInfoArgs = {
  type: Scalars['String'];
  value: Scalars['String'];
};


export type QueryCheckPointBonusAvailableArgs = {
  where: PostCommentWhereUserInput;
};


export type QueryCheckVerifiedInfoArgs = {
  type: Scalars['String'];
};


export type QueryChildrenUsersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserOrderByInput>;
  parentUserId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserWhereInput>;
};


export type QueryChildrenUsersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserOrderByInput>;
  parentUserId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserWhereInput>;
};


export type QueryCountEcommerceShopPromotionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionFilterWhereInput>;
};


export type QueryDistrictsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where: DistrictsWhereInput;
};


export type QueryEcommerceFlashSaleProductArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionWhereInput>;
};


export type QueryEcommerceIncentivesForNewUsersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionWhereInput>;
};


export type QueryEcommerceOrderArgs = {
  where: EcommerceOrderWhereInput;
};


export type QueryEcommerceOrderConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryEcommerceOrderReviewtyAdminArgs = {
  where: EcommerceOrderWhereInput;
};


export type QueryEcommerceOrderReviwetyAdminConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryEcommerceOrderShopAdminConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryEcommerceOrdersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryEcommerceOrdersAdminArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryEcommerceOrdersShopAdminArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryEcommerceReviewArgs = {
  where: EcommerceReviewWhereInput;
};


export type QueryEcommerceReviewConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceReviewWhereInput>;
};


export type QueryEcommerceReviewsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceReviewWhereInput>;
};


export type QueryEcommerceShopPromotionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionFilterWhereInput>;
};


export type QueryEvaluationArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderEvaluationWhereInput>;
};


export type QueryEvaluationConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderEvaluationWhereInput>;
};


export type QueryEvaluationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderEvaluationWhereInput>;
};


export type QueryEventArgs = {
  where: EventWhereUniqueInput;
};


export type QueryEventCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventCommentWhereInput>;
};


export type QueryEventCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventCommentWhereInput>;
};


export type QueryEventsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventWhereInput>;
};


export type QueryEventsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventWhereInput>;
};


export type QueryFreeshipPromotionsOnCartArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceProductIdsWhereInput>;
};


export type QueryFunctionArgs = {
  where: FunctionWhereUniqueInput;
};


export type QueryFunctionTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionTranslationWhereInput>;
};


export type QueryFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type QueryFunctionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type QueryFundingArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingWhereInput>;
};


export type QueryFundingOrderArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderWhereInput>;
};


export type QueryFundingOrderConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderWhereInput>;
};


export type QueryFundingOrdersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderWhereInput>;
};


export type QueryFundingProductArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingWhereInput>;
};


export type QueryFundingsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingWhereInput>;
};


export type QueryFundingsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingWhereInput>;
};


export type QueryGetBannerImageArgs = {
  where: BannerImageWhereInput;
};


export type QueryGetBannerImagesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BannerImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BannerImageWhereInput>;
};


export type QueryGetBrandStoreArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherStoreWhereInput>;
};


export type QueryGetChangedPostHistorysArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostReservedSlotOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostReservedSlotWhereInput>;
};


export type QueryGetExternalProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<GoogleProductOrderInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where: GoogleProductWhereInput;
};


export type QueryGetFeedItemsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where: RecommendationPageInput;
};


export type QueryGetGoodsListArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<GoodsListArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<GoodsListWhereInput>;
};


export type QueryGetMyVoucherArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VoucherArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherWhereInput>;
};


export type QueryGetPointHistoriesStatisticArgs = {
  statisticType: StatisticType;
  where?: InputMaybe<PointHistoryStatisticWhereInput>;
};


export type QueryGetProductsFromGoogleArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<GoogleProductOrderInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where: GoogleProductWhereInput;
};


export type QueryGetVoucherCategoriesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherCategoriesWhereInput>;
};


export type QueryHashtagArgs = {
  where: HashtagWhereUniqueInput;
};


export type QueryHashtagsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<HashtagWhereInput>;
};


export type QueryHashtagsBookmarkedByUserArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<HashtagsBookmarkedByUserOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryImagesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ImageWhereInput>;
};


export type QueryImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ImageWhereInput>;
};


export type QueryIngredientArgs = {
  where: IngredientWhereUniqueInput;
};


export type QueryIngredientsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientWhereInput>;
};


export type QueryIngredientsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientWhereInput>;
};


export type QueryLiveStreamArgs = {
  where: LiveStreamWhereUniqueInput;
};


export type QueryLiveStreamProductArgs = {
  where: LiveStreamProductWhereUniqueInput;
};


export type QueryLiveStreamProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LiveStreamProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: LiveStreamProductWhereInput;
};


export type QueryLiveStreamsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LiveStreamOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<LiveStreamWhereInput>;
};


export type QueryMediasArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<MediaOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: MediaWhereInput;
};


export type QueryMediasConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<MediaOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: MediaWhereInput;
};


export type QueryMyShopProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopProductWhereInput>;
};


export type QueryMyShopProductsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopProductWhereInput>;
};


export type QueryMyShopsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopOrderByInput>;
  orderBys?: InputMaybe<Array<ShopOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopWhereInput>;
};


export type QueryMyTopShopProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<TopShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: TopShopProductWhereInput;
};


export type QueryNbEcommerceOrderStatisticArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryNbOrderStatisticOfUserArgs = {
  statisticType?: InputMaybe<StatisticType>;
  where?: InputMaybe<FundingOrderStatisticWhereInput>;
};


export type QueryNewestViewStoreArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductViewStoreOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductViewStoreWhereInput>;
};


export type QueryNotificationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<NotificationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<NotificationWhereInput>;
};


export type QueryNotificationsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<NotificationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<NotificationWhereInput>;
};


export type QueryOrderArgs = {
  where: OrderWhereUniqueInput;
};


export type QueryOrderShippingFeeArgs = {
  data: OrderShippingFeeInput;
};


export type QueryOrderStatisticAdminArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryOrderStatisticOfShopAdminArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryOrderStatisticOfShopAdminWithPeriodArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryOrdersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<OrderOrderByInput>;
  role?: InputMaybe<Role>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<OrderWhereInput>;
};


export type QueryOrdersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<OrderOrderByInput>;
  role?: InputMaybe<Role>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<OrderWhereInput>;
};


export type QueryPointHistoriesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PointHistoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PointHistoryWhereInput>;
};


export type QueryPointHistoriesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PointHistoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PointHistoryWhereInput>;
};


export type QueryPostArgs = {
  where: PostArgsWhereUniqueInput;
};


export type QueryPostCategoriesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCategoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCategoryWhereInput>;
};


export type QueryPostCommentReportsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentReportWhereInput>;
};


export type QueryPostCommentReportsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentReportWhereInput>;
};


export type QueryPostCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type QueryPostCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type QueryPostReservedSlotArgs = {
  where: PostReservedSlotWhereInput;
};


export type QueryPostReservedSlotsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostReservedSlotOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostReservedSlotWhereInput>;
};


export type QueryPostsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<PostOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostWhereInput>;
};


export type QueryPostsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<PostOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostWhereInput>;
};


export type QueryPrintEcommOrderArgs = {
  transportationCode: Scalars['String'];
};


export type QueryProductArgs = {
  where: ProductWhereUniqueInput;
};


export type QueryProductRankingBrandIdsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type QueryProductRankingsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type QueryProductRankingsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type QueryProductRankingsV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type QueryProductRequestsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductRequestOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRequestWhereInput>;
};


export type QueryProductRequestsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductRequestOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRequestWhereInput>;
};


export type QueryProductTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductTranslationWhereInput>;
};


export type QueryProductUsageLogsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductUsageLogOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductUsageLogWhereInput>;
};


export type QueryProductVariantArgs = {
  where: ProductVariantWhereInput;
};


export type QueryProductVariantConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductVariantOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductVariantWhereInput>;
};


export type QueryProductVariantsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductVariantOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductVariantWhereInput>;
};


export type QueryProductViewStoreArgs = {
  where: ProductViewStoreWhereInput;
};


export type QueryProductViewStoreConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductViewStoreOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductViewStoreWhereInput>;
};


export type QueryProductViewStoresArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductViewStoreOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductViewStoreWhereInput>;
};


export type QueryProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};


export type QueryProductsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};


export type QueryRecommendationBackgroundArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where: RecommendationBackgroundInput;
};


export type QueryRelatedProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};


export type QueryRevenueOfShopAdminArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryReviewArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type QueryReviewQuestionSetArgs = {
  where: ReviewQuestionSetWhereUniqueInput;
};


export type QueryReviewQuestionSetsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewQuestionSetWhereInput>;
};


export type QueryReviewQuestionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewQuestionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewQuestionWhereInput>;
};


export type QueryReviewQuestionsForProductArgs = {
  productId: Scalars['ID'];
};


export type QueryReviewReportArgs = {
  where: ReviewReportWhereUniqueInput;
};


export type QueryReviewReportsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewReportWhereInput>;
};


export type QueryReviewReportsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewReportWhereInput>;
};


export type QueryReviewsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type QueryReviewsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type QueryReviewtyPromotionsOnCartArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceProductIdsWhereInput>;
};


export type QuerySavedMediasArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SavedMediaOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SavedMediaWhereInput>;
};


export type QuerySavedMediasConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SavedMediaOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SavedMediaWhereInput>;
};


export type QuerySearchBrandsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandWhereInput>;
};


export type QuerySearchProductsArgs = {
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductSearchOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  text: Scalars['String'];
  where?: InputMaybe<ProductWhereInput>;
};


export type QuerySearchProductsV2Args = {
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductSearchOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};


export type QuerySearchShopProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopProductElasticSearchOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopProductElasticSearchWhereInput>;
};


export type QueryShippingAddressArgs = {
  where: ShippingAddressWhereUniqueInput;
};


export type QueryShippingAddressesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShippingAddressOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShippingAddressWhereInput>;
};


export type QueryShippingFeeArgs = {
  data: ShippingFeeInput;
};


export type QueryShopArgs = {
  where: ShopWhereUniqueInput;
};


export type QueryShopConditionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopOrderByInput>;
  orderBys?: InputMaybe<Array<ShopOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopWhereInput>;
};


export type QueryShopOnReviewtyByProductArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopProductWhereInput>;
};


export type QueryShopProductArgs = {
  where: ShopProductWhereInput;
};


export type QueryShopProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopProductWhereInput>;
};


export type QueryShopProductsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopProductWhereInput>;
};


export type QueryShopPromotionsOnCartArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceProductIdsWhereInput>;
};


export type QueryShopToProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopToProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: ShopToProductWhereInput;
};


export type QueryShopToProductsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopToProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: ShopToProductWhereInput;
};


export type QueryShopsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopOrderByInput>;
  orderBys?: InputMaybe<Array<ShopOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopWhereInput>;
};


export type QueryShopsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopOrderByInput>;
  orderBys?: InputMaybe<Array<ShopOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopWhereInput>;
};


export type QuerySuggestKeywordsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where: SuggestKeywordInput;
};


export type QuerySystemConfigArgs = {
  where: SystemConfigWhereInput;
};


export type QuerySystemConfigsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SystemConfigOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SystemConfigWhereInput>;
};


export type QueryTopBuyerArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryTopHighlightShopsArgs = {
  first?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryTopHighlightShopsV2Args = {
  first?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryTopRankHighlightProductsArgs = {
  first?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryTrendingViewStoreArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductViewStoreOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductViewStoreWhereInput>;
};


export type QueryUserArgs = {
  dbDirect?: InputMaybe<Scalars['Boolean']>;
  where: UserWhereUniqueInput;
};


export type QueryUserByRoleArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<RolesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<RolesWhereInput>;
};


export type QueryUserRequestsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserRequestOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserRequestWhereInput>;
};


export type QueryUserRequestsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserRequestOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserRequestWhereInput>;
};


export type QueryUsersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserOrderByInput>;
  parentUserId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserWhereInput>;
};


export type QueryUsersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserOrderByInput>;
  parentUserId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserWhereInput>;
};


export type QueryVerifyCurrentEmailArgs = {
  otp: Scalars['Int'];
};


export type QueryVerifyOtpArgs = {
  email: Scalars['String'];
  otp: Scalars['Int'];
};


export type QueryVerifyReferralCodeArgs = {
  referralCode: Scalars['String'];
};


export type QueryVerifyShopOtpArgs = {
  otp: Scalars['Int'];
  type: Scalars['String'];
  value: Scalars['String'];
};


export type QueryVideosArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VideoOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VideosWhereInput>;
};


export type QueryViewCountPostArgs = {
  where: PostWhereUniqueInput;
};


export type QueryVisitedContentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VisitedContentArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: VisitedContentWhereInput;
};


export type QueryVnBankArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BankWhereInput>;
};


export type QueryWardsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where: WardInput;
};

export enum ReactStatus {
  Dislike = 'DISLIKE',
  Like = 'LIKE',
  None = 'NONE'
}

export type Recommendation = {
  __typename?: 'Recommendation';
  id: Scalars['Int'];
  recommendation?: Maybe<PostRecommentdationEntityUnion>;
  type?: Maybe<Scalars['Float']>;
};

export type RecommendationBackground = {
  __typename?: 'RecommendationBackground';
  url?: Maybe<Scalars['String']>;
};

export type RecommendationBackgroundInput = {
  content: Scalars['String'];
  page?: InputMaybe<Scalars['Int']>;
};

export type RecommendationPageInput = {
  page?: InputMaybe<Scalars['Int']>;
  userID?: InputMaybe<Scalars['Int']>;
};

export type ResourceApi = {
  __typename?: 'ResourceAPI';
  createdAt?: Maybe<Scalars['DateTime']>;
  createdUser?: Maybe<Scalars['String']>;
  displayName?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  method?: Maybe<ApiMethod>;
  name?: Maybe<Scalars['String']>;
  resourceId?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  urlPath?: Maybe<Scalars['String']>;
};

export type Review = {
  __typename?: 'Review';
  answers?: Maybe<Array<ReviewAnswer>>;
  bookMark?: Maybe<Scalars['Boolean']>;
  commentsConnection: PostCommentsConnection;
  content?: Maybe<Scalars['String']>;
  countViewedReview?: Maybe<Scalars['Float']>;
  createdAt: Scalars['DateTime'];
  hashtags?: Maybe<Array<Hashtag>>;
  id: Scalars['Int'];
  images: Array<Image>;
  imagesConnection: ReviewImagesConnection;
  isDeleted: Scalars['Boolean'];
  isPin: Scalars['Boolean'];
  isRecommended?: Maybe<Scalars['Boolean']>;
  post?: Maybe<Post>;
  postComments: Array<PostComment>;
  product: Product;
  rate: Scalars['Int'];
  reactionOfViewer: ReactStatus;
  reactionsConnection: ReviewReactionsConnection;
  reports?: Maybe<Array<ReviewReport>>;
  shop?: Maybe<Shop>;
  status: ReviewStatus;
  title?: Maybe<Scalars['String']>;
  uid: Scalars['ID'];
  updatedAt: Scalars['DateTime'];
  user: User;
};


export type ReviewCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type ReviewImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type ReviewPostCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};

export type ReviewAnswer = {
  __typename?: 'ReviewAnswer';
  answerOption: ReviewAnswerOption;
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
};

export type ReviewAnswerInput = {
  id?: InputMaybe<Scalars['ID']>;
  reviewAnswerOptionId: Scalars['ID'];
  reviewId?: InputMaybe<Scalars['ID']>;
};

export type ReviewAnswerOption = {
  __typename?: 'ReviewAnswerOption';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  order: Scalars['Int'];
  question: ReviewQuestion;
  summary: Scalars['String'];
};

export type ReviewAvgOrderByInput = {
  rate?: InputMaybe<OrderBy>;
};

export type ReviewCreateInput = {
  content: Scalars['String'];
  hashtags?: InputMaybe<Array<HashtagInput>>;
  images?: InputMaybe<ImageCreateManyWithoutReviewInput>;
  product: ProductWhereUniqueInput;
  rate: Scalars['Int'];
  shop?: InputMaybe<ShopCreateOneWithoutReviewsInput>;
};

export type ReviewCreateOneWithoutPostCommentsInput = {
  connect?: InputMaybe<ReviewWhereUniqueInput>;
};

export type ReviewCreateOneWithoutPostInput = {
  connect?: InputMaybe<ReviewWhereUniqueInput>;
};

export type ReviewCreateOneWithoutReportInput = {
  connect: ReviewWhereUniqueInput;
};

export type ReviewCreatePayload = CommonError | Review;

export type ReviewImagesAggregate = {
  __typename?: 'ReviewImagesAggregate';
  count: Scalars['Int'];
};

export type ReviewImagesConnection = {
  __typename?: 'ReviewImagesConnection';
  aggregate: ReviewImagesAggregate;
};

export type ReviewInput = {
  answers?: InputMaybe<Array<ReviewAnswerInput>>;
  content?: InputMaybe<Scalars['String']>;
  hashtags?: InputMaybe<Array<HashtagInput>>;
  id?: InputMaybe<Scalars['ID']>;
  images?: InputMaybe<Array<ImageInput>>;
  isRecommended?: InputMaybe<Scalars['Boolean']>;
  productId: Scalars['ID'];
  rate: Scalars['Int'];
  shopId?: InputMaybe<Scalars['ID']>;
  title?: InputMaybe<Scalars['String']>;
};

export type ReviewOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  reactionsConnection?: InputMaybe<ReviewReactionsConnectionOrderByInput>;
  updatedAt?: InputMaybe<OrderBy>;
};

export enum ReviewQueryType {
  Following = 'FOLLOWING',
  Normal = 'NORMAL'
}

export type ReviewQuestion = {
  __typename?: 'ReviewQuestion';
  answerOptions: Array<ReviewAnswerOption>;
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  language: LanguageCode;
  order: Scalars['Int'];
  reviewQuestionSetId: Scalars['ID'];
};

export type ReviewQuestionOrderByInput = {
  order?: InputMaybe<OrderBy>;
};

export type ReviewQuestionSet = {
  __typename?: 'ReviewQuestionSet';
  id: Scalars['ID'];
  name: Scalars['String'];
  questions: Array<ReviewQuestion>;
};

export type ReviewQuestionSetWhereInput = {
  language?: InputMaybe<LanguageCodeFilter>;
  reviewQuestionSetId?: InputMaybe<Scalars['ID']>;
};

export type ReviewQuestionSetWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type ReviewQuestionWhereInput = {
  language?: InputMaybe<LanguageCodeFilter>;
  reviewQuestionSetId?: InputMaybe<StringFilter>;
};

export type ReviewReactionsAggregate = {
  __typename?: 'ReviewReactionsAggregate';
  count: Scalars['Int'];
};

export type ReviewReactionsAggregateOrderByInput = {
  count?: InputMaybe<OrderBy>;
};

export type ReviewReactionsConnection = {
  __typename?: 'ReviewReactionsConnection';
  aggregate: ReviewReactionsAggregate;
};

export type ReviewReactionsConnectionOrderByInput = {
  aggregate?: InputMaybe<ReviewReactionsAggregateOrderByInput>;
};

export type ReviewReport = {
  __typename?: 'ReviewReport';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  reason?: Maybe<Scalars['String']>;
  review: Review;
  status: ReviewReportStatus;
  type: ReviewReportType;
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type ReviewReportCreateInput = {
  reason?: InputMaybe<Scalars['String']>;
  review: ReviewCreateOneWithoutReportInput;
  type: ReviewReportType;
};

export type ReviewReportOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export enum ReviewReportStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Rejected = 'REJECTED'
}

export enum ReviewReportType {
  Advertising = 'ADVERTISING',
  NotRelated = 'NOT_RELATED',
  Other = 'OTHER',
  PersonalInformation = 'PERSONAL_INFORMATION',
  Repeat = 'REPEAT',
  Simple = 'SIMPLE',
  Swearing = 'SWEARING'
}

export type ReviewReportWhereInput = {
  reasonContains?: InputMaybe<Scalars['String']>;
  review?: InputMaybe<ReviewWhereInput>;
  status?: InputMaybe<ReviewReportStatus>;
  user?: InputMaybe<UserWhereInput>;
};

export type ReviewReportWhereUniqueInput = {
  id: Scalars['Int'];
};

export type ReviewReportsAggregate = {
  __typename?: 'ReviewReportsAggregate';
  count: Scalars['Int'];
};

export type ReviewReportsConnection = {
  __typename?: 'ReviewReportsConnection';
  aggregate: ReviewReportsAggregate;
};

export enum ReviewStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Hidden = 'HIDDEN'
}

export type ReviewUpdateInput = {
  content: Scalars['String'];
  hashtags?: InputMaybe<Array<HashtagInput>>;
  images?: InputMaybe<ImageUpdateManyWithoutReviewInput>;
  rate: Scalars['Int'];
  shop?: InputMaybe<ShopUpdateOneWithoutReviewsInput>;
};

export type ReviewUpdatePayload = CommonError | Review;

export type ReviewWhereInput = {
  contentContains?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<DateTimeFilter>;
  hashtagsSome?: InputMaybe<HashtagWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  idIn?: InputMaybe<Array<Scalars['Int']>>;
  idIsNull?: InputMaybe<Scalars['Boolean']>;
  isBookMark?: InputMaybe<Scalars['Boolean']>;
  isDeleted?: InputMaybe<Scalars['Boolean']>;
  isPined?: InputMaybe<Scalars['Boolean']>;
  isQuickRating?: InputMaybe<Scalars['Boolean']>;
  product?: InputMaybe<ProductWhereInput>;
  queryType?: InputMaybe<ReviewQueryType>;
  rateOR?: InputMaybe<Array<IntFilter>>;
  shop?: InputMaybe<ShopWhereInput>;
  status?: InputMaybe<ReviewStatus>;
  statusIn?: InputMaybe<Array<ReviewStatus>>;
  user?: InputMaybe<UserWhereInput>;
};

export type ReviewWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<Scalars['Boolean']>;
};

export type ReviewsAggregate = {
  __typename?: 'ReviewsAggregate';
  avg: ReviewsAvg;
  count: Scalars['Int'];
};

export type ReviewsAggregateOrderByInput = {
  avg?: InputMaybe<ReviewAvgOrderByInput>;
  count?: InputMaybe<OrderBy>;
};

export type ReviewsAvg = {
  __typename?: 'ReviewsAvg';
  rate: Scalars['Float'];
};

export type ReviewsConnection = {
  __typename?: 'ReviewsConnection';
  aggregate: ReviewsAggregate;
};

export type ReviewsConnectionOrderByInput = {
  aggregate?: InputMaybe<ReviewsAggregateOrderByInput>;
};

export type ReviewsCountByRate = {
  __typename?: 'ReviewsCountByRate';
  five?: Maybe<Scalars['Int']>;
  four?: Maybe<Scalars['Int']>;
  one?: Maybe<Scalars['Int']>;
  three?: Maybe<Scalars['Int']>;
  two?: Maybe<Scalars['Int']>;
};

export enum Role {
  Admin = 'ADMIN',
  Anonymous = 'ANONYMOUS',
  Shop = 'SHOP',
  User = 'USER'
}

export enum RoleStatus {
  Active = 'ACTIVE',
  Deactive = 'DEACTIVE'
}

export type Roles = {
  __typename?: 'Roles';
  createdAt: Scalars['DateTime'];
  createdUser?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  name: Scalars['String'];
};

export type RolesOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type RolesWhereInput = {
  _id?: InputMaybe<IntFilter>;
  createdUser?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  name?: InputMaybe<StringFilter>;
};

export type SavedMediaOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
};

export type SavedMediaWhereInput = {
  createdAt?: InputMaybe<DateTimeFilter>;
  type?: InputMaybe<MediaType>;
};

export type SavedMediasAggregate = {
  __typename?: 'SavedMediasAggregate';
  count: Scalars['Int'];
};

export type SavedMediasConnection = {
  __typename?: 'SavedMediasConnection';
  aggregate: SavedMediasAggregate;
};

export type ShipmentHistory = {
  __typename?: 'ShipmentHistory';
  createdAt: Scalars['DateTime'];
  fundingOrderId: Scalars['Int'];
  id: Scalars['Int'];
  status: Scalars['Int'];
  statusText: Scalars['String'];
};

export type ShipmentHistoryOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type ShipmentHistoryWhereInput = {
  funding_order_id?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['Int']>;
};

export type ShippingAddress = {
  __typename?: 'ShippingAddress';
  address: Scalars['String'];
  country: Scalars['String'];
  createdAt: Scalars['DateTime'];
  default: Scalars['Boolean'];
  distance?: Maybe<Scalars['Float']>;
  district: Scalars['String'];
  fullName: Scalars['String'];
  id: Scalars['Int'];
  isPickupAddress?: Maybe<Scalars['Boolean']>;
  location?: Maybe<Location>;
  phoneNumber?: Maybe<Scalars['String']>;
  province: Scalars['String'];
  updatedAt: Scalars['DateTime'];
  ward: Scalars['String'];
};


export type ShippingAddressDistanceArgs = {
  location: LocationInput;
};

export type ShippingAddressCreateInput = {
  address: Scalars['String'];
  country: Scalars['String'];
  default?: InputMaybe<Scalars['Boolean']>;
  district: Scalars['String'];
  districtId?: InputMaybe<Scalars['ID']>;
  fullName: Scalars['String'];
  latitude?: InputMaybe<Scalars['Float']>;
  longitude?: InputMaybe<Scalars['Float']>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  province: Scalars['String'];
  provinceId?: InputMaybe<Scalars['ID']>;
  ward: Scalars['String'];
  wardId?: InputMaybe<Scalars['ID']>;
};

export type ShippingAddressCreateOneWithoutOrderInput = {
  connect: ShippingAddressWhereUniqueInput;
};

export type ShippingAddressCreateOneWithoutShopInput = {
  create: ShippingAddressCreateInput;
};

export type ShippingAddressOrError = CommonError | ShippingAddress;

export type ShippingAddressOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type ShippingAddressUpdateInput = {
  address: Scalars['String'];
  country: Scalars['String'];
  default?: InputMaybe<Scalars['Boolean']>;
  district: Scalars['String'];
  districtId?: InputMaybe<Scalars['ID']>;
  fullName: Scalars['String'];
  latitude?: InputMaybe<Scalars['Float']>;
  longitude?: InputMaybe<Scalars['Float']>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  province: Scalars['String'];
  provinceId?: InputMaybe<Scalars['ID']>;
  ward: Scalars['String'];
  wardId?: InputMaybe<Scalars['ID']>;
};

export type ShippingAddressUpdateOneWithoutShopInput = {
  create?: InputMaybe<ShippingAddressCreateInput>;
  update?: InputMaybe<ShippingAddressUpdateWithWhereUniqueWithoutShopInput>;
};

export type ShippingAddressUpdateWithWhereUniqueWithoutShopInput = {
  data: ShippingAddressUpdateWithoutShopInput;
  where: ShippingAddressWhereUniqueInput;
};

export type ShippingAddressUpdateWithoutShopInput = {
  address: Scalars['String'];
  country: Scalars['String'];
  default?: InputMaybe<Scalars['Boolean']>;
  district: Scalars['String'];
  districtId?: InputMaybe<Scalars['ID']>;
  fullName: Scalars['String'];
  latitude?: InputMaybe<Scalars['Float']>;
  longitude?: InputMaybe<Scalars['Float']>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  province: Scalars['String'];
  provinceId?: InputMaybe<Scalars['ID']>;
  ward: Scalars['String'];
  wardId?: InputMaybe<Scalars['ID']>;
};

export type ShippingAddressWhereInput = {
  default?: InputMaybe<Scalars['Boolean']>;
  district?: InputMaybe<Scalars['String']>;
  nearBy?: InputMaybe<NearByInput>;
  province?: InputMaybe<Scalars['String']>;
  user?: InputMaybe<UserWhereInput>;
};

export type ShippingAddressWhereUniqueInput = {
  id: Scalars['Int'];
};

export type ShippingFeeInput = {
  address?: InputMaybe<Scalars['String']>;
  deliverOption: DeliverOption;
  district: Scalars['String'];
  province: Scalars['String'];
  ward?: InputMaybe<Scalars['String']>;
  weight: Scalars['Float'];
};

export type Shop = {
  __typename?: 'Shop';
  bankCard?: Maybe<BankCard>;
  cover?: Maybe<Image>;
  createdAt: Scalars['DateTime'];
  description?: Maybe<Scalars['String']>;
  externalLinks?: Maybe<Array<ExternalLink>>;
  id: Scalars['Int'];
  name: Scalars['String'];
  nbOfShopProduct: Scalars['Int'];
  pickupAddress?: Maybe<ShippingAddress>;
  reviewsConnection: ReviewsConnection;
  shopRanking?: Maybe<ShopRanking>;
  status: ShopStatus;
  updatedAt: Scalars['DateTime'];
  user: User;
};


export type ShopReviewsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};

export type ShopCondition = {
  __typename?: 'ShopCondition';
  hasPickAddress: Scalars['Boolean'];
  isApproved: Scalars['Boolean'];
  isVerifiedPhone: Scalars['Boolean'];
};

export type ShopCreateInput = {
  avatar?: InputMaybe<ImageCreateOneWithoutShopInput>;
  cover?: InputMaybe<ImageCreateOneWithoutShopInput>;
  description?: InputMaybe<Scalars['String']>;
  externalLink?: InputMaybe<ExternalLinkCreateManyWithoutShopInput>;
  name: Scalars['String'];
  pickupAddress?: InputMaybe<ShippingAddressCreateOneWithoutShopInput>;
};

export type ShopCreateOneWithoutOrderInput = {
  connect: ShopWhereUniqueInput;
};

export type ShopCreateOneWithoutReviewsInput = {
  connect: ShopWhereUniqueInput;
};

export type ShopCreateOneWithoutShopToProductsInput = {
  connect: ShopWhereUniqueInput;
};

export type ShopMutationError = Error & {
  __typename?: 'ShopMutationError';
  code: ShopMutationErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum ShopMutationErrorCode {
  NameAlreadyInUse = 'NAME_ALREADY_IN_USE',
  YouAreNotTheOwner = 'YOU_ARE_NOT_THE_OWNER'
}

export type ShopOnReviewtyByProduct = {
  __typename?: 'ShopOnReviewtyByProduct';
  productId?: Maybe<Scalars['Int']>;
  reviewtyProduct: Product;
  reviewtyProductId?: Maybe<Scalars['Int']>;
  shop: Shop;
  shopId?: Maybe<Scalars['Int']>;
  shoppingProduct?: Maybe<ShopProduct>;
};

export type ShopOrMutationError = CommonError | Shop | ShopMutationError;

export type ShopOrderByInput = {
  id?: InputMaybe<OrderBy>;
  status?: InputMaybe<OrderBy>;
  updatedAt?: InputMaybe<OrderBy>;
};

export type ShopOrderInput = {
  shippingFee: Scalars['Float'];
  shopId: Scalars['Int'];
  shopPromotions?: InputMaybe<Array<Scalars['Int']>>;
  variantsInfo: Array<EcommerceOrderProductInput>;
};

export type ShopProduct = {
  __typename?: 'ShopProduct';
  avgRateOfShopProductReviews?: Maybe<Scalars['Float']>;
  blockedReason?: Maybe<Scalars['String']>;
  brand: Brand;
  brandId?: Maybe<Scalars['Int']>;
  categoryId?: Maybe<Scalars['Int']>;
  comparativePrice?: Maybe<Scalars['Float']>;
  createdAt: Scalars['DateTime'];
  createdUser: User;
  description?: Maybe<Scalars['String']>;
  flashsale?: Maybe<Array<EcommercePromotion>>;
  functions?: Maybe<FunctionProducts>;
  id: Scalars['Int'];
  images?: Maybe<Images>;
  isBlocked?: Maybe<Scalars['Boolean']>;
  isVisible?: Maybe<Scalars['Boolean']>;
  name?: Maybe<Scalars['String']>;
  nbReviewsOfShopProduct?: Maybe<Scalars['Int']>;
  productVariants?: Maybe<Array<ProductVariant>>;
  productViewstores?: Maybe<Array<ProductViewStore>>;
  promotions?: Maybe<Array<EcommercePromotion>>;
  rangeOfOriginalPrices?: Maybe<Scalars['String']>;
  rangeOfPrices?: Maybe<Scalars['String']>;
  revenueOfShopProduct?: Maybe<Scalars['Float']>;
  reviewtyProduct?: Maybe<Product>;
  reviewtyProductId?: Maybe<Scalars['Int']>;
  shop: Shop;
  shopId?: Maybe<Scalars['Int']>;
  shopProductReviews?: Maybe<Array<EcommerceReview>>;
  totalQuantity?: Maybe<Scalars['Int']>;
  totalSoldQuantity?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  userId?: Maybe<Scalars['Int']>;
};


export type ShopProductProductVariantsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductVariantOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductVariantWhereInput>;
};


export type ShopProductProductViewstoresArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductViewStoreOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductViewStoreWhereInput>;
};

export type ShopProductElasticSearchOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
  rateAvg?: InputMaybe<OrderBy>;
};

export type ShopProductElasticSearchWhereInput = {
  brandIdsFilter?: InputMaybe<Array<Scalars['Int']>>;
  price?: InputMaybe<IntFilter>;
  shopIdsFilter?: InputMaybe<Array<Scalars['Int']>>;
  textSearch?: InputMaybe<Scalars['String']>;
};

export type ShopProductInsertInput = {
  brandId?: InputMaybe<Scalars['Int']>;
  categoryId?: InputMaybe<Scalars['Int']>;
  comparativePrice?: InputMaybe<Scalars['Float']>;
  description?: InputMaybe<Scalars['String']>;
  functions?: InputMaybe<FunctionProductUpsertInput>;
  images?: InputMaybe<ImageUpsertInput>;
  name: Scalars['String'];
  reviewtyProductId?: InputMaybe<Scalars['Int']>;
  variants: Array<ProductVariantInsertInput>;
};

export type ShopProductOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type ShopProductSearchResult = {
  __typename?: 'ShopProductSearchResult';
  products: Array<ShopProduct>;
  total: Scalars['Int'];
};

export type ShopProductUpdateInput = {
  blockedReason?: InputMaybe<Scalars['String']>;
  brandId?: InputMaybe<Scalars['Int']>;
  categoryId?: InputMaybe<Scalars['Int']>;
  comparativePrice?: InputMaybe<Scalars['Float']>;
  description?: InputMaybe<Scalars['String']>;
  functions?: InputMaybe<FunctionProductUpsertInput>;
  images?: InputMaybe<ImageUpsertInput>;
  isBlocked?: InputMaybe<Scalars['Boolean']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  name?: InputMaybe<Scalars['String']>;
  reviewtyProductId?: InputMaybe<Scalars['Int']>;
  variants?: InputMaybe<Array<ProductVariantUpdateInput>>;
};

export type ShopProductWhereInput = {
  brandName?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  isBlocked?: InputMaybe<Scalars['Boolean']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  price?: InputMaybe<IntFilter>;
  productName?: InputMaybe<Scalars['String']>;
  quantity?: InputMaybe<IntFilter>;
  reviewtyProductId?: InputMaybe<Scalars['Int']>;
  shopId?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['Int']>;
};

export type ShopProductsAggregate = {
  __typename?: 'ShopProductsAggregate';
  count: Scalars['Int'];
};

export type ShopProductsConnection = {
  __typename?: 'ShopProductsConnection';
  aggregate: ShopProductsAggregate;
};

export type ShopRanking = {
  __typename?: 'ShopRanking';
  ranking: Scalars['Int'];
  rankingChange?: Maybe<Scalars['Int']>;
  shop: Shop;
};

export enum ShopStatus {
  Approved = 'APPROVED',
  Blocked = 'BLOCKED',
  Created = 'CREATED'
}

export type ShopToProduct = {
  __typename?: 'ShopToProduct';
  createdAt: Scalars['DateTime'];
  externalLinks?: Maybe<Array<ExternalLink>>;
  price?: Maybe<Scalars['Int']>;
  product: Product;
  shop: Shop;
  stockQuantity?: Maybe<Scalars['Int']>;
  updatedAt: Scalars['DateTime'];
};

export type ShopToProductCreateInput = {
  externalLink?: InputMaybe<ExternalLinkCreateManyWithoutShopToProductInput>;
  price: Scalars['Int'];
  product: ProductCreateOneWithoutShopToProductsInput;
  shop: ShopCreateOneWithoutShopToProductsInput;
  stockQuantity?: InputMaybe<Scalars['Int']>;
};

export type ShopToProductOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
};

export type ShopToProductUpdateInput = {
  externalLink?: InputMaybe<ExternalLinkUpdateManyWithoutShopToProductInput>;
  price?: InputMaybe<Scalars['Int']>;
  stockQuantity?: InputMaybe<Scalars['Int']>;
};

export type ShopToProductWhereInput = {
  ecommerceSite?: InputMaybe<EcommerceSite>;
  price?: InputMaybe<IntFilter>;
  product?: InputMaybe<ProductWhereInput>;
  shop?: InputMaybe<ShopWhereInput>;
};

export type ShopToProductWhereUniqueInput = {
  product: ProductWhereUniqueInput;
  shop: ShopWhereUniqueInput;
};

export type ShopToProductsAggregate = {
  __typename?: 'ShopToProductsAggregate';
  count: Scalars['Int'];
};

export type ShopToProductsConnection = {
  __typename?: 'ShopToProductsConnection';
  aggregate: ShopToProductsAggregate;
};

export type ShopUpdateInput = {
  avatar?: InputMaybe<ImageUpdateOneWithoutShopInput>;
  cover?: InputMaybe<ImageUpdateOneWithoutShopInput>;
  description?: InputMaybe<Scalars['String']>;
  externalLink?: InputMaybe<ExternalLinkUpdateManyWithoutShopInput>;
  name: Scalars['String'];
  pickupAddress?: InputMaybe<ShippingAddressUpdateOneWithoutShopInput>;
};

export type ShopUpdateOneWithoutReviewsInput = {
  connect: ShopWhereUniqueInput;
};

export type ShopWhereInput = {
  _id?: InputMaybe<IntFilter>;
  ecommerceSite?: InputMaybe<EcommerceSite>;
  followersSome?: InputMaybe<UserWhereInput>;
  followingsSome?: InputMaybe<UserWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  name?: InputMaybe<StringFilter>;
  owner?: InputMaybe<UserWhereUniqueInput>;
  pickupAddress?: InputMaybe<ShippingAddressWhereInput>;
  productsSome?: InputMaybe<ProductWhereInput>;
  status?: InputMaybe<ShopStatus>;
};

export type ShopWhereUniqueInput = {
  id: Scalars['Int'];
};

export type ShopsAggregate = {
  __typename?: 'ShopsAggregate';
  count: Scalars['Int'];
};

export type ShopsConnection = {
  __typename?: 'ShopsConnection';
  aggregate: ShopsAggregate;
};

export type SignUpInput = {
  accessToken?: InputMaybe<Scalars['String']>;
  account?: InputMaybe<Scalars['String']>;
  birthYear?: InputMaybe<Scalars['Int']>;
  displayName?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  gender?: InputMaybe<Gender>;
  oauthProvider?: InputMaybe<OauthProvider>;
  otp?: InputMaybe<Scalars['Int']>;
  password?: InputMaybe<Scalars['String']>;
  referralCode?: InputMaybe<Scalars['String']>;
  roleId?: InputMaybe<Scalars['Int']>;
};

export enum SkinType {
  Dry = 'DRY',
  Mixed = 'MIXED',
  Neutral = 'NEUTRAL',
  Oily = 'OILY',
  Sensitive = 'SENSITIVE'
}

export type SkinTypeObject = {
  __typename?: 'SkinTypeObject';
  ALL: Scalars['Boolean'];
  DRY: Scalars['Boolean'];
  MIXED: Scalars['Boolean'];
  NEUTRAL: Scalars['Boolean'];
  OILY: Scalars['Boolean'];
  SENSITIVE: Scalars['Boolean'];
};

export type SocialMedia = {
  __typename?: 'SocialMedia';
  id: Scalars['String'];
  provider: OauthProvider;
};

export type SocialMediaError = Error & {
  __typename?: 'SocialMediaError';
  code: SocialMediaErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum SocialMediaErrorCode {
  AccountAlreadyBeingUsed = 'ACCOUNT_ALREADY_BEING_USED'
}

export type SocialMediaOrError = CommonError | SocialMedia | SocialMediaError;

export type SpecialIngredientFunction = {
  __typename?: 'SpecialIngredientFunction';
  id: Scalars['Int'];
  symbolUrl?: Maybe<Scalars['String']>;
  type: SpecialIngredientFunctionType;
};

export type SpecialIngredientFunctionOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type SpecialIngredientFunctionTranslationWhereInput = {
  language?: InputMaybe<LanguageCode>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export enum SpecialIngredientFunctionType {
  Advantage = 'ADVANTAGE',
  Disadvantage = 'DISADVANTAGE',
  Recommendation = 'RECOMMENDATION'
}

export type SpecialIngredientFunctionWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  ingredientsSome?: InputMaybe<IngredientWhereInput>;
  translationsSome?: InputMaybe<SpecialIngredientFunctionTranslationWhereInput>;
  type?: InputMaybe<SpecialIngredientFunctionType>;
};

export type SpecialIngredientFunctionsAggregate = {
  __typename?: 'SpecialIngredientFunctionsAggregate';
  id: Scalars['Int'];
};

export type SpecialIngredientFunctionsConnection = {
  __typename?: 'SpecialIngredientFunctionsConnection';
  aggregate: SpecialIngredientFunctionsAggregate;
  id: Scalars['Int'];
};

export enum StatisticType {
  Day = 'DAY',
  Month = 'MONTH',
  Quarter = 'QUARTER',
  Week = 'WEEK'
}

export type StringBox = {
  __typename?: 'StringBox';
  value: Scalars['String'];
};

export type StringFilter = {
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  not?: InputMaybe<StringFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type SucessfullEcommOrder = {
  __typename?: 'SucessfullEcommOrder';
  message: Scalars['String'];
  paymentMethod: PaymentMethod;
};

export type SucessfullEcommOrderOrError = CommonError | EcommOrderCreateError | SucessfullEcommOrder;

export type SuggestKeywordInput = {
  keyword: Scalars['String'];
  page?: InputMaybe<Scalars['Int']>;
};

export type SystemConfig = {
  __typename?: 'SystemConfig';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  name: Scalars['String'];
  value: Scalars['String'];
};

export type SystemConfigCreateInput = {
  name: Scalars['String'];
  value: Scalars['String'];
};

export type SystemConfigOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type SystemConfigUpdateInput = {
  name?: InputMaybe<Scalars['String']>;
  value?: InputMaybe<Scalars['String']>;
};

export type SystemConfigWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  name?: InputMaybe<StringFilter>;
};

export type Tag = {
  __typename?: 'Tag';
  id: Scalars['Int'];
  translations: Array<TagTranslation>;
};


export type TagTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<TagTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<TagTranslationWhereInput>;
};

export type TagTranslation = {
  __typename?: 'TagTranslation';
  content: Scalars['String'];
  id: Scalars['Int'];
  language: LanguageCode;
};

export type TagTranslationOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type TagTranslationWhereInput = {
  contentContains?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Float']>;
  language?: InputMaybe<LanguageCode>;
};

export type TopBuyer = {
  __typename?: 'TopBuyer';
  buyer: User;
  buyerId: Scalars['Int'];
  orderTotal?: Maybe<Scalars['Int']>;
};

export enum TopProductFilter {
  Revenue = 'REVENUE',
  SoldTotal = 'SOLD_TOTAL'
}

export type TopShopProduct = {
  __typename?: 'TopShopProduct';
  filterValue?: Maybe<Scalars['Int']>;
  productId: Scalars['Int'];
  shopProduct?: Maybe<ShopProduct>;
};

export type TopShopProductOrderByInput = {
  filterValue?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type TopShopProductWhereInput = {
  filter?: InputMaybe<TopProductFilter>;
  userId?: InputMaybe<Scalars['Int']>;
};

export type UnbookmarkHashtagPayload = CommonError | Hashtag;

export enum Unit {
  Cfu = 'CFU',
  Cal = 'cal',
  G = 'g',
  Kcal = 'kcal',
  L = 'l',
  Mcg = 'mcg',
  Mg = 'mg',
  Micronutrient = 'micronutrient',
  Ml = 'ml'
}

export type User = {
  __typename?: 'User';
  account: Scalars['String'];
  /** @deprecated use account instead */
  accountName: Scalars['String'];
  /** @deprecated This field will be removed in Sep, 2021 */
  address?: Maybe<Scalars['String']>;
  avatar?: Maybe<Image>;
  baumannAnswers?: Maybe<Array<BaumannAnswer>>;
  baumannSkinType?: Maybe<BaumannSkinType>;
  birthYear?: Maybe<Scalars['Int']>;
  brandAdmin?: Maybe<BrandAdmin>;
  brandsFollowingConnection: BrandsFollowingConnection;
  currentLevel?: Maybe<Level>;
  displayName?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  emailVerified: Scalars['Boolean'];
  /** @deprecated This field will be removed in Sep, 2021 */
  firstName?: Maybe<Scalars['String']>;
  followersConnection: FollowersConnection;
  followingsConnection: FollowingsConnection;
  gender?: Maybe<Gender>;
  id: Scalars['Int'];
  isAnonymous: Scalars['Boolean'];
  isBlockedByViewer?: Maybe<Scalars['Boolean']>;
  /** @deprecated use isFollowedByViewer instead */
  isFollowed?: Maybe<Scalars['Boolean']>;
  isFollowedByViewer?: Maybe<Scalars['Boolean']>;
  /** @deprecated use isFollowingViewer instead */
  isFollowing?: Maybe<Scalars['Boolean']>;
  isFollowingViewer?: Maybe<Scalars['Boolean']>;
  isOfficial?: Maybe<Scalars['Boolean']>;
  isVerified: Scalars['Boolean'];
  /** @deprecated This field will be removed in Sep, 2021 */
  lastName?: Maybe<Scalars['String']>;
  levelPoints: Scalars['Int'];
  nationality?: Maybe<Scalars['String']>;
  nextLevel?: Maybe<Level>;
  phoneNumber?: Maybe<Scalars['String']>;
  postsConnection: PostsConnection;
  profiles: Array<UserProfile>;
  redeemablePoints: Scalars['Int'];
  referralCode: Scalars['ID'];
  reviewReactionsConnection: ReviewReactionsConnection;
  reviewsConnection: ReviewsConnection;
  role: Scalars['String'];
  roleId?: Maybe<Scalars['Int']>;
  shippingAddressUser?: Maybe<ShippingAddress>;
  shop?: Maybe<Shop>;
  /** @deprecated This field will be replaced by Baumann skin types in Sep, 2021 */
  skinType: SkinType;
  socialMediaAccounts?: Maybe<Array<OauthProvider>>;
  totalPoints: Scalars['Int'];
  totalReactPoints?: Maybe<Scalars['Int']>;
  type?: Maybe<AccountType>;
  uid: Scalars['ID'];
  userPath?: Maybe<Scalars['String']>;
  userResources?: Maybe<Array<UserResource>>;
  userRole?: Maybe<Array<Roles>>;
  wishedProductsConnection: ProductsConnection;
  /** @deprecated This field will be removed in Sep, 2021 */
  zipCode?: Maybe<Scalars['Int']>;
};


export type UserBrandsFollowingConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandWhereInput>;
};


export type UserProfilesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserProfileWhereInput>;
};


export type UserReviewsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type UserUserResourcesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IamOrderByInput>;
  parentId?: InputMaybe<Scalars['Int']>;
  parentRoleId?: InputMaybe<Scalars['Int']>;
  resourceId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['Int']>;
};


export type UserUserRoleArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<RolesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<RolesWhereInput>;
};

export type UserDeleteInput = {
  note?: InputMaybe<Scalars['String']>;
  reason: UserDeleteReason;
};

export enum UserDeleteReason {
  AppIsSlowAndBugs = 'APP_IS_SLOW_AND_BUGS',
  InformationIsNotHelpful = 'INFORMATION_IS_NOT_HELPFUL',
  IngredientAreNotAccurate = 'INGREDIENT_ARE_NOT_ACCURATE',
  NeverWinGiveaways = 'NEVER_WIN_GIVEAWAYS',
  Other = 'OTHER',
  PostsAreUnhelpfulAndOrOffensive = 'POSTS_ARE_UNHELPFUL_AND_OR_OFFENSIVE',
  ReviewsAreNotHelpful = 'REVIEWS_ARE_NOT_HELPFUL',
  TakeToMuchEffortToEarnPoints = 'TAKE_TO_MUCH_EFFORT_TO_EARN_POINTS'
}

export type UserLevelObject = {
  __typename?: 'UserLevelObject';
  ALL: Scalars['Boolean'];
  LV1: Scalars['Boolean'];
  LV2: Scalars['Boolean'];
  LV3: Scalars['Boolean'];
  LV4: Scalars['Boolean'];
  LV5: Scalars['Boolean'];
  LV6: Scalars['Boolean'];
  LV7: Scalars['Boolean'];
  LV8: Scalars['Boolean'];
};

export type UserNotificationConfig = {
  __typename?: 'UserNotificationConfig';
  activity?: Maybe<Scalars['Boolean']>;
  events?: Maybe<Scalars['Boolean']>;
  message?: Maybe<Scalars['Boolean']>;
  news?: Maybe<Scalars['Boolean']>;
  points?: Maybe<Scalars['Boolean']>;
};

export type UserNotificationConfigUpdateInput = {
  noType: Scalars['String'];
  value: Scalars['Boolean'];
};

export type UserOrderByInput = {
  id?: InputMaybe<OrderBy>;
  reviewReactions?: InputMaybe<ReviewReactionsConnectionOrderByInput>;
  reviews?: InputMaybe<ReviewsConnectionOrderByInput>;
};

export type UserProfile = {
  __typename?: 'UserProfile';
  displayName: Scalars['String'];
  email?: Maybe<Scalars['String']>;
  externalId: Scalars['String'];
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  provider: OauthProvider;
  userId: Scalars['Int'];
};

export type UserProfileWhereInput = {
  provider?: InputMaybe<OauthProvider>;
  user?: InputMaybe<UserWhereInput>;
};

export type UserRequest = {
  __typename?: 'UserRequest';
  assignedIDsProductByAdminForUser?: Maybe<Array<Scalars['Float']>>;
  changeBy?: Maybe<Scalars['String']>;
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  images?: Maybe<Array<Scalars['String']>>;
  ownerId: Scalars['Int'];
  product?: Maybe<Product>;
  pushedNotificationAt?: Maybe<Scalars['DateTime']>;
  requestedObjectId?: Maybe<Scalars['Float']>;
  status?: Maybe<UserRequestStatus>;
  statusSendNotification?: Maybe<UserRequestSendNotificationStatus>;
  type: UserRequestType;
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type UserRequestAggreate = {
  __typename?: 'UserRequestAggreate';
  count: Scalars['Int'];
};

export type UserRequestConnection = {
  __typename?: 'UserRequestConnection';
  aggregate: UserRequestAggreate;
};

export type UserRequestInput = {
  brandName?: InputMaybe<Scalars['String']>;
  content: Scalars['String'];
  images?: InputMaybe<Array<Scalars['String']>>;
  productName?: InputMaybe<Scalars['String']>;
  requestedObjectId?: InputMaybe<Scalars['Int']>;
  type: UserRequestType;
};

export type UserRequestOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export enum UserRequestSendNotificationStatus {
  Done = 'Done',
  Fail = 'Fail',
  Inprogress = 'Inprogress',
  Unsent = 'Unsent'
}

export enum UserRequestStatus {
  Closed = 'Closed',
  Pending = 'Pending',
  Receive = 'Receive'
}

export enum UserRequestStatusAssign {
  Assigned = 'Assigned',
  Unassigned = 'Unassigned'
}

export enum UserRequestType {
  AddNewProduct = 'AddNewProduct',
  AddNewProductIngredients = 'AddNewProductIngredients',
  EditProductInformation = 'EditProductInformation',
  OthersShop = 'OthersShop',
  ShopReviewty = 'ShopReviewty'
}

export type UserRequestUpdateInput = {
  ids?: InputMaybe<Array<Scalars['Int']>>;
};

export type UserRequestUpdateResponse = {
  __typename?: 'UserRequestUpdateResponse';
  code: Scalars['String'];
  message: Scalars['String'];
  status: Scalars['Float'];
};

export type UserRequestUpdateStatusInput = {
  status: UserRequestStatus;
};

export type UserRequestWhereInput = {
  brandId?: InputMaybe<Scalars['Float']>;
  contentContains?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<Scalars['Int']>;
  idContains?: InputMaybe<Array<Scalars['Float']>>;
  notificationDeliveryStatus?: InputMaybe<UserRequestSendNotificationStatus>;
  ownerId?: InputMaybe<Scalars['Int']>;
  productId?: InputMaybe<Scalars['Int']>;
  productNameContains?: InputMaybe<Scalars['String']>;
  requestedObjectId?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<UserRequestStatus>;
  statusAssign?: InputMaybe<UserRequestStatusAssign>;
  type?: InputMaybe<UserRequestType>;
  userNameContains?: InputMaybe<Scalars['String']>;
};

export type UserRequestWhereUniqueInput = {
  id: Scalars['Int'];
};

export type UserResource = {
  __typename?: 'UserResource';
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['Int']>;
  isSaved?: Maybe<Scalars['Boolean']>;
  resourceId?: Maybe<Scalars['Int']>;
  status?: Maybe<RoleStatus>;
  userId?: Maybe<Scalars['Int']>;
};

export type UserUpdateInput = {
  account?: InputMaybe<Scalars['String']>;
  address?: InputMaybe<Scalars['String']>;
  avatar?: InputMaybe<ImageUpdateManyWithoutUserInput>;
  baumannSkinType?: InputMaybe<BaumannSkinType>;
  birthYear?: InputMaybe<Scalars['Int']>;
  displayName?: InputMaybe<Scalars['String']>;
  fullName?: InputMaybe<Scalars['String']>;
  fullname?: InputMaybe<Scalars['String']>;
  gender?: InputMaybe<Gender>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  role?: InputMaybe<Scalars['String']>;
  roleId?: InputMaybe<Scalars['Int']>;
  skinType?: InputMaybe<SkinType>;
  userPath?: InputMaybe<Scalars['String']>;
  zipCode?: InputMaybe<Scalars['Int']>;
};

export type UserWhereInput = {
  account?: InputMaybe<StringFilter>;
  accountContains?: InputMaybe<Scalars['String']>;
  accountStartsWith?: InputMaybe<Scalars['String']>;
  baumannSkinType?: InputMaybe<BaumannSkinTypeFilter>;
  birthYearOR?: InputMaybe<Array<IntFilter>>;
  displayName?: InputMaybe<StringFilter>;
  followersSome?: InputMaybe<UserWhereInput>;
  followingsSome?: InputMaybe<UserWhereInput>;
  gender?: InputMaybe<Gender>;
  genderIn?: InputMaybe<Array<Gender>>;
  id?: InputMaybe<Scalars['Int']>;
  isBlockedByViewer?: InputMaybe<Scalars['Boolean']>;
  skinType?: InputMaybe<SkinType>;
  skinTypeIn?: InputMaybe<Array<SkinType>>;
  userOnly?: InputMaybe<Scalars['Boolean']>;
};

export type UserWhereUniqueInput = {
  account?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  isOfficial?: InputMaybe<Scalars['Boolean']>;
};

export type UsersAggregate = {
  __typename?: 'UsersAggregate';
  count: Scalars['Int'];
};

export type UsersConnection = {
  __typename?: 'UsersConnection';
  aggregate: UsersAggregate;
};

export type VariantReviewInsertInput = {
  comment?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<Array<ImageReviewInput>>;
  productId: Scalars['Int'];
  rate: Scalars['Int'];
  variantId: Scalars['Int'];
};

export type Video = {
  __typename?: 'Video';
  details?: Maybe<VideoDetails>;
  status: VideoStatus;
  thumbnail?: Maybe<VideoThumbnail>;
};

export type VideoCreateManyWithoutPostInput = {
  create?: InputMaybe<Array<VideoCreateWithoutPostInput>>;
};

export type VideoCreateWithoutPostInput = {
  name?: InputMaybe<Scalars['String']>;
  thumbnailUrl?: InputMaybe<Scalars['String']>;
  url: Scalars['String'];
};

export type VideoDetails = {
  __typename?: 'VideoDetails';
  height: Scalars['Int'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type VideoDetailsUpdateInput = {
  height: Scalars['Int'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type VideoObject = {
  __typename?: 'VideoObject';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  name: Scalars['String'];
  thumbnailUrl?: Maybe<Scalars['String']>;
  url: Scalars['String'];
};

export type VideoOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export enum VideoStatus {
  Complete = 'COMPLETE',
  Error = 'ERROR',
  InProgressing = 'IN_PROGRESSING'
}

export type VideoThumbnail = {
  __typename?: 'VideoThumbnail';
  height: Scalars['Int'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type VideoThumbnailUpdateInput = {
  height: Scalars['Int'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type VideoUpdateManyWithoutPostInput = {
  update?: InputMaybe<Array<VideosUpdateWithoutPostInput>>;
};

export type VideoUpdateOneWithoutPostInput = {
  update: VideoUpdateWithoutPostInput;
};

export type VideoUpdateWithoutPostInput = {
  details: VideoDetailsUpdateInput;
  thumbnail: VideoThumbnailUpdateInput;
};

export type VideoWhereInput = {
  nonNull?: InputMaybe<Scalars['Boolean']>;
  status?: InputMaybe<VideoStatus>;
};

export type VideosUpdateWithoutPostInput = {
  id?: InputMaybe<Scalars['Int']>;
  thumbnailUrl?: InputMaybe<Scalars['String']>;
};

export type VideosWhereInput = {
  review?: InputMaybe<ReviewWhereInput>;
};

export type ViewedPostStatistic = {
  __typename?: 'ViewedPostStatistic';
  click?: Maybe<Scalars['Int']>;
  multiple?: Maybe<Scalars['Int']>;
  single?: Maybe<Scalars['Int']>;
};

export type VisitedContentArgsOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
};

export type VisitedContentCreateInput = {
  entityId?: InputMaybe<Scalars['Int']>;
  type: VisitedContentType;
};

export type VisitedContentEntityUnion = Post | Product;

export enum VisitedContentType {
  Post = 'Post',
  Product = 'Product'
}

export type VisitedContentWhereInput = {
  createdAt?: InputMaybe<DateTimeFilter>;
  type?: InputMaybe<VisitedContentType>;
};

export type VnBank = {
  __typename?: 'VnBank';
  bankName: Scalars['String'];
  brandName?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  logo: Scalars['String'];
};

export type Voucher = {
  __typename?: 'Voucher';
  barcode?: Maybe<Scalars['String']>;
  brandCode?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  expiryDate: Scalars['DateTime'];
  goodsCode: Scalars['String'];
  goodsList: GoodsList;
  id: Scalars['Int'];
  imgCoupon?: Maybe<Scalars['String']>;
  imgOrigin?: Maybe<Scalars['String']>;
  note: Scalars['String'];
  password?: Maybe<Scalars['String']>;
  pinNo: Scalars['String'];
  pinUrl: Scalars['String'];
  redeemOnDeepLink?: Maybe<Scalars['String']>;
  rpPoint?: Maybe<Scalars['Int']>;
  status?: Maybe<Scalars['String']>;
  storeCode?: Maybe<Scalars['String']>;
  trId: Scalars['String'];
  usedTime?: Maybe<Scalars['DateTime']>;
  user: User;
  userId: Scalars['Int'];
};

export type VoucherArgsOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type VoucherBrand = {
  __typename?: 'VoucherBrand';
  code: Scalars['String'];
  id: Scalars['Int'];
  logo: Scalars['String'];
  name: Scalars['String'];
};

export type VoucherCategories = {
  __typename?: 'VoucherCategories';
  code: Scalars['String'];
  id: Scalars['Int'];
  name: Scalars['String'];
  name_en: Scalars['String'];
  name_kr: Scalars['String'];
};

export type VoucherCategoriesWhereInput = {
  code?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type VoucherGiftpopListUpdateInput = {
  voucherList: Array<VoucherGiftpopUpdateInput>;
};

export type VoucherGiftpopUpdateInput = {
  storeCode: Scalars['String'];
  useStatus: Scalars['String'];
  usedTime: Scalars['String'];
  voucherId: Scalars['String'];
};

export type VoucherIssuesInput = {
  goodsId: Scalars['String'];
  quantity: Scalars['Int'];
};

export type VoucherOrder = {
  __typename?: 'VoucherOrder';
  createdAt?: Maybe<Scalars['DateTime']>;
  giftpopMessage?: Maybe<Scalars['String']>;
  giftpopStatus?: Maybe<Scalars['Boolean']>;
  goodsId?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  nbVouchers?: Maybe<Scalars['Int']>;
  pointAfter?: Maybe<Scalars['Int']>;
  pointBefore?: Maybe<Scalars['Int']>;
  userId?: Maybe<Scalars['Int']>;
};

export enum VoucherStatus {
  Expired = 'Expired',
  InUse = 'InUse',
  Used = 'Used'
}

export type VoucherStoreWhereInput = {
  brandCode: Scalars['String'];
};

export type VoucherVendor = {
  __typename?: 'VoucherVendor';
  id: Scalars['Int'];
  logo: Scalars['String'];
  name: Scalars['String'];
};

export type VoucherWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<VoucherStatus>;
};

export type WardInput = {
  districtCode: Scalars['String'];
};

export type Wards = {
  __typename?: 'Wards';
  district_code: Scalars['String'];
  id: Scalars['Int'];
  province_code: Scalars['String'];
  ward_code: Scalars['String'];
  ward_name: Scalars['String'];
  ward_normalized_name: Scalars['String'];
};

export type SignInByFacebookMutationVariables = Exact<{
  facebookId: Scalars['String'];
}>;


export type SignInByFacebookMutation = { __typename?: 'Mutation', loginByFacebookV2: { __typename: 'AuthError', AEStatus: ErrorStatus, AECode: AuthErrorCode } | { __typename: 'AuthPayload', token: string, user: { __typename: 'User', id: number } } | { __typename: 'CommonError', CEStatus: ErrorStatus, CEMsg: string } };

export type CheckUserExistsQueryVariables = Exact<{
  where: UserWhereUniqueInput;
}>;


export type CheckUserExistsQuery = { __typename?: 'Query', user?: { __typename: 'User', id: number, email?: string | null, account: string, gender?: Gender | null, baumannSkinType?: BaumannSkinType | null, birthYear?: number | null, nationality?: string | null, phoneNumber?: string | null, isVerified: boolean, isOfficial?: boolean | null, type?: AccountType | null, referralCode: string, isFollowingViewer?: boolean | null, totalPoints: number, levelPoints: number, redeemablePoints: number, isBlockedByViewer?: boolean | null, isAnonymous: boolean, avatar?: { __typename: 'Image', url: string } | null, shop?: { __typename: 'Shop', id: number, name: string, status: ShopStatus } | null, followingsConnection: { __typename: 'FollowingsConnection', aggregate: { __typename: 'FollowingsAggregate', count: number } }, followersConnection: { __typename: 'FollowersConnection', aggregate: { __typename: 'FollowersAggregate', count: number } }, reviewsConnection: { __typename: 'ReviewsConnection', aggregate: { __typename: 'ReviewsAggregate', count: number } }, postsConnection: { __typename: 'PostsConnection', aggregate: { __typename: 'PostsAggregate', count: number } }, currentLevel?: { __typename: 'Level', id: number, maxPoints?: number | null, minPoints: number } | null, nextLevel?: { __typename: 'Level', id: number, maxPoints?: number | null, minPoints: number } | null } | null };

export type UserInfoFieldFragment = { __typename: 'User', id: number, email?: string | null, account: string, gender?: Gender | null, baumannSkinType?: BaumannSkinType | null, birthYear?: number | null, nationality?: string | null, phoneNumber?: string | null, isVerified: boolean, isOfficial?: boolean | null, type?: AccountType | null, referralCode: string, isFollowingViewer?: boolean | null, totalPoints: number, levelPoints: number, redeemablePoints: number, isBlockedByViewer?: boolean | null, isAnonymous: boolean, avatar?: { __typename: 'Image', url: string } | null, shop?: { __typename: 'Shop', id: number, name: string, status: ShopStatus } | null, followingsConnection: { __typename: 'FollowingsConnection', aggregate: { __typename: 'FollowingsAggregate', count: number } }, followersConnection: { __typename: 'FollowersConnection', aggregate: { __typename: 'FollowersAggregate', count: number } }, reviewsConnection: { __typename: 'ReviewsConnection', aggregate: { __typename: 'ReviewsAggregate', count: number } }, postsConnection: { __typename: 'PostsConnection', aggregate: { __typename: 'PostsAggregate', count: number } }, currentLevel?: { __typename: 'Level', id: number, maxPoints?: number | null, minPoints: number } | null, nextLevel?: { __typename: 'Level', id: number, maxPoints?: number | null, minPoints: number } | null };

export type CommentEventQueryVariables = Exact<{
  where: EventCommentWhereInput;
  offset?: InputMaybe<Scalars['Int']>;
  limit?: InputMaybe<Scalars['Int']>;
}>;


export type CommentEventQuery = { __typename?: 'Query', eventCommentsConnection: { __typename: 'EventCommentConnection', aggregate: { __typename: 'EventCommentAggregate', count: number } }, eventComments: Array<{ __typename: 'EventComment', id: number, content: string, createdAt: any, status: EventCommentStatus, user: { __typename: 'User', id: number } }> };

export type CreateEventCommentV3MutationVariables = Exact<{
  data: EventCommentCreateInput;
}>;


export type CreateEventCommentV3Mutation = { __typename?: 'Mutation', createEventCommentV3_UncheckCondition: { __typename: 'CommonError', message: string, status: ErrorStatus } | { __typename: 'EventComment', id: number } | { __typename: 'EventCommentCreateError', message: string, status: ErrorStatus, code: EventCommentCreateErrorCode, currentValue?: number | null, expectedValue?: number | null, comments?: Array<{ __typename?: 'EventComment', id: number }> | null } };

export type CreateFundingOrderMutationVariables = Exact<{
  data: FundingOrderInput;
}>;


export type CreateFundingOrderMutation = { __typename?: 'Mutation', createFundingOrder: string };

export type CreateShippingAddressMutationVariables = Exact<{
  data: ShippingAddressCreateInput;
}>;


export type CreateShippingAddressMutation = { __typename?: 'Mutation', createShippingAddress: { __typename?: 'CommonError', message: string, status: ErrorStatus, code: CommonErrorCode } | { __typename?: 'ShippingAddress', id: number } };

export type DeleteShippingAddressMutationVariables = Exact<{
  where: ShippingAddressWhereUniqueInput;
}>;


export type DeleteShippingAddressMutation = { __typename?: 'Mutation', deleteShippingAddress: { __typename?: 'CommonError', status: ErrorStatus, message: string, code: CommonErrorCode } | { __typename?: 'ShippingAddress', id: number } };

export type ForgetPasswordMutationVariables = Exact<{
  email: Scalars['String'];
}>;


export type ForgetPasswordMutation = { __typename?: 'Mutation', forgetPassword: boolean };

export type GetDistrictsQueryVariables = Exact<{
  where: DistrictsWhereInput;
}>;


export type GetDistrictsQuery = { __typename?: 'Query', districts: Array<{ __typename?: 'Districts', id: number, name: string, code: string }> };

export type ShippingAddressQueryVariables = Exact<{
  where: ShippingAddressWhereUniqueInput;
}>;


export type ShippingAddressQuery = { __typename?: 'Query', shippingAddress: { __typename?: 'CommonError', code: CommonErrorCode, message: string } | { __typename?: 'ShippingAddress', fullName: string, address: string, ward: string, district: string, province: string, phoneNumber?: string | null, default: boolean } };

export type FundingOrdersQueryVariables = Exact<{
  skip?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderWhereInput>;
  orderBy?: InputMaybe<FundingOrderOrderByInput>;
}>;


export type FundingOrdersQuery = { __typename?: 'Query', fundingOrders: Array<{ __typename?: 'FundingOrder', totalAmount?: number | null, id: number, fundingId: number, productId: number, createdAt?: any | null, userId: number, quantity?: number | null, pickAddress?: string | null, deliveryAddress?: string | null, shipmentStatus?: number | null, status?: FundingOrderStatus | null, statusText?: string | null, isPaid?: boolean | null, deliveryFee?: number | null, insuranceFee?: number | null, deliveryFeeInfo?: string | null, transportId?: string | null, shipmentStatusText?: string | null, paymentMethod?: string | null, discount?: number | null, fundingProducts: Array<{ __typename?: 'FundingProduct', product?: { __typename?: 'Product', price?: number | null, id: number, attributes?: { __typename?: 'ProductAttribute', netWeightPerPackage?: number | null, weightUnit?: Unit | null } | null, thumbnail?: { __typename?: 'Image', url: string } | null, translations: Array<{ __typename?: 'ProductTranslation', name: string }> } | null }>, orderEvaluations?: Array<{ __typename?: 'FundingOrderEvaluation', id: number, orderId?: number | null, productId?: number | null, rate?: number | null, comment?: string | null, createdAt?: any | null, updatedAt?: any | null, images?: Array<{ __typename?: 'Image', url: string }> | null }> | null }>, fundingOrderConnection: { __typename?: 'FundingOrderConnection', aggregate: { __typename?: 'FundingOrderAggregate', countFundingOrder: number } } };

export type GetOneFundingOrderQueryVariables = Exact<{
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderWhereInput>;
  orderBy?: InputMaybe<FundingOrderOrderByInput>;
}>;


export type GetOneFundingOrderQuery = { __typename?: 'Query', fundingOrder: { __typename: 'FundingOrder', estimatedDeliverTime?: string | null, userName?: string | null, phoneNumber?: string | null, id: number, fundingId: number, productId: number, createdAt?: any | null, userId: number, quantity?: number | null, pickAddress?: string | null, deliveryAddress?: string | null, shipmentStatus?: number | null, status?: FundingOrderStatus | null, isPaid?: boolean | null, deliveryFee?: number | null, insuranceFee?: number | null, deliveryFeeInfo?: string | null, transportId?: string | null, shipmentStatusText?: string | null, paymentMethod?: string | null, discount?: number | null, totalAmount?: number | null, shipmentHistory?: Array<{ __typename: 'ShipmentHistory', id: number, fundingOrderId: number, status: number, statusText: string, createdAt: any }> | null, orderEvaluations?: Array<{ __typename?: 'FundingOrderEvaluation', id: number, orderId?: number | null, productId?: number | null, rate?: number | null, comment?: string | null, createdAt?: any | null, updatedAt?: any | null, images?: Array<{ __typename?: 'Image', url: string, name: string }> | null }> | null, fundingProducts: Array<{ __typename: 'FundingProduct', discount?: number | null, quantity?: number | null, fundingID?: number | null, id: number, productId?: number | null, productEvaluation?: Array<{ __typename?: 'FundingOrderEvaluation', id: number, orderId?: number | null, productId?: number | null, rate?: number | null, comment?: string | null, createdAt?: any | null, updatedAt?: any | null, images?: Array<{ __typename?: 'Image', url: string, name: string }> | null }> | null, product?: { __typename: 'Product', price?: number | null, thumbnail?: { __typename: 'Image', url: string } | null, translations: Array<{ __typename: 'ProductTranslation', name: string }> } | null }> } };

export type GetProvincesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetProvincesQuery = { __typename?: 'Query', provinces: Array<{ __typename?: 'Provinces', id: number, name: string }> };

export type GetShippingAddressesQueryVariables = Exact<{
  skip?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShippingAddressWhereInput>;
  orderBy?: InputMaybe<ShippingAddressOrderByInput>;
}>;


export type GetShippingAddressesQuery = { __typename?: 'Query', shippingAddresses: Array<{ __typename?: 'ShippingAddress', id: number, fullName: string, address: string, ward: string, district: string, province: string, country: string, phoneNumber?: string | null, createdAt: any, updatedAt: any, default: boolean }> };

export type GetShippingFeeQueryVariables = Exact<{
  data: ShippingFeeInput;
}>;


export type GetShippingFeeQuery = { __typename?: 'Query', shippingFee: string };

export type GetWardsQueryVariables = Exact<{
  where: WardInput;
}>;


export type GetWardsQuery = { __typename?: 'Query', wards: Array<{ __typename?: 'Wards', id: number, ward_code: string, ward_name: string, district_code: string, province_code: string }> };

export type UserLoginMutationVariables = Exact<{
  password: Scalars['String'];
  email: Scalars['String'];
}>;


export type UserLoginMutation = { __typename?: 'Mutation', loginV2: { __typename: 'AuthError' } | { __typename: 'AuthPayload', token: string } | { __typename: 'CommonError' } };

export type GetMeQueryVariables = Exact<{ [key: string]: never; }>;


export type GetMeQuery = { __typename?: 'Query', me: { __typename?: 'User', id: number, isOfficial?: boolean | null, account: string, email?: string | null, gender?: Gender | null, baumannSkinType?: BaumannSkinType | null, birthYear?: number | null, nationality?: string | null, phoneNumber?: string | null, type?: AccountType | null, isVerified: boolean, redeemablePoints: number, levelPoints: number, isFollowingViewer?: boolean | null, referralCode: string, isAnonymous: boolean, isBlockedByViewer?: boolean | null, wishedProductsConnection: { __typename?: 'ProductsConnection', aggregate: { __typename?: 'ProductsAggregate', count: number } }, postsConnection: { __typename?: 'PostsConnection', aggregate: { __typename?: 'PostsAggregate', count: number } }, reviewsConnection: { __typename?: 'ReviewsConnection', aggregate: { __typename?: 'ReviewsAggregate', count: number } }, currentLevel?: { __typename?: 'Level', id: number, minPoints: number, maxPoints?: number | null } | null, shop?: { __typename?: 'Shop', id: number, name: string } | null, brandAdmin?: { __typename?: 'BrandAdmin', id: string, name: string, brandId?: string | null, logoUrl?: string | null, status: BrandAdminStatus } | null, avatar?: { __typename?: 'Image', url: string } | null } };

export type UserFullFieldsFragment = { __typename?: 'User', id: number, isOfficial?: boolean | null, account: string, email?: string | null, gender?: Gender | null, baumannSkinType?: BaumannSkinType | null, birthYear?: number | null, nationality?: string | null, phoneNumber?: string | null, type?: AccountType | null, isVerified: boolean, redeemablePoints: number, levelPoints: number, isFollowingViewer?: boolean | null, referralCode: string, isAnonymous: boolean, isBlockedByViewer?: boolean | null, avatar?: { __typename?: 'Image', url: string } | null };

export type OrderStatisticOfUserQueryVariables = Exact<{
  where?: InputMaybe<FundingOrderStatisticWhereInput>;
}>;


export type OrderStatisticOfUserQuery = { __typename?: 'Query', nbOrderStatisticOfUser: Array<{ __typename: 'NbOrderStatistic', orderTotal?: number | null, evaluationTotal?: number | null, orderStatus?: string | null }> };

export type PaymentVnPayMutationVariables = Exact<{
  orderInfo: Scalars['String'];
  amount: Scalars['Float'];
}>;


export type PaymentVnPayMutation = { __typename?: 'Mutation', vnpayUrl: string };

export type PayBackFundingOrderMutationVariables = Exact<{
  where?: InputMaybe<FundingOrderWhereInput>;
}>;


export type PayBackFundingOrderMutation = { __typename?: 'Mutation', payBackFundingOrder: string };

export type RegisterCustomerAccountMutationVariables = Exact<{
  data: SignUpInput;
}>;


export type RegisterCustomerAccountMutation = { __typename?: 'Mutation', signUp: { __typename?: 'AuthError', status: ErrorStatus } | { __typename?: 'AuthPayload', token: string, user: { __typename?: 'User', email?: string | null, id: number } } | { __typename?: 'CommonError', status: ErrorStatus } };

export type CreateEvaluationMutationVariables = Exact<{
  data: FundingOrderEvaluationInput;
}>;


export type CreateEvaluationMutation = { __typename?: 'Mutation', createEvaluation: { __typename?: 'FundingOrderEvaluation', id: number } };

export type GetEvaluationQueryVariables = Exact<{
  where?: InputMaybe<FundingOrderEvaluationWhereInput>;
  orderBy?: InputMaybe<BaseOrderByInput>;
}>;


export type GetEvaluationQuery = { __typename?: 'Query', evaluation: { __typename?: 'FundingOrderEvaluation', id: number, orderId?: number | null, rate?: number | null, comment?: string | null, createdAt?: any | null, productId?: number | null, updatedAt?: any | null, fundingOrder: { __typename?: 'FundingOrder', id: number, quantity?: number | null, discount?: number | null, fundingId: number, fundingProducts: Array<{ __typename?: 'FundingProduct', productId?: number | null, product?: { __typename?: 'Product', thumbnail?: { __typename?: 'Image', url: string } | null, translations: Array<{ __typename?: 'ProductTranslation', name: string }> } | null }> }, images?: Array<{ __typename?: 'Image', name: string, url: string }> | null } };

export type GetListReviewQueryVariables = Exact<{
  skip?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderEvaluationWhereInput>;
  orderBy?: InputMaybe<BaseOrderByInput>;
}>;


export type GetListReviewQuery = { __typename?: 'Query', evaluations: Array<{ __typename?: 'FundingOrderEvaluation', id: number, orderId?: number | null, rate?: number | null, comment?: string | null, createdAt?: any | null, productId?: number | null, updatedAt?: any | null, fundingOrder: { __typename?: 'FundingOrder', userName?: string | null, fundingId: number, userOrder: { __typename: 'User', id: number, email?: string | null, account: string, gender?: Gender | null, baumannSkinType?: BaumannSkinType | null, birthYear?: number | null, nationality?: string | null, isOfficial?: boolean | null, isFollowingViewer?: boolean | null, type?: AccountType | null, isBlockedByViewer?: boolean | null, isAnonymous: boolean, avatar?: { __typename: 'Image', url: string } | null, shop?: { __typename: 'Shop', id: number, name: string, status: ShopStatus } | null }, fundingProducts: Array<{ __typename?: 'FundingProduct', productId?: number | null, product?: { __typename?: 'Product', translations: Array<{ __typename?: 'ProductTranslation', name: string }> } | null }> }, images?: Array<{ __typename?: 'Image', url: string, name: string }> | null }>, evaluationConnection: { __typename?: 'EvaluationConnection', aggregate: { __typename?: 'EvaluationAggregate', countEvaluation: number } } };

export type UserInfoShortFieldFragment = { __typename: 'User', id: number, email?: string | null, account: string, gender?: Gender | null, baumannSkinType?: BaumannSkinType | null, birthYear?: number | null, nationality?: string | null, isOfficial?: boolean | null, isFollowingViewer?: boolean | null, type?: AccountType | null, isBlockedByViewer?: boolean | null, isAnonymous: boolean, avatar?: { __typename: 'Image', url: string } | null, shop?: { __typename: 'Shop', id: number, name: string, status: ShopStatus } | null };

export type UpdateEvaluationMutationVariables = Exact<{
  where: FundingOrderEvaluationWhereInput;
  data: FundingOrderEvaluationUpdateInput;
}>;


export type UpdateEvaluationMutation = { __typename?: 'Mutation', updateEvaluation: { __typename?: 'FundingOrderEvaluation', id: number } };

export type UpdateShippingAddressMutationVariables = Exact<{
  data: ShippingAddressUpdateInput;
  where: ShippingAddressWhereUniqueInput;
}>;


export type UpdateShippingAddressMutation = { __typename?: 'Mutation', updateShippingAddress: { __typename?: 'CommonError', message: string, status: ErrorStatus, code: CommonErrorCode } | { __typename?: 'ShippingAddress', id: number } };

export const UserInfoFieldFragmentDoc = gql`
    fragment userInfoField on User {
  __typename
  id
  email
  account
  gender
  baumannSkinType
  birthYear
  avatar {
    __typename
    url
  }
  nationality
  phoneNumber
  isVerified
  isOfficial
  type
  referralCode
  shop {
    __typename
    id
    name
    status
  }
  followingsConnection {
    __typename
    aggregate {
      __typename
      count
    }
  }
  followersConnection {
    __typename
    aggregate {
      __typename
      count
    }
  }
  reviewsConnection(where: {statusIn: [CREATED, APPROVED]}) {
    __typename
    aggregate {
      __typename
      count
    }
  }
  postsConnection {
    __typename
    aggregate {
      __typename
      count
    }
  }
  isFollowingViewer
  totalPoints
  levelPoints
  redeemablePoints
  currentLevel {
    __typename
    id
    maxPoints
    minPoints
  }
  nextLevel {
    __typename
    id
    maxPoints
    minPoints
  }
  isBlockedByViewer
  isAnonymous
}
    `;
export const UserFullFieldsFragmentDoc = gql`
    fragment UserFullFields on User {
  id
  avatar {
    url
  }
  isOfficial
  account
  email
  gender
  baumannSkinType
  birthYear
  nationality
  phoneNumber
  type
  isVerified
  redeemablePoints
  levelPoints
  isFollowingViewer
  referralCode
  isAnonymous
  isBlockedByViewer
}
    `;
export const UserInfoShortFieldFragmentDoc = gql`
    fragment userInfoShortField on User {
  __typename
  id
  email
  account
  gender
  baumannSkinType
  birthYear
  avatar {
    __typename
    url
  }
  shop {
    __typename
    id
    name
    status
  }
  nationality
  isOfficial
  isFollowingViewer
  type
  isBlockedByViewer
  isAnonymous
}
    `;
export const SignInByFacebookDocument = gql`
    mutation SignInByFacebook($facebookId: String!) {
  loginByFacebookV2(accessToken: $facebookId) {
    __typename
    ... on AuthPayload {
      __typename
      token
      user {
        __typename
        id
      }
    }
    ... on AuthError {
      __typename
      AEStatus: status
      AECode: code
    }
    ... on CommonError {
      __typename
      CEStatus: status
      CEMsg: message
    }
  }
}
    `;

/**
 * __useSignInByFacebookMutation__
 *
 * To run a mutation, you first call `useSignInByFacebookMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignInByFacebookMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signInByFacebookMutation, { data, loading, error }] = useSignInByFacebookMutation({
 *   variables: {
 *      facebookId: // value for 'facebookId'
 *   },
 * });
 */
export function useSignInByFacebookMutation(baseOptions?: Apollo.MutationHookOptions<SignInByFacebookMutation, SignInByFacebookMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SignInByFacebookMutation, SignInByFacebookMutationVariables>(SignInByFacebookDocument, options);
      }
export type SignInByFacebookMutationHookResult = ReturnType<typeof useSignInByFacebookMutation>;
export type SignInByFacebookMutationResult = Apollo.MutationResult<SignInByFacebookMutation>;
export type SignInByFacebookMutationOptions = Apollo.BaseMutationOptions<SignInByFacebookMutation, SignInByFacebookMutationVariables>;
export const CheckUserExistsDocument = gql`
    query CheckUserExists($where: UserWhereUniqueInput!) {
  user(where: $where) {
    __typename
    ...userInfoField
  }
}
    ${UserInfoFieldFragmentDoc}`;

/**
 * __useCheckUserExistsQuery__
 *
 * To run a query within a React component, call `useCheckUserExistsQuery` and pass it any options that fit your needs.
 * When your component renders, `useCheckUserExistsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCheckUserExistsQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useCheckUserExistsQuery(baseOptions: Apollo.QueryHookOptions<CheckUserExistsQuery, CheckUserExistsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CheckUserExistsQuery, CheckUserExistsQueryVariables>(CheckUserExistsDocument, options);
      }
export function useCheckUserExistsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CheckUserExistsQuery, CheckUserExistsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CheckUserExistsQuery, CheckUserExistsQueryVariables>(CheckUserExistsDocument, options);
        }
export type CheckUserExistsQueryHookResult = ReturnType<typeof useCheckUserExistsQuery>;
export type CheckUserExistsLazyQueryHookResult = ReturnType<typeof useCheckUserExistsLazyQuery>;
export type CheckUserExistsQueryResult = Apollo.QueryResult<CheckUserExistsQuery, CheckUserExistsQueryVariables>;
export const CommentEventDocument = gql`
    query CommentEvent($where: EventCommentWhereInput!, $offset: Int, $limit: Int) {
  eventCommentsConnection(where: $where) {
    __typename
    aggregate {
      __typename
      count
    }
  }
  eventComments(where: $where, orderBy: {id: DESC}, skip: $offset, first: $limit) {
    __typename
    id
    content
    createdAt
    status
    user {
      __typename
      id
    }
  }
}
    `;

/**
 * __useCommentEventQuery__
 *
 * To run a query within a React component, call `useCommentEventQuery` and pass it any options that fit your needs.
 * When your component renders, `useCommentEventQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCommentEventQuery({
 *   variables: {
 *      where: // value for 'where'
 *      offset: // value for 'offset'
 *      limit: // value for 'limit'
 *   },
 * });
 */
export function useCommentEventQuery(baseOptions: Apollo.QueryHookOptions<CommentEventQuery, CommentEventQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CommentEventQuery, CommentEventQueryVariables>(CommentEventDocument, options);
      }
export function useCommentEventLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CommentEventQuery, CommentEventQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CommentEventQuery, CommentEventQueryVariables>(CommentEventDocument, options);
        }
export type CommentEventQueryHookResult = ReturnType<typeof useCommentEventQuery>;
export type CommentEventLazyQueryHookResult = ReturnType<typeof useCommentEventLazyQuery>;
export type CommentEventQueryResult = Apollo.QueryResult<CommentEventQuery, CommentEventQueryVariables>;
export const CreateEventCommentV3Document = gql`
    mutation createEventCommentV3($data: EventCommentCreateInput!) {
  createEventCommentV3_UncheckCondition(data: $data) {
    __typename
    ... on EventComment {
      id
    }
    ... on CommonError {
      message
      status
    }
    ... on EventCommentCreateError {
      message
      status
      code
      currentValue
      expectedValue
      comments {
        id
      }
    }
  }
}
    `;

/**
 * __useCreateEventCommentV3Mutation__
 *
 * To run a mutation, you first call `useCreateEventCommentV3Mutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateEventCommentV3Mutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createEventCommentV3Mutation, { data, loading, error }] = useCreateEventCommentV3Mutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useCreateEventCommentV3Mutation(baseOptions?: Apollo.MutationHookOptions<CreateEventCommentV3Mutation, CreateEventCommentV3MutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateEventCommentV3Mutation, CreateEventCommentV3MutationVariables>(CreateEventCommentV3Document, options);
      }
export type CreateEventCommentV3MutationHookResult = ReturnType<typeof useCreateEventCommentV3Mutation>;
export type CreateEventCommentV3MutationResult = Apollo.MutationResult<CreateEventCommentV3Mutation>;
export type CreateEventCommentV3MutationOptions = Apollo.BaseMutationOptions<CreateEventCommentV3Mutation, CreateEventCommentV3MutationVariables>;
export const CreateFundingOrderDocument = gql`
    mutation createFundingOrder($data: FundingOrderInput!) {
  createFundingOrder(data: $data)
}
    `;

/**
 * __useCreateFundingOrderMutation__
 *
 * To run a mutation, you first call `useCreateFundingOrderMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateFundingOrderMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createFundingOrderMutation, { data, loading, error }] = useCreateFundingOrderMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useCreateFundingOrderMutation(baseOptions?: Apollo.MutationHookOptions<CreateFundingOrderMutation, CreateFundingOrderMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateFundingOrderMutation, CreateFundingOrderMutationVariables>(CreateFundingOrderDocument, options);
      }
export type CreateFundingOrderMutationHookResult = ReturnType<typeof useCreateFundingOrderMutation>;
export type CreateFundingOrderMutationResult = Apollo.MutationResult<CreateFundingOrderMutation>;
export type CreateFundingOrderMutationOptions = Apollo.BaseMutationOptions<CreateFundingOrderMutation, CreateFundingOrderMutationVariables>;
export const CreateShippingAddressDocument = gql`
    mutation createShippingAddress($data: ShippingAddressCreateInput!) {
  createShippingAddress(data: $data) {
    ... on ShippingAddress {
      id
    }
    ... on CommonError {
      message
      status
      code
    }
  }
}
    `;

/**
 * __useCreateShippingAddressMutation__
 *
 * To run a mutation, you first call `useCreateShippingAddressMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateShippingAddressMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createShippingAddressMutation, { data, loading, error }] = useCreateShippingAddressMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useCreateShippingAddressMutation(baseOptions?: Apollo.MutationHookOptions<CreateShippingAddressMutation, CreateShippingAddressMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateShippingAddressMutation, CreateShippingAddressMutationVariables>(CreateShippingAddressDocument, options);
      }
export type CreateShippingAddressMutationHookResult = ReturnType<typeof useCreateShippingAddressMutation>;
export type CreateShippingAddressMutationResult = Apollo.MutationResult<CreateShippingAddressMutation>;
export type CreateShippingAddressMutationOptions = Apollo.BaseMutationOptions<CreateShippingAddressMutation, CreateShippingAddressMutationVariables>;
export const DeleteShippingAddressDocument = gql`
    mutation deleteShippingAddress($where: ShippingAddressWhereUniqueInput!) {
  deleteShippingAddress(where: $where) {
    ... on ShippingAddress {
      id
    }
    ... on CommonError {
      status
      message
      code
    }
  }
}
    `;

/**
 * __useDeleteShippingAddressMutation__
 *
 * To run a mutation, you first call `useDeleteShippingAddressMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteShippingAddressMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteShippingAddressMutation, { data, loading, error }] = useDeleteShippingAddressMutation({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useDeleteShippingAddressMutation(baseOptions?: Apollo.MutationHookOptions<DeleteShippingAddressMutation, DeleteShippingAddressMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteShippingAddressMutation, DeleteShippingAddressMutationVariables>(DeleteShippingAddressDocument, options);
      }
export type DeleteShippingAddressMutationHookResult = ReturnType<typeof useDeleteShippingAddressMutation>;
export type DeleteShippingAddressMutationResult = Apollo.MutationResult<DeleteShippingAddressMutation>;
export type DeleteShippingAddressMutationOptions = Apollo.BaseMutationOptions<DeleteShippingAddressMutation, DeleteShippingAddressMutationVariables>;
export const ForgetPasswordDocument = gql`
    mutation ForgetPassword($email: String!) {
  forgetPassword(email: $email)
}
    `;

/**
 * __useForgetPasswordMutation__
 *
 * To run a mutation, you first call `useForgetPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useForgetPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [forgetPasswordMutation, { data, loading, error }] = useForgetPasswordMutation({
 *   variables: {
 *      email: // value for 'email'
 *   },
 * });
 */
export function useForgetPasswordMutation(baseOptions?: Apollo.MutationHookOptions<ForgetPasswordMutation, ForgetPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ForgetPasswordMutation, ForgetPasswordMutationVariables>(ForgetPasswordDocument, options);
      }
export type ForgetPasswordMutationHookResult = ReturnType<typeof useForgetPasswordMutation>;
export type ForgetPasswordMutationResult = Apollo.MutationResult<ForgetPasswordMutation>;
export type ForgetPasswordMutationOptions = Apollo.BaseMutationOptions<ForgetPasswordMutation, ForgetPasswordMutationVariables>;
export const GetDistrictsDocument = gql`
    query GetDistricts($where: DistrictsWhereInput!) {
  districts(where: $where) {
    id
    name
    code
  }
}
    `;

/**
 * __useGetDistrictsQuery__
 *
 * To run a query within a React component, call `useGetDistrictsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetDistrictsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetDistrictsQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useGetDistrictsQuery(baseOptions: Apollo.QueryHookOptions<GetDistrictsQuery, GetDistrictsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetDistrictsQuery, GetDistrictsQueryVariables>(GetDistrictsDocument, options);
      }
export function useGetDistrictsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetDistrictsQuery, GetDistrictsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetDistrictsQuery, GetDistrictsQueryVariables>(GetDistrictsDocument, options);
        }
export type GetDistrictsQueryHookResult = ReturnType<typeof useGetDistrictsQuery>;
export type GetDistrictsLazyQueryHookResult = ReturnType<typeof useGetDistrictsLazyQuery>;
export type GetDistrictsQueryResult = Apollo.QueryResult<GetDistrictsQuery, GetDistrictsQueryVariables>;
export const ShippingAddressDocument = gql`
    query shippingAddress($where: ShippingAddressWhereUniqueInput!) {
  shippingAddress(where: $where) {
    ... on CommonError {
      code
      message
    }
    ... on ShippingAddress {
      fullName
      address
      ward
      district
      province
      phoneNumber
      default
    }
  }
}
    `;

/**
 * __useShippingAddressQuery__
 *
 * To run a query within a React component, call `useShippingAddressQuery` and pass it any options that fit your needs.
 * When your component renders, `useShippingAddressQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useShippingAddressQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useShippingAddressQuery(baseOptions: Apollo.QueryHookOptions<ShippingAddressQuery, ShippingAddressQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ShippingAddressQuery, ShippingAddressQueryVariables>(ShippingAddressDocument, options);
      }
export function useShippingAddressLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ShippingAddressQuery, ShippingAddressQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ShippingAddressQuery, ShippingAddressQueryVariables>(ShippingAddressDocument, options);
        }
export type ShippingAddressQueryHookResult = ReturnType<typeof useShippingAddressQuery>;
export type ShippingAddressLazyQueryHookResult = ReturnType<typeof useShippingAddressLazyQuery>;
export type ShippingAddressQueryResult = Apollo.QueryResult<ShippingAddressQuery, ShippingAddressQueryVariables>;
export const FundingOrdersDocument = gql`
    query fundingOrders($skip: Int = 0, $after: ID, $before: ID, $first: Int = 10, $last: Int, $where: FundingOrderWhereInput, $orderBy: FundingOrderOrderByInput) {
  fundingOrders(
    skip: $skip
    after: $after
    before: $before
    first: $first
    last: $last
    where: $where
    orderBy: $orderBy
  ) {
    fundingProducts {
      product {
        attributes {
          netWeightPerPackage
          weightUnit
        }
        price
        thumbnail {
          url
        }
        id
        translations {
          name
        }
      }
    }
    totalAmount
    id
    fundingId
    productId
    createdAt
    userId
    quantity
    pickAddress
    deliveryAddress
    shipmentStatus
    status
    statusText
    isPaid
    deliveryFee
    insuranceFee
    deliveryFeeInfo
    transportId
    shipmentStatusText
    paymentMethod
    discount
    orderEvaluations {
      id
      orderId
      productId
      rate
      comment
      images {
        url
      }
      createdAt
      updatedAt
    }
  }
  fundingOrderConnection(
    skip: $skip
    first: $first
    where: $where
    orderBy: $orderBy
  ) {
    aggregate {
      countFundingOrder
    }
  }
}
    `;

/**
 * __useFundingOrdersQuery__
 *
 * To run a query within a React component, call `useFundingOrdersQuery` and pass it any options that fit your needs.
 * When your component renders, `useFundingOrdersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFundingOrdersQuery({
 *   variables: {
 *      skip: // value for 'skip'
 *      after: // value for 'after'
 *      before: // value for 'before'
 *      first: // value for 'first'
 *      last: // value for 'last'
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *   },
 * });
 */
export function useFundingOrdersQuery(baseOptions?: Apollo.QueryHookOptions<FundingOrdersQuery, FundingOrdersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FundingOrdersQuery, FundingOrdersQueryVariables>(FundingOrdersDocument, options);
      }
export function useFundingOrdersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FundingOrdersQuery, FundingOrdersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FundingOrdersQuery, FundingOrdersQueryVariables>(FundingOrdersDocument, options);
        }
export type FundingOrdersQueryHookResult = ReturnType<typeof useFundingOrdersQuery>;
export type FundingOrdersLazyQueryHookResult = ReturnType<typeof useFundingOrdersLazyQuery>;
export type FundingOrdersQueryResult = Apollo.QueryResult<FundingOrdersQuery, FundingOrdersQueryVariables>;
export const GetOneFundingOrderDocument = gql`
    query getOneFundingOrder($skip: Int = 0, $first: Int = 10, $where: FundingOrderWhereInput, $orderBy: FundingOrderOrderByInput) {
  fundingOrder(skip: $skip, first: $first, where: $where, orderBy: $orderBy) {
    shipmentHistory {
      id
      fundingOrderId
      status
      statusText
      createdAt
      __typename
    }
    estimatedDeliverTime
    userName
    phoneNumber
    id
    fundingId
    productId
    createdAt
    userId
    quantity
    pickAddress
    deliveryAddress
    shipmentStatus
    status
    isPaid
    deliveryFee
    insuranceFee
    deliveryFeeInfo
    transportId
    shipmentStatusText
    paymentMethod
    discount
    totalAmount
    orderEvaluations {
      id
      orderId
      productId
      rate
      comment
      images {
        url
        name
      }
      createdAt
      updatedAt
    }
    fundingProducts {
      productEvaluation {
        id
        orderId
        productId
        rate
        comment
        images {
          url
          name
        }
        createdAt
        updatedAt
      }
      discount
      quantity
      fundingID
      id
      productId
      product {
        thumbnail {
          url
          __typename
        }
        price
        translations {
          name
          __typename
        }
        __typename
      }
      __typename
    }
    __typename
  }
}
    `;

/**
 * __useGetOneFundingOrderQuery__
 *
 * To run a query within a React component, call `useGetOneFundingOrderQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetOneFundingOrderQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetOneFundingOrderQuery({
 *   variables: {
 *      skip: // value for 'skip'
 *      first: // value for 'first'
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *   },
 * });
 */
export function useGetOneFundingOrderQuery(baseOptions?: Apollo.QueryHookOptions<GetOneFundingOrderQuery, GetOneFundingOrderQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetOneFundingOrderQuery, GetOneFundingOrderQueryVariables>(GetOneFundingOrderDocument, options);
      }
export function useGetOneFundingOrderLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetOneFundingOrderQuery, GetOneFundingOrderQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetOneFundingOrderQuery, GetOneFundingOrderQueryVariables>(GetOneFundingOrderDocument, options);
        }
export type GetOneFundingOrderQueryHookResult = ReturnType<typeof useGetOneFundingOrderQuery>;
export type GetOneFundingOrderLazyQueryHookResult = ReturnType<typeof useGetOneFundingOrderLazyQuery>;
export type GetOneFundingOrderQueryResult = Apollo.QueryResult<GetOneFundingOrderQuery, GetOneFundingOrderQueryVariables>;
export const GetProvincesDocument = gql`
    query GetProvinces {
  provinces {
    id
    name
  }
}
    `;

/**
 * __useGetProvincesQuery__
 *
 * To run a query within a React component, call `useGetProvincesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetProvincesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetProvincesQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetProvincesQuery(baseOptions?: Apollo.QueryHookOptions<GetProvincesQuery, GetProvincesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetProvincesQuery, GetProvincesQueryVariables>(GetProvincesDocument, options);
      }
export function useGetProvincesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetProvincesQuery, GetProvincesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetProvincesQuery, GetProvincesQueryVariables>(GetProvincesDocument, options);
        }
export type GetProvincesQueryHookResult = ReturnType<typeof useGetProvincesQuery>;
export type GetProvincesLazyQueryHookResult = ReturnType<typeof useGetProvincesLazyQuery>;
export type GetProvincesQueryResult = Apollo.QueryResult<GetProvincesQuery, GetProvincesQueryVariables>;
export const GetShippingAddressesDocument = gql`
    query getShippingAddresses($skip: Int = 0, $after: ID, $before: ID, $first: Int = 10, $last: Int, $where: ShippingAddressWhereInput, $orderBy: ShippingAddressOrderByInput) {
  shippingAddresses(
    skip: $skip
    after: $after
    before: $before
    first: $first
    last: $last
    where: $where
    orderBy: $orderBy
  ) {
    id
    fullName
    address
    ward
    district
    province
    country
    phoneNumber
    createdAt
    updatedAt
    default
  }
}
    `;

/**
 * __useGetShippingAddressesQuery__
 *
 * To run a query within a React component, call `useGetShippingAddressesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetShippingAddressesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetShippingAddressesQuery({
 *   variables: {
 *      skip: // value for 'skip'
 *      after: // value for 'after'
 *      before: // value for 'before'
 *      first: // value for 'first'
 *      last: // value for 'last'
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *   },
 * });
 */
export function useGetShippingAddressesQuery(baseOptions?: Apollo.QueryHookOptions<GetShippingAddressesQuery, GetShippingAddressesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetShippingAddressesQuery, GetShippingAddressesQueryVariables>(GetShippingAddressesDocument, options);
      }
export function useGetShippingAddressesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetShippingAddressesQuery, GetShippingAddressesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetShippingAddressesQuery, GetShippingAddressesQueryVariables>(GetShippingAddressesDocument, options);
        }
export type GetShippingAddressesQueryHookResult = ReturnType<typeof useGetShippingAddressesQuery>;
export type GetShippingAddressesLazyQueryHookResult = ReturnType<typeof useGetShippingAddressesLazyQuery>;
export type GetShippingAddressesQueryResult = Apollo.QueryResult<GetShippingAddressesQuery, GetShippingAddressesQueryVariables>;
export const GetShippingFeeDocument = gql`
    query getShippingFee($data: ShippingFeeInput!) {
  shippingFee(data: $data)
}
    `;

/**
 * __useGetShippingFeeQuery__
 *
 * To run a query within a React component, call `useGetShippingFeeQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetShippingFeeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetShippingFeeQuery({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useGetShippingFeeQuery(baseOptions: Apollo.QueryHookOptions<GetShippingFeeQuery, GetShippingFeeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetShippingFeeQuery, GetShippingFeeQueryVariables>(GetShippingFeeDocument, options);
      }
export function useGetShippingFeeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetShippingFeeQuery, GetShippingFeeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetShippingFeeQuery, GetShippingFeeQueryVariables>(GetShippingFeeDocument, options);
        }
export type GetShippingFeeQueryHookResult = ReturnType<typeof useGetShippingFeeQuery>;
export type GetShippingFeeLazyQueryHookResult = ReturnType<typeof useGetShippingFeeLazyQuery>;
export type GetShippingFeeQueryResult = Apollo.QueryResult<GetShippingFeeQuery, GetShippingFeeQueryVariables>;
export const GetWardsDocument = gql`
    query GetWards($where: WardInput!) {
  wards(where: $where) {
    id
    ward_code
    ward_name
    district_code
    province_code
  }
}
    `;

/**
 * __useGetWardsQuery__
 *
 * To run a query within a React component, call `useGetWardsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetWardsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetWardsQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useGetWardsQuery(baseOptions: Apollo.QueryHookOptions<GetWardsQuery, GetWardsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetWardsQuery, GetWardsQueryVariables>(GetWardsDocument, options);
      }
export function useGetWardsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetWardsQuery, GetWardsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetWardsQuery, GetWardsQueryVariables>(GetWardsDocument, options);
        }
export type GetWardsQueryHookResult = ReturnType<typeof useGetWardsQuery>;
export type GetWardsLazyQueryHookResult = ReturnType<typeof useGetWardsLazyQuery>;
export type GetWardsQueryResult = Apollo.QueryResult<GetWardsQuery, GetWardsQueryVariables>;
export const UserLoginDocument = gql`
    mutation UserLogin($password: String!, $email: String!) {
  loginV2(password: $password, email: $email) {
    ... on AuthPayload {
      token
    }
    __typename
  }
}
    `;

/**
 * __useUserLoginMutation__
 *
 * To run a mutation, you first call `useUserLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUserLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [userLoginMutation, { data, loading, error }] = useUserLoginMutation({
 *   variables: {
 *      password: // value for 'password'
 *      email: // value for 'email'
 *   },
 * });
 */
export function useUserLoginMutation(baseOptions?: Apollo.MutationHookOptions<UserLoginMutation, UserLoginMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UserLoginMutation, UserLoginMutationVariables>(UserLoginDocument, options);
      }
export type UserLoginMutationHookResult = ReturnType<typeof useUserLoginMutation>;
export type UserLoginMutationResult = Apollo.MutationResult<UserLoginMutation>;
export type UserLoginMutationOptions = Apollo.BaseMutationOptions<UserLoginMutation, UserLoginMutationVariables>;
export const GetMeDocument = gql`
    query GetMe {
  me {
    ...UserFullFields
    wishedProductsConnection {
      aggregate {
        count
      }
    }
    postsConnection {
      aggregate {
        count
      }
    }
    reviewsConnection {
      aggregate {
        count
      }
    }
    currentLevel {
      id
      minPoints
      maxPoints
    }
    shop {
      id
      name
    }
    brandAdmin {
      id
      name
      brandId
      logoUrl
      status
    }
  }
}
    ${UserFullFieldsFragmentDoc}`;

/**
 * __useGetMeQuery__
 *
 * To run a query within a React component, call `useGetMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetMeQuery(baseOptions?: Apollo.QueryHookOptions<GetMeQuery, GetMeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetMeQuery, GetMeQueryVariables>(GetMeDocument, options);
      }
export function useGetMeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetMeQuery, GetMeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetMeQuery, GetMeQueryVariables>(GetMeDocument, options);
        }
export type GetMeQueryHookResult = ReturnType<typeof useGetMeQuery>;
export type GetMeLazyQueryHookResult = ReturnType<typeof useGetMeLazyQuery>;
export type GetMeQueryResult = Apollo.QueryResult<GetMeQuery, GetMeQueryVariables>;
export const OrderStatisticOfUserDocument = gql`
    query OrderStatisticOfUser($where: FundingOrderStatisticWhereInput) {
  nbOrderStatisticOfUser(where: $where) {
    orderTotal
    evaluationTotal
    orderStatus
    __typename
  }
}
    `;

/**
 * __useOrderStatisticOfUserQuery__
 *
 * To run a query within a React component, call `useOrderStatisticOfUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useOrderStatisticOfUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useOrderStatisticOfUserQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useOrderStatisticOfUserQuery(baseOptions?: Apollo.QueryHookOptions<OrderStatisticOfUserQuery, OrderStatisticOfUserQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<OrderStatisticOfUserQuery, OrderStatisticOfUserQueryVariables>(OrderStatisticOfUserDocument, options);
      }
export function useOrderStatisticOfUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<OrderStatisticOfUserQuery, OrderStatisticOfUserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<OrderStatisticOfUserQuery, OrderStatisticOfUserQueryVariables>(OrderStatisticOfUserDocument, options);
        }
export type OrderStatisticOfUserQueryHookResult = ReturnType<typeof useOrderStatisticOfUserQuery>;
export type OrderStatisticOfUserLazyQueryHookResult = ReturnType<typeof useOrderStatisticOfUserLazyQuery>;
export type OrderStatisticOfUserQueryResult = Apollo.QueryResult<OrderStatisticOfUserQuery, OrderStatisticOfUserQueryVariables>;
export const PaymentVnPayDocument = gql`
    mutation paymentVNPay($orderInfo: String!, $amount: Float!) {
  vnpayUrl(orderInfo: $orderInfo, amount: $amount)
}
    `;

/**
 * __usePaymentVnPayMutation__
 *
 * To run a mutation, you first call `usePaymentVnPayMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePaymentVnPayMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [paymentVnPayMutation, { data, loading, error }] = usePaymentVnPayMutation({
 *   variables: {
 *      orderInfo: // value for 'orderInfo'
 *      amount: // value for 'amount'
 *   },
 * });
 */
export function usePaymentVnPayMutation(baseOptions?: Apollo.MutationHookOptions<PaymentVnPayMutation, PaymentVnPayMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<PaymentVnPayMutation, PaymentVnPayMutationVariables>(PaymentVnPayDocument, options);
      }
export type PaymentVnPayMutationHookResult = ReturnType<typeof usePaymentVnPayMutation>;
export type PaymentVnPayMutationResult = Apollo.MutationResult<PaymentVnPayMutation>;
export type PaymentVnPayMutationOptions = Apollo.BaseMutationOptions<PaymentVnPayMutation, PaymentVnPayMutationVariables>;
export const PayBackFundingOrderDocument = gql`
    mutation payBackFundingOrder($where: FundingOrderWhereInput) {
  payBackFundingOrder(where: $where)
}
    `;

/**
 * __usePayBackFundingOrderMutation__
 *
 * To run a mutation, you first call `usePayBackFundingOrderMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePayBackFundingOrderMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [payBackFundingOrderMutation, { data, loading, error }] = usePayBackFundingOrderMutation({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function usePayBackFundingOrderMutation(baseOptions?: Apollo.MutationHookOptions<PayBackFundingOrderMutation, PayBackFundingOrderMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<PayBackFundingOrderMutation, PayBackFundingOrderMutationVariables>(PayBackFundingOrderDocument, options);
      }
export type PayBackFundingOrderMutationHookResult = ReturnType<typeof usePayBackFundingOrderMutation>;
export type PayBackFundingOrderMutationResult = Apollo.MutationResult<PayBackFundingOrderMutation>;
export type PayBackFundingOrderMutationOptions = Apollo.BaseMutationOptions<PayBackFundingOrderMutation, PayBackFundingOrderMutationVariables>;
export const RegisterCustomerAccountDocument = gql`
    mutation RegisterCustomerAccount($data: SignUpInput!) {
  signUp(data: $data) {
    ... on AuthPayload {
      token
      user {
        email
        id
      }
    }
    ... on AuthError {
      status
    }
    ... on CommonError {
      status
    }
  }
}
    `;

/**
 * __useRegisterCustomerAccountMutation__
 *
 * To run a mutation, you first call `useRegisterCustomerAccountMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterCustomerAccountMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerCustomerAccountMutation, { data, loading, error }] = useRegisterCustomerAccountMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useRegisterCustomerAccountMutation(baseOptions?: Apollo.MutationHookOptions<RegisterCustomerAccountMutation, RegisterCustomerAccountMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterCustomerAccountMutation, RegisterCustomerAccountMutationVariables>(RegisterCustomerAccountDocument, options);
      }
export type RegisterCustomerAccountMutationHookResult = ReturnType<typeof useRegisterCustomerAccountMutation>;
export type RegisterCustomerAccountMutationResult = Apollo.MutationResult<RegisterCustomerAccountMutation>;
export type RegisterCustomerAccountMutationOptions = Apollo.BaseMutationOptions<RegisterCustomerAccountMutation, RegisterCustomerAccountMutationVariables>;
export const CreateEvaluationDocument = gql`
    mutation createEvaluation($data: FundingOrderEvaluationInput!) {
  createEvaluation(data: $data) {
    id
  }
}
    `;

/**
 * __useCreateEvaluationMutation__
 *
 * To run a mutation, you first call `useCreateEvaluationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateEvaluationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createEvaluationMutation, { data, loading, error }] = useCreateEvaluationMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useCreateEvaluationMutation(baseOptions?: Apollo.MutationHookOptions<CreateEvaluationMutation, CreateEvaluationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateEvaluationMutation, CreateEvaluationMutationVariables>(CreateEvaluationDocument, options);
      }
export type CreateEvaluationMutationHookResult = ReturnType<typeof useCreateEvaluationMutation>;
export type CreateEvaluationMutationResult = Apollo.MutationResult<CreateEvaluationMutation>;
export type CreateEvaluationMutationOptions = Apollo.BaseMutationOptions<CreateEvaluationMutation, CreateEvaluationMutationVariables>;
export const GetEvaluationDocument = gql`
    query getEvaluation($where: FundingOrderEvaluationWhereInput, $orderBy: BaseOrderByInput) {
  evaluation(where: $where, orderBy: $orderBy) {
    id
    orderId
    rate
    comment
    createdAt
    productId
    updatedAt
    fundingOrder {
      id
      quantity
      discount
      fundingId
      fundingProducts {
        productId
        product {
          thumbnail {
            url
          }
          translations {
            name
          }
        }
      }
    }
    images {
      name
      url
    }
  }
}
    `;

/**
 * __useGetEvaluationQuery__
 *
 * To run a query within a React component, call `useGetEvaluationQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetEvaluationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetEvaluationQuery({
 *   variables: {
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *   },
 * });
 */
export function useGetEvaluationQuery(baseOptions?: Apollo.QueryHookOptions<GetEvaluationQuery, GetEvaluationQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetEvaluationQuery, GetEvaluationQueryVariables>(GetEvaluationDocument, options);
      }
export function useGetEvaluationLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetEvaluationQuery, GetEvaluationQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetEvaluationQuery, GetEvaluationQueryVariables>(GetEvaluationDocument, options);
        }
export type GetEvaluationQueryHookResult = ReturnType<typeof useGetEvaluationQuery>;
export type GetEvaluationLazyQueryHookResult = ReturnType<typeof useGetEvaluationLazyQuery>;
export type GetEvaluationQueryResult = Apollo.QueryResult<GetEvaluationQuery, GetEvaluationQueryVariables>;
export const GetListReviewDocument = gql`
    query getListReview($skip: Int = 0, $after: ID, $before: ID, $first: Int = 10, $last: Int, $where: FundingOrderEvaluationWhereInput, $orderBy: BaseOrderByInput) {
  evaluations(
    skip: $skip
    after: $after
    before: $before
    first: $first
    last: $last
    where: $where
    orderBy: $orderBy
  ) {
    id
    orderId
    rate
    comment
    createdAt
    productId
    updatedAt
    fundingOrder {
      userName
      userOrder {
        ...userInfoShortField
      }
      fundingId
      fundingProducts {
        productId
        product {
          translations {
            name
          }
        }
      }
    }
    images {
      url
      name
    }
  }
  evaluationConnection(where: $where) {
    aggregate {
      countEvaluation
    }
  }
}
    ${UserInfoShortFieldFragmentDoc}`;

/**
 * __useGetListReviewQuery__
 *
 * To run a query within a React component, call `useGetListReviewQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetListReviewQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetListReviewQuery({
 *   variables: {
 *      skip: // value for 'skip'
 *      after: // value for 'after'
 *      before: // value for 'before'
 *      first: // value for 'first'
 *      last: // value for 'last'
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *   },
 * });
 */
export function useGetListReviewQuery(baseOptions?: Apollo.QueryHookOptions<GetListReviewQuery, GetListReviewQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetListReviewQuery, GetListReviewQueryVariables>(GetListReviewDocument, options);
      }
export function useGetListReviewLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetListReviewQuery, GetListReviewQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetListReviewQuery, GetListReviewQueryVariables>(GetListReviewDocument, options);
        }
export type GetListReviewQueryHookResult = ReturnType<typeof useGetListReviewQuery>;
export type GetListReviewLazyQueryHookResult = ReturnType<typeof useGetListReviewLazyQuery>;
export type GetListReviewQueryResult = Apollo.QueryResult<GetListReviewQuery, GetListReviewQueryVariables>;
export const UpdateEvaluationDocument = gql`
    mutation updateEvaluation($where: FundingOrderEvaluationWhereInput!, $data: FundingOrderEvaluationUpdateInput!) {
  updateEvaluation(data: $data, where: $where) {
    id
  }
}
    `;

/**
 * __useUpdateEvaluationMutation__
 *
 * To run a mutation, you first call `useUpdateEvaluationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateEvaluationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateEvaluationMutation, { data, loading, error }] = useUpdateEvaluationMutation({
 *   variables: {
 *      where: // value for 'where'
 *      data: // value for 'data'
 *   },
 * });
 */
export function useUpdateEvaluationMutation(baseOptions?: Apollo.MutationHookOptions<UpdateEvaluationMutation, UpdateEvaluationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateEvaluationMutation, UpdateEvaluationMutationVariables>(UpdateEvaluationDocument, options);
      }
export type UpdateEvaluationMutationHookResult = ReturnType<typeof useUpdateEvaluationMutation>;
export type UpdateEvaluationMutationResult = Apollo.MutationResult<UpdateEvaluationMutation>;
export type UpdateEvaluationMutationOptions = Apollo.BaseMutationOptions<UpdateEvaluationMutation, UpdateEvaluationMutationVariables>;
export const UpdateShippingAddressDocument = gql`
    mutation updateShippingAddress($data: ShippingAddressUpdateInput!, $where: ShippingAddressWhereUniqueInput!) {
  updateShippingAddress(data: $data, where: $where) {
    ... on ShippingAddress {
      id
    }
    ... on CommonError {
      message
      status
      code
    }
  }
}
    `;

/**
 * __useUpdateShippingAddressMutation__
 *
 * To run a mutation, you first call `useUpdateShippingAddressMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateShippingAddressMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateShippingAddressMutation, { data, loading, error }] = useUpdateShippingAddressMutation({
 *   variables: {
 *      data: // value for 'data'
 *      where: // value for 'where'
 *   },
 * });
 */
export function useUpdateShippingAddressMutation(baseOptions?: Apollo.MutationHookOptions<UpdateShippingAddressMutation, UpdateShippingAddressMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateShippingAddressMutation, UpdateShippingAddressMutationVariables>(UpdateShippingAddressDocument, options);
      }
export type UpdateShippingAddressMutationHookResult = ReturnType<typeof useUpdateShippingAddressMutation>;
export type UpdateShippingAddressMutationResult = Apollo.MutationResult<UpdateShippingAddressMutation>;
export type UpdateShippingAddressMutationOptions = Apollo.BaseMutationOptions<UpdateShippingAddressMutation, UpdateShippingAddressMutationVariables>;