import gql from 'graphql-tag';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
  JSONObject: any;
};

export enum ApiMethod {
  Delete = 'DELETE',
  Get = 'GET',
  Patch = 'PATCH',
  Post = 'POST',
  Put = 'PUT'
}

export enum AccountType {
  Organization = 'ORGANIZATION',
  Person = 'PERSON'
}

export type ActivityLogCreateInput = {
  message?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
};

export type Address = {
  __typename?: 'Address';
  address: Scalars['String'];
  country: Scalars['String'];
  district: Scalars['String'];
  fullName: Scalars['String'];
  phoneNumber: Scalars['String'];
  province: Scalars['String'];
  ward: Scalars['String'];
};

export type AddressIdInput = {
  id: Scalars['Float'];
};

export type AdultRdi = {
  __typename?: 'AdultRdi';
  maxAge?: Maybe<Scalars['Int']>;
  minAge?: Maybe<Scalars['Int']>;
  /** Reference Daily Intake */
  rdi?: Maybe<Scalars['Float']>;
  /** Reference Daily Intake Unit */
  rdiUnit?: Maybe<Unit>;
  /** Tolerable Upper Intake Level */
  ul?: Maybe<Scalars['Float']>;
  /** Tolerable Upper Intake Level Unit */
  ulUnit?: Maybe<Unit>;
};

export type Attribute = {
  __typename?: 'Attribute';
  color?: Maybe<Scalars['String']>;
  ingredients?: Maybe<Scalars['String']>;
  store?: Maybe<Scalars['String']>;
  weight: Scalars['Float'];
};

export type AttributeInputType = {
  color?: InputMaybe<Scalars['String']>;
  ingredients?: InputMaybe<Scalars['String']>;
  store?: InputMaybe<Scalars['String']>;
  weight?: InputMaybe<Scalars['Float']>;
};

export type AttributeUpsertInput = {
  upsert?: InputMaybe<Array<AttributeInputType>>;
};

export type AuthError = Error & {
  __typename?: 'AuthError';
  code: AuthErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum AuthErrorCode {
  AccountAlreadyInUse = 'ACCOUNT_ALREADY_IN_USE',
  AccountIsBlocked = 'ACCOUNT_IS_BLOCKED',
  AppleAlreadyInUse = 'APPLE_ALREADY_IN_USE',
  AppleNotExists = 'APPLE_NOT_EXISTS',
  EmailAlreadyInUse = 'EMAIL_ALREADY_IN_USE',
  FacebookAlreadyInUse = 'FACEBOOK_ALREADY_IN_USE',
  FacebookNotExists = 'FACEBOOK_NOT_EXISTS',
  InvalidReferralCode = 'INVALID_REFERRAL_CODE',
  OtpNotValid = 'OTP_NOT_VALID',
  ReferralCodeInvalid = 'REFERRAL_CODE_INVALID',
  Unauthorized = 'UNAUTHORIZED'
}

export type AuthPayload = {
  __typename?: 'AuthPayload';
  token: Scalars['String'];
  user: User;
};

export type AuthPayloadOrError = AuthError | AuthPayload | CommonError;

export type BabyRdi = {
  __typename?: 'BabyRdi';
  maxMonth?: Maybe<Scalars['Int']>;
  minMonth?: Maybe<Scalars['Int']>;
  /** Reference Daily Intake */
  rdi?: Maybe<Scalars['Float']>;
  /** Reference Daily Intake Unit */
  rdiUnit?: Maybe<Unit>;
  /** Tolerable Upper Intake Level */
  ul?: Maybe<Scalars['Float']>;
  /** Tolerable Upper Intake Level Unit */
  ulUnit?: Maybe<Unit>;
};

export type BankCard = {
  __typename?: 'BankCard';
  bankId: Scalars['String'];
  bankInfo?: Maybe<VnBank>;
  cardName?: Maybe<Scalars['String']>;
  cardNumber: Scalars['String'];
  id?: Maybe<Scalars['Int']>;
  shopId?: Maybe<Scalars['Int']>;
};

export type BankShopCreateInput = {
  bankId: Scalars['String'];
  cardName: Scalars['String'];
  cardNumber: Scalars['String'];
};

export type BankWhereInput = {
  bankName?: InputMaybe<Scalars['String']>;
  brandName?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['Int']>;
};

export type Banner = {
  __typename?: 'Banner';
  liveStreamId: Scalars['Int'];
  thumbnailUrl: Scalars['String'];
};

export enum BannerCategoriesType {
  BannerOnBar = 'BANNER_ON_BAR',
  BannerOnCommunity = 'BANNER_ON_COMMUNITY',
  BannerOnEvent = 'BANNER_ON_EVENT',
  BannerOnExplore = 'BANNER_ON_EXPLORE',
  BannerOnHomePage = 'BANNER_ON_HOME_PAGE',
  BannerOnProduct = 'BANNER_ON_PRODUCT',
  BannerOnShopProduct = 'BANNER_ON_SHOP_PRODUCT'
}

export type BannerImage = {
  __typename?: 'BannerImage';
  bannerType: BannerCategoriesType;
  content?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  createdUid?: Maybe<Scalars['String']>;
  height?: Maybe<Scalars['Float']>;
  id: Scalars['Int'];
  imageUrl?: Maybe<Scalars['String']>;
  redirectLink?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['Boolean']>;
  title?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  width?: Maybe<Scalars['Float']>;
};

export type BannerImageConnection = {
  __typename?: 'BannerImageConnection';
  aggregate: BannerImageConnectionAggregate;
};

export type BannerImageConnectionAggregate = {
  __typename?: 'BannerImageConnectionAggregate';
  count: Scalars['Int'];
};

export type BannerImageInput = {
  bannerType: Scalars['String'];
  content?: InputMaybe<Scalars['String']>;
  height?: InputMaybe<Scalars['Float']>;
  imageUrl: Scalars['String'];
  redirectLink?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  width?: InputMaybe<Scalars['Float']>;
};

export type BannerImageOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type BannerImageTagging = {
  __typename?: 'BannerImageTagging';
  bannerImage?: Maybe<BannerImage>;
  id: Scalars['Int'];
};

export type BannerImageUpdateInput = {
  bannerType: Scalars['String'];
  content?: InputMaybe<Scalars['String']>;
  imageUrl: Scalars['String'];
  redirectLink?: InputMaybe<Scalars['String']>;
  status: Scalars['Boolean'];
  title?: InputMaybe<Scalars['String']>;
};

export type BannerImageWhereInput = {
  banner_type?: InputMaybe<Scalars['String']>;
  created_uid?: InputMaybe<Scalars['String']>;
  fromDate?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<Scalars['Boolean']>;
  toDate?: InputMaybe<Scalars['DateTime']>;
};

export type Barcode = {
  __typename?: 'Barcode';
  id: Scalars['Int'];
  value: Scalars['String'];
};

export type BarcodeOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type BarcodeWhereInput = {
  product?: InputMaybe<ProductWhereInput>;
};

export type BarcodesAggregate = {
  __typename?: 'BarcodesAggregate';
  count: Scalars['Int'];
};

export type BarcodesConnection = {
  __typename?: 'BarcodesConnection';
  aggregate: BarcodesAggregate;
};

export type BaseOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type BaumannAnswer = {
  __typename?: 'BaumannAnswer';
  questionId: Scalars['ID'];
  value: BaumannOptionValue;
};

export type BaumannAnswerCreateInput = {
  question: BaumannQuestionCreateNestedOneWithoutAnswersInput;
  value: BaumannAnswerValueInput;
};

export type BaumannAnswerValueInput = {
  booleanValue?: InputMaybe<Scalars['Boolean']>;
  stringValue?: InputMaybe<Scalars['String']>;
};

export type BaumannOption = {
  __typename?: 'BaumannOption';
  content: Scalars['String'];
  value: BaumannOptionValue;
};

export type BaumannOptionValue = BooleanBox | StringBox;

export type BaumannQuestion = {
  __typename?: 'BaumannQuestion';
  content: Scalars['String'];
  id: Scalars['ID'];
  options: Array<BaumannOption>;
};

export type BaumannQuestionCreateNestedOneWithoutAnswersInput = {
  connect: BaumannQuestionWhereUniqueInput;
};

export type BaumannQuestionWhereUniqueInput = {
  id: Scalars['ID'];
};

export enum BaumannSkinType {
  Drnt = 'DRNT',
  Drnw = 'DRNW',
  Drpt = 'DRPT',
  Drpw = 'DRPW',
  Dsnt = 'DSNT',
  Dsnw = 'DSNW',
  Dspt = 'DSPT',
  Dspw = 'DSPW',
  Ornt = 'ORNT',
  Ornw = 'ORNW',
  Orpt = 'ORPT',
  Orpw = 'ORPW',
  Osnt = 'OSNT',
  Osnw = 'OSNW',
  Ospt = 'OSPT',
  Ospw = 'OSPW'
}

export type BaumannSkinTypeFilter = {
  equals?: InputMaybe<BaumannSkinType>;
  in?: InputMaybe<Array<BaumannSkinType>>;
};

export type BooleanBox = {
  __typename?: 'BooleanBox';
  value: Scalars['Boolean'];
};

export type BooleanFilter = {
  equals?: InputMaybe<Scalars['Boolean']>;
  not?: InputMaybe<Scalars['Boolean']>;
};

export type Brand = {
  __typename?: 'Brand';
  brandAdmins?: Maybe<Array<BrandAdmin>>;
  /** Country of origin */
  coo?: Maybe<Scalars['String']>;
  fixedLogoUrl?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isFavouriteBrandOfViewer?: Maybe<Scalars['Boolean']>;
  isFollowed?: Maybe<Scalars['Boolean']>;
  logoUrl: Scalars['String'];
  /** ID of FacetValue in Vendure system */
  mallId?: Maybe<Scalars['ID']>;
  nbFollowing: Scalars['Int'];
  status: BrandStatus;
  translations: Array<BrandTranslation>;
  types: Array<BrandType>;
  /** @deprecated should be removed in the future, just use id instead */
  uid: Scalars['ID'];
};


export type BrandBrandAdminsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandAdminWhereInput>;
};


export type BrandFixedLogoUrlArgs = {
  width: FixedSize;
};


export type BrandTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandTranslationWhereInput>;
};

export type BrandAdmin = {
  __typename?: 'BrandAdmin';
  bannerUrl?: Maybe<Scalars['String']>;
  brandId?: Maybe<Scalars['ID']>;
  coverUrl?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  facebookUrl?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  instagramUrl?: Maybe<Scalars['String']>;
  isFollowedByViewer?: Maybe<Scalars['Boolean']>;
  logoUrl?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  owner: User;
  ownerId: Scalars['ID'];
  promotionalProducts?: Maybe<Array<BrandPromotionalProduct>>;
  slogan?: Maybe<Scalars['String']>;
  status: BrandAdminStatus;
  storyUrl?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
  webSite?: Maybe<Scalars['String']>;
};

export enum BrandAdminStatus {
  Approved = 'APPROVED',
  Pending = 'PENDING',
  Rejected = 'REJECTED'
}

export type BrandAdminUpdateInput = {
  bannerUrl?: InputMaybe<Scalars['String']>;
  coverUrl?: InputMaybe<Scalars['String']>;
  facbookUrl?: InputMaybe<Scalars['String']>;
  instagramUrl?: InputMaybe<Scalars['String']>;
  logoUrl?: InputMaybe<Scalars['String']>;
  slogan?: InputMaybe<Scalars['String']>;
  storyUrl?: InputMaybe<Scalars['String']>;
  webSite?: InputMaybe<Scalars['String']>;
};

export type BrandAdminWhereInput = {
  brandId?: InputMaybe<Scalars['ID']>;
  id?: InputMaybe<Scalars['ID']>;
  ownerId?: InputMaybe<Scalars['ID']>;
  status?: InputMaybe<BrandAdminStatus>;
};

export type BrandAdminWhereUniqueInput = {
  id: Scalars['ID'];
};

export type BrandOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type BrandPromotionalProducWhereInput = {
  productId: Scalars['Int'];
};

export type BrandPromotionalProduct = {
  __typename?: 'BrandPromotionalProduct';
  brandAdminId: Scalars['ID'];
  product?: Maybe<Product>;
  productId: Scalars['ID'];
  productName?: Maybe<Scalars['String']>;
};

export type BrandPromotionalProductInput = {
  productId?: InputMaybe<Scalars['ID']>;
  productName?: InputMaybe<Scalars['String']>;
};

export type BrandSearchResult = {
  __typename?: 'BrandSearchResult';
  brands: Array<Brand>;
  total: Scalars['Int'];
};

export enum BrandStatus {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export type BrandStore = {
  __typename?: 'BrandStore';
  address: Scalars['String'];
  brandCode: Scalars['String'];
  code: Scalars['String'];
  id: Scalars['Int'];
  mapCoord: Scalars['String'];
  name: Scalars['String'];
  telephone: Scalars['String'];
};

export type BrandTranslation = {
  __typename?: 'BrandTranslation';
  brand: Brand;
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isOriginal: Scalars['Boolean'];
  language: LanguageCode;
  name: Scalars['String'];
  slug?: Maybe<Scalars['String']>;
};

export type BrandTranslationOrderByInput = {
  id?: InputMaybe<OrderBy>;
  name?: InputMaybe<OrderBy>;
};

export type BrandTranslationWhereInput = {
  brand?: InputMaybe<BrandWhereInput>;
  language?: InputMaybe<LanguageCode>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type BrandType = {
  __typename?: 'BrandType';
  id: Scalars['ID'];
  value: BrandTypeValue;
};

export enum BrandTypeValue {
  DepartmentStore = 'DEPARTMENT_STORE',
  DrugStore = 'DRUG_STORE',
  OnlineOther = 'ONLINE_OTHER',
  RoadStore = 'ROAD_STORE'
}

export type BrandTypeWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
  value?: InputMaybe<BrandTypeValue>;
};

export type BrandWhereInput = {
  brandIdFilters?: InputMaybe<Array<Scalars['Int']>>;
  coo?: InputMaybe<Scalars['String']>;
  followedByUser?: InputMaybe<UserWhereUniqueInput>;
  followersSome?: InputMaybe<UserWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  mallId?: InputMaybe<StringFilter>;
  productsSome?: InputMaybe<ProductWhereInput>;
  status?: InputMaybe<BrandStatus>;
  text?: InputMaybe<Scalars['String']>;
  translationsSome?: InputMaybe<BrandTranslationWhereInput>;
  type?: InputMaybe<BrandTypeWhereUniqueInput>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type BrandWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  mallId?: InputMaybe<Scalars['ID']>;
};

export type BrandsAggregate = {
  __typename?: 'BrandsAggregate';
  count: Scalars['Int'];
  following: Scalars['Int'];
};

export type BrandsConnection = {
  __typename?: 'BrandsConnection';
  aggregate: BrandsAggregate;
};

export type BrandsFollowingConnection = {
  __typename?: 'BrandsFollowingConnection';
  aggregate: BrandsFollowingsAggregate;
};

export type BrandsFollowingsAggregate = {
  __typename?: 'BrandsFollowingsAggregate';
  count: Scalars['Int'];
};

export type CartItem = {
  __typename?: 'CartItem';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  liveStreamProduct: LiveStreamProduct;
  note?: Maybe<Scalars['String']>;
  order?: Maybe<Order>;
  quantity: Scalars['Int'];
  updatedAt: Scalars['DateTime'];
};

export type CartItemCreateInput = {
  liveStreamProduct: LiveStreamProductCreateOneWithoutCartItemsInput;
  note?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<OrderCreateOneWithoutCartItemInput>;
  quantity: Scalars['Int'];
};

export type CartItemCreateManyWithoutOrderInput = {
  connect: Array<CartItemWhereUniqueInput>;
};

export type CartItemOrError = CartItem | CommonError | OrderError;

export type CartItemOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type CartItemUpdateInput = {
  note?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<OrderUpdateOneWithoutCartItemsInput>;
  quantity?: InputMaybe<Scalars['Int']>;
};

export type CartItemUpdateManyWithoutOrderInput = {
  set: Array<CartItemWhereUniqueInput>;
};

export type CartItemWhereInput = {
  liveStreamProduct?: InputMaybe<LiveStreamProductWhereInput>;
  order?: InputMaybe<OrderWhereInput>;
  user?: InputMaybe<UserWhereInput>;
};

export type CartItemWhereUniqueInput = {
  id: Scalars['Int'];
};

export type CartItemsAggregate = {
  __typename?: 'CartItemsAggregate';
  count: Scalars['Int'];
  sum: CartItemsSum;
};

export type CartItemsConnection = {
  __typename?: 'CartItemsConnection';
  aggregate: CartItemsAggregate;
};

export type CartItemsSum = {
  __typename?: 'CartItemsSum';
  price: Scalars['Int'];
};

export type CartProduct = {
  __typename?: 'CartProduct';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  quantity: Scalars['Int'];
  shop: Shop;
  shopId: Scalars['Int'];
  updatedAt?: Maybe<Scalars['DateTime']>;
  user: User;
  userId: Scalars['Int'];
  variant: ProductVariant;
  variantId: Scalars['Int'];
};

export type CartProductCreateError = Error & {
  __typename?: 'CartProductCreateError';
  code: Scalars['String'];
  message: Scalars['String'];
  status: ErrorStatus;
};

export type CartProductInsertInput = {
  quantity: Scalars['Int'];
  shopId: Scalars['Int'];
  variantId: Scalars['Int'];
};

export type CartProductOrError = CartProduct | CartProductCreateError | CommonError;

export type CartProductOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type CartProductUpdateInput = {
  quantity?: InputMaybe<Scalars['Int']>;
};

export type CartProductWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  ids?: InputMaybe<Array<Scalars['Int']>>;
  userId?: InputMaybe<Scalars['Int']>;
  variantId?: InputMaybe<Scalars['Int']>;
};

export type CartProductsOfShop = {
  __typename?: 'CartProductsOfShop';
  cartProducts: Array<CartProduct>;
  shop: Shop;
  shopId: Scalars['Int'];
};

export type CartProductsOfShopAggregate = {
  __typename?: 'CartProductsOfShopAggregate';
  countProductsOfCart: Scalars['Int'];
};

export type CartProductsOfShopConnection = {
  __typename?: 'CartProductsOfShopConnection';
  aggregate: CartProductsOfShopAggregate;
};

export type Cast = {
  __typename?: 'Cast';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  description?: Maybe<Scalars['String']>;
  /** @deprecated use uid instead */
  id: Scalars['Int'];
  name: Scalars['String'];
  products: Array<Product>;
  savedByViewer: Scalars['Boolean'];
  tags: Array<Tag>;
  thumbnailUrl: Scalars['String'];
  uid: Scalars['ID'];
  updatedAt: Scalars['DateTime'];
};


export type CastProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};

export type CastAggregate = {
  __typename?: 'CastAggregate';
  count: Scalars['Int'];
};

export type CastComment = {
  __typename?: 'CastComment';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type CastCommentCreateInput = {
  cast: CastCreateOneWithoutCommentsInput;
  content: Scalars['String'];
};

export type CastCommentOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type CastCommentWhereInput = {
  cast: CastWhereUniqueInput;
};

export type CastCommentsAggregate = {
  __typename?: 'CastCommentsAggregate';
  count: Scalars['Int'];
};

export type CastCommentsConnection = {
  __typename?: 'CastCommentsConnection';
  aggregate: CastCommentsAggregate;
};

export type CastConnection = {
  __typename?: 'CastConnection';
  aggregate: CastAggregate;
};

export type CastCreateOneWithoutCommentsInput = {
  connect: CastWhereUniqueInput;
};

export type CastOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type CastWhereInput = {
  nameContains?: InputMaybe<Scalars['String']>;
};

export type CastWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type CategoriesAggregate = {
  __typename?: 'CategoriesAggregate';
  count: Scalars['Int'];
};

export type CategoriesConnection = {
  __typename?: 'CategoriesConnection';
  aggregate: CategoriesAggregate;
};

export type Category = {
  __typename?: 'Category';
  activeLogo?: Maybe<Image>;
  children?: Maybe<Array<Category>>;
  id: Scalars['Int'];
  inactiveLogo?: Maybe<Image>;
  parent?: Maybe<Category>;
  productRankings?: Maybe<Array<ProductRanking>>;
  productRankingsConnection: ProductRankingsConnection;
  productRankingsV2?: Maybe<Array<ProductRanking>>;
  reviewQuestionSetId?: Maybe<Scalars['ID']>;
  status: CategoryStatus;
  translations: Array<CategoryTranslation>;
  uid: Scalars['ID'];
};


export type CategoryChildrenArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  idFilters?: InputMaybe<Array<Scalars['Int']>>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CategoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CategoryWhereInput>;
};


export type CategoryProductRankingsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type CategoryProductRankingsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type CategoryProductRankingsV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type CategoryTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CategoryTranslationWhereInput>;
};

export type CategoryOrderByInput = {
  id?: InputMaybe<OrderBy>;
  mixedRows?: InputMaybe<Scalars['Boolean']>;
};

export type CategorySearchResult = {
  __typename?: 'CategorySearchResult';
  categories: Array<Category>;
  total: Scalars['Int'];
};

export enum CategoryStatus {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export type CategoryTranslation = {
  __typename?: 'CategoryTranslation';
  id: Scalars['Int'];
  isOriginal: Scalars['Boolean'];
  language: LanguageCode;
  name: Scalars['String'];
  slug?: Maybe<Scalars['String']>;
};

export type CategoryTranslationWhereInput = {
  language?: InputMaybe<LanguageCode>;
  name?: InputMaybe<StringFilter>;
  slug?: InputMaybe<Scalars['String']>;
};

export type CategoryWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  idNot?: InputMaybe<Scalars['Int']>;
  isLeaf?: InputMaybe<Scalars['Boolean']>;
  parent?: InputMaybe<CategoryWhereUniqueInput>;
  productType?: InputMaybe<ProductType>;
  status?: InputMaybe<CategoryStatus>;
  translationsSome?: InputMaybe<CategoryTranslationWhereInput>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type CategoryWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  idFilters?: InputMaybe<Array<Scalars['Int']>>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type CautionWhereInput = {
  id?: InputMaybe<IntFilter>;
  productType?: InputMaybe<ProductType>;
  productsSome?: InputMaybe<ProductWhereInput>;
  translationsSome?: InputMaybe<IngredientCautionTranslationWhereInput>;
};

export type CautionWhereUniqueInput = {
  id: Scalars['Int'];
};

export type Channel = {
  __typename?: 'Channel';
  id: Scalars['ID'];
};

export enum ChannelCommand {
  Start = 'START',
  Status = 'STATUS',
  Stop = 'STOP'
}

export type ChannelError = Error & {
  __typename?: 'ChannelError';
  command: ChannelCommand;
  id?: Maybe<Scalars['String']>;
  message: Scalars['String'];
  status: ErrorStatus;
};

export type ChannelOrError = Channel | ChannelError | CommonError;

export type ChatNotificationInput = {
  message: Scalars['String'];
  receiver: UserWhereUniqueInput;
};

export type CheckInPayload = {
  __typename?: 'CheckInPayload';
  checkInList: Array<Scalars['Int']>;
  isCheckedInToday: Scalars['Boolean'];
  todayIndex: Scalars['Int'];
};

export type CommonError = Error & {
  __typename?: 'CommonError';
  code: CommonErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

/** Common error codes */
export enum CommonErrorCode {
  Forbidden = 'FORBIDDEN',
  InternalServerError = 'INTERNAL_SERVER_ERROR',
  NotFound = 'NOT_FOUND'
}

export type ConfirmingOrderError = {
  __typename?: 'ConfirmingOrderError';
  id: Scalars['Int'];
  message: Scalars['String'];
  success: Scalars['Boolean'];
};

export enum Currency {
  Vnd = 'VND'
}

export type DateTimeFilter = {
  equals?: InputMaybe<Scalars['DateTime']>;
  gt?: InputMaybe<Scalars['DateTime']>;
  gte?: InputMaybe<Scalars['DateTime']>;
  in?: InputMaybe<Array<Scalars['DateTime']>>;
  lt?: InputMaybe<Scalars['DateTime']>;
  lte?: InputMaybe<Scalars['DateTime']>;
  not?: InputMaybe<Scalars['DateTime']>;
  notIn?: InputMaybe<Array<Scalars['DateTime']>>;
};

export enum DeliverOption {
  None = 'NONE',
  Xteam = 'XTEAM'
}

export type DeliveryHistory = {
  __typename?: 'DeliveryHistory';
  createdAt: Scalars['DateTime'];
  detail?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  orderId: Scalars['Int'];
  status: Scalars['Int'];
  statusText: Scalars['String'];
};

export type DeliveryHistoryOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type DeliveryHistoryWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  order_id?: InputMaybe<Scalars['Int']>;
};

export type Device = {
  __typename?: 'Device';
  deviceType: DeviceType;
  id: Scalars['Int'];
  token: Scalars['String'];
};

export type DeviceCreateInput = {
  deviceType: DeviceType;
  token: Scalars['String'];
};

export enum DeviceType {
  Android = 'ANDROID',
  Ios = 'IOS'
}

export type Districts = {
  __typename?: 'Districts';
  code: Scalars['String'];
  id: Scalars['Int'];
  name: Scalars['String'];
  province_id: Scalars['Int'];
};

export type DistrictsWhereInput = {
  provinceId: Scalars['Int'];
};

export type EcommOrderCreateError = Error & {
  __typename?: 'EcommOrderCreateError';
  code: EcommOrderCreateErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum EcommOrderCreateErrorCode {
  AddressOfShopOrShopNotExist = 'ADDRESS_OF_SHOP_OR_SHOP_NOT_EXIST',
  DeliveryAddressError = 'DELIVERY_ADDRESS_ERROR',
  InvalidPromotion = 'INVALID_PROMOTION',
  NotEnoughProduct = 'NOT_ENOUGH_PRODUCT',
  TotalAmountIsLess_10000OrMore_20000000 = 'TOTAL_AMOUNT_IS_LESS_10000_OR_MORE_20000000'
}

export type EcommerceFlashSaleInput = {
  amount?: InputMaybe<Scalars['Float']>;
  amountUnit?: InputMaybe<PromotionAmountUnitType>;
  content?: InputMaybe<Scalars['String']>;
  fromTime?: InputMaybe<Scalars['DateTime']>;
  images?: InputMaybe<Array<Scalars['String']>>;
  maxUsingTimes?: InputMaybe<Scalars['Float']>;
  maximumPrice?: InputMaybe<Scalars['Float']>;
  minimumPrice?: InputMaybe<Scalars['Float']>;
  productId?: InputMaybe<Scalars['Float']>;
  quantity?: InputMaybe<Scalars['Float']>;
  redirectUrl?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  toTime?: InputMaybe<Scalars['DateTime']>;
};

export type EcommerceFlashSaleInsertInput = {
  flashsaleList: Array<EcommerceFlashSaleInput>;
};

export type EcommerceOrder = {
  __typename?: 'EcommerceOrder';
  allowPayingForShop?: Maybe<Scalars['Boolean']>;
  buyer: User;
  buyerAddress: Scalars['String'];
  buyerId: Scalars['Int'];
  buyerName: Scalars['String'];
  buyerPhone: Scalars['String'];
  completedDate?: Maybe<Scalars['DateTime']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  deliveryHistory?: Maybe<Array<DeliveryHistory>>;
  ecommFee?: Maybe<Scalars['Float']>;
  estimatedDeliverTime?: Maybe<Scalars['String']>;
  estimatedPickTime?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isPaid?: Maybe<Scalars['Boolean']>;
  orderCode: Scalars['String'];
  paymentMethod?: Maybe<Scalars['String']>;
  productVariantsByOrderId?: Maybe<Array<EcommerceOrderProduct>>;
  promotionDetails?: Maybe<Array<EcommercePromotionHistory>>;
  reviewtyDiscount?: Maybe<Scalars['Float']>;
  sellerAddress?: Maybe<Scalars['String']>;
  shippingFee?: Maybe<Scalars['Float']>;
  shop: Shop;
  shopDiscount?: Maybe<Scalars['Float']>;
  shopId: Scalars['Int'];
  status?: Maybe<EcommerceOrderStatus>;
  statusText?: Maybe<Scalars['String']>;
  totalAmount?: Maybe<Scalars['Float']>;
  transportationCode?: Maybe<Scalars['String']>;
  vnpayUrl?: Maybe<Scalars['String']>;
};


export type EcommerceOrderDeliveryHistoryArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<DeliveryHistoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<DeliveryHistoryWhereInput>;
};


export type EcommerceOrderProductVariantsByOrderIdArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderProductWhereInput>;
};

export type EcommerceOrderAggregate = {
  __typename?: 'EcommerceOrderAggregate';
  countEcommerceOrder: Scalars['Int'];
};

export type EcommerceOrderCancelInput = {
  id: Scalars['Int'];
  reason?: InputMaybe<Scalars['String']>;
};

export type EcommerceOrderConfirmInput = {
  ids: Array<Scalars['Int']>;
};

export type EcommerceOrderConnection = {
  __typename?: 'EcommerceOrderConnection';
  aggregate: EcommerceOrderAggregate;
};

export type EcommerceOrderInput = {
  bankCode?: InputMaybe<Scalars['String']>;
  buyerAddress: Scalars['String'];
  buyerName: Scalars['String'];
  buyerPhone: Scalars['String'];
  freeshipPromotions?: InputMaybe<Array<Scalars['Int']>>;
  paymentMethod: PaymentMethod;
  reviewtyPromotions?: InputMaybe<Array<Scalars['Int']>>;
  shopOrders: Array<ShopOrderInput>;
};

export type EcommerceOrderOrderByInput = {
  id?: InputMaybe<OrderBy>;
  shopId?: InputMaybe<OrderBy>;
};

export type EcommerceOrderProduct = {
  __typename?: 'EcommerceOrderProduct';
  id: Scalars['Int'];
  orderId: Scalars['Int'];
  price: Scalars['Float'];
  quantity: Scalars['Int'];
  review?: Maybe<EcommerceReview>;
  totalPrice: Scalars['Float'];
  variant?: Maybe<ProductVariant>;
  variantId: Scalars['Int'];
};


export type EcommerceOrderProductReviewArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceReviewWhereInput>;
};

export type EcommerceOrderProductInput = {
  price: Scalars['Float'];
  quantity: Scalars['Int'];
  variantId: Scalars['Int'];
};

export type EcommerceOrderProductOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type EcommerceOrderProductWhereInput = {
  shopId?: InputMaybe<Scalars['Int']>;
  variantId?: InputMaybe<Scalars['Int']>;
};

export enum EcommerceOrderStatus {
  Cancelled = 'CANCELLED',
  Completed = 'COMPLETED',
  Delivering = 'DELIVERING',
  InProcessing = 'IN_PROCESSING',
  PaymentError = 'PAYMENT_ERROR',
  Refunded = 'REFUNDED',
  Returns = 'RETURNS',
  SystemError = 'SYSTEM_ERROR',
  WaitConfirming = 'WAIT_CONFIRMING',
  WaitDelivering = 'WAIT_DELIVERING',
  WaitRefunding = 'WAIT_REFUNDING'
}

export enum EcommerceOrderStatusFilter {
  Cancelled = 'CANCELLED',
  Completed = 'COMPLETED',
  Delivering = 'DELIVERING',
  ReturnsOrRefund = 'RETURNS_OR_REFUND',
  WaitConfirming = 'WAIT_CONFIRMING',
  WaitDelivering = 'WAIT_DELIVERING'
}

export type EcommerceOrderWhereInput = {
  allowPayingForShop?: InputMaybe<Scalars['Boolean']>;
  buyerId?: InputMaybe<Scalars['Int']>;
  buyerName?: InputMaybe<Scalars['String']>;
  fromDate?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['Int']>;
  isFinance?: InputMaybe<Scalars['Boolean']>;
  orderCode?: InputMaybe<Scalars['String']>;
  paymentMethod?: InputMaybe<PaymentMethod>;
  periodType?: InputMaybe<PeriodType>;
  productName?: InputMaybe<Scalars['String']>;
  shopId?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<EcommerceOrderStatusFilter>;
  textSearch?: InputMaybe<Scalars['String']>;
  toDate?: InputMaybe<Scalars['DateTime']>;
  userId?: InputMaybe<Scalars['Int']>;
  variantId?: InputMaybe<Scalars['Int']>;
};

export type EcommerceProductIdsWhereInput = {
  ids?: InputMaybe<Array<Scalars['Int']>>;
};

export type EcommercePromotion = {
  __typename?: 'EcommercePromotion';
  amount?: Maybe<Scalars['Float']>;
  amountUnit?: Maybe<PromotionAmountUnitType>;
  code?: Maybe<Scalars['String']>;
  content?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  createdBy?: Maybe<PromotionCreatedByType>;
  entity?: Maybe<Scalars['Int']>;
  entityObject?: Maybe<EcommercePromotionEntityUnion>;
  entityType?: Maybe<PromotionEntityType>;
  fromTime?: Maybe<Scalars['DateTime']>;
  id: Scalars['Int'];
  images?: Maybe<Array<Scalars['String']>>;
  inStock?: Maybe<Scalars['Float']>;
  maxUsingTimes?: Maybe<Scalars['Float']>;
  maximumPrice?: Maybe<Scalars['Float']>;
  minimumPrice?: Maybe<Scalars['Float']>;
  quantity?: Maybe<Scalars['Float']>;
  redirectUrl?: Maybe<Scalars['String']>;
  shopId?: Maybe<Scalars['Int']>;
  targetObjects?: Maybe<Array<EcommercePromotionEntityUnion>>;
  title?: Maybe<Scalars['String']>;
  toTime?: Maybe<Scalars['DateTime']>;
  type: PromotionType;
  usedTimes?: Maybe<Scalars['Float']>;
  userId?: Maybe<Scalars['Int']>;
};

export type EcommercePromotionEntityUnion = Shop | ShopProduct;

export type EcommercePromotionFilterWhereInput = {
  code?: InputMaybe<Scalars['String']>;
  createdBy?: InputMaybe<PromotionCreatedByType>;
  fromDate?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['Int']>;
  isExpired?: InputMaybe<Scalars['Boolean']>;
  title?: InputMaybe<Scalars['String']>;
  toDate?: InputMaybe<Scalars['DateTime']>;
};

export type EcommercePromotionGroup = {
  __typename?: 'EcommercePromotionGroup';
  code?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  fromTime?: Maybe<Scalars['DateTime']>;
  isVisible?: Maybe<Scalars['Boolean']>;
  promotions?: Maybe<Array<EcommercePromotion>>;
  toTime?: Maybe<Scalars['DateTime']>;
  type: PromotionType;
};

export type EcommercePromotionHistory = {
  __typename?: 'EcommercePromotionHistory';
  discount: Scalars['Int'];
  ecommercePromotion?: Maybe<EcommercePromotion>;
  promotion_id: Scalars['Int'];
};

export type EcommercePromotionInsertInput = {
  amount?: InputMaybe<Scalars['Float']>;
  amountUnit?: InputMaybe<PromotionAmountUnitType>;
  code?: InputMaybe<Scalars['String']>;
  content?: InputMaybe<Scalars['String']>;
  entity?: InputMaybe<Scalars['Int']>;
  entityType: PromotionEntityType;
  fromTime?: InputMaybe<Scalars['DateTime']>;
  images?: InputMaybe<Array<Scalars['String']>>;
  maxUsingTimes?: InputMaybe<Scalars['Float']>;
  maximumPrice?: InputMaybe<Scalars['Float']>;
  minimumPrice?: InputMaybe<Scalars['Float']>;
  quantity?: InputMaybe<Scalars['Float']>;
  redirectUrl?: InputMaybe<Scalars['String']>;
  shopId?: InputMaybe<Scalars['Int']>;
  title?: InputMaybe<Scalars['String']>;
  toTime?: InputMaybe<Scalars['DateTime']>;
  type: PromotionType;
  userId?: InputMaybe<Scalars['Int']>;
};

export type EcommercePromotionUpdateInput = {
  amount?: InputMaybe<Scalars['Float']>;
  amountUnit?: InputMaybe<PromotionAmountUnitType>;
  code?: InputMaybe<Scalars['String']>;
  content?: InputMaybe<Scalars['String']>;
  entity?: InputMaybe<Scalars['Int']>;
  entityType: PromotionEntityType;
  fromTime?: InputMaybe<Scalars['DateTime']>;
  id: Scalars['Int'];
  images?: InputMaybe<Array<Scalars['String']>>;
  maxUsingTimes?: InputMaybe<Scalars['Float']>;
  maximumPrice?: InputMaybe<Scalars['Float']>;
  minimumPrice?: InputMaybe<Scalars['Float']>;
  quantity?: InputMaybe<Scalars['Float']>;
  redirectUrl?: InputMaybe<Scalars['String']>;
  shopId?: InputMaybe<Scalars['Int']>;
  title?: InputMaybe<Scalars['String']>;
  toTime?: InputMaybe<Scalars['DateTime']>;
  type: PromotionType;
  userId?: InputMaybe<Scalars['Int']>;
};

export type EcommercePromotionWhereInput = {
  code?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
};

export type EcommerceReview = {
  __typename?: 'EcommerceReview';
  comment?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  id: Scalars['Int'];
  images?: Maybe<Array<Image>>;
  orderId?: Maybe<Scalars['Int']>;
  orderInfo?: Maybe<EcommerceOrder>;
  productId?: Maybe<Scalars['Int']>;
  rate?: Maybe<Scalars['Int']>;
  reviewer: User;
  updatedAt?: Maybe<Scalars['DateTime']>;
  userId?: Maybe<Scalars['Int']>;
  variant?: Maybe<ProductVariant>;
  variantId?: Maybe<Scalars['Int']>;
};

export type EcommerceReviewAggregate = {
  __typename?: 'EcommerceReviewAggregate';
  countEcommerceReview: Scalars['Int'];
};

export type EcommerceReviewConnection = {
  __typename?: 'EcommerceReviewConnection';
  aggregate: EcommerceReviewAggregate;
};

export type EcommerceReviewInsertInput = {
  orderId: Scalars['Int'];
  variantReviewInfo: Array<VariantReviewInsertInput>;
};

export type EcommerceReviewUpdateInput = {
  comment?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<Array<ImageReviewInput>>;
  rate?: InputMaybe<Scalars['Int']>;
};

export type EcommerceReviewWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  orderId?: InputMaybe<Scalars['Int']>;
  productId?: InputMaybe<Scalars['Int']>;
  rate?: InputMaybe<IntFilter>;
  variantId?: InputMaybe<Scalars['Int']>;
};

export type EcommerceShippingFee = {
  __typename?: 'EcommerceShippingFee';
  fee: Scalars['Float'];
  shopId: Scalars['Int'];
};

export enum EcommerceSite {
  Lazada = 'LAZADA',
  Shopee = 'SHOPEE',
  Tiki = 'TIKI'
}

export type EmailError = Error & {
  __typename?: 'EmailError';
  code: EmailErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum EmailErrorCode {
  EmailAddressAlreadyBeingUsed = 'EMAIL_ADDRESS_ALREADY_BEING_USED'
}

export type EmailOrError = CommonError | EmailError | EmailPayload;

export type EmailPayload = {
  __typename?: 'EmailPayload';
  email: Scalars['String'];
};

export type Error = {
  message: Scalars['String'];
  status: ErrorStatus;
};

/** Common error status */
export enum ErrorStatus {
  BadRequest = 'BAD_REQUEST',
  Forbidden = 'FORBIDDEN',
  InternalServerError = 'INTERNAL_SERVER_ERROR',
  NotFound = 'NOT_FOUND',
  Unauthorized = 'UNAUTHORIZED',
  Unprocessable = 'UNPROCESSABLE'
}

export type EvaluationAggregate = {
  __typename?: 'EvaluationAggregate';
  countEvaluation: Scalars['Int'];
};

export type EvaluationConnection = {
  __typename?: 'EvaluationConnection';
  aggregate: EvaluationAggregate;
};

export type Event = {
  __typename?: 'Event';
  commentsConnection: EventCommentConnection;
  condition: Scalars['String'];
  content: Scalars['String'];
  coverUrl: Scalars['String'];
  coverUrlFunding?: Maybe<Scalars['String']>;
  currentNumberOfWinners: Scalars['Int'];
  endedAt: Scalars['DateTime'];
  eventToProducts: Array<EventToProduct>;
  id: Scalars['Int'];
  isParticipantOfCurrentUser?: Maybe<Scalars['Boolean']>;
  /** @deprecated use maximumNumberOfWinners instead */
  maximumNumberOfParticipants?: Maybe<Scalars['Int']>;
  /** @deprecated use numberOfWinner instead */
  maximumNumberOfWinners?: Maybe<Scalars['Int']>;
  minimumNumberOfReviews?: Maybe<Scalars['Int']>;
  name: Scalars['String'];
  notifications: Array<EventNotification>;
  numberOfWinners?: Maybe<Scalars['Int']>;
  points?: Maybe<Scalars['Int']>;
  products: Array<Product>;
  productsCount: Scalars['Int'];
  referred_url?: Maybe<Scalars['String']>;
  reminderDates?: Maybe<Array<Scalars['DateTime']>>;
  reviewDeadline?: Maybe<Scalars['DateTime']>;
  startedAt: Scalars['DateTime'];
  tags: Array<Tag>;
  type: EventType;
  /** @deprecated please use id instead */
  uid: Scalars['ID'];
  visible: Scalars['Boolean'];
};


export type EventCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventCommentWhereInput>;
};


export type EventProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};

export type EventAggregate = {
  __typename?: 'EventAggregate';
  count: Scalars['Int'];
};

export type EventComment = {
  __typename?: 'EventComment';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  event: Event;
  id: Scalars['Int'];
  product?: Maybe<Product>;
  shippingAddress: ShippingAddress;
  status: EventCommentStatus;
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type EventCommentAggregate = {
  __typename?: 'EventCommentAggregate';
  count: Scalars['Int'];
};

export type EventCommentConnection = {
  __typename?: 'EventCommentConnection';
  aggregate: EventCommentAggregate;
};

export type EventCommentCreateError = Error & {
  __typename?: 'EventCommentCreateError';
  code: EventCommentCreateErrorCode;
  comments?: Maybe<Array<EventComment>>;
  currentValue?: Maybe<Scalars['Int']>;
  expectedValue?: Maybe<Scalars['Int']>;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum EventCommentCreateErrorCode {
  AlreadyParticipatedEvent = 'ALREADY_PARTICIPATED_EVENT',
  ExceedMaxParticipants = 'EXCEED_MAX_PARTICIPANTS',
  HaveNotLinkedSocialMedia = 'HAVE_NOT_LINKED_SOCIAL_MEDIA',
  HaveNotWrittenReviewForTrialProducts = 'HAVE_NOT_WRITTEN_REVIEW_FOR_TRIAL_PRODUCTS',
  NotEnoughPoints = 'NOT_ENOUGH_POINTS',
  NotEnoughReviews = 'NOT_ENOUGH_REVIEWS',
  ShippingAddressNotExisted = 'SHIPPING_ADDRESS_NOT_EXISTED'
}

export type EventCommentCreateInput = {
  content: Scalars['String'];
  event: EventWhereUniqueInput;
  isNewApi?: InputMaybe<Scalars['Boolean']>;
  usePoints?: InputMaybe<Scalars['Boolean']>;
};

export type EventCommentOrError = CommonError | EventComment | EventCommentCreateError;

export type EventCommentOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export enum EventCommentStatus {
  Cancelled = 'CANCELLED',
  Confirmed = 'CONFIRMED',
  Created = 'CREATED'
}

export type EventCommentWhereInput = {
  event?: InputMaybe<EventWhereInput>;
  status?: InputMaybe<EventCommentStatus>;
  user?: InputMaybe<UserWhereInput>;
};

export type EventCommentWhereUniqueInput = {
  id: Scalars['Int'];
};

export type EventConnection = {
  __typename?: 'EventConnection';
  aggregate: EventAggregate;
};

export type EventNotification = {
  __typename?: 'EventNotification';
  content: Scalars['String'];
  id: Scalars['String'];
  notifiedAt?: Maybe<Scalars['DateTime']>;
  title: Scalars['String'];
};

export type EventOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type EventToProduct = {
  __typename?: 'EventToProduct';
  quantity?: Maybe<Scalars['Int']>;
};

export enum EventType {
  Sale = 'SALE',
  Trial = 'TRIAL'
}

export type EventWhereInput = {
  conditionContains?: InputMaybe<Scalars['String']>;
  hasReferredUrl?: InputMaybe<Scalars['Boolean']>;
  id?: InputMaybe<Scalars['Int']>;
  isExpired?: InputMaybe<Scalars['Boolean']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  nameContains?: InputMaybe<Scalars['String']>;
  productsSome?: InputMaybe<ProductWhereInput>;
  type?: InputMaybe<EventType>;
  visible?: InputMaybe<Scalars['Boolean']>;
};

export type EventWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export enum EwgRiskType {
  High = 'HIGH',
  Low = 'LOW',
  Medium = 'MEDIUM',
  Unknown = 'UNKNOWN'
}

export type ExternalLink = {
  __typename?: 'ExternalLink';
  affiliateUrl?: Maybe<Scalars['String']>;
  image?: Maybe<Image>;
  title: Scalars['String'];
  url: Scalars['String'];
};

export type ExternalLinkCreateManyWithoutShopInput = {
  create: Array<ExternalLinkCreateWithoutShopInput>;
};

export type ExternalLinkCreateManyWithoutShopToProductInput = {
  create: Array<ExternalLinkCreateWithoutShopToProductInput>;
};

export type ExternalLinkCreateWithoutShopInput = {
  affiliateUrl?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageCreateOneWithoutExternalLinkInput>;
  title: Scalars['String'];
  url: Scalars['String'];
};

export type ExternalLinkCreateWithoutShopToProductInput = {
  affiliateUrl?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageCreateOneWithoutExternalLinkInput>;
  title: Scalars['String'];
  url: Scalars['String'];
};

export type ExternalLinkUpdateManyWithoutShopInput = {
  create: Array<ExternalLinkCreateWithoutShopInput>;
};

export type ExternalLinkUpdateManyWithoutShopToProductInput = {
  create: Array<ExternalLinkCreateWithoutShopToProductInput>;
};

export enum ExternalProvider {
  Lazada = 'LAZADA',
  Other = 'OTHER',
  Sendo = 'SENDO',
  Shopee = 'SHOPEE',
  Tiki = 'TIKI'
}

export type Feature = {
  __typename?: 'Feature';
  liveStream?: Maybe<Scalars['Boolean']>;
  video?: Maybe<Scalars['Boolean']>;
};

export type FieldType = {
  __typename?: 'FieldType';
  name: Scalars['String'];
  type: TypeEnum;
  value: Scalars['String'];
};

export type FieldTypeInput = {
  name: Scalars['String'];
  type: TypeEnum;
  value: Scalars['String'];
};

export enum FixedSize {
  Large = 'LARGE',
  Medium = 'MEDIUM',
  Small = 'SMALL'
}

export type FollowersAggregate = {
  __typename?: 'FollowersAggregate';
  count: Scalars['Int'];
};

export type FollowersConnection = {
  __typename?: 'FollowersConnection';
  aggregate: FollowersAggregate;
};

export type FollowingsAggregate = {
  __typename?: 'FollowingsAggregate';
  count: Scalars['Int'];
};

export type FollowingsConnection = {
  __typename?: 'FollowingsConnection';
  aggregate: FollowingsAggregate;
};

export type Function = {
  __typename?: 'Function';
  id: Scalars['Int'];
  /** This function is for cosmetic or functional food product */
  productType: ProductType;
  symbolUrl?: Maybe<Scalars['String']>;
  translations: Array<FunctionTranslation>;
  type: FunctionType;
  uid: Scalars['ID'];
};


export type FunctionTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionTranslationWhereInput>;
};

export type FunctionOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type FunctionProductUpsertInput = {
  ids?: InputMaybe<Array<FuntionProductInputType>>;
};

export type FunctionProducts = {
  __typename?: 'FunctionProducts';
  ids?: Maybe<Array<Function>>;
};

export type FunctionTranslation = {
  __typename?: 'FunctionTranslation';
  description?: Maybe<Scalars['String']>;
  function: Function;
  id: Scalars['Int'];
  language: LanguageCode;
  name: Scalars['String'];
};

export type FunctionTranslationWhereInput = {
  function?: InputMaybe<FunctionWhereInput>;
  language?: InputMaybe<LanguageCode>;
  name?: InputMaybe<StringFilter>;
};

export enum FunctionType {
  Advantage = 'ADVANTAGE',
  Disadvantage = 'DISADVANTAGE',
  Recommendation = 'RECOMMENDATION'
}

export type FunctionWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  ingredientToFunctionsSome?: InputMaybe<IngredientToFunctionWhereInput>;
  ingredientsSome?: InputMaybe<IngredientWhereInput>;
  productType?: InputMaybe<ProductType>;
  translationsSome?: InputMaybe<FunctionTranslationWhereInput>;
  type?: InputMaybe<FunctionType>;
};

export type FunctionWhereUniqueInput = {
  id: Scalars['Int'];
};

export type FunctionsAggregate = {
  __typename?: 'FunctionsAggregate';
  count: Scalars['Int'];
};

export type FunctionsConnection = {
  __typename?: 'FunctionsConnection';
  aggregate: FunctionsAggregate;
};

export type Funding = {
  __typename?: 'Funding';
  condition?: Maybe<Scalars['String']>;
  coverUrl?: Maybe<Scalars['String']>;
  endDate?: Maybe<Scalars['DateTime']>;
  event?: Maybe<Event>;
  eventId?: Maybe<Scalars['Int']>;
  freeShip?: Maybe<Scalars['Boolean']>;
  fundingProducts: Array<FundingProduct>;
  id: Scalars['Int'];
  isVisible?: Maybe<Scalars['Boolean']>;
  longDescription?: Maybe<Scalars['String']>;
  minimumNumberOfPoints?: Maybe<Scalars['Int']>;
  minimumNumberOfReviews?: Maybe<Scalars['Int']>;
  nbOfPreOrder?: Maybe<Scalars['Int']>;
  preOrder?: Maybe<Scalars['Boolean']>;
  preOrderQuantity?: Maybe<Scalars['Int']>;
  productNbOfPreOrder?: Maybe<Scalars['Int']>;
  shortDescription?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  subTitle?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
};

export type FundingCreateInput = {
  condition?: InputMaybe<Scalars['String']>;
  coverUrl: Scalars['String'];
  endDate: Scalars['DateTime'];
  freeShip?: InputMaybe<Scalars['Boolean']>;
  fundingProducts?: InputMaybe<Array<FundingProductUpsertInput>>;
  longDescription: Scalars['String'];
  minimumNumberOfPoints?: InputMaybe<Scalars['Float']>;
  minimumNumberOfReviews?: InputMaybe<Scalars['Float']>;
  preOrder?: InputMaybe<Scalars['Boolean']>;
  preOrderQuantity?: InputMaybe<Scalars['Int']>;
  shortDescription: Scalars['String'];
  startDate: Scalars['DateTime'];
  subTitle?: InputMaybe<Scalars['String']>;
  title: Scalars['String'];
};

export type FundingOrder = {
  __typename?: 'FundingOrder';
  createdAt?: Maybe<Scalars['DateTime']>;
  deliveryAddress?: Maybe<Scalars['String']>;
  deliveryFee?: Maybe<Scalars['Float']>;
  deliveryFeeInfo?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['Float']>;
  estimatedDeliverTime?: Maybe<Scalars['String']>;
  estimatedPickTime?: Maybe<Scalars['String']>;
  freeShip?: Maybe<Scalars['Boolean']>;
  fundingId: Scalars['Int'];
  fundingProducts: Array<FundingProduct>;
  id: Scalars['Int'];
  insuranceFee?: Maybe<Scalars['Float']>;
  isPaid?: Maybe<Scalars['Boolean']>;
  orderEvaluations?: Maybe<Array<FundingOrderEvaluation>>;
  paymentMethod?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
  pickAddress?: Maybe<Scalars['String']>;
  preOrder?: Maybe<Scalars['Boolean']>;
  productId: Scalars['Int'];
  quantity?: Maybe<Scalars['Int']>;
  shipmentHistory?: Maybe<Array<ShipmentHistory>>;
  shipmentStatus?: Maybe<Scalars['Int']>;
  shipmentStatusText?: Maybe<Scalars['String']>;
  status?: Maybe<FundingOrderStatus>;
  statusText?: Maybe<Scalars['String']>;
  totalAmount?: Maybe<Scalars['Float']>;
  transportId?: Maybe<Scalars['String']>;
  userId: Scalars['Int'];
  userName?: Maybe<Scalars['String']>;
  userOrder: User;
};


export type FundingOrderShipmentHistoryArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShipmentHistoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShipmentHistoryWhereInput>;
};

export type FundingOrderAggregate = {
  __typename?: 'FundingOrderAggregate';
  countFundingOrder: Scalars['Int'];
};

export type FundingOrderByInput = {
  id?: InputMaybe<OrderBy>;
  startDate?: InputMaybe<OrderBy>;
};

export type FundingOrderConnection = {
  __typename?: 'FundingOrderConnection';
  aggregate: FundingOrderAggregate;
};

export type FundingOrderEvaluation = {
  __typename?: 'FundingOrderEvaluation';
  comment?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  fundingOrder: FundingOrder;
  id: Scalars['Int'];
  images?: Maybe<Array<Image>>;
  orderId?: Maybe<Scalars['Int']>;
  productId?: Maybe<Scalars['Int']>;
  rate?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type FundingOrderEvaluationInput = {
  comment?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<Array<ImageInput>>;
  orderId: Scalars['Int'];
  productId: Scalars['Int'];
  rate?: InputMaybe<Scalars['Int']>;
};

export type FundingOrderEvaluationUpdateInput = {
  comment?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<Array<ImageInput>>;
  rate?: InputMaybe<Scalars['Int']>;
};

export type FundingOrderEvaluationWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  orderId?: InputMaybe<Scalars['Int']>;
  productId?: InputMaybe<Scalars['Int']>;
  rate?: InputMaybe<IntFilter>;
  userId?: InputMaybe<Scalars['Int']>;
};

export type FundingOrderInput = {
  bankCode?: InputMaybe<Scalars['String']>;
  deliveryAddress: Scalars['String'];
  deliveryFee: Scalars['Float'];
  deliveryFeeInfo?: InputMaybe<Scalars['String']>;
  discount: Scalars['Float'];
  freeShip?: InputMaybe<Scalars['Boolean']>;
  fundingId: Scalars['Int'];
  insuranceFee?: InputMaybe<Scalars['Float']>;
  paymentMethod: PaymentMethod;
  preOrder?: InputMaybe<Scalars['Boolean']>;
  productId: Scalars['Int'];
  quantity: Scalars['Int'];
  telephone: Scalars['String'];
  userName: Scalars['String'];
};

export type FundingOrderOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type FundingOrderStatisticWhereInput = {
  fromDate?: InputMaybe<Scalars['DateTime']>;
  fundingId?: InputMaybe<Scalars['Int']>;
  groupBy?: InputMaybe<GroupByType>;
  keyword?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<FundingOrderType>;
  toDate?: InputMaybe<Scalars['DateTime']>;
  userId?: InputMaybe<Scalars['Int']>;
};

export enum FundingOrderStatus {
  Cancelled = 'CANCELLED',
  Completed = 'COMPLETED',
  Delivering = 'DELIVERING',
  InProcessing = 'IN_PROCESSING',
  PaymentError = 'PAYMENT_ERROR',
  Refunded = 'REFUNDED',
  SystemError = 'SYSTEM_ERROR',
  WaitDelivering = 'WAIT_DELIVERING',
  WaitRefunding = 'WAIT_REFUNDING'
}

export enum FundingOrderType {
  Cancel = 'CANCEL',
  Completed = 'COMPLETED',
  Delivering = 'DELIVERING',
  InProcessing = 'IN_PROCESSING',
  WaitDelivering = 'WAIT_DELIVERING'
}

export type FundingOrderWhereInput = {
  fromDate?: InputMaybe<Scalars['DateTime']>;
  fundingId?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['Int']>;
  preOrder?: InputMaybe<Scalars['Boolean']>;
  productId?: InputMaybe<Scalars['Int']>;
  productName?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<FundingOrderStatus>;
  toDate?: InputMaybe<Scalars['DateTime']>;
  userId?: InputMaybe<Scalars['Int']>;
  userName?: InputMaybe<Scalars['String']>;
};

export type FundingProduct = {
  __typename?: 'FundingProduct';
  discount?: Maybe<Scalars['Float']>;
  funding: Funding;
  fundingID?: Maybe<Scalars['Int']>;
  id: Scalars['Int'];
  product?: Maybe<Product>;
  productEvaluation?: Maybe<Array<FundingOrderEvaluation>>;
  productId?: Maybe<Scalars['Int']>;
  quantity?: Maybe<Scalars['Int']>;
  totalQuantity?: Maybe<Scalars['Int']>;
};

export type FundingProductUpsertInput = {
  discount?: InputMaybe<Scalars['Float']>;
  productId?: InputMaybe<Scalars['Int']>;
  quantity?: InputMaybe<Scalars['Int']>;
};

export enum FundingStatus {
  Created = 'CREATED',
  Running = 'RUNNING',
  Stopped = 'STOPPED'
}

export type FundingUpdateInput = {
  condition?: InputMaybe<Scalars['String']>;
  coverUrl?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Scalars['DateTime']>;
  freeShip?: InputMaybe<Scalars['Boolean']>;
  fundingProducts?: InputMaybe<Array<FundingProductUpsertInput>>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  longDescription?: InputMaybe<Scalars['String']>;
  minimumNumberOfPoints?: InputMaybe<Scalars['Float']>;
  minimumNumberOfReviews?: InputMaybe<Scalars['Float']>;
  preOrder?: InputMaybe<Scalars['Boolean']>;
  preOrderQuantity?: InputMaybe<Scalars['Int']>;
  shortDescription?: InputMaybe<Scalars['String']>;
  startDate?: InputMaybe<Scalars['DateTime']>;
  subTitle?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
};

export type FundingWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  objectType?: InputMaybe<ObjectType>;
  productId?: InputMaybe<Scalars['Int']>;
  statusFilter?: InputMaybe<FundingStatus>;
  textSearch?: InputMaybe<Scalars['String']>;
};

export type FundingsAggregate = {
  __typename?: 'FundingsAggregate';
  countFunding: Scalars['Int'];
};

export type FundingsConnection = {
  __typename?: 'FundingsConnection';
  aggregate: FundingsAggregate;
};

export type FuntionProductInputType = {
  id: Scalars['Int'];
};

export enum Gender {
  Female = 'FEMALE',
  Male = 'MALE',
  Other = 'OTHER'
}

export type GoodsList = {
  __typename?: 'GoodsList';
  brandCode: Scalars['String'];
  brandLogo?: Maybe<Scalars['String']>;
  brandName: Scalars['String'];
  cateCode: Scalars['String'];
  commtGuide: Scalars['String'];
  commtGuideEn: Scalars['String'];
  commtGuideKr: Scalars['String'];
  commtProduct: Scalars['String'];
  commtProductEn: Scalars['String'];
  commtProductKr: Scalars['String'];
  enGoodsName: Scalars['String'];
  expiryDate: Scalars['String'];
  expiryDateEn: Scalars['String'];
  expiryDateKr: Scalars['String'];
  goodsId: Scalars['String'];
  goodsName: Scalars['String'];
  goodsType: Scalars['String'];
  id: Scalars['Int'];
  krGoodsName: Scalars['String'];
  listPrice: Scalars['Float'];
  modDate: Scalars['String'];
  originImg: Scalars['String'];
  rpPoint: Scalars['Int'];
  saleFeeType: Scalars['String'];
  salePrice: Scalars['Float'];
  useYn: Scalars['String'];
  vendorId?: Maybe<Scalars['Int']>;
  vendorLogo?: Maybe<Scalars['String']>;
  vendorName?: Maybe<Scalars['String']>;
  voucherBrand: VoucherBrand;
  voucherCategories?: Maybe<VoucherCategories>;
  voucherVendor: VoucherVendor;
};

export type GoodsListArgsOrderByInput = {
  id?: InputMaybe<OrderBy>;
  rp_point?: InputMaybe<OrderBy>;
};

export type GoodsListWhereInput = {
  categories?: InputMaybe<Array<Scalars['String']>>;
  id?: InputMaybe<Scalars['Int']>;
};

export type GoogleProduct = {
  __typename?: 'GoogleProduct';
  googleUrl: Scalars['String'];
  id: Scalars['Int'];
  image: Scalars['String'];
  name: Scalars['String'];
  price: Scalars['String'];
  productId: Scalars['Int'];
  rating?: Maybe<Scalars['Float']>;
  searchOrder: Scalars['Int'];
  shippingPrivacy: Scalars['String'];
  shopName: Scalars['String'];
  updatedTime: Scalars['Int'];
};

export type GoogleProductOrderInput = {
  googleOrder?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
  rating?: InputMaybe<OrderBy>;
};

export type GoogleProductWhereInput = {
  productId: Scalars['Int'];
  shopName?: InputMaybe<ExternalProvider>;
};

export enum GroupByType {
  Product = 'PRODUCT',
  User = 'USER'
}

export type Hashtag = {
  __typename?: 'Hashtag';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  isBookmarkedByViewer?: Maybe<Scalars['Boolean']>;
  postCount: Scalars['Int'];
  reviewCount: Scalars['Int'];
  value: Scalars['String'];
};

export type HashtagBookmarkPayload = CommonError | Hashtag;

export type HashtagInput = {
  id?: InputMaybe<Scalars['ID']>;
  value: Scalars['String'];
};

export type HashtagWhereInput = {
  value: StringFilter;
};

export type HashtagWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
  value?: InputMaybe<Scalars['String']>;
};

export type HashtagsBookmarkedByUserOrderByInput = {
  bookmarkedAt?: InputMaybe<OrderBy>;
};

export type HighlightProduct = {
  __typename?: 'HighlightProduct';
  product: Product;
  ranking?: Maybe<Scalars['Int']>;
  rankingChange?: Maybe<Scalars['Int']>;
};

export type HotKeywordRanking = {
  __typename?: 'HotKeywordRanking';
  isNew?: Maybe<Scalars['Boolean']>;
  keywordList?: Maybe<Array<KeywordList>>;
  name: Scalars['String'];
  nbEntries?: Maybe<Scalars['Int']>;
  rank: Scalars['Int'];
};

export type IamOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type Image = {
  __typename?: 'Image';
  createdAt: Scalars['DateTime'];
  fixed: Image;
  height?: Maybe<Scalars['Float']>;
  id: Scalars['Int'];
  name: Scalars['String'];
  url: Scalars['String'];
  width?: Maybe<Scalars['Float']>;
};


export type ImageFixedArgs = {
  width: FixedSize;
};

export type ImageCreateManyWithoutPostCommentInput = {
  create?: InputMaybe<Array<ImageCreateWithoutPostCommentInput>>;
};

export type ImageCreateManyWithoutPostInput = {
  create?: InputMaybe<Array<ImageCreateWithoutPostInput>>;
};

export type ImageCreateManyWithoutReviewInput = {
  create?: InputMaybe<Array<ImageCreateWithoutReviewInput>>;
};

export type ImageCreateOneWithoutExternalLinkInput = {
  create?: InputMaybe<ImageCreateWithoutExternalLinkInput>;
};

export type ImageCreateOneWithoutLiveStreamInput = {
  create: ImageCreateWithoutLiveStreamInput;
};

export type ImageCreateOneWithoutShopInput = {
  create: ImageCreateWithoutShopInput;
};

export type ImageCreateWithoutExternalLinkInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutLiveStreamInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutPostCommentInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutPostInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutReviewInput = {
  height: Scalars['Int'];
  name: Scalars['String'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type ImageCreateWithoutShopInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutUserInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageInput = {
  height?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  url?: InputMaybe<Scalars['String']>;
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageInputType = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  height?: InputMaybe<Scalars['Float']>;
  id?: InputMaybe<Scalars['Int']>;
  name?: InputMaybe<Scalars['String']>;
  url?: InputMaybe<Scalars['String']>;
  width?: InputMaybe<Scalars['Float']>;
};

export type ImageOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type ImageReviewInput = {
  height?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  url?: InputMaybe<Scalars['String']>;
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageUpdateManyWithoutPostCommentInput = {
  create?: InputMaybe<Array<ImageCreateWithoutPostCommentInput>>;
  disconnect?: InputMaybe<Array<ImageWhereUniqueInput>>;
  set?: InputMaybe<Array<ImageWhereUniqueInput>>;
};

export type ImageUpdateManyWithoutPostInput = {
  create?: InputMaybe<Array<ImageCreateWithoutPostInput>>;
  set?: InputMaybe<Array<ImageWhereUniqueInput>>;
};

export type ImageUpdateManyWithoutReviewInput = {
  create?: InputMaybe<Array<ImageCreateWithoutReviewInput>>;
  set?: InputMaybe<Array<ImageWhereUniqueInput>>;
};

export type ImageUpdateManyWithoutUserInput = {
  create?: InputMaybe<ImageCreateWithoutUserInput>;
};

export type ImageUpdateOneWithoutShopInput = {
  create: ImageCreateWithoutShopInput;
};

export type ImageUpsertInput = {
  urls?: InputMaybe<Array<ImageInputType>>;
};

export type ImageWhereInput = {
  review?: InputMaybe<ReviewWhereInput>;
};

export type ImageWhereUniqueInput = {
  id: Scalars['Int'];
};

export type Images = {
  __typename?: 'Images';
  urls?: Maybe<Array<Image>>;
};

export type ImagesAggregate = {
  __typename?: 'ImagesAggregate';
  count: Scalars['Int'];
};

export type ImagesConnection = {
  __typename?: 'ImagesConnection';
  aggregate: ImagesAggregate;
};

export type Ingredient = {
  __typename?: 'Ingredient';
  attributes?: Maybe<IngredientAttribute>;
  /** @deprecated please use cosmeticCautions or functionalFoodCautions */
  cautions?: Maybe<Array<IngredientCaution>>;
  cosmeticCautions?: Maybe<Array<IngredientCaution>>;
  cosmeticFunctions?: Maybe<Array<Function>>;
  /** @deprecated please use description of translations instead */
  description?: Maybe<Scalars['String']>;
  ewg?: Maybe<Scalars['String']>;
  ewgRiskType?: Maybe<EwgRiskType>;
  functionalFoodCautions?: Maybe<Array<IngredientCaution>>;
  functionalFoodFunctions?: Maybe<Array<Function>>;
  id: Scalars['Int'];
  /** @deprecated please use cosmeticFunctions or functionalFoodFunctions */
  specialFunctions?: Maybe<Array<SpecialIngredientFunction>>;
  translations: Array<IngredientTranslation>;
  uid: Scalars['ID'];
};


export type IngredientCautionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type IngredientCosmeticCautionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type IngredientCosmeticFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type IngredientFunctionalFoodCautionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type IngredientFunctionalFoodFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type IngredientSpecialFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SpecialIngredientFunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SpecialIngredientFunctionWhereInput>;
};


export type IngredientTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientTranslationWhereInput>;
};

export type IngredientAttribute = {
  __typename?: 'IngredientAttribute';
  demographic?: Maybe<IngredientDemographic>;
  /** Reference Daily Intake */
  rdi?: Maybe<Scalars['Float']>;
  /** Reference Daily Intake Unit */
  rdiUnit?: Maybe<Unit>;
  /** Tolerable Upper Intake Level */
  ul?: Maybe<Scalars['Float']>;
  /** Tolerable Upper Intake Level Unit */
  ulUnit?: Maybe<Unit>;
};

export type IngredientCaution = {
  __typename?: 'IngredientCaution';
  icon?: Maybe<Image>;
  id: Scalars['Int'];
  /** This caution is for cosmetic or functional food product */
  productType: ProductType;
  translations: Array<IngredientCautionTranslation>;
  uid: Scalars['ID'];
};


export type IngredientCautionTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientCautionTranslationWhereInput>;
};

export type IngredientCautionTranslation = {
  __typename?: 'IngredientCautionTranslation';
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  language: LanguageCode;
  name: Scalars['String'];
};

export type IngredientCautionTranslationWhereInput = {
  caution?: InputMaybe<CautionWhereInput>;
  language?: InputMaybe<LanguageCode>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type IngredientCautionsAggregate = {
  __typename?: 'IngredientCautionsAggregate';
  count: Scalars['Int'];
};

export type IngredientCautionsConnection = {
  __typename?: 'IngredientCautionsConnection';
  aggregate: IngredientCautionsAggregate;
};

export type IngredientCosmeticAttribute = {
  __typename?: 'IngredientCosmeticAttribute';
  description?: Maybe<Scalars['String']>;
  effect?: Maybe<Scalars['String']>;
};

export type IngredientDemographic = {
  __typename?: 'IngredientDemographic';
  baby?: Maybe<Array<BabyRdi>>;
  breastfeedingWoman?: Maybe<AdultRdi>;
  man?: Maybe<Array<AdultRdi>>;
  pregnantWoman?: Maybe<AdultRdi>;
  woman?: Maybe<Array<AdultRdi>>;
};

export type IngredientFunctionFoodAttribute = {
  __typename?: 'IngredientFunctionFoodAttribute';
  attention?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  dose?: Maybe<Scalars['String']>;
};

export type IngredientOrderByInput = {
  id?: InputMaybe<OrderBy>;
  priority?: InputMaybe<OrderBy>;
};

export type IngredientSearchResult = {
  __typename?: 'IngredientSearchResult';
  ingredients: Array<Maybe<Ingredient>>;
  total: Scalars['Int'];
};

export enum IngredientToFunctionLevel {
  Major = 'MAJOR',
  Minor = 'MINOR'
}

export type IngredientToFunctionWhereInput = {
  functionId?: InputMaybe<IntFilter>;
  functionType?: InputMaybe<FunctionType>;
  ingredient?: InputMaybe<IngredientWhereInput>;
  /** MAJOR OR MINOR */
  level?: InputMaybe<IngredientToFunctionLevel>;
  productType?: InputMaybe<ProductType>;
};

export type IngredientTranslation = {
  __typename?: 'IngredientTranslation';
  cosmeticAttributes?: Maybe<IngredientCosmeticAttribute>;
  /** @deprecated please use functionalFoodAttributes.description or cosmeticAttributes.description */
  description?: Maybe<Scalars['String']>;
  functionalFoodAttributes?: Maybe<IngredientFunctionFoodAttribute>;
  id: Scalars['Int'];
  language: LanguageCode;
  name: Scalars['String'];
  /** @deprecated use description instead */
  purpose?: Maybe<Scalars['String']>;
};

export type IngredientTranslationOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type IngredientTranslationWhereInput = {
  ingredient?: InputMaybe<IngredientWhereInput>;
  language?: InputMaybe<LanguageCode>;
  name?: InputMaybe<StringFilter>;
  nameContains?: InputMaybe<Scalars['String']>;
};

/** Ingredient used by user */
export type IngredientUsage = {
  __typename?: 'IngredientUsage';
  attributes?: Maybe<ProductToIngredientAttribute>;
  ingredient: Ingredient;
  ingredientId: Scalars['ID'];
  level?: Maybe<ProductToIngredientLevel>;
};

export type IngredientWhereInput = {
  cautionsSome?: InputMaybe<CautionWhereInput>;
  ewg?: InputMaybe<Scalars['String']>;
  ewgRiskType?: InputMaybe<EwgRiskType>;
  functionsSome?: InputMaybe<SpecialIngredientFunctionWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  ingredientToFunctionsSome?: InputMaybe<IngredientToFunctionWhereInput>;
  isCosmetic?: InputMaybe<BooleanFilter>;
  isFunctionalFood?: InputMaybe<BooleanFilter>;
  productsSome?: InputMaybe<ProductWhereInput>;
  translationsSome?: InputMaybe<IngredientTranslationWhereInput>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type IngredientWhereUniqueInput = {
  id: Scalars['Int'];
};

export type IngredientsAggregate = {
  __typename?: 'IngredientsAggregate';
  count: Scalars['Int'];
};

export type IngredientsConnection = {
  __typename?: 'IngredientsConnection';
  aggregate: IngredientsAggregate;
};

export type IntFilter = {
  equals?: InputMaybe<Scalars['Int']>;
  gt?: InputMaybe<Scalars['Int']>;
  gte?: InputMaybe<Scalars['Int']>;
  in?: InputMaybe<Array<Scalars['Int']>>;
  lt?: InputMaybe<Scalars['Int']>;
  lte?: InputMaybe<Scalars['Int']>;
  ne?: InputMaybe<Scalars['Int']>;
  notIn?: InputMaybe<Array<Scalars['Int']>>;
};

export type KeywordList = {
  __typename?: 'KeywordList';
  key: Scalars['String'];
  value: Scalars['Int'];
};

export enum LanguageCode {
  En = 'EN',
  Ko = 'KO',
  Vi = 'VI'
}

export type LanguageCodeFilter = {
  equals?: InputMaybe<LanguageCode>;
  in?: InputMaybe<Array<LanguageCode>>;
};

export type Level = {
  __typename?: 'Level';
  id: Scalars['Int'];
  maxPoints?: Maybe<Scalars['Int']>;
  minPoints: Scalars['Int'];
};

export type LiveStream = {
  __typename?: 'LiveStream';
  background?: Maybe<Image>;
  channelId?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  description?: Maybe<Scalars['String']>;
  host: User;
  id: Scalars['Int'];
  liveUrl?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  roomId?: Maybe<Scalars['String']>;
  savedByViewer?: Maybe<Scalars['Boolean']>;
  savedDateByViewer?: Maybe<Scalars['DateTime']>;
  startedAt?: Maybe<Scalars['DateTime']>;
  status: LiveStreamStatus;
  streamUrl?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
  vodUrl?: Maybe<Scalars['String']>;
};

export type LiveStreamCreateInput = {
  background?: InputMaybe<ImageCreateOneWithoutLiveStreamInput>;
  description?: InputMaybe<Scalars['String']>;
  lsProducts?: InputMaybe<LiveStreamProductCreateManyWithoutLiveStreamInput>;
  name: Scalars['String'];
  startedAt?: InputMaybe<Scalars['DateTime']>;
};

export type LiveStreamError = Error & {
  __typename?: 'LiveStreamError';
  code: LiveStreamErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum LiveStreamErrorCode {
  HaveNotCreatedShop = 'HAVE_NOT_CREATED_SHOP',
  NotEnoughChannels = 'NOT_ENOUGH_CHANNELS',
  ShopIsBlocked = 'SHOP_IS_BLOCKED',
  ShopNotApprovedYet = 'SHOP_NOT_APPROVED_YET'
}

export type LiveStreamOrError = CommonError | LiveStream | LiveStreamError;

export type LiveStreamOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type LiveStreamProduct = {
  __typename?: 'LiveStreamProduct';
  code: Scalars['String'];
  createdAt: Scalars['DateTime'];
  currency: Currency;
  id: Scalars['Int'];
  name?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Int']>;
  product?: Maybe<Product>;
  updatedAt: Scalars['DateTime'];
};

export type LiveStreamProductCreateManyWithoutLiveStreamInput = {
  create?: InputMaybe<Array<LiveStreamProductCreateWithoutLiveStreamInput>>;
};

export type LiveStreamProductCreateOneWithoutCartItemsInput = {
  connect: LiveStreamProductWhereUniqueInput;
};

export type LiveStreamProductCreateWithoutLiveStreamInput = {
  code: Scalars['String'];
  currency?: InputMaybe<Currency>;
  name?: InputMaybe<Scalars['String']>;
  price?: InputMaybe<Scalars['Int']>;
  product?: InputMaybe<ProductCreateOneWithoutLiveStreamProductInput>;
};

export type LiveStreamProductOrError = CommonError | LiveStreamProduct;

export type LiveStreamProductOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type LiveStreamProductUpdateInput = {
  name?: InputMaybe<Scalars['String']>;
  price?: InputMaybe<Scalars['Int']>;
  product?: InputMaybe<ProductUpdateOneWithoutLiveStreamProductInput>;
};

export type LiveStreamProductUpdateManyWithoutLiveStreamInput = {
  create?: InputMaybe<Array<LiveStreamProductCreateWithoutLiveStreamInput>>;
  set?: InputMaybe<Array<LiveStreamProductWhereUniqueInput>>;
  update?: InputMaybe<Array<LiveStreamProductUpdateWithoutLiveStreamInput>>;
};

export type LiveStreamProductUpdateWithoutLiveStreamInput = {
  code: Scalars['String'];
  currency?: InputMaybe<Currency>;
  id: Scalars['Int'];
  name?: InputMaybe<Scalars['String']>;
  price?: InputMaybe<Scalars['Int']>;
  product?: InputMaybe<ProductUpdateOneWithoutLiveStreamProductInput>;
};

export type LiveStreamProductWhereInput = {
  liveStream: LiveStreamWhereInput;
};

export type LiveStreamProductWhereUniqueInput = {
  id: Scalars['Int'];
};

export type LiveStreamStartInput = {
  roomId: Scalars['String'];
};

export enum LiveStreamStatus {
  Created = 'CREATED',
  Running = 'RUNNING',
  Stopped = 'STOPPED'
}

export type LiveStreamUpdateInput = {
  background?: InputMaybe<ImageCreateOneWithoutLiveStreamInput>;
  description?: InputMaybe<Scalars['String']>;
  lsProducts?: InputMaybe<LiveStreamProductUpdateManyWithoutLiveStreamInput>;
  name: Scalars['String'];
  startedAt?: InputMaybe<Scalars['DateTime']>;
};

export type LiveStreamWhereInput = {
  host?: InputMaybe<UserWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<LiveStreamStatus>;
  statusIn?: InputMaybe<Array<LiveStreamStatus>>;
};

export type LiveStreamWhereUniqueInput = {
  id: Scalars['Int'];
};

export type Location = {
  __typename?: 'Location';
  /** Vĩ độ */
  latitude: Scalars['Float'];
  /** Kinh độ */
  longitude: Scalars['Float'];
};

export type LocationInput = {
  /** Vĩ độ */
  latitude: Scalars['Float'];
  /** Kinh độ */
  longitude: Scalars['Float'];
};

export type M_LikeReviewResult = {
  __typename?: 'M_LikeReviewResult';
  count: Scalars['Int'];
  message: Scalars['String'];
  status: Scalars['Boolean'];
};

export type MediaEntityUnion = LiveStream | Post;

export type MediaOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
};

export enum MediaType {
  LiveStream = 'LIVE_STREAM',
  Video = 'VIDEO'
}

export type MediaWhereInput = {
  createdAt?: InputMaybe<DateTimeFilter>;
  type?: InputMaybe<MediaType>;
  user: UserWhereUniqueInput;
};

export type MediaWhereUniqueInput = {
  liveStream?: InputMaybe<LiveStreamWhereUniqueInput>;
  post?: InputMaybe<PostWhereUniqueInput>;
};

export type MediasAggregate = {
  __typename?: 'MediasAggregate';
  count: Scalars['Int'];
};

export type MediasConnection = {
  __typename?: 'MediasConnection';
  aggregate: MediasAggregate;
};

export type Mutation = {
  __typename?: 'Mutation';
  actitvityLogs: Scalars['Boolean'];
  activityLogs: Scalars['Boolean'];
  addBankCard: Scalars['Boolean'];
  approveShop: Shop;
  blockShop: Shop;
  blockUser: Scalars['Boolean'];
  bookmarkPost: Scalars['Boolean'];
  bookmarkProduct: Scalars['Boolean'];
  bookmarkReview: Scalars['Boolean'];
  brandAdminUpdate: BrandAdmin;
  brandPromotionalProductCreate?: Maybe<BrandPromotionalProduct>;
  brandPromotionalProductDelete?: Maybe<BrandPromotionalProduct>;
  brandPromotionalProductUpdate?: Maybe<BrandPromotionalProduct>;
  cancelEcommerceOrders: Scalars['String'];
  cancelEventComment: EventComment;
  cancelOrder: OrderOrError;
  changeEmail: EmailOrError;
  changePassword: AuthPayload;
  changePhone: Scalars['Boolean'];
  changePickupAddress: Scalars['Boolean'];
  checkIn: Scalars['Boolean'];
  confirmEmail: AuthPayloadOrError;
  confirmOrder: OrderOrError;
  confirmOrders: Scalars['Boolean'];
  confirmOrdersV2: Array<ConfirmingOrderError>;
  createBannerImage: BannerImage;
  createBaumannAnswers: BaumannSkinType;
  createCartItem: CartItemOrError;
  createCartProduct: CartProduct;
  createCartProductV2: CartProductOrError;
  createCastComment: CastComment;
  createChannel: ChannelOrError;
  createChatNotification: Scalars['Boolean'];
  createDevice: Device;
  createEcommerceFlashSale: Scalars['Boolean'];
  createEcommerceOrdersV2: SucessfullEcommOrderOrError;
  createEcommercePromotion: Scalars['Boolean'];
  createEcommerceReview: Array<EcommerceReview>;
  createEvaluation: FundingOrderEvaluation;
  createEventComment: EventComment;
  createEventCommentV2: EventCommentOrError;
  createEventCommentV3: EventCommentOrError;
  createEventCommentV3_UncheckCondition: EventCommentOrError;
  createFunding: Funding;
  createFundingOrder: Scalars['String'];
  createLiveStream: LiveStreamOrError;
  createObjectTagging: ObjectTaggingOrError;
  createOrder: OrderOrError;
  createPost: PostOrError;
  createPostComment: PostComment;
  /** @deprecated please use createPost instead */
  createPostV2: PostOrError;
  createProductEdit: ProductEdit;
  createProductRequest: ProductRequest;
  createProductUsageLog: ProductUsageLog;
  createQAShopProduct: QaShopProduct;
  /** @deprecated please use reviewCreate instead, this mutation will be removed on 1/4/2022 */
  createReview: Review;
  createReviewReport: ReviewReport;
  createShippingAddress: ShippingAddressOrError;
  /** @deprecated use createShopV2 instead */
  createShop: Shop;
  createShopProduct: ShopProduct;
  createShopToProducts: Array<ShopToProduct>;
  createShopV2: ShopOrMutationError;
  createSlot: PostReservedSlot;
  createSystemConfig: SystemConfig;
  createTag: TagEntity;
  deleteBannerImage: BannerImage;
  deleteCartItem: CartItemOrError;
  deleteCartProduct: CartProduct;
  deleteEcommercePromotion: Scalars['Boolean'];
  deleteEcommerceReview: EcommerceReview;
  deleteEvaluation: FundingOrderEvaluation;
  deleteEventComment: EventComment;
  deleteFunding: Funding;
  deleteLiveStream: LiveStreamOrError;
  deleteLiveStreamProduct: LiveStreamProductOrError;
  deleteNotification: NotificationMutatePayload;
  deleteNotifications: NotificationMutatePayload;
  deleteObjectTagging: ObjectTagging;
  deletePost: Post;
  deletePostComment: PostComment;
  deletePostReservedSlot: PostReservedSlot;
  deleteProductUsageLogs: Scalars['Int'];
  deleteQAShopProduct: QaShopProductOrError;
  deleteReview: Review;
  deleteShippingAddress: ShippingAddressOrError;
  deleteShopProduct: ShopProduct;
  deleteShopToProduct: ShopToProduct;
  deleteSystemConfig: SystemConfig;
  deleteTag: TagEntity;
  deleteUser: User;
  dislikeReview: Scalars['Boolean'];
  followBrand: Scalars['Boolean'];
  followUser: User;
  forgetPassword: Scalars['Boolean'];
  generateCurrentEmailOTP: Scalars['Boolean'];
  generateOTP: Scalars['Boolean'];
  generatePresignedUrl: PresignedUrl;
  generateRegisterMultiProductForms?: Maybe<Scalars['Boolean']>;
  generateSmsOTP: Scalars['Boolean'];
  /** @deprecated use checkIn instead */
  getActivePoints: Scalars['Boolean'];
  /** @deprecated use checkIn instead */
  getDailyActivePoints: Scalars['Boolean'];
  ghtkApi: Scalars['String'];
  handleIsDeletedField: TagEntity;
  handleVisible: Funding;
  handleVisibleProductVariant: ProductVariant;
  handleVisibleProductViewStore: ProductViewStore;
  handleVisibleShopProduct: ShopProduct;
  hashtagBookmark: HashtagBookmarkPayload;
  hashtagUnbookmark: UnbookmarkHashtagPayload;
  hidePostComment: PostComment;
  likeDislikeReview: M_LikeReviewResult;
  likeReview: M_LikeReviewResult;
  linkFacebook: SocialMediaOrError;
  linkInstagram: SocialMediaOrError;
  /** @deprecated please use loginByAppleV2 instead. This mutation will be removed on 01/03/2022 */
  loginByApple: AuthPayload;
  loginByAppleV2: AuthPayloadOrError;
  /** @deprecated please use loginByFacebookV2 instead. This mutation will be removed on 01/03/2022 */
  loginByFacebook: AuthPayload;
  loginByFacebookV2: AuthPayloadOrError;
  loginV2: AuthPayloadOrError;
  logout: Scalars['Boolean'];
  markBestAnswer: Scalars['Boolean'];
  notifyLiveStream: Scalars['Boolean'];
  payBackEcommerceOrder: Scalars['String'];
  payBackFundingOrder: Scalars['String'];
  pinReview: Scalars['String'];
  purchaseVouchers: Scalars['Boolean'];
  reactPost: Post;
  reactPostComment: PostComment;
  readNotification: NotificationMutatePayload;
  readNotifications: NotificationMutatePayload;
  reportPost: PostReport;
  reportPostComment: PostCommentReport;
  returnsEcommerceOrders: Scalars['String'];
  reviewCreate: ReviewCreatePayload;
  reviewUpdate: ReviewUpdatePayload;
  saveFavouriteBrands: Scalars['Boolean'];
  saveMedia: Scalars['Boolean'];
  /** @deprecated please use bookmarkProduct instead, this field will be remove from 1/11/2021 */
  saveProduct: Scalars['Boolean'];
  sendNotification?: Maybe<Scalars['Boolean']>;
  signUp: AuthPayloadOrError;
  startLiveStream: LiveStreamOrError;
  stopChannel: ChannelOrError;
  stopLiveStream: LiveStreamOrError;
  subscribePost: Scalars['Boolean'];
  terminateEcommercePromotion: Scalars['Boolean'];
  unBookmarkPost: Scalars['Boolean'];
  unBookmarkProduct: Scalars['Boolean'];
  unBookmarkReview: Scalars['Boolean'];
  unblockUser: Scalars['Boolean'];
  unfollowBrand: Brand;
  unfollowUser: User;
  unhidePostComment: PostComment;
  unlinkFacebook: Scalars['Boolean'];
  unlinkInstagram: Scalars['Boolean'];
  unsaveMedia: Scalars['Boolean'];
  /** @deprecated please use unBookmarkProduct instead, this field will be remove from 1/11/2021 */
  unsaveProduct: Scalars['Boolean'];
  unsubscribePost: Scalars['Boolean'];
  updateBannerImage: BannerImage;
  updateCartItem: CartItemOrError;
  updateCartProduct: CartProduct;
  updateEcommercePromotion: Scalars['Boolean'];
  updateEcommerceReview: EcommerceReview;
  updateEvaluation: FundingOrderEvaluation;
  updateFunding: Funding;
  updateLiveStream: LiveStreamOrError;
  updateLiveStreamProduct: LiveStreamProductOrError;
  updateObjectTagging: ObjectTaggingOrError;
  updateOrder: OrderOrError;
  updatePost: Post;
  updatePostComment: PostComment;
  updateProductUserRequestByAdmin?: Maybe<UserRequestUpdateResponse>;
  updateProductUserStatus?: Maybe<UserRequestUpdateResponse>;
  updateProductVariant: ProductVariant;
  updateQAShopProduct: QaShopProductOrError;
  /** @deprecated please use reviewUpdate instead, this mutation will be removed on 1/4/2022 */
  updateReview: Review;
  updateShippingAddress: ShippingAddressOrError;
  /** @deprecated use updateShopV2 instead */
  updateShop: Shop;
  updateShopProduct: ShopProduct;
  updateShopToProduct: ShopToProduct;
  updateShopV2: ShopOrMutationError;
  updateSlot: PostReservedSlot;
  updateSystemConfig: SystemConfig;
  updateTag: TagEntity;
  updateUser: User;
  updateUserNotificationConfig: Scalars['Boolean'];
  updateVoucherStatus: Scalars['Boolean'];
  userRequestCreate: UserRequest;
  viewPost: Scalars['Boolean'];
  viewedPost: Scalars['Boolean'];
  visitedContentCreate: Scalars['Boolean'];
  vnpayUrl: Scalars['String'];
  voucherBrand: Scalars['Boolean'];
};


export type MutationActitvityLogsArgs = {
  data: ActivityLogCreateInput;
};


export type MutationActivityLogsArgs = {
  data: ActivityLogCreateInput;
};


export type MutationAddBankCardArgs = {
  data: BankShopCreateInput;
};


export type MutationApproveShopArgs = {
  where: ShopWhereUniqueInput;
};


export type MutationBlockShopArgs = {
  reason: Scalars['String'];
  status: ShopStatus;
  where: ShopWhereUniqueInput;
};


export type MutationBlockUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationBookmarkPostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationBookmarkProductArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationBookmarkReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationBrandAdminUpdateArgs = {
  data: BrandAdminUpdateInput;
  where: BrandAdminWhereUniqueInput;
};


export type MutationBrandPromotionalProductCreateArgs = {
  data: BrandPromotionalProductInput;
};


export type MutationBrandPromotionalProductDeleteArgs = {
  where: BrandPromotionalProducWhereInput;
};


export type MutationBrandPromotionalProductUpdateArgs = {
  data: BrandPromotionalProductInput;
  where: BrandPromotionalProducWhereInput;
};


export type MutationCancelEcommerceOrdersArgs = {
  data: EcommerceOrderCancelInput;
};


export type MutationCancelEventCommentArgs = {
  where: EventCommentWhereUniqueInput;
};


export type MutationCancelOrderArgs = {
  where: OrderWhereUniqueInput;
};


export type MutationChangeEmailArgs = {
  email: Scalars['String'];
};


export type MutationChangePasswordArgs = {
  currentPassword: Scalars['String'];
  newPassword: Scalars['String'];
};


export type MutationChangePhoneArgs = {
  phone: Scalars['String'];
};


export type MutationChangePickupAddressArgs = {
  data: AddressIdInput;
};


export type MutationConfirmEmailArgs = {
  otp: Scalars['Int'];
};


export type MutationConfirmOrderArgs = {
  where: OrderWhereUniqueInput;
};


export type MutationConfirmOrdersArgs = {
  data: EcommerceOrderConfirmInput;
};


export type MutationConfirmOrdersV2Args = {
  data: EcommerceOrderConfirmInput;
};


export type MutationCreateBannerImageArgs = {
  data: BannerImageInput;
};


export type MutationCreateBaumannAnswersArgs = {
  data: Array<BaumannAnswerCreateInput>;
};


export type MutationCreateCartItemArgs = {
  data: CartItemCreateInput;
};


export type MutationCreateCartProductArgs = {
  data: CartProductInsertInput;
};


export type MutationCreateCartProductV2Args = {
  data: CartProductInsertInput;
};


export type MutationCreateCastCommentArgs = {
  data: CastCommentCreateInput;
};


export type MutationCreateChatNotificationArgs = {
  data: ChatNotificationInput;
};


export type MutationCreateDeviceArgs = {
  data: DeviceCreateInput;
};


export type MutationCreateEcommerceFlashSaleArgs = {
  data: EcommerceFlashSaleInsertInput;
};


export type MutationCreateEcommerceOrdersV2Args = {
  data: EcommerceOrderInput;
};


export type MutationCreateEcommercePromotionArgs = {
  data: EcommercePromotionInsertInput;
};


export type MutationCreateEcommerceReviewArgs = {
  data: EcommerceReviewInsertInput;
};


export type MutationCreateEvaluationArgs = {
  data: FundingOrderEvaluationInput;
};


export type MutationCreateEventCommentArgs = {
  data: EventCommentCreateInput;
};


export type MutationCreateEventCommentV2Args = {
  data: EventCommentCreateInput;
};


export type MutationCreateEventCommentV3Args = {
  data: EventCommentCreateInput;
};


export type MutationCreateEventCommentV3_UncheckConditionArgs = {
  data: EventCommentCreateInput;
};


export type MutationCreateFundingArgs = {
  data: FundingCreateInput;
};


export type MutationCreateFundingOrderArgs = {
  data: FundingOrderInput;
};


export type MutationCreateLiveStreamArgs = {
  data: LiveStreamCreateInput;
};


export type MutationCreateObjectTaggingArgs = {
  data: ObjectTaggingInsertInput;
};


export type MutationCreateOrderArgs = {
  data: OrderCreateInput;
};


export type MutationCreatePostArgs = {
  data: PostCreateInput;
};


export type MutationCreatePostCommentArgs = {
  data: PostCommentCreateInput;
};


export type MutationCreatePostV2Args = {
  data: PostCreateInput;
};


export type MutationCreateProductEditArgs = {
  data: ProductEditCreateInput;
};


export type MutationCreateProductRequestArgs = {
  data: ProductRequestCreateInput;
};


export type MutationCreateProductUsageLogArgs = {
  data: ProductUsageLogCreateInput;
};


export type MutationCreateQaShopProductArgs = {
  data: QaShopProductInsertInput;
};


export type MutationCreateReviewArgs = {
  data: ReviewCreateInput;
};


export type MutationCreateReviewReportArgs = {
  data: ReviewReportCreateInput;
};


export type MutationCreateShippingAddressArgs = {
  data: ShippingAddressCreateInput;
};


export type MutationCreateShopArgs = {
  data: ShopCreateInput;
};


export type MutationCreateShopProductArgs = {
  data: ShopProductInsertInput;
};


export type MutationCreateShopToProductsArgs = {
  data: Array<ShopToProductCreateInput>;
};


export type MutationCreateShopV2Args = {
  data: ShopCreateInput;
};


export type MutationCreateSlotArgs = {
  data: PostReservedSlotCreateInput;
};


export type MutationCreateSystemConfigArgs = {
  data: SystemConfigCreateInput;
};


export type MutationCreateTagArgs = {
  data: TagEntityInsertInput;
};


export type MutationDeleteBannerImageArgs = {
  where: BannerImageWhereInput;
};


export type MutationDeleteCartItemArgs = {
  where: CartItemWhereUniqueInput;
};


export type MutationDeleteCartProductArgs = {
  where: CartProductWhereInput;
};


export type MutationDeleteEcommercePromotionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionWhereInput>;
};


export type MutationDeleteEcommerceReviewArgs = {
  where: EcommerceReviewWhereInput;
};


export type MutationDeleteEvaluationArgs = {
  where: FundingOrderEvaluationWhereInput;
};


export type MutationDeleteEventCommentArgs = {
  where: EventCommentWhereUniqueInput;
};


export type MutationDeleteFundingArgs = {
  where: FundingWhereInput;
};


export type MutationDeleteLiveStreamArgs = {
  where: LiveStreamWhereUniqueInput;
};


export type MutationDeleteLiveStreamProductArgs = {
  where: LiveStreamProductWhereUniqueInput;
};


export type MutationDeleteNotificationArgs = {
  where: NotificationWhereUniqueInput;
};


export type MutationDeleteObjectTaggingArgs = {
  where: ObjectTaggingWhereInput;
};


export type MutationDeletePostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationDeletePostCommentArgs = {
  where: PostCommentWhereUniqueInput;
};


export type MutationDeletePostReservedSlotArgs = {
  where: PostReservedSlotWhereInput;
};


export type MutationDeleteProductUsageLogsArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationDeleteQaShopProductArgs = {
  where: QaShopProductWhereInput;
};


export type MutationDeleteReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationDeleteShippingAddressArgs = {
  where: ShippingAddressWhereUniqueInput;
};


export type MutationDeleteShopProductArgs = {
  where: ShopProductWhereInput;
};


export type MutationDeleteShopToProductArgs = {
  where: ShopToProductWhereUniqueInput;
};


export type MutationDeleteSystemConfigArgs = {
  where: SystemConfigWhereInput;
};


export type MutationDeleteTagArgs = {
  where: TagsWhereInput;
};


export type MutationDeleteUserArgs = {
  data: UserDeleteInput;
  where: UserWhereUniqueInput;
};


export type MutationDislikeReviewArgs = {
  reviewId: Scalars['Int'];
};


export type MutationFollowBrandArgs = {
  where: BrandWhereUniqueInput;
};


export type MutationFollowUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationForgetPasswordArgs = {
  email: Scalars['String'];
};


export type MutationGenerateOtpArgs = {
  email: Scalars['String'];
};


export type MutationGenerateSmsOtpArgs = {
  phone: Scalars['String'];
};


export type MutationGhtkApiArgs = {
  params: Scalars['String'];
};


export type MutationHandleIsDeletedFieldArgs = {
  isDeleted: Scalars['Boolean'];
  where: TagsWhereInput;
};


export type MutationHandleVisibleArgs = {
  data: FundingUpdateInput;
  where: FundingWhereInput;
};


export type MutationHandleVisibleProductVariantArgs = {
  data: ProductVariantUpdateInput;
  where: ProductVariantWhereInput;
};


export type MutationHandleVisibleProductViewStoreArgs = {
  isVisible: Scalars['Boolean'];
  where: ProductViewStoreWhereInput;
};


export type MutationHandleVisibleShopProductArgs = {
  data: ShopProductUpdateInput;
  where: ShopProductWhereInput;
};


export type MutationHashtagBookmarkArgs = {
  id: Scalars['ID'];
};


export type MutationHashtagUnbookmarkArgs = {
  id: Scalars['ID'];
};


export type MutationHidePostCommentArgs = {
  where: PostCommentWhereUniqueInput;
};


export type MutationLikeDislikeReviewArgs = {
  authorId?: InputMaybe<Scalars['Int']>;
  productId: Scalars['Int'];
  productName?: InputMaybe<Scalars['String']>;
  reactStatus?: InputMaybe<ReactStatus>;
  reviewId: Scalars['Int'];
};


export type MutationLikeReviewArgs = {
  authorId?: InputMaybe<Scalars['Int']>;
  productId: Scalars['Int'];
  productName?: InputMaybe<Scalars['String']>;
  reviewId: Scalars['Int'];
};


export type MutationLinkFacebookArgs = {
  accessToken: Scalars['String'];
};


export type MutationLinkInstagramArgs = {
  token: Scalars['String'];
};


export type MutationLoginByAppleArgs = {
  accessToken: Scalars['String'];
};


export type MutationLoginByAppleV2Args = {
  accessToken: Scalars['String'];
};


export type MutationLoginByFacebookArgs = {
  accessToken: Scalars['String'];
};


export type MutationLoginByFacebookV2Args = {
  accessToken: Scalars['String'];
};


export type MutationLoginV2Args = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationLogoutArgs = {
  token?: InputMaybe<Scalars['String']>;
};


export type MutationMarkBestAnswerArgs = {
  where: PostCommentWhereUniqueInput;
};


export type MutationNotifyLiveStreamArgs = {
  where: LiveStreamWhereUniqueInput;
};


export type MutationPayBackEcommerceOrderArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type MutationPayBackFundingOrderArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderWhereInput>;
};


export type MutationPinReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationPurchaseVouchersArgs = {
  data: VoucherIssuesInput;
};


export type MutationReactPostArgs = {
  status: ReactStatus;
  where: PostWhereUniqueInput;
};


export type MutationReactPostCommentArgs = {
  status: ReactStatus;
  where: PostCommentWhereUniqueInput;
};


export type MutationReadNotificationArgs = {
  where: NotificationWhereUniqueInput;
};


export type MutationReportPostArgs = {
  data: PostReportCreateInput;
};


export type MutationReportPostCommentArgs = {
  data: PostCommentReportCreateInput;
};


export type MutationReturnsEcommerceOrdersArgs = {
  data: EcommerceOrderCancelInput;
};


export type MutationReviewCreateArgs = {
  data: ReviewInput;
};


export type MutationReviewUpdateArgs = {
  data: ReviewInput;
};


export type MutationSaveFavouriteBrandsArgs = {
  data: Array<BrandWhereUniqueInput>;
};


export type MutationSaveMediaArgs = {
  where: MediaWhereUniqueInput;
};


export type MutationSaveProductArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationSendNotificationArgs = {
  where: UserRequestUpdateInput;
};


export type MutationSignUpArgs = {
  data: SignUpInput;
};


export type MutationStartLiveStreamArgs = {
  data: LiveStreamStartInput;
  where: LiveStreamWhereUniqueInput;
};


export type MutationStopChannelArgs = {
  id: Scalars['ID'];
};


export type MutationStopLiveStreamArgs = {
  where: LiveStreamWhereUniqueInput;
};


export type MutationSubscribePostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationTerminateEcommercePromotionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionWhereInput>;
};


export type MutationUnBookmarkPostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationUnBookmarkProductArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationUnBookmarkReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationUnblockUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationUnfollowBrandArgs = {
  where: BrandWhereUniqueInput;
};


export type MutationUnfollowUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationUnhidePostCommentArgs = {
  where: PostCommentWhereUniqueInput;
};


export type MutationUnsaveMediaArgs = {
  where: MediaWhereUniqueInput;
};


export type MutationUnsaveProductArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationUnsubscribePostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationUpdateBannerImageArgs = {
  data: BannerImageUpdateInput;
  where: BannerImageWhereInput;
};


export type MutationUpdateCartItemArgs = {
  data: CartItemUpdateInput;
  where: CartItemWhereUniqueInput;
};


export type MutationUpdateCartProductArgs = {
  data: CartProductUpdateInput;
  where: CartProductWhereInput;
};


export type MutationUpdateEcommercePromotionArgs = {
  data: EcommercePromotionUpdateInput;
};


export type MutationUpdateEcommerceReviewArgs = {
  data: EcommerceReviewUpdateInput;
  where: EcommerceReviewWhereInput;
};


export type MutationUpdateEvaluationArgs = {
  data: FundingOrderEvaluationUpdateInput;
  where: FundingOrderEvaluationWhereInput;
};


export type MutationUpdateFundingArgs = {
  data: FundingUpdateInput;
  where: FundingWhereInput;
};


export type MutationUpdateLiveStreamArgs = {
  data: LiveStreamUpdateInput;
  where: LiveStreamWhereUniqueInput;
};


export type MutationUpdateLiveStreamProductArgs = {
  data: LiveStreamProductUpdateInput;
  where: LiveStreamProductWhereUniqueInput;
};


export type MutationUpdateObjectTaggingArgs = {
  data: ObjectTaggingUpdateInput;
  where: ObjectTaggingWhereInput;
};


export type MutationUpdateOrderArgs = {
  data: OrderUpdateInput;
  where: OrderWhereUniqueInput;
};


export type MutationUpdatePostArgs = {
  data: PostUpdateInput;
  where: PostWhereUniqueInput;
};


export type MutationUpdatePostCommentArgs = {
  data: PostCommentUpdateInput;
  where: PostCommentWhereUniqueInput;
};


export type MutationUpdateProductUserRequestByAdminArgs = {
  data: UserRequestUpdateInput;
  where: UserRequestWhereUniqueInput;
};


export type MutationUpdateProductUserStatusArgs = {
  data: UserRequestUpdateStatusInput;
  where: UserRequestUpdateInput;
};


export type MutationUpdateProductVariantArgs = {
  data: ProductVariantUpdateInput;
  where: ProductVariantWhereInput;
};


export type MutationUpdateQaShopProductArgs = {
  data: QaShopProductUpdateInput;
  where: QaShopProductWhereInput;
};


export type MutationUpdateReviewArgs = {
  data: ReviewUpdateInput;
  where: ReviewWhereUniqueInput;
};


export type MutationUpdateShippingAddressArgs = {
  data: ShippingAddressUpdateInput;
  where: ShippingAddressWhereUniqueInput;
};


export type MutationUpdateShopArgs = {
  data: ShopUpdateInput;
  where: ShopWhereUniqueInput;
};


export type MutationUpdateShopProductArgs = {
  data: ShopProductUpdateInput;
  where: ShopProductWhereInput;
};


export type MutationUpdateShopToProductArgs = {
  data: ShopToProductUpdateInput;
  where: ShopToProductWhereUniqueInput;
};


export type MutationUpdateShopV2Args = {
  data: ShopUpdateInput;
  where: ShopWhereUniqueInput;
};


export type MutationUpdateSlotArgs = {
  data: PostReservedSlotInput;
  where: PostReservedSlotWhereInput;
};


export type MutationUpdateSystemConfigArgs = {
  data: SystemConfigUpdateInput;
  where: SystemConfigWhereInput;
};


export type MutationUpdateTagArgs = {
  data: TagEntityUpdateInput;
  where: TagsWhereInput;
};


export type MutationUpdateUserArgs = {
  data: UserUpdateInput;
  where: UserWhereUniqueInput;
};


export type MutationUpdateUserNotificationConfigArgs = {
  data: UserNotificationConfigUpdateInput;
};


export type MutationUpdateVoucherStatusArgs = {
  data: VoucherGiftpopListUpdateInput;
};


export type MutationUserRequestCreateArgs = {
  data: UserRequestInput;
};


export type MutationViewPostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationViewedPostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationVisitedContentCreateArgs = {
  data: VisitedContentCreateInput;
};


export type MutationVnpayUrlArgs = {
  amount: Scalars['Float'];
  orderInfo: Scalars['String'];
};

export type NbEcommerceOrderStatistic = {
  __typename?: 'NbEcommerceOrderStatistic';
  billTotal?: Maybe<Scalars['Int']>;
  orderStatus?: Maybe<Scalars['String']>;
  orderTotal?: Maybe<Scalars['Int']>;
  productTotal?: Maybe<Scalars['Int']>;
};

export type NbOrderStatistic = {
  __typename?: 'NbOrderStatistic';
  billTotal?: Maybe<Scalars['Int']>;
  evaluationTotal?: Maybe<Scalars['Int']>;
  orderStatus?: Maybe<Scalars['String']>;
  orderTotal?: Maybe<Scalars['Int']>;
  productTotal?: Maybe<Scalars['Int']>;
};

export type NearByInput = {
  /** Khoảng cách, đơn vị m */
  distance: Scalars['Float'];
  /** Vĩ độ */
  latitude: Scalars['Float'];
  /** Kinh độ */
  longitude: Scalars['Float'];
};

export type Notification = {
  __typename?: 'Notification';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  isViewed: Scalars['Boolean'];
  notificationObject: NotificationObject;
};

export type NotificationChange = {
  __typename?: 'NotificationChange';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  user: User;
};

export type NotificationEntityUnion = EcommerceOrder | EcommerceReview | Event | NotificationEvent | PointHistory | Post | PostComment | Product | Review | User;

export type NotificationEvent = {
  __typename?: 'NotificationEvent';
  active?: Maybe<Scalars['Boolean']>;
  alarmTime?: Maybe<Scalars['DateTime']>;
  attachedLink?: Maybe<Scalars['String']>;
  buttonName?: Maybe<Scalars['String']>;
  changeBy?: Maybe<Scalars['String']>;
  content?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  creatorId?: Maybe<Scalars['String']>;
  creatorName?: Maybe<Scalars['String']>;
  dataExcelFile?: Maybe<NotificationEventAttribute>;
  detail?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  nbUserSuccess?: Maybe<Scalars['Int']>;
  nbUserTotal?: Maybe<Scalars['Int']>;
  noEventType?: Maybe<Scalars['String']>;
  noId?: Maybe<Scalars['Int']>;
  receivedObjectData?: Maybe<NotificationObjectByUserInfo>;
  receivedObjectType?: Maybe<Scalars['String']>;
  status?: Maybe<NotificationEventStatus>;
  title?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type NotificationEventAttribute = {
  __typename?: 'NotificationEventAttribute';
  name?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
};

export enum NotificationEventStatus {
  Pending = 'PENDING',
  Ready = 'READY',
  Success = 'SUCCESS'
}

export type NotificationMutatePayload = {
  __typename?: 'NotificationMutatePayload';
  affected?: Maybe<Scalars['Int']>;
};

export type NotificationObject = {
  __typename?: 'NotificationObject';
  createdAt: Scalars['DateTime'];
  entity?: Maybe<NotificationEntityUnion>;
  id: Scalars['Int'];
  mainContent?: Maybe<Scalars['String']>;
  notificationChanges: Array<NotificationChange>;
  subContent?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  type: NotificationObjectType;
};

export type NotificationObjectByUserInfo = {
  __typename?: 'NotificationObjectByUserInfo';
  fromAge?: Maybe<Scalars['Int']>;
  skinType?: Maybe<SkinTypeObject>;
  toAge?: Maybe<Scalars['Float']>;
  userLevel?: Maybe<UserLevelObject>;
};

export enum NotificationObjectType {
  CompletedEcommerceOrder = 'COMPLETED_ECOMMERCE_ORDER',
  ConfirmedEcommerceOrder = 'CONFIRMED_ECOMMERCE_ORDER',
  CreatePost = 'CREATE_POST',
  EventAdmin = 'EVENT_ADMIN',
  EventRemindWriteReview = 'EVENT_REMIND_WRITE_REVIEW',
  FollowUser = 'FOLLOW_USER',
  MentionInComment = 'MENTION_IN_COMMENT',
  MentionInPost = 'MENTION_IN_POST',
  NewsUpgrade = 'NEWS_UPGRADE',
  NewEcommerceOrder = 'NEW_ECOMMERCE_ORDER',
  NewOnlineEcommerceOrder = 'NEW_ONLINE_ECOMMERCE_ORDER',
  PointApprovedReviewReport = 'POINT_APPROVED_REVIEW_REPORT',
  PointApprovedVideo = 'POINT_APPROVED_VIDEO',
  PointBestAnswer = 'POINT_BEST_ANSWER',
  PointDailyActive = 'POINT_DAILY_ACTIVE',
  PointEditProduct = 'POINT_EDIT_PRODUCT',
  PointEventPoints = 'POINT_EVENT_POINTS',
  PointExchangePointsForCoupons = 'POINT_EXCHANGE_POINTS_FOR_COUPONS',
  PointFirstApprovedVideo = 'POINT_FIRST_APPROVED_VIDEO',
  PointFirstComment = 'POINT_FIRST_COMMENT',
  PointFirstPost = 'POINT_FIRST_POST',
  PointFirstReview = 'POINT_FIRST_REVIEW',
  PointGift = 'POINT_GIFT',
  PointHave_20Followers = 'POINT_HAVE_20_FOLLOWERS',
  PointMonthlyActive = 'POINT_MONTHLY_ACTIVE',
  PointNormalComment = 'POINT_NORMAL_COMMENT',
  PointNormalReview = 'POINT_NORMAL_REVIEW',
  PointPostHasManyLikes = 'POINT_POST_HAS_MANY_LIKES',
  PointPostWithHighReactions = 'POINT_POST_WITH_HIGH_REACTIONS',
  PointReferee = 'POINT_REFEREE',
  PointReferral = 'POINT_REFERRAL',
  PointReferralBonus = 'POINT_REFERRAL_BONUS',
  PointReportedComment = 'POINT_REPORTED_COMMENT',
  PointRequestProduct = 'POINT_REQUEST_PRODUCT',
  PointSignUp = 'POINT_SIGN_UP',
  PointVideoLikes = 'POINT_VIDEO_LIKES',
  PointWeeklyActive = 'POINT_WEEKLY_ACTIVE',
  ReactComment = 'REACT_COMMENT',
  ReactPost = 'REACT_POST',
  ReplyComment = 'REPLY_COMMENT',
  ReplyPost = 'REPLY_POST',
  ReviewedEcommerceProduct = 'REVIEWED_ECOMMERCE_PRODUCT',
  ReviewApproved = 'REVIEW_APPROVED',
  ReviewRejected = 'REVIEW_REJECTED',
  ReviewReported = 'REVIEW_REPORTED',
  ShopCancelledEcommerceOrder = 'SHOP_CANCELLED_ECOMMERCE_ORDER',
  SystemCancelledEcommerceOrder = 'SYSTEM_CANCELLED_ECOMMERCE_ORDER',
  TrialEvent = 'TRIAL_EVENT',
  UserCancelledEcommerceOrder = 'USER_CANCELLED_ECOMMERCE_ORDER',
  UserRequest = 'USER_REQUEST'
}

export type NotificationOrderByInput = {
  created_at?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type NotificationWhereInput = {
  eventAdminType?: InputMaybe<Scalars['Float']>;
  id?: InputMaybe<Scalars['ID']>;
  idGt?: InputMaybe<Scalars['ID']>;
  idGte?: InputMaybe<Scalars['ID']>;
  idIn?: InputMaybe<Array<Scalars['ID']>>;
  idLt?: InputMaybe<Scalars['ID']>;
  idLte?: InputMaybe<Scalars['ID']>;
  idNot?: InputMaybe<Scalars['ID']>;
  idNotIn?: InputMaybe<Array<Scalars['ID']>>;
  isViewed?: InputMaybe<Scalars['Boolean']>;
  noObjectId?: InputMaybe<Scalars['Int']>;
  user?: InputMaybe<UserWhereUniqueInput>;
};

export type NotificationWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  noObjectId?: InputMaybe<Scalars['Int']>;
};

export type NotificationsAggregate = {
  __typename?: 'NotificationsAggregate';
  count: Scalars['Int'];
};

export type NotificationsConnection = {
  __typename?: 'NotificationsConnection';
  aggregate: NotificationsAggregate;
};

export enum OauthProvider {
  Apple = 'Apple',
  Facebook = 'Facebook',
  Instagram = 'Instagram'
}

export type ObjectEntityUnion = BannerImage | Brand | Category | OtherObjectType | Post | Product | ShopProduct;

export type ObjectTagging = {
  __typename?: 'ObjectTagging';
  bannerImages?: Maybe<Array<BannerImageTagging>>;
  content: Scalars['JSONObject'];
  createdAt?: Maybe<Scalars['DateTime']>;
  createdBy?: Maybe<Scalars['Int']>;
  id: Scalars['Int'];
  objectDetail?: Maybe<ObjectEntityUnion>;
  objectId?: Maybe<Scalars['String']>;
  objectType?: Maybe<ObjectTypeEnum>;
  order?: Maybe<Scalars['Int']>;
  posts?: Maybe<Array<PostTagging>>;
  products?: Maybe<Array<ShopProductTagging>>;
  tagId: Scalars['Int'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};


export type ObjectTaggingProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ObjectTaggingOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ObjectTaggingWhereInput>;
};

export type ObjectTaggingAggregate = {
  __typename?: 'ObjectTaggingAggregate';
  countObjectTagging: Scalars['Int'];
};

export type ObjectTaggingConnection = {
  __typename?: 'ObjectTaggingConnection';
  aggregate: ObjectTaggingAggregate;
};

export enum ObjectTaggingCreateCode {
  ObjectIdAlreadyExist = 'OBJECT_ID_ALREADY_EXIST'
}

export type ObjectTaggingError = Error & {
  __typename?: 'ObjectTaggingError';
  code: ObjectTaggingCreateCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export type ObjectTaggingInsertInput = {
  content?: InputMaybe<Scalars['JSONObject']>;
  objectId?: InputMaybe<Scalars['String']>;
  objectType?: InputMaybe<ObjectTypeEnum>;
  order?: InputMaybe<Scalars['Int']>;
  tagId: Scalars['Int'];
};

export type ObjectTaggingOrError = CommonError | ObjectTagging | ObjectTaggingError;

export type ObjectTaggingOrderByInput = {
  id?: InputMaybe<OrderBy>;
  order?: InputMaybe<OrderBy>;
};

export type ObjectTaggingUpdateInput = {
  content?: InputMaybe<Scalars['JSONObject']>;
  objectId?: InputMaybe<Scalars['String']>;
  objectType?: InputMaybe<ObjectTypeEnum>;
  order?: InputMaybe<Scalars['Int']>;
  tagId?: InputMaybe<Scalars['Int']>;
};

export type ObjectTaggingWhereInput = {
  fromDate?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['Int']>;
  objectId?: InputMaybe<Scalars['String']>;
  objectType?: InputMaybe<ObjectTypeEnum>;
  tagCode?: InputMaybe<Scalars['String']>;
  tagId?: InputMaybe<Scalars['Int']>;
  toDate?: InputMaybe<Scalars['DateTime']>;
};

export enum ObjectType {
  Event = 'EVENT',
  Funding = 'FUNDING',
  PreOrder = 'PRE_ORDER'
}

export enum ObjectTypeEnum {
  BannerImage = 'BANNER_IMAGE',
  Brand = 'BRAND',
  Category = 'CATEGORY',
  Hashtag = 'HASHTAG',
  Keyword = 'KEYWORD',
  OtherObject = 'OTHER_OBJECT',
  Post = 'POST',
  Product = 'PRODUCT',
  Review = 'REVIEW',
  ShopProduct = 'SHOP_PRODUCT'
}

export type Order = {
  __typename?: 'Order';
  address?: Maybe<Address>;
  cartItems: Array<CartItem>;
  cartItemsConnection: CartItemsConnection;
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  shop: Shop;
  status: OrderStatus;
  updatedAt: Scalars['DateTime'];
  user: User;
};


export type OrderCartItemsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CartItemOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CartItemWhereInput>;
};


export type OrderCartItemsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CartItemOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CartItemWhereInput>;
};

export enum OrderBy {
  Asc = 'ASC',
  AscNullsFirst = 'ASC_NULLS_FIRST',
  AscNullsLast = 'ASC_NULLS_LAST',
  Desc = 'DESC',
  DescNullsFirst = 'DESC_NULLS_FIRST',
  DescNullsLast = 'DESC_NULLS_LAST'
}

export type OrderCreateInput = {
  cartItems: CartItemCreateManyWithoutOrderInput;
  shippingAddress: ShippingAddressCreateOneWithoutOrderInput;
  shop: ShopCreateOneWithoutOrderInput;
};

export type OrderCreateOneWithoutCartItemInput = {
  connect: OrderWhereUniqueInput;
};

export type OrderError = Error & {
  __typename?: 'OrderError';
  code: OrderErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum OrderErrorCode {
  OrderCancelled = 'ORDER_CANCELLED',
  OrderConfirmed = 'ORDER_CONFIRMED'
}

export type OrderOrError = CommonError | Order | OrderError;

export type OrderOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type OrderShippingFeeInput = {
  address?: InputMaybe<Scalars['String']>;
  deliverOption: DeliverOption;
  district: Scalars['String'];
  orderShopInfo: Array<OrderShopInput>;
  province: Scalars['String'];
  ward?: InputMaybe<Scalars['String']>;
};

export type OrderShopInput = {
  shopId: Scalars['Int'];
  weight: Scalars['Float'];
};

export type OrderStatisticOfShop = {
  __typename?: 'OrderStatisticOfShop';
  billTotal?: Maybe<Scalars['Int']>;
  ecommFeeTotal?: Maybe<Scalars['Int']>;
  orderTotal?: Maybe<Scalars['Int']>;
  revenueTotal?: Maybe<Scalars['Int']>;
  shippingFeeTotal?: Maybe<Scalars['Int']>;
};

export type OrderStatisticWithPeriod = {
  __typename?: 'OrderStatisticWithPeriod';
  orderTotal?: Maybe<Array<Scalars['Int']>>;
  revenueTotal?: Maybe<Array<Scalars['Int']>>;
  time?: Maybe<Array<Scalars['String']>>;
};

export enum OrderStatus {
  Cancelled = 'CANCELLED',
  Confirmed = 'CONFIRMED',
  Created = 'CREATED'
}

export type OrderUpdateInput = {
  cartItems?: InputMaybe<CartItemUpdateManyWithoutOrderInput>;
  shippingAddress?: InputMaybe<ShippingAddressCreateOneWithoutOrderInput>;
};

export type OrderUpdateOneWithoutCartItemsInput = {
  connect: OrderWhereUniqueInput;
};

export type OrderWhereInput = {
  cartItemsSome?: InputMaybe<CartItemWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  shop?: InputMaybe<ShopWhereInput>;
  status?: InputMaybe<OrderStatus>;
  user?: InputMaybe<UserWhereInput>;
};

export type OrderWhereUniqueInput = {
  id: Scalars['Int'];
};

export type OrdersAggregate = {
  __typename?: 'OrdersAggregate';
  count: Scalars['Int'];
};

export type OrdersConnection = {
  __typename?: 'OrdersConnection';
  aggregate: OrdersAggregate;
};

export type OtherObjectType = {
  __typename?: 'OtherObjectType';
  title: Scalars['String'];
};

export enum PackagingtUnit {
  Box = 'BOX',
  Pack = 'PACK',
  Pill = 'PILL',
  /** Tablespoon */
  Tbsp = 'TBSP',
  Tube = 'TUBE'
}

export enum PaymentMethod {
  Cod = 'COD',
  Vnpay = 'VNPAY'
}

export enum PeriodType {
  Day = 'DAY',
  Month = 'MONTH',
  Quarter = 'QUARTER',
  Week = 'WEEK'
}

export type PointEntityUnion = Post | PostComment | Review | User | VoucherOrder;

export type PointHistoriesAggregate = {
  __typename?: 'PointHistoriesAggregate';
  count: Scalars['Int'];
  sum: Scalars['Int'];
};

export type PointHistoriesConnection = {
  __typename?: 'PointHistoriesConnection';
  aggregate: PointHistoriesAggregate;
};

export type PointHistory = {
  __typename?: 'PointHistory';
  createdAt: Scalars['DateTime'];
  entity?: Maybe<PointEntityUnion>;
  id: Scalars['Int'];
  point: Scalars['Int'];
  status: PointStatus;
  type: PointType;
};

export type PointHistoryOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type PointHistoryStatistic = {
  __typename?: 'PointHistoryStatistic';
  pointTotal?: Maybe<Array<Scalars['Int']>>;
  time?: Maybe<Array<Scalars['String']>>;
};

export type PointHistoryStatisticWhereInput = {
  fromDate?: InputMaybe<Scalars['DateTime']>;
  status?: InputMaybe<Scalars['Boolean']>;
  toDate?: InputMaybe<Scalars['DateTime']>;
};

export type PointHistoryWhereInput = {
  pointGt?: InputMaybe<Scalars['Int']>;
  redeemable?: InputMaybe<Scalars['Boolean']>;
  review?: InputMaybe<ReviewWhereInput>;
  typeNot?: InputMaybe<PointType>;
  user?: InputMaybe<UserWhereInput>;
};

export enum PointStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Rejected = 'REJECTED'
}

export enum PointType {
  ApprovedReviewReport = 'APPROVED_REVIEW_REPORT',
  ApprovedVideo = 'APPROVED_VIDEO',
  Attend_5TrialEvent = 'ATTEND_5_TRIAL_EVENT',
  Attend_10TrialEvent = 'ATTEND_10_TRIAL_EVENT',
  BestAnswer = 'BEST_ANSWER',
  DailyActive = 'DAILY_ACTIVE',
  EditProduct = 'EDIT_PRODUCT',
  EventPoints = 'EVENT_POINTS',
  ExchangePointsForCoupons = 'EXCHANGE_POINTS_FOR_COUPONS',
  First_5ApprovedReviews = 'FIRST_5_APPROVED_REVIEWS',
  First_10ApprovedReviews = 'FIRST_10_APPROVED_REVIEWS',
  First_50ApprovedReviews = 'FIRST_50_APPROVED_REVIEWS',
  FirstApprovedVideo = 'FIRST_APPROVED_VIDEO',
  FirstComment = 'FIRST_COMMENT',
  FirstPost = 'FIRST_POST',
  FirstReview = 'FIRST_REVIEW',
  Follow_20Users = 'FOLLOW_20_USERS',
  Gift = 'GIFT',
  Have_20Followers = 'HAVE_20_FOLLOWERS',
  Have_50Followers = 'HAVE_50_FOLLOWERS',
  Have_100Followers = 'HAVE_100_FOLLOWERS',
  MonthlyActive = 'MONTHLY_ACTIVE',
  NormalComment = 'NORMAL_COMMENT',
  NormalReview = 'NORMAL_REVIEW',
  PostHasManyLikes = 'POST_HAS_MANY_LIKES',
  PostWithHighReactions = 'POST_WITH_HIGH_REACTIONS',
  Referee = 'REFEREE',
  Referral = 'REFERRAL',
  ReferralBonus = 'REFERRAL_BONUS',
  ReportedComment = 'REPORTED_COMMENT',
  RequestProduct = 'REQUEST_PRODUCT',
  SignUp = 'SIGN_UP',
  VideoLikes = 'VIDEO_LIKES',
  Voucher = 'VOUCHER',
  WeeklyActive = 'WEEKLY_ACTIVE'
}

export type Post = {
  __typename?: 'Post';
  author: User;
  bestAnswer?: Maybe<PostComment>;
  bookMark?: Maybe<Scalars['Boolean']>;
  category?: Maybe<PostCategory>;
  commentedAt?: Maybe<Scalars['DateTime']>;
  comments?: Maybe<Array<PostComment>>;
  commentsConnection: PostCommentsConnection;
  content?: Maybe<Scalars['String']>;
  countPostLoaderViewedByUser?: Maybe<Scalars['Float']>;
  countViewedPostConnection?: Maybe<Scalars['Float']>;
  createdAt: Scalars['DateTime'];
  fakePostView?: Maybe<Scalars['Float']>;
  hashtags?: Maybe<Array<Hashtag>>;
  id: Scalars['Int'];
  images: Array<Image>;
  imagesConnection: PostImagesConnection;
  isBookMark?: Maybe<Scalars['Boolean']>;
  isUnsubscribedByViewer?: Maybe<Scalars['Boolean']>;
  points?: Maybe<Scalars['Int']>;
  postToProducts?: Maybe<Array<PostToProduct>>;
  products: Array<Product>;
  productsConnection: PostProductsConnection;
  /** @deprecated deprecationReason: 'use reactionOfViewer instead', */
  reactStatus: ReactStatus;
  reactionOfViewer: ReactStatus;
  reactionsConnection: PostReactionsConnection;
  realPostView?: Maybe<Scalars['Float']>;
  review?: Maybe<Review>;
  savedByViewer?: Maybe<Scalars['Boolean']>;
  savedDateByViewer?: Maybe<Scalars['DateTime']>;
  shopProductViewStore?: Maybe<ProductViewStore>;
  slug?: Maybe<Scalars['String']>;
  status: PostStatus;
  title?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
  video?: Maybe<Video>;
  videos?: Maybe<Array<VideoObject>>;
  viewedPostStatistic?: Maybe<ViewedPostStatistic>;
};


export type PostCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type PostCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type PostImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type PostProductsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};

export type PostArgsWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type PostCategory = {
  __typename?: 'PostCategory';
  id: Scalars['Int'];
  isCreatable: Scalars['Boolean'];
  menuOrder: Scalars['Int'];
  translations: Array<PostCategoryTranslation>;
};


export type PostCategoryTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCategoryTranslationWhereInput>;
};

export type PostCategoryCreateOneWithoutPostsInput = {
  connect?: InputMaybe<PostCategoryWhereUniqueInput>;
};

export type PostCategoryOrderByInput = {
  id?: InputMaybe<OrderBy>;
  menuOrder?: InputMaybe<OrderBy>;
};

export type PostCategoryTranslation = {
  __typename?: 'PostCategoryTranslation';
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isOriginal: Scalars['Boolean'];
  language: Scalars['String'];
  name: Scalars['String'];
};

export type PostCategoryTranslationWhereInput = {
  language?: InputMaybe<LanguageCode>;
};

export type PostCategoryUpdateOneWithoutPostsInput = {
  connect?: InputMaybe<PostCategoryWhereUniqueInput>;
};

export type PostCategoryWhereInput = {
  id?: InputMaybe<Scalars['ID']>;
  idGt?: InputMaybe<Scalars['ID']>;
  idGte?: InputMaybe<Scalars['ID']>;
  idIn?: InputMaybe<Array<Scalars['ID']>>;
  idLt?: InputMaybe<Scalars['ID']>;
  idLte?: InputMaybe<Scalars['ID']>;
  idNot?: InputMaybe<Scalars['ID']>;
  idNotIn?: InputMaybe<Array<Scalars['ID']>>;
  isCreatable?: InputMaybe<Scalars['Boolean']>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type PostCategoryWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
};

export type PostComment = {
  __typename?: 'PostComment';
  author: User;
  children: Array<PostComment>;
  childrenConnection: PostCommentChildrenConnection;
  content?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  images: Array<Image>;
  imagesConnection: PostCommentImagesConnection;
  parent?: Maybe<PostComment>;
  post: Post;
  /** @deprecated use reactionOfViewer instead */
  reactStatus: ReactStatus;
  reactionConnections: PostCommentReactionsConnection;
  reactionOfViewer: ReactStatus;
  status: PostCommentStatus;
  updatedAt: Scalars['DateTime'];
};


export type PostCommentChildrenArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type PostCommentChildrenConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type PostCommentImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};

export type PostCommentChildrenAggregate = {
  __typename?: 'PostCommentChildrenAggregate';
  count: Scalars['Int'];
};

export type PostCommentChildrenConnection = {
  __typename?: 'PostCommentChildrenConnection';
  aggregate: PostCommentChildrenAggregate;
};

export type PostCommentCreateInput = {
  content?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<ImageCreateManyWithoutPostCommentInput>;
  mentions?: InputMaybe<Array<UserWhereUniqueInput>>;
  parent?: InputMaybe<PostCommentCreateOneWithoutChildrenInput>;
  post: PostCreateOneWithoutPostCommentsInput;
  review?: InputMaybe<ReviewCreateOneWithoutPostCommentsInput>;
};

export type PostCommentCreateManyWithoutPostInput = {
  create?: InputMaybe<Array<PostCommentCreateWithoutPostInput>>;
};

export type PostCommentCreateOneWithoutChildrenInput = {
  connect?: InputMaybe<PostCommentWhereUniqueInput>;
};

export type PostCommentCreateOneWithoutReportsInput = {
  connect?: InputMaybe<PostCommentWhereUniqueInput>;
};

export type PostCommentCreateWithoutPostInput = {
  content?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<ImageCreateManyWithoutPostCommentInput>;
  mentions?: InputMaybe<Array<UserWhereUniqueInput>>;
};

export type PostCommentImagesAggregate = {
  __typename?: 'PostCommentImagesAggregate';
  count: Scalars['Int'];
};

export type PostCommentImagesConnection = {
  __typename?: 'PostCommentImagesConnection';
  aggregate: PostCommentImagesAggregate;
};

export type PostCommentOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type PostCommentReactionsAggregate = {
  __typename?: 'PostCommentReactionsAggregate';
  sum: PostCommentReactionsSum;
};

export type PostCommentReactionsConnection = {
  __typename?: 'PostCommentReactionsConnection';
  aggregate: PostCommentReactionsAggregate;
};

export type PostCommentReactionsSum = {
  __typename?: 'PostCommentReactionsSum';
  value: Scalars['Int'];
};

export type PostCommentReport = {
  __typename?: 'PostCommentReport';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  postComment: PostComment;
  postCommentId: Scalars['Int'];
  reason?: Maybe<Scalars['String']>;
  status: PostCommentReportStatus;
  type: PostCommentReportType;
  updatedAt: Scalars['DateTime'];
  user: User;
  userId: Scalars['Int'];
};

export type PostCommentReportCreateInput = {
  postComment: PostCommentCreateOneWithoutReportsInput;
  reason?: InputMaybe<Scalars['String']>;
  type?: InputMaybe<PostCommentReportType>;
};

export type PostCommentReportOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export enum PostCommentReportStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Rejected = 'REJECTED'
}

export enum PostCommentReportType {
  Advertising = 'ADVERTISING',
  Harass = 'HARASS',
  Other = 'OTHER',
  PersonalInformation = 'PERSONAL_INFORMATION',
  Spam = 'SPAM',
  Swearing = 'SWEARING'
}

export type PostCommentReportWhereInput = {
  comment?: InputMaybe<PostCommentWhereInput>;
  post?: InputMaybe<PostWhereInput>;
  reasonContains?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<PostCommentReportStatus>;
  user?: InputMaybe<UserWhereInput>;
};

export type PostCommentReportsAggregate = {
  __typename?: 'PostCommentReportsAggregate';
  count: Scalars['Int'];
};

export type PostCommentReportsConnection = {
  __typename?: 'PostCommentReportsConnection';
  aggregate: PostCommentReportsAggregate;
};

export enum PostCommentStatus {
  Created = 'CREATED',
  Hidden = 'HIDDEN',
  Reported = 'REPORTED'
}

export type PostCommentUpdateInput = {
  content?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<ImageUpdateManyWithoutPostCommentInput>;
};

export type PostCommentWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  idGt?: InputMaybe<Scalars['Int']>;
  idGte?: InputMaybe<Scalars['Int']>;
  idIsNull?: InputMaybe<Scalars['Boolean']>;
  idLt?: InputMaybe<Scalars['Int']>;
  idLte?: InputMaybe<Scalars['Int']>;
  parent?: InputMaybe<PostCommentWhereInput>;
  post?: InputMaybe<PostWhereInput>;
};

export type PostCommentWhereUniqueInput = {
  id: Scalars['Int'];
};

export type PostCommentWhereUserInput = {
  id: Scalars['Int'];
};

export type PostCommentsAggregate = {
  __typename?: 'PostCommentsAggregate';
  count: Scalars['Int'];
};

export type PostCommentsConnection = {
  __typename?: 'PostCommentsConnection';
  aggregate: PostCommentsAggregate;
};

export type PostCreateError = Error & {
  __typename?: 'PostCreateError';
  code: PostCreateErrorCode;
  currentValue?: Maybe<Scalars['Int']>;
  expectedValue?: Maybe<Scalars['Int']>;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum PostCreateErrorCode {
  ExceedMaxSharedPointsPerDay = 'EXCEED_MAX_SHARED_POINTS_PER_DAY',
  HaveNotVerifiedEmail = 'HAVE_NOT_VERIFIED_EMAIL',
  HaveNotVerifiedPhoneNumber = 'HAVE_NOT_VERIFIED_PHONE_NUMBER',
  NotEnoughPoints = 'NOT_ENOUGH_POINTS'
}

export type PostCreateInput = {
  category?: InputMaybe<PostCategoryCreateOneWithoutPostsInput>;
  comments?: InputMaybe<PostCommentCreateManyWithoutPostInput>;
  content?: InputMaybe<Scalars['String']>;
  hashtags?: InputMaybe<Array<HashtagInput>>;
  images?: InputMaybe<ImageCreateManyWithoutPostInput>;
  isNewApi?: InputMaybe<Scalars['Boolean']>;
  mentions?: InputMaybe<Array<UserWhereUniqueInput>>;
  points?: InputMaybe<Scalars['Int']>;
  products?: InputMaybe<ProductCreateManyWithoutPostInput>;
  review?: InputMaybe<ReviewCreateOneWithoutPostInput>;
  shopProductId?: InputMaybe<Scalars['Int']>;
  slug?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  videoKey?: InputMaybe<Scalars['String']>;
  videos?: InputMaybe<VideoCreateManyWithoutPostInput>;
};

export type PostCreateOneWithoutPostCommentsInput = {
  connect?: InputMaybe<PostWhereUniqueInput>;
  create?: InputMaybe<PostCreateWithoutPostCommentsInput>;
};

export type PostCreateOneWithoutReportsInput = {
  connect?: InputMaybe<PostWhereUniqueInput>;
};

export type PostCreateWithoutPostCommentsInput = {
  review?: InputMaybe<ReviewCreateOneWithoutPostInput>;
};

export type PostImagesAggregate = {
  __typename?: 'PostImagesAggregate';
  count: Scalars['Int'];
};

export type PostImagesConnection = {
  __typename?: 'PostImagesConnection';
  aggregate: PostImagesAggregate;
};

export type PostOrError = CommonError | Post | PostCreateError;

export type PostOrderByInput = {
  clicksAggregate?: InputMaybe<Scalars['String']>;
  commentedAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  likesAggregate?: InputMaybe<Scalars['String']>;
  newest?: InputMaybe<OrderBy>;
};

export type PostProductsAggregate = {
  __typename?: 'PostProductsAggregate';
  count: Scalars['Int'];
};

export type PostProductsConnection = {
  __typename?: 'PostProductsConnection';
  aggregate: PostProductsAggregate;
};

export enum PostQueryType {
  Discovery = 'DISCOVERY',
  Following = 'FOLLOWING',
  Normal = 'NORMAL'
}

export type PostReactionsAggregate = {
  __typename?: 'PostReactionsAggregate';
  sum: PostReactionsSum;
};

export type PostReactionsConnection = {
  __typename?: 'PostReactionsConnection';
  aggregate: PostReactionsAggregate;
};

export type PostReactionsSum = {
  __typename?: 'PostReactionsSum';
  value: Scalars['Int'];
};

export type PostRecommentdationEntityUnion = Post | Review;

export type PostReport = {
  __typename?: 'PostReport';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  post: Post;
  postId: Scalars['Int'];
  reason?: Maybe<Scalars['String']>;
  status?: Maybe<PostReportStatus>;
  type: PostReportType;
  updatedAt: Scalars['DateTime'];
  user: User;
  userId: Scalars['Int'];
};

export type PostReportCreateInput = {
  post: PostCreateOneWithoutReportsInput;
  reason?: InputMaybe<Scalars['String']>;
  type?: InputMaybe<PostReportType>;
};

export enum PostReportStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Rejected = 'REJECTED'
}

export enum PostReportType {
  Advertising = 'ADVERTISING',
  Other = 'OTHER',
  PersonalInformation = 'PERSONAL_INFORMATION',
  Repeat = 'REPEAT',
  Swearing = 'SWEARING'
}

export type PostReservedSlot = {
  __typename?: 'PostReservedSlot';
  id: Scalars['Int'];
  post?: Maybe<Post>;
  post_id: Scalars['String'];
  slot_name: Scalars['String'];
  slot_position: Scalars['String'];
  type: PostReservedSlotType;
};

export type PostReservedSlotConnection = {
  __typename?: 'PostReservedSlotConnection';
  total: Scalars['Int'];
};

export type PostReservedSlotCreateInput = {
  post_id: Scalars['Float'];
  slot_name: Scalars['String'];
  slot_position: Scalars['String'];
  type: Scalars['String'];
};

export type PostReservedSlotHistory = {
  __typename?: 'PostReservedSlotHistory';
  id: Scalars['Int'];
  post?: Maybe<Post>;
  post_id: Scalars['Float'];
  slot_id?: Maybe<Scalars['Float']>;
  updated_at: Scalars['DateTime'];
  updated_user: Scalars['Float'];
  user: User;
};

export type PostReservedSlotInput = {
  post_id: Scalars['Float'];
  slot_name?: InputMaybe<Scalars['String']>;
  slot_position?: InputMaybe<Scalars['String']>;
  type: Scalars['String'];
};

export type PostReservedSlotOrderByInput = {
  id?: InputMaybe<OrderBy>;
  updatedAt?: InputMaybe<OrderBy>;
};

export enum PostReservedSlotType {
  Mobile = 'MOBILE',
  Web = 'WEB'
}

export type PostReservedSlotWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  post_id?: InputMaybe<Scalars['Int']>;
  slot_name?: InputMaybe<Scalars['String']>;
  slot_position?: InputMaybe<Scalars['Int']>;
  type?: InputMaybe<Scalars['String']>;
};

export enum PostStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Hidden = 'HIDDEN'
}

export type PostTagging = {
  __typename?: 'PostTagging';
  id: Scalars['Int'];
  post?: Maybe<Post>;
};

export type PostToProduct = {
  __typename?: 'PostToProduct';
  externalLinks?: Maybe<Array<ExternalLink>>;
  price?: Maybe<Scalars['Int']>;
  product: Product;
  productId: Scalars['Int'];
  shop?: Maybe<Shop>;
  shopId?: Maybe<Scalars['Int']>;
};

export type PostUpdateInput = {
  category?: InputMaybe<PostCategoryUpdateOneWithoutPostsInput>;
  content?: InputMaybe<Scalars['String']>;
  hashtags?: InputMaybe<Array<HashtagInput>>;
  images?: InputMaybe<ImageUpdateManyWithoutPostInput>;
  products?: InputMaybe<ProductUpdateManyWithoutPostInput>;
  shopProductId?: InputMaybe<Scalars['Int']>;
  slug?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  video?: InputMaybe<VideoUpdateOneWithoutPostInput>;
  videos?: InputMaybe<VideoUpdateManyWithoutPostInput>;
};

export type PostWhereInput = {
  author?: InputMaybe<UserWhereInput>;
  category?: InputMaybe<PostCategoryWhereInput>;
  commentedAtLt?: InputMaybe<Scalars['DateTime']>;
  contentContains?: InputMaybe<Scalars['String']>;
  createdAtGte?: InputMaybe<Scalars['DateTime']>;
  createdAtLte?: InputMaybe<Scalars['DateTime']>;
  hashtagsSome?: InputMaybe<HashtagWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  idGt?: InputMaybe<Scalars['Int']>;
  idGte?: InputMaybe<Scalars['Int']>;
  idLt?: InputMaybe<Scalars['Int']>;
  idLte?: InputMaybe<Scalars['Int']>;
  inId?: InputMaybe<Array<Scalars['Int']>>;
  isBookMark?: InputMaybe<Scalars['Boolean']>;
  productsSome?: InputMaybe<ProductWhereInput>;
  queryType?: InputMaybe<PostQueryType>;
  review?: InputMaybe<ReviewWhereInput>;
  status?: InputMaybe<PostStatus>;
  text?: InputMaybe<Scalars['String']>;
  video?: InputMaybe<VideoWhereInput>;
};

export type PostWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type PostsAggregate = {
  __typename?: 'PostsAggregate';
  count: Scalars['Int'];
};

export type PostsConnection = {
  __typename?: 'PostsConnection';
  aggregate: PostsAggregate;
};

export type PresignedUrl = {
  __typename?: 'PresignedUrl';
  key: Scalars['String'];
  url: Scalars['String'];
};

export type Product = {
  __typename?: 'Product';
  alreadySoldByViewer: Scalars['Boolean'];
  amount?: Maybe<Scalars['Float']>;
  amountUnit?: Maybe<Scalars['String']>;
  attributes?: Maybe<ProductAttribute>;
  barcodes: Array<Barcode>;
  barcodesConnection: BarcodesConnection;
  /**
   * Number of users bookmark this product
   * @deprecated use numberOfUserBookmarks instead
   */
  bookmarkedByUser: Scalars['Int'];
  brand: Brand;
  categories: Array<Category>;
  customerServicePhone?: Maybe<Scalars['String']>;
  expiry?: Maybe<Scalars['String']>;
  /** @deprecated please use functionsV2 instead */
  functions: Array<SpecialIngredientFunction>;
  /** @deprecated please use functionsConnectionV2 instead, this field will be removed on 01/01/2022 */
  functionsConnection: SpecialIngredientFunctionsConnection;
  functionsConnectionV2: FunctionsConnection;
  functionsV2: Array<Function>;
  highlightProductRanking?: Maybe<HighlightProduct>;
  id: Scalars['Int'];
  images: Array<Image>;
  imagesConnection: ImagesConnection;
  ingredientCautionsConnection: IngredientCautionsConnection;
  ingredients: Array<Ingredient>;
  ingredientsConnection: IngredientsConnection;
  /** Whether or not product is used by viewer */
  isUsedByViewer?: Maybe<Scalars['Boolean']>;
  manufacturer?: Maybe<Scalars['String']>;
  manufacturerAddress?: Maybe<Scalars['String']>;
  measure: Scalars['String'];
  /** Number of users bookmark this product */
  numberOfUserBookmarks: Scalars['Int'];
  /** Number of users using this product */
  numberOfUsers: Scalars['Int'];
  postsConnection: PostsConnection;
  price?: Maybe<Scalars['Int']>;
  priceUnit: Scalars['String'];
  productToIngredients: Array<ProductToIngredient>;
  productToIngredientsConnection: ProductToIngredientsConnection;
  productToVideos?: Maybe<Array<Post>>;
  rankings?: Maybe<Array<ProductRanking>>;
  rankingsV2: Array<ProductRanking>;
  reviewedByViewer: Scalars['Boolean'];
  reviews?: Maybe<Array<Review>>;
  reviewsConnection: ReviewsConnection;
  reviewsCountByRate: ReviewsCountByRate;
  soldByShops: Scalars['Boolean'];
  status: ProductStatus;
  stockInfoOfViewer?: Maybe<ShopToProduct>;
  targetAudience?: Maybe<Scalars['String']>;
  thumbnail?: Maybe<Image>;
  translations: Array<ProductTranslation>;
  type: ProductType;
  uid: Scalars['ID'];
  updatedAt: Scalars['DateTime'];
  videoTitle?: Maybe<Scalars['String']>;
  videoUrl?: Maybe<Scalars['String']>;
  wishedByViewer: Scalars['Boolean'];
};


export type ProductBarcodesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type ProductBarcodesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BarcodeOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BarcodeWhereInput>;
};


export type ProductFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SpecialIngredientFunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SpecialIngredientFunctionWhereInput>;
};


export type ProductFunctionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SpecialIngredientFunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SpecialIngredientFunctionWhereInput>;
};


export type ProductFunctionsConnectionV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type ProductFunctionsV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type ProductImagesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ImageWhereInput>;
};


export type ProductImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ImageWhereInput>;
};


export type ProductIngredientCautionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type ProductIngredientsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientWhereInput>;
};


export type ProductIngredientsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientWhereInput>;
};


export type ProductPostsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<PostOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostWhereInput>;
};


export type ProductProductToIngredientsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductToIngredientOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductToIngredientWhereInput>;
};


export type ProductProductToIngredientsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductToIngredientOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductToIngredientWhereInput>;
};


export type ProductRankingsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type ProductRankingsV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type ProductReviewsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type ProductReviewsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type ProductTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductTranslationWhereInput>;
};

export type ProductAttribute = {
  __typename?: 'ProductAttribute';
  /** Energy of a packaging unit */
  energyPerPackage?: Maybe<Scalars['Float']>;
  energyUnit?: Maybe<Unit>;
  /** Net volume of a packaging unit */
  netVolumePerPackage?: Maybe<Scalars['Float']>;
  /** Net weight of a packaging unit */
  netWeightPerPackage?: Maybe<Scalars['Float']>;
  /** Reference Daily Intake Unit */
  packagingUnit?: Maybe<PackagingtUnit>;
  /** Reference Daily Intake = number of packaging unit */
  rdi?: Maybe<Scalars['Float']>;
  volumeUnit?: Maybe<Unit>;
  weightUnit?: Maybe<Unit>;
};

export type ProductCreateManyWithoutPostInput = {
  connect?: InputMaybe<Array<ProductWhereUniqueInput>>;
};

export type ProductCreateNestedOneWithoutProductUsageLogInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductCreateOneWithoutEditInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductCreateOneWithoutLiveStreamProductInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductCreateOneWithoutShopToProductsInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductEdit = {
  __typename?: 'ProductEdit';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  images?: Maybe<Array<Scalars['String']>>;
  status?: Maybe<ProductEditStatus>;
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type ProductEditCreateInput = {
  content: Scalars['String'];
  images?: InputMaybe<Array<Scalars['String']>>;
  product: ProductCreateOneWithoutEditInput;
};

export enum ProductEditStatus {
  Created = 'CREATED'
}

export type ProductOrderByInput = {
  id?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
  productToIngredients?: InputMaybe<ProductToIngredientOrderByInput>;
  reviewsConnection?: InputMaybe<ReviewsAggregateOrderByInput>;
  updatedAt?: InputMaybe<OrderBy>;
};

export type ProductRanking = {
  __typename?: 'ProductRanking';
  category: Category;
  product: Product;
  ranking: Scalars['Int'];
  rankingChange?: Maybe<Scalars['Int']>;
};

export type ProductRankingBrandIds = {
  __typename?: 'ProductRankingBrandIds';
  brandIds: Array<Scalars['Int']>;
};

export type ProductRankingOrderByInput = {
  product?: InputMaybe<ProductOrderByInput>;
  ranking?: InputMaybe<OrderBy>;
};

export type ProductRankingWhereInput = {
  category?: InputMaybe<CategoryWhereInput>;
  product?: InputMaybe<ProductWhereInput>;
};

export type ProductRankingsAggregate = {
  __typename?: 'ProductRankingsAggregate';
  count: Scalars['Int'];
  countV2: Scalars['Int'];
};

export type ProductRankingsConnection = {
  __typename?: 'ProductRankingsConnection';
  aggregate: ProductRankingsAggregate;
};

export type ProductRequest = {
  __typename?: 'ProductRequest';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  images?: Maybe<Array<Scalars['String']>>;
  isDone: Scalars['Boolean'];
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type ProductRequestCreateInput = {
  content: Scalars['String'];
  images?: InputMaybe<Array<Scalars['String']>>;
};

export type ProductRequestOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type ProductRequestWhereInput = {
  contentContains?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['Boolean']>;
};

export type ProductRequestsAggregate = {
  __typename?: 'ProductRequestsAggregate';
  count: Scalars['Int'];
};

export type ProductRequestsConnection = {
  __typename?: 'ProductRequestsConnection';
  aggregate: ProductRequestsAggregate;
};

export type ProductSearchOrderByInput = {
  avg?: InputMaybe<ReviewAvgOrderByInput>;
  count?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
};

export type ProductSearchResult = {
  __typename?: 'ProductSearchResult';
  brandIds: Array<Scalars['Int']>;
  categoryIdParentChildrens: Array<Scalars['Int']>;
  categoryIdRoots: Array<Category>;
  products: Array<Product>;
  total: Scalars['Int'];
};

export enum ProductStatus {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export type ProductToIngredient = {
  __typename?: 'ProductToIngredient';
  attributes?: Maybe<ProductToIngredientAttribute>;
  ingredient: Ingredient;
  ingredientId: Scalars['ID'];
  level?: Maybe<ProductToIngredientLevel>;
  order?: Maybe<Scalars['Int']>;
  productId: Scalars['ID'];
};

export type ProductToIngredientAttribute = {
  __typename?: 'ProductToIngredientAttribute';
  /** Amount of ingredient in a smallest packaging of product */
  amount?: Maybe<Scalars['Float']>;
  amountUnit?: Maybe<Unit>;
};

export enum ProductToIngredientLevel {
  Major = 'MAJOR',
  Minor = 'MINOR'
}

export type ProductToIngredientOrderByInput = {
  amount?: InputMaybe<OrderBy>;
  ingredient?: InputMaybe<IngredientOrderByInput>;
  level?: InputMaybe<OrderBy>;
  order?: InputMaybe<OrderBy>;
};

export type ProductToIngredientWhereInput = {
  ingredient?: InputMaybe<IngredientWhereInput>;
  level?: InputMaybe<ProductToIngredientLevel>;
  product?: InputMaybe<ProductWhereInput>;
};

export type ProductToIngredientsAggregate = {
  __typename?: 'ProductToIngredientsAggregate';
  count: Scalars['Int'];
};

export type ProductToIngredientsConnection = {
  __typename?: 'ProductToIngredientsConnection';
  aggregate: ProductToIngredientsAggregate;
};

export type ProductTranslation = {
  __typename?: 'ProductTranslation';
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  imageDescription?: Maybe<Scalars['String']>;
  isOriginal: Scalars['Boolean'];
  language: LanguageCode;
  name: Scalars['String'];
  product: Product;
  slug?: Maybe<Scalars['String']>;
};

export type ProductTranslationOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type ProductTranslationWhereInput = {
  language?: InputMaybe<LanguageCode>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export enum ProductType {
  Cosmetic = 'COSMETIC',
  FunctionalFood = 'FUNCTIONAL_FOOD'
}

export type ProductUpdateManyWithoutPostInput = {
  set?: InputMaybe<Array<ProductWhereUniqueInput>>;
};

export type ProductUpdateOneWithoutLiveStreamProductInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductUsageLog = {
  __typename?: 'ProductUsageLog';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  product: Product;
  productId: Scalars['Int'];
  userId: Scalars['Int'];
};

export type ProductUsageLogCreateInput = {
  product: ProductCreateNestedOneWithoutProductUsageLogInput;
};

export type ProductUsageLogOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type ProductUsageLogWhereInput = {
  createdAt?: InputMaybe<DateTimeFilter>;
  productId?: InputMaybe<Scalars['ID']>;
  userId?: InputMaybe<Scalars['ID']>;
};

export type ProductVariant = {
  __typename?: 'ProductVariant';
  attributes?: Maybe<Array<Attribute>>;
  id: Scalars['Int'];
  images?: Maybe<Images>;
  isBlocked?: Maybe<Scalars['Boolean']>;
  isVisible?: Maybe<Scalars['Boolean']>;
  originalPrice: Scalars['Int'];
  price: Scalars['Int'];
  productId: Scalars['Int'];
  quantity: Scalars['Int'];
  shopProduct?: Maybe<ShopProduct>;
  soldQuantity: Scalars['Int'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type ProductVariantConnection = {
  __typename?: 'ProductVariantConnection';
  aggregate: ProductViewstoreAggregate;
};

export type ProductVariantInsertInput = {
  attributes?: InputMaybe<AttributeUpsertInput>;
  images?: InputMaybe<ImageUpsertInput>;
  originalPrice: Scalars['Int'];
  price: Scalars['Int'];
  quantity: Scalars['Int'];
};

export type ProductVariantOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
  soldQuantity?: InputMaybe<OrderBy>;
};

export type ProductVariantUpdateInput = {
  attributes?: InputMaybe<AttributeUpsertInput>;
  images?: InputMaybe<ImageUpsertInput>;
  isBlocked?: InputMaybe<Scalars['Boolean']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  originalPrice?: InputMaybe<Scalars['Int']>;
  price?: InputMaybe<Scalars['Int']>;
  quantity?: InputMaybe<Scalars['Int']>;
  variantId?: InputMaybe<Scalars['Int']>;
};

export type ProductVariantWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  price?: InputMaybe<IntFilter>;
  productId?: InputMaybe<Scalars['Int']>;
  quantity?: InputMaybe<IntFilter>;
  soldQuantity?: InputMaybe<IntFilter>;
};

export type ProductViewStore = {
  __typename?: 'ProductViewStore';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  isBlocked?: Maybe<Scalars['Boolean']>;
  isVisible?: Maybe<Scalars['Boolean']>;
  post?: Maybe<Post>;
  postId?: Maybe<Scalars['Int']>;
  productId?: Maybe<Scalars['Int']>;
  shopId?: Maybe<Scalars['Int']>;
  shopProduct?: Maybe<ShopProduct>;
};

export type ProductViewStoreConnection = {
  __typename?: 'ProductViewStoreConnection';
  aggregate: ProductViewstoreAggregate;
};

export type ProductViewStoreOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type ProductViewStoreWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  isBlocked?: InputMaybe<Scalars['Boolean']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  postId?: InputMaybe<Scalars['Int']>;
  productId?: InputMaybe<Scalars['Int']>;
  productName?: InputMaybe<Scalars['String']>;
  shopId?: InputMaybe<Scalars['Int']>;
};

export type ProductViewstoreAggregate = {
  __typename?: 'ProductViewstoreAggregate';
  count: Scalars['Int'];
};

export type ProductWhereInput = {
  _id?: InputMaybe<IntFilter>;
  brand?: InputMaybe<BrandWhereInput>;
  brandFilter?: InputMaybe<Array<Scalars['Int']>>;
  brandTextFilter?: InputMaybe<Scalars['String']>;
  categoriesSome?: InputMaybe<CategoryWhereInput>;
  categoryFilter?: InputMaybe<Array<Scalars['Int']>>;
  fansSome?: InputMaybe<UserWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  ingredientsSome?: InputMaybe<IngredientWhereInput>;
  isFunctionalFood?: InputMaybe<BooleanFilter>;
  isUsedByViewer?: InputMaybe<BooleanFilter>;
  nameContains?: InputMaybe<Scalars['String']>;
  price?: InputMaybe<IntFilter>;
  productTextsSearch?: InputMaybe<Array<Scalars['String']>>;
  reviewsSome?: InputMaybe<ReviewWhereInput>;
  shopsSome?: InputMaybe<ShopWhereInput>;
  status?: InputMaybe<ProductStatus>;
  type?: InputMaybe<ProductType>;
  uid?: InputMaybe<Scalars['ID']>;
  updatedAtGte?: InputMaybe<Scalars['DateTime']>;
};

export type ProductWhereUniqueInput = {
  barcode?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type ProductsAggregate = {
  __typename?: 'ProductsAggregate';
  count: Scalars['Int'];
  max: ProductsMax;
};

export type ProductsConnection = {
  __typename?: 'ProductsConnection';
  aggregate: ProductsAggregate;
};

export type ProductsMax = {
  __typename?: 'ProductsMax';
  price: Scalars['Float'];
};

export enum PromotionAmountUnitType {
  Percent = 'PERCENT',
  Vnd = 'VND'
}

export enum PromotionCreatedByType {
  Reviewty = 'REVIEWTY',
  Shop = 'SHOP'
}

export enum PromotionEntityType {
  All = 'ALL',
  Product = 'PRODUCT',
  Shop = 'SHOP',
  Variant = 'VARIANT'
}

export enum PromotionType {
  Discount = 'DISCOUNT',
  FlashSale = 'FLASH_SALE',
  Freeship = 'FREESHIP',
  NewUser = 'NEW_USER'
}

export type Provinces = {
  __typename?: 'Provinces';
  code: Scalars['String'];
  country_id: Scalars['Int'];
  id: Scalars['Int'];
  name: Scalars['String'];
  tax: Scalars['String'];
  tax_name: Scalars['String'];
  tax_percentage: Scalars['String'];
  tax_type: Scalars['String'];
};

export type QaShopProduct = {
  __typename?: 'QAShopProduct';
  answer?: Maybe<Array<QaShopProduct>>;
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  productId?: Maybe<Scalars['Int']>;
  questionId?: Maybe<Scalars['Int']>;
  questioner?: Maybe<User>;
  respondent?: Maybe<Shop>;
  shopId?: Maybe<Scalars['Int']>;
  shopProduct?: Maybe<ShopProduct>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  userId?: Maybe<Scalars['Int']>;
};

export type QaShopProductAggregate = {
  __typename?: 'QAShopProductAggregate';
  count: Scalars['Int'];
};

export type QaShopProductInsertInput = {
  content: Scalars['String'];
  productId: Scalars['Int'];
  questionId?: InputMaybe<Scalars['Int']>;
};

export type QaShopProductOrError = CommonError | QaShopProduct;

export type QaShopProductOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type QaShopProductUpdateInput = {
  content: Scalars['String'];
};

export type QaShopProductWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  isQuestion?: InputMaybe<Scalars['Boolean']>;
  productId?: InputMaybe<Scalars['Int']>;
  questionId?: InputMaybe<Scalars['Int']>;
  shopId?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['Int']>;
};

export type Query = {
  __typename?: 'Query';
  analyzeIngredients: IngredientSearchResult;
  bannerImageConnectionAggrs: BannerImageConnection;
  banners: Array<Banner>;
  baumannQuestions: Array<BaumannQuestion>;
  brand?: Maybe<Brand>;
  brandAdmin?: Maybe<BrandAdmin>;
  brandAdmins?: Maybe<Array<BrandAdmin>>;
  brandTranslations: Array<BrandTranslation>;
  brands: Array<Brand>;
  brandsConnection: BrandsConnection;
  cartItems: Array<CartItem>;
  cartProductsOfShop: Array<CartProductsOfShop>;
  cartProductsOfShopConnection: CartProductsOfShopConnection;
  cast: Cast;
  castComments: Array<CastComment>;
  castCommentsConnection: CastCommentsConnection;
  casts: Array<Cast>;
  castsConnection: CastConnection;
  categories: Array<Category>;
  categoriesConnection: CategoriesConnection;
  category?: Maybe<Category>;
  categorySearch: CategorySearchResult;
  caution: IngredientCaution;
  cautions: Array<IngredientCaution>;
  cautionsConnection: IngredientCautionsConnection;
  changedPostHistorysConnection: PostReservedSlotConnection;
  checkExistedInfo: Scalars['Boolean'];
  checkInList: Array<Scalars['Int']>;
  checkPointBonusAvailable: Scalars['Float'];
  checkVerifiedInfo: Scalars['Boolean'];
  childrenUsers: Array<User>;
  childrenUsersConnection: UsersConnection;
  countEcommerceShopPromotion: Scalars['Float'];
  districts: Array<Districts>;
  ecommerceFlashSale: Array<EcommercePromotionGroup>;
  ecommerceFlashSaleProduct: Array<ShopProduct>;
  ecommerceIncentivesForNewUsers: Array<ShopProduct>;
  ecommerceOrder: EcommerceOrder;
  ecommerceOrderConnection: EcommerceOrderConnection;
  ecommerceOrderReviewtyAdmin: EcommerceOrder;
  ecommerceOrderReviwetyAdminConnection: EcommerceOrderConnection;
  ecommerceOrderShopAdminConnection: EcommerceOrderConnection;
  ecommerceOrders: Array<EcommerceOrder>;
  ecommerceOrdersAdmin: Array<EcommerceOrder>;
  ecommerceOrdersShopAdmin: Array<EcommerceOrder>;
  ecommerceReview?: Maybe<EcommerceReview>;
  ecommerceReviewConnection: EcommerceReviewConnection;
  ecommerceReviews: Array<EcommerceReview>;
  ecommerceShopPromotion: Array<EcommercePromotion>;
  evaluation: FundingOrderEvaluation;
  evaluationConnection: EvaluationConnection;
  evaluations: Array<FundingOrderEvaluation>;
  event?: Maybe<Event>;
  eventComments: Array<EventComment>;
  eventCommentsConnection: EventCommentConnection;
  events: Array<Event>;
  eventsConnection: EventConnection;
  feature?: Maybe<Feature>;
  flashSaleOnMall: Array<EcommercePromotion>;
  freeshipPromotionsOnCart: Array<EcommercePromotion>;
  function: Function;
  functionTranslations: Array<FunctionTranslation>;
  functions: Array<Function>;
  functionsConnection: FunctionsConnection;
  funding: Funding;
  fundingOrder: FundingOrder;
  fundingOrderConnection: FundingOrderConnection;
  fundingOrders: Array<FundingOrder>;
  fundingProduct: FundingProduct;
  fundings: Array<Funding>;
  fundingsConnection: FundingsConnection;
  getBannerImage?: Maybe<BannerImage>;
  getBannerImages: Array<BannerImage>;
  getBrandStore: Array<BrandStore>;
  getChangedPostHistorys: Array<PostReservedSlotHistory>;
  getCheckInPayload: CheckInPayload;
  getExternalProducts: Array<ExternalProvider>;
  getFeedItems?: Maybe<Array<Recommendation>>;
  getGoodsList: Array<GoodsList>;
  getMyVoucher: Array<Voucher>;
  getPointHistoriesStatistic: PointHistoryStatistic;
  getProductsFromGoogle: Array<GoogleProduct>;
  getUserAPIs: Array<ResourceApi>;
  getUserNotificationConfig: UserNotificationConfig;
  getVoucherCategories: Array<VoucherCategories>;
  hashtag?: Maybe<Hashtag>;
  hashtags?: Maybe<Array<Hashtag>>;
  /** Hashtags bookmarked by user */
  hashtagsBookmarkedByUser: Array<Hashtag>;
  images: Array<Image>;
  imagesConnection: ImagesConnection;
  ingredient?: Maybe<Ingredient>;
  ingredientUsages: Array<IngredientUsage>;
  ingredients: Array<Ingredient>;
  ingredientsConnection: IngredientsConnection;
  liveStream: LiveStreamOrError;
  liveStreamProduct: LiveStreamProduct;
  liveStreamProducts: Array<LiveStreamProduct>;
  liveStreams: Array<LiveStream>;
  me: User;
  medias: Array<MediaEntityUnion>;
  mediasConnection: MediasConnection;
  myShopProducts: Array<ShopProduct>;
  myShopProductsConnection: ShopProductsConnection;
  myShops: Array<Shop>;
  myTopShopProducts: Array<TopShopProduct>;
  nbEcommerceOrderStatistic: Array<NbEcommerceOrderStatistic>;
  nbOrderStatisticOfUser: Array<NbOrderStatistic>;
  newestViewStore: Array<ProductViewStore>;
  notifications: Array<Notification>;
  notificationsConnection: NotificationsConnection;
  objectTagging: ObjectTagging;
  objectTaggingConnection: ObjectTaggingConnection;
  objectTaggings: Array<ObjectTagging>;
  order: OrderOrError;
  orderShippingFee: Array<EcommerceShippingFee>;
  orderStatisticAdmin: OrderStatisticOfShop;
  orderStatisticOfShopAdmin: OrderStatisticOfShop;
  orderStatisticOfShopAdminWithPeriod: OrderStatisticWithPeriod;
  orders: Array<OrderOrError>;
  ordersConnection: OrdersConnection;
  pointHistories: Array<PointHistory>;
  pointHistoriesConnection: PointHistoriesConnection;
  post?: Maybe<Post>;
  postCategories: Array<PostCategory>;
  postCommentReports: Array<PostCommentReport>;
  postCommentReportsConnection: PostCommentReportsConnection;
  postComments: Array<PostComment>;
  postCommentsConnection: PostCommentsConnection;
  postReservedSlot?: Maybe<PostReservedSlot>;
  postReservedSlots: Array<PostReservedSlot>;
  posts: Array<Post>;
  postsConnection: PostsConnection;
  printEcommOrder: Scalars['String'];
  product?: Maybe<Product>;
  productRankingBrandIds: ProductRankingBrandIds;
  productRankings: Array<ProductRanking>;
  productRankingsConnection: ProductRankingsConnection;
  productRankingsV2: Array<ProductRanking>;
  productRequests: Array<ProductRequest>;
  productRequestsConnection: ProductRequestsConnection;
  productTranslations: Array<ProductTranslation>;
  productUsageLogs: Array<ProductUsageLog>;
  productVariant: ProductVariant;
  productVariantConnection: ProductVariantConnection;
  productVariants: Array<ProductVariant>;
  productViewStore: ProductViewStore;
  productViewStoreConnection: ProductViewStoreConnection;
  productViewStores: Array<ProductViewStore>;
  products: Array<Product>;
  productsConnection: ProductsConnection;
  provinces: Array<Provinces>;
  questionShopProduct: QaShopProduct;
  questionShopProductConnection: QuestionShopProductConnection;
  questionShopProducts: Array<QaShopProduct>;
  recommendationBackground: Array<RecommendationBackground>;
  relatedProducts: Array<Product>;
  revenueOfShopAdmin: Array<NbEcommerceOrderStatistic>;
  review: Review;
  reviewQuestionSet: ReviewQuestionSet;
  reviewQuestionSets: Array<ReviewQuestionSet>;
  reviewQuestions: Array<ReviewQuestion>;
  reviewQuestionsForProduct: Array<ReviewQuestion>;
  reviewReport: ReviewReport;
  reviewReports: Array<ReviewReport>;
  reviewReportsConnection: ReviewReportsConnection;
  reviews: Array<Review>;
  reviewsConnection: ReviewsConnection;
  reviewtyPromotionsOnCart: Array<EcommercePromotion>;
  savedMedias: Array<MediaEntityUnion>;
  savedMediasConnection: SavedMediasConnection;
  searchBrands: BrandSearchResult;
  searchHotKeywords: Array<HotKeywordRanking>;
  searchProducts: ProductSearchResult;
  searchProductsV2: ProductSearchResult;
  searchShopProducts: ShopProductSearchResult;
  searchShopProductsV2: ShopProductSearchResult;
  shippingAddress: ShippingAddressOrError;
  shippingAddresses: Array<ShippingAddress>;
  shippingFee: Scalars['String'];
  shop?: Maybe<Shop>;
  shopConditions: ShopCondition;
  shopOnReviewtyByProduct: Array<ShopOnReviewtyByProduct>;
  shopProduct: ShopProduct;
  shopProducts: Array<ShopProduct>;
  shopProductsConnection: ShopProductsConnection;
  shopPromotionsOnCart: Array<EcommercePromotion>;
  shopToProducts: Array<ShopToProduct>;
  shopToProductsConnection: ShopToProductsConnection;
  shops: Array<Shop>;
  shopsConnection: ShopsConnection;
  suggestKeywords: Array<Scalars['String']>;
  systemConfig?: Maybe<SystemConfig>;
  systemConfigs: Array<SystemConfig>;
  tag: TagEntity;
  tags: Array<TagEntity>;
  tagsConnection: TagsConnection;
  topBuyer: Array<TopBuyer>;
  /** @deprecated use topHighlightShopsV2 instead */
  topHighlightShops: Array<Shop>;
  topHighlightShopsV2: Array<ShopRanking>;
  topRankHighlightProducts: Array<HighlightProduct>;
  trendKeywords: Array<HotKeywordRanking>;
  /** Get 50 trending brands on the last day */
  trendingBrands: Array<Brand>;
  trendingViewStore: Array<ProductViewStore>;
  user?: Maybe<User>;
  userByRole: Array<User>;
  userRequests: Array<UserRequest>;
  userRequestsConnection: UserRequestConnection;
  users: Array<User>;
  usersConnection: UsersConnection;
  verifyCurrentEmail: Scalars['Boolean'];
  verifyOTP: Scalars['Boolean'];
  verifyReferralCode: Scalars['Boolean'];
  verifyShopOTP: Scalars['Boolean'];
  videos: Array<VideoObject>;
  viewCountPost: Scalars['Float'];
  visitedContents: Array<VisitedContentEntityUnion>;
  vnBank: Array<VnBank>;
  wards: Array<Wards>;
};


export type QueryAnalyzeIngredientsArgs = {
  productType: ProductType;
  text: Scalars['String'];
};


export type QueryBannerImageConnectionAggrsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BannerImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BannerImageWhereInput>;
};


export type QueryBrandArgs = {
  where: BrandWhereUniqueInput;
};


export type QueryBrandAdminArgs = {
  where: BrandAdminWhereInput;
};


export type QueryBrandAdminsArgs = {
  where: BrandAdminWhereInput;
};


export type QueryBrandTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandTranslationWhereInput>;
};


export type QueryBrandsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandWhereInput>;
};


export type QueryBrandsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandWhereInput>;
};


export type QueryCartItemsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CartItemOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CartItemWhereInput>;
};


export type QueryCartProductsOfShopArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CartProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CartProductWhereInput>;
};


export type QueryCartProductsOfShopConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CartProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CartProductWhereInput>;
};


export type QueryCastArgs = {
  where: CastWhereUniqueInput;
};


export type QueryCastCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CastCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: CastCommentWhereInput;
};


export type QueryCastCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CastCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: CastCommentWhereInput;
};


export type QueryCastsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CastOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CastWhereInput>;
};


export type QueryCastsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CastOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CastWhereInput>;
};


export type QueryCategoriesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  idFilters?: InputMaybe<Array<Scalars['Int']>>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CategoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CategoryWhereInput>;
};


export type QueryCategoriesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  idFilters?: InputMaybe<Array<Scalars['Int']>>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CategoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CategoryWhereInput>;
};


export type QueryCategoryArgs = {
  idFilters: Array<Scalars['Int']>;
  where: CategoryWhereUniqueInput;
};


export type QueryCategorySearchArgs = {
  first?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  text: Scalars['String'];
};


export type QueryCautionArgs = {
  where: CautionWhereUniqueInput;
};


export type QueryCautionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type QueryCautionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type QueryChangedPostHistorysConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostReservedSlotOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostReservedSlotWhereInput>;
};


export type QueryCheckExistedInfoArgs = {
  type: Scalars['String'];
  value: Scalars['String'];
};


export type QueryCheckPointBonusAvailableArgs = {
  where: PostCommentWhereUserInput;
};


export type QueryCheckVerifiedInfoArgs = {
  type: Scalars['String'];
};


export type QueryChildrenUsersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserOrderByInput>;
  parentUserId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserWhereInput>;
};


export type QueryChildrenUsersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserOrderByInput>;
  parentUserId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserWhereInput>;
};


export type QueryCountEcommerceShopPromotionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionFilterWhereInput>;
};


export type QueryDistrictsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where: DistrictsWhereInput;
};


export type QueryEcommerceFlashSaleArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionFilterWhereInput>;
};


export type QueryEcommerceFlashSaleProductArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionWhereInput>;
};


export type QueryEcommerceIncentivesForNewUsersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionWhereInput>;
};


export type QueryEcommerceOrderArgs = {
  where: EcommerceOrderWhereInput;
};


export type QueryEcommerceOrderConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryEcommerceOrderReviewtyAdminArgs = {
  where: EcommerceOrderWhereInput;
};


export type QueryEcommerceOrderReviwetyAdminConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryEcommerceOrderShopAdminConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryEcommerceOrdersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryEcommerceOrdersAdminArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryEcommerceOrdersShopAdminArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryEcommerceReviewArgs = {
  where: EcommerceReviewWhereInput;
};


export type QueryEcommerceReviewConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceReviewWhereInput>;
};


export type QueryEcommerceReviewsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceReviewWhereInput>;
};


export type QueryEcommerceShopPromotionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionFilterWhereInput>;
};


export type QueryEvaluationArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderEvaluationWhereInput>;
};


export type QueryEvaluationConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderEvaluationWhereInput>;
};


export type QueryEvaluationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderEvaluationWhereInput>;
};


export type QueryEventArgs = {
  where: EventWhereUniqueInput;
};


export type QueryEventCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventCommentWhereInput>;
};


export type QueryEventCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventCommentWhereInput>;
};


export type QueryEventsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventWhereInput>;
};


export type QueryEventsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventWhereInput>;
};


export type QueryFlashSaleOnMallArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommercePromotionFilterWhereInput>;
};


export type QueryFreeshipPromotionsOnCartArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceProductIdsWhereInput>;
};


export type QueryFunctionArgs = {
  where: FunctionWhereUniqueInput;
};


export type QueryFunctionTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionTranslationWhereInput>;
};


export type QueryFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type QueryFunctionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type QueryFundingArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingWhereInput>;
};


export type QueryFundingOrderArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderWhereInput>;
};


export type QueryFundingOrderConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderWhereInput>;
};


export type QueryFundingOrdersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingOrderWhereInput>;
};


export type QueryFundingProductArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingWhereInput>;
};


export type QueryFundingsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingWhereInput>;
};


export type QueryFundingsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingWhereInput>;
};


export type QueryGetBannerImageArgs = {
  where: BannerImageWhereInput;
};


export type QueryGetBannerImagesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BannerImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BannerImageWhereInput>;
};


export type QueryGetBrandStoreArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherStoreWhereInput>;
};


export type QueryGetChangedPostHistorysArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostReservedSlotOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostReservedSlotWhereInput>;
};


export type QueryGetExternalProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<GoogleProductOrderInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where: GoogleProductWhereInput;
};


export type QueryGetFeedItemsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where: RecommendationPageInput;
};


export type QueryGetGoodsListArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<GoodsListArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<GoodsListWhereInput>;
};


export type QueryGetMyVoucherArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VoucherArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherWhereInput>;
};


export type QueryGetPointHistoriesStatisticArgs = {
  statisticType: StatisticType;
  where?: InputMaybe<PointHistoryStatisticWhereInput>;
};


export type QueryGetProductsFromGoogleArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<GoogleProductOrderInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where: GoogleProductWhereInput;
};


export type QueryGetVoucherCategoriesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherCategoriesWhereInput>;
};


export type QueryHashtagArgs = {
  where: HashtagWhereUniqueInput;
};


export type QueryHashtagsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<HashtagWhereInput>;
};


export type QueryHashtagsBookmarkedByUserArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<HashtagsBookmarkedByUserOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryImagesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ImageWhereInput>;
};


export type QueryImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ImageWhereInput>;
};


export type QueryIngredientArgs = {
  where: IngredientWhereUniqueInput;
};


export type QueryIngredientsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientWhereInput>;
};


export type QueryIngredientsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientWhereInput>;
};


export type QueryLiveStreamArgs = {
  where: LiveStreamWhereUniqueInput;
};


export type QueryLiveStreamProductArgs = {
  where: LiveStreamProductWhereUniqueInput;
};


export type QueryLiveStreamProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LiveStreamProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: LiveStreamProductWhereInput;
};


export type QueryLiveStreamsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LiveStreamOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<LiveStreamWhereInput>;
};


export type QueryMediasArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<MediaOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: MediaWhereInput;
};


export type QueryMediasConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<MediaOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: MediaWhereInput;
};


export type QueryMyShopProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopProductWhereInput>;
};


export type QueryMyShopProductsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopProductWhereInput>;
};


export type QueryMyShopsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopOrderByInput>;
  orderBys?: InputMaybe<Array<ShopOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopWhereInput>;
};


export type QueryMyTopShopProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<TopShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: TopShopProductWhereInput;
};


export type QueryNbEcommerceOrderStatisticArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryNbOrderStatisticOfUserArgs = {
  statisticType?: InputMaybe<StatisticType>;
  where?: InputMaybe<FundingOrderStatisticWhereInput>;
};


export type QueryNewestViewStoreArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductViewStoreOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductViewStoreWhereInput>;
};


export type QueryNotificationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<NotificationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<NotificationWhereInput>;
};


export type QueryNotificationsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<NotificationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<NotificationWhereInput>;
};


export type QueryObjectTaggingArgs = {
  where: ObjectTaggingWhereInput;
};


export type QueryObjectTaggingConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ObjectTaggingOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ObjectTaggingWhereInput>;
};


export type QueryObjectTaggingsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ObjectTaggingOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ObjectTaggingWhereInput>;
};


export type QueryOrderArgs = {
  where: OrderWhereUniqueInput;
};


export type QueryOrderShippingFeeArgs = {
  data: OrderShippingFeeInput;
};


export type QueryOrderStatisticAdminArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryOrderStatisticOfShopAdminArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryOrderStatisticOfShopAdminWithPeriodArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryOrdersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<OrderOrderByInput>;
  role?: InputMaybe<Role>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<OrderWhereInput>;
};


export type QueryOrdersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<OrderOrderByInput>;
  role?: InputMaybe<Role>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<OrderWhereInput>;
};


export type QueryPointHistoriesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PointHistoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PointHistoryWhereInput>;
};


export type QueryPointHistoriesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PointHistoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PointHistoryWhereInput>;
};


export type QueryPostArgs = {
  where: PostArgsWhereUniqueInput;
};


export type QueryPostCategoriesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCategoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCategoryWhereInput>;
};


export type QueryPostCommentReportsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentReportWhereInput>;
};


export type QueryPostCommentReportsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentReportWhereInput>;
};


export type QueryPostCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type QueryPostCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type QueryPostReservedSlotArgs = {
  where: PostReservedSlotWhereInput;
};


export type QueryPostReservedSlotsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostReservedSlotOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostReservedSlotWhereInput>;
};


export type QueryPostsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<PostOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostWhereInput>;
};


export type QueryPostsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<PostOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostWhereInput>;
};


export type QueryPrintEcommOrderArgs = {
  transportationCode: Scalars['String'];
};


export type QueryProductArgs = {
  where: ProductWhereUniqueInput;
};


export type QueryProductRankingBrandIdsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type QueryProductRankingsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type QueryProductRankingsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type QueryProductRankingsV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type QueryProductRequestsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductRequestOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRequestWhereInput>;
};


export type QueryProductRequestsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductRequestOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRequestWhereInput>;
};


export type QueryProductTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductTranslationWhereInput>;
};


export type QueryProductUsageLogsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductUsageLogOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductUsageLogWhereInput>;
};


export type QueryProductVariantArgs = {
  where: ProductVariantWhereInput;
};


export type QueryProductVariantConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductVariantOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductVariantWhereInput>;
};


export type QueryProductVariantsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductVariantOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductVariantWhereInput>;
};


export type QueryProductViewStoreArgs = {
  where: ProductViewStoreWhereInput;
};


export type QueryProductViewStoreConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductViewStoreOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductViewStoreWhereInput>;
};


export type QueryProductViewStoresArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductViewStoreOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductViewStoreWhereInput>;
};


export type QueryProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};


export type QueryProductsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};


export type QueryQuestionShopProductArgs = {
  where: QaShopProductWhereInput;
};


export type QueryQuestionShopProductConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<QaShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<QaShopProductWhereInput>;
};


export type QueryQuestionShopProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<QaShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<QaShopProductWhereInput>;
};


export type QueryRecommendationBackgroundArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where: RecommendationBackgroundInput;
};


export type QueryRelatedProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};


export type QueryRevenueOfShopAdminArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryReviewArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type QueryReviewQuestionSetArgs = {
  where: ReviewQuestionSetWhereUniqueInput;
};


export type QueryReviewQuestionSetsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewQuestionSetWhereInput>;
};


export type QueryReviewQuestionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewQuestionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewQuestionWhereInput>;
};


export type QueryReviewQuestionsForProductArgs = {
  productId: Scalars['ID'];
};


export type QueryReviewReportArgs = {
  where: ReviewReportWhereUniqueInput;
};


export type QueryReviewReportsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewReportWhereInput>;
};


export type QueryReviewReportsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewReportWhereInput>;
};


export type QueryReviewsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type QueryReviewsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type QueryReviewtyPromotionsOnCartArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceProductIdsWhereInput>;
};


export type QuerySavedMediasArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SavedMediaOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SavedMediaWhereInput>;
};


export type QuerySavedMediasConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SavedMediaOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SavedMediaWhereInput>;
};


export type QuerySearchBrandsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandWhereInput>;
};


export type QuerySearchProductsArgs = {
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductSearchOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  text: Scalars['String'];
  where?: InputMaybe<ProductWhereInput>;
};


export type QuerySearchProductsV2Args = {
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductSearchOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};


export type QuerySearchShopProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopProductElasticSearchOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopProductElasticSearchWhereInput>;
};


export type QuerySearchShopProductsV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopProductElasticSearchOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopProductElasticSearchWhereInput>;
};


export type QueryShippingAddressArgs = {
  where: ShippingAddressWhereUniqueInput;
};


export type QueryShippingAddressesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShippingAddressOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShippingAddressWhereInput>;
};


export type QueryShippingFeeArgs = {
  data: ShippingFeeInput;
};


export type QueryShopArgs = {
  where: ShopWhereUniqueInput;
};


export type QueryShopConditionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopOrderByInput>;
  orderBys?: InputMaybe<Array<ShopOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopWhereInput>;
};


export type QueryShopOnReviewtyByProductArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopProductWhereInput>;
};


export type QueryShopProductArgs = {
  where: ShopProductWhereInput;
};


export type QueryShopProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopProductWhereInput>;
};


export type QueryShopProductsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopProductWhereInput>;
};


export type QueryShopPromotionsOnCartArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceProductIdsWhereInput>;
};


export type QueryShopToProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopToProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: ShopToProductWhereInput;
};


export type QueryShopToProductsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopToProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: ShopToProductWhereInput;
};


export type QueryShopsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopOrderByInput>;
  orderBys?: InputMaybe<Array<ShopOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopWhereInput>;
};


export type QueryShopsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopOrderByInput>;
  orderBys?: InputMaybe<Array<ShopOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopWhereInput>;
};


export type QuerySuggestKeywordsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where: SuggestKeywordInput;
};


export type QuerySystemConfigArgs = {
  where: SystemConfigWhereInput;
};


export type QuerySystemConfigsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SystemConfigOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SystemConfigWhereInput>;
};


export type QueryTagArgs = {
  where: TagsWhereInput;
};


export type QueryTagsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<TagsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<TagsWhereInput>;
};


export type QueryTagsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<TagsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<TagsWhereInput>;
};


export type QueryTopBuyerArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EcommerceOrderOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EcommerceOrderWhereInput>;
};


export type QueryTopHighlightShopsArgs = {
  first?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryTopHighlightShopsV2Args = {
  first?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryTopRankHighlightProductsArgs = {
  first?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryTrendingViewStoreArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductViewStoreOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductViewStoreWhereInput>;
};


export type QueryUserArgs = {
  dbDirect?: InputMaybe<Scalars['Boolean']>;
  where: UserWhereUniqueInput;
};


export type QueryUserByRoleArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<RolesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<RolesWhereInput>;
};


export type QueryUserRequestsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserRequestOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserRequestWhereInput>;
};


export type QueryUserRequestsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserRequestOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserRequestWhereInput>;
};


export type QueryUsersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserOrderByInput>;
  parentUserId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserWhereInput>;
};


export type QueryUsersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserOrderByInput>;
  parentUserId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserWhereInput>;
};


export type QueryVerifyCurrentEmailArgs = {
  otp: Scalars['Int'];
};


export type QueryVerifyOtpArgs = {
  email: Scalars['String'];
  otp: Scalars['Int'];
};


export type QueryVerifyReferralCodeArgs = {
  referralCode: Scalars['String'];
};


export type QueryVerifyShopOtpArgs = {
  otp: Scalars['Int'];
  type: Scalars['String'];
  value: Scalars['String'];
};


export type QueryVideosArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VideoOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VideosWhereInput>;
};


export type QueryViewCountPostArgs = {
  where: PostWhereUniqueInput;
};


export type QueryVisitedContentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VisitedContentArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: VisitedContentWhereInput;
};


export type QueryVnBankArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BankWhereInput>;
};


export type QueryWardsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where: WardInput;
};

export type QuestionShopProductConnection = {
  __typename?: 'QuestionShopProductConnection';
  aggregate: QaShopProductAggregate;
};

export enum ReactStatus {
  Dislike = 'DISLIKE',
  Like = 'LIKE',
  None = 'NONE'
}

export type Recommendation = {
  __typename?: 'Recommendation';
  id: Scalars['Int'];
  recommendation?: Maybe<PostRecommentdationEntityUnion>;
  type?: Maybe<Scalars['Float']>;
};

export type RecommendationBackground = {
  __typename?: 'RecommendationBackground';
  url?: Maybe<Scalars['String']>;
};

export type RecommendationBackgroundInput = {
  content: Scalars['String'];
  page?: InputMaybe<Scalars['Int']>;
};

export type RecommendationPageInput = {
  page?: InputMaybe<Scalars['Int']>;
  userID?: InputMaybe<Scalars['Int']>;
};

export type ResourceApi = {
  __typename?: 'ResourceAPI';
  createdAt?: Maybe<Scalars['DateTime']>;
  createdUser?: Maybe<Scalars['String']>;
  displayName?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  method?: Maybe<ApiMethod>;
  name?: Maybe<Scalars['String']>;
  resourceId?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  urlPath?: Maybe<Scalars['String']>;
};

export type Review = {
  __typename?: 'Review';
  answers?: Maybe<Array<ReviewAnswer>>;
  bookMark?: Maybe<Scalars['Boolean']>;
  commentsConnection: PostCommentsConnection;
  content?: Maybe<Scalars['String']>;
  countViewedReview?: Maybe<Scalars['Float']>;
  createdAt: Scalars['DateTime'];
  hashtags?: Maybe<Array<Hashtag>>;
  id: Scalars['Int'];
  images: Array<Image>;
  imagesConnection: ReviewImagesConnection;
  isDeleted: Scalars['Boolean'];
  isPin: Scalars['Boolean'];
  isRecommended?: Maybe<Scalars['Boolean']>;
  post?: Maybe<Post>;
  postComments: Array<PostComment>;
  product: Product;
  rate: Scalars['Int'];
  reactionOfViewer: ReactStatus;
  reactionsConnection: ReviewReactionsConnection;
  reports?: Maybe<Array<ReviewReport>>;
  shop?: Maybe<Shop>;
  status: ReviewStatus;
  title?: Maybe<Scalars['String']>;
  uid: Scalars['ID'];
  updatedAt: Scalars['DateTime'];
  user: User;
};


export type ReviewCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type ReviewImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type ReviewPostCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};

export type ReviewAnswer = {
  __typename?: 'ReviewAnswer';
  answerOption: ReviewAnswerOption;
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
};

export type ReviewAnswerInput = {
  id?: InputMaybe<Scalars['ID']>;
  reviewAnswerOptionId: Scalars['ID'];
  reviewId?: InputMaybe<Scalars['ID']>;
};

export type ReviewAnswerOption = {
  __typename?: 'ReviewAnswerOption';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  order: Scalars['Int'];
  question: ReviewQuestion;
  summary: Scalars['String'];
};

export type ReviewAvgOrderByInput = {
  rate?: InputMaybe<OrderBy>;
};

export type ReviewCreateInput = {
  content: Scalars['String'];
  hashtags?: InputMaybe<Array<HashtagInput>>;
  images?: InputMaybe<ImageCreateManyWithoutReviewInput>;
  product: ProductWhereUniqueInput;
  rate: Scalars['Int'];
  shop?: InputMaybe<ShopCreateOneWithoutReviewsInput>;
};

export type ReviewCreateOneWithoutPostCommentsInput = {
  connect?: InputMaybe<ReviewWhereUniqueInput>;
};

export type ReviewCreateOneWithoutPostInput = {
  connect?: InputMaybe<ReviewWhereUniqueInput>;
};

export type ReviewCreateOneWithoutReportInput = {
  connect: ReviewWhereUniqueInput;
};

export type ReviewCreatePayload = CommonError | Review;

export type ReviewImagesAggregate = {
  __typename?: 'ReviewImagesAggregate';
  count: Scalars['Int'];
};

export type ReviewImagesConnection = {
  __typename?: 'ReviewImagesConnection';
  aggregate: ReviewImagesAggregate;
};

export type ReviewInput = {
  answers?: InputMaybe<Array<ReviewAnswerInput>>;
  content?: InputMaybe<Scalars['String']>;
  hashtags?: InputMaybe<Array<HashtagInput>>;
  id?: InputMaybe<Scalars['ID']>;
  images?: InputMaybe<Array<ImageInput>>;
  isRecommended?: InputMaybe<Scalars['Boolean']>;
  productId: Scalars['ID'];
  rate: Scalars['Int'];
  shopId?: InputMaybe<Scalars['ID']>;
  title?: InputMaybe<Scalars['String']>;
};

export type ReviewOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  reactionsConnection?: InputMaybe<ReviewReactionsConnectionOrderByInput>;
  updatedAt?: InputMaybe<OrderBy>;
};

export enum ReviewQueryType {
  Following = 'FOLLOWING',
  Normal = 'NORMAL'
}

export type ReviewQuestion = {
  __typename?: 'ReviewQuestion';
  answerOptions: Array<ReviewAnswerOption>;
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  language: LanguageCode;
  order: Scalars['Int'];
  reviewQuestionSetId: Scalars['ID'];
};

export type ReviewQuestionOrderByInput = {
  order?: InputMaybe<OrderBy>;
};

export type ReviewQuestionSet = {
  __typename?: 'ReviewQuestionSet';
  id: Scalars['ID'];
  name: Scalars['String'];
  questions: Array<ReviewQuestion>;
};

export type ReviewQuestionSetWhereInput = {
  language?: InputMaybe<LanguageCodeFilter>;
  reviewQuestionSetId?: InputMaybe<Scalars['ID']>;
};

export type ReviewQuestionSetWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type ReviewQuestionWhereInput = {
  language?: InputMaybe<LanguageCodeFilter>;
  reviewQuestionSetId?: InputMaybe<StringFilter>;
};

export type ReviewReactionsAggregate = {
  __typename?: 'ReviewReactionsAggregate';
  count: Scalars['Int'];
};

export type ReviewReactionsAggregateOrderByInput = {
  count?: InputMaybe<OrderBy>;
};

export type ReviewReactionsConnection = {
  __typename?: 'ReviewReactionsConnection';
  aggregate: ReviewReactionsAggregate;
};

export type ReviewReactionsConnectionOrderByInput = {
  aggregate?: InputMaybe<ReviewReactionsAggregateOrderByInput>;
};

export type ReviewReport = {
  __typename?: 'ReviewReport';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  reason?: Maybe<Scalars['String']>;
  review: Review;
  status: ReviewReportStatus;
  type: ReviewReportType;
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type ReviewReportCreateInput = {
  reason?: InputMaybe<Scalars['String']>;
  review: ReviewCreateOneWithoutReportInput;
  type: ReviewReportType;
};

export type ReviewReportOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export enum ReviewReportStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Rejected = 'REJECTED'
}

export enum ReviewReportType {
  Advertising = 'ADVERTISING',
  NotRelated = 'NOT_RELATED',
  Other = 'OTHER',
  PersonalInformation = 'PERSONAL_INFORMATION',
  Repeat = 'REPEAT',
  Simple = 'SIMPLE',
  Swearing = 'SWEARING'
}

export type ReviewReportWhereInput = {
  reasonContains?: InputMaybe<Scalars['String']>;
  review?: InputMaybe<ReviewWhereInput>;
  status?: InputMaybe<ReviewReportStatus>;
  user?: InputMaybe<UserWhereInput>;
};

export type ReviewReportWhereUniqueInput = {
  id: Scalars['Int'];
};

export type ReviewReportsAggregate = {
  __typename?: 'ReviewReportsAggregate';
  count: Scalars['Int'];
};

export type ReviewReportsConnection = {
  __typename?: 'ReviewReportsConnection';
  aggregate: ReviewReportsAggregate;
};

export enum ReviewStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Hidden = 'HIDDEN'
}

export type ReviewUpdateInput = {
  content: Scalars['String'];
  hashtags?: InputMaybe<Array<HashtagInput>>;
  images?: InputMaybe<ImageUpdateManyWithoutReviewInput>;
  rate: Scalars['Int'];
  shop?: InputMaybe<ShopUpdateOneWithoutReviewsInput>;
};

export type ReviewUpdatePayload = CommonError | Review;

export type ReviewWhereInput = {
  contentContains?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<DateTimeFilter>;
  hashtagsSome?: InputMaybe<HashtagWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  idIn?: InputMaybe<Array<Scalars['Int']>>;
  idIsNull?: InputMaybe<Scalars['Boolean']>;
  isBookMark?: InputMaybe<Scalars['Boolean']>;
  isDeleted?: InputMaybe<Scalars['Boolean']>;
  isPined?: InputMaybe<Scalars['Boolean']>;
  isQuickRating?: InputMaybe<Scalars['Boolean']>;
  product?: InputMaybe<ProductWhereInput>;
  queryType?: InputMaybe<ReviewQueryType>;
  rateOR?: InputMaybe<Array<IntFilter>>;
  shop?: InputMaybe<ShopWhereInput>;
  status?: InputMaybe<ReviewStatus>;
  statusIn?: InputMaybe<Array<ReviewStatus>>;
  user?: InputMaybe<UserWhereInput>;
};

export type ReviewWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<Scalars['Boolean']>;
};

export type ReviewsAggregate = {
  __typename?: 'ReviewsAggregate';
  avg: ReviewsAvg;
  count: Scalars['Int'];
};

export type ReviewsAggregateOrderByInput = {
  avg?: InputMaybe<ReviewAvgOrderByInput>;
  count?: InputMaybe<OrderBy>;
};

export type ReviewsAvg = {
  __typename?: 'ReviewsAvg';
  rate: Scalars['Float'];
};

export type ReviewsConnection = {
  __typename?: 'ReviewsConnection';
  aggregate: ReviewsAggregate;
};

export type ReviewsConnectionOrderByInput = {
  aggregate?: InputMaybe<ReviewsAggregateOrderByInput>;
};

export type ReviewsCountByRate = {
  __typename?: 'ReviewsCountByRate';
  five?: Maybe<Scalars['Int']>;
  four?: Maybe<Scalars['Int']>;
  one?: Maybe<Scalars['Int']>;
  three?: Maybe<Scalars['Int']>;
  two?: Maybe<Scalars['Int']>;
};

export enum Role {
  Admin = 'ADMIN',
  Anonymous = 'ANONYMOUS',
  Shop = 'SHOP',
  User = 'USER'
}

export enum RoleStatus {
  Active = 'ACTIVE',
  Deactive = 'DEACTIVE'
}

export type Roles = {
  __typename?: 'Roles';
  createdAt: Scalars['DateTime'];
  createdUser?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  name: Scalars['String'];
};

export type RolesOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type RolesWhereInput = {
  _id?: InputMaybe<IntFilter>;
  createdUser?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  name?: InputMaybe<StringFilter>;
};

export type SavedMediaOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
};

export type SavedMediaWhereInput = {
  createdAt?: InputMaybe<DateTimeFilter>;
  type?: InputMaybe<MediaType>;
};

export type SavedMediasAggregate = {
  __typename?: 'SavedMediasAggregate';
  count: Scalars['Int'];
};

export type SavedMediasConnection = {
  __typename?: 'SavedMediasConnection';
  aggregate: SavedMediasAggregate;
};

export enum ScreenNameType {
  KhamPha = 'KHAM_PHA',
  ProductDetail = 'PRODUCT_DETAIL'
}

export type ShipmentHistory = {
  __typename?: 'ShipmentHistory';
  createdAt: Scalars['DateTime'];
  fundingOrderId: Scalars['Int'];
  id: Scalars['Int'];
  status: Scalars['Int'];
  statusText: Scalars['String'];
};

export type ShipmentHistoryOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type ShipmentHistoryWhereInput = {
  funding_order_id?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['Int']>;
};

export type ShippingAddress = {
  __typename?: 'ShippingAddress';
  address: Scalars['String'];
  country: Scalars['String'];
  createdAt: Scalars['DateTime'];
  default: Scalars['Boolean'];
  /** Khoảng cách từ shop đến vị trí hiện tại, đơn vị m */
  distance?: Maybe<Scalars['Float']>;
  district: Scalars['String'];
  fullName: Scalars['String'];
  id: Scalars['Int'];
  isPickupAddress?: Maybe<Scalars['Boolean']>;
  location?: Maybe<Location>;
  phoneNumber?: Maybe<Scalars['String']>;
  province: Scalars['String'];
  updatedAt: Scalars['DateTime'];
  ward: Scalars['String'];
};


export type ShippingAddressDistanceArgs = {
  location: LocationInput;
};

export type ShippingAddressCreateInput = {
  address: Scalars['String'];
  country: Scalars['String'];
  default?: InputMaybe<Scalars['Boolean']>;
  /** Quận, huyện */
  district: Scalars['String'];
  districtId?: InputMaybe<Scalars['ID']>;
  fullName: Scalars['String'];
  latitude?: InputMaybe<Scalars['Float']>;
  longitude?: InputMaybe<Scalars['Float']>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  /** Tỉnh và thành phố trực thuộc trung ương */
  province: Scalars['String'];
  provinceId?: InputMaybe<Scalars['ID']>;
  /** Phường, xã */
  ward: Scalars['String'];
  wardId?: InputMaybe<Scalars['ID']>;
};

export type ShippingAddressCreateOneWithoutOrderInput = {
  connect: ShippingAddressWhereUniqueInput;
};

export type ShippingAddressCreateOneWithoutShopInput = {
  create: ShippingAddressCreateInput;
};

export type ShippingAddressOrError = CommonError | ShippingAddress;

export type ShippingAddressOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type ShippingAddressUpdateInput = {
  address: Scalars['String'];
  country: Scalars['String'];
  default?: InputMaybe<Scalars['Boolean']>;
  /** Quận, huyện */
  district: Scalars['String'];
  districtId?: InputMaybe<Scalars['ID']>;
  fullName: Scalars['String'];
  latitude?: InputMaybe<Scalars['Float']>;
  longitude?: InputMaybe<Scalars['Float']>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  /** Tỉnh và thành phố trực thuộc trung ương */
  province: Scalars['String'];
  provinceId?: InputMaybe<Scalars['ID']>;
  /** Phường, xã */
  ward: Scalars['String'];
  wardId?: InputMaybe<Scalars['ID']>;
};

export type ShippingAddressUpdateOneWithoutShopInput = {
  create?: InputMaybe<ShippingAddressCreateInput>;
  update?: InputMaybe<ShippingAddressUpdateWithWhereUniqueWithoutShopInput>;
};

export type ShippingAddressUpdateWithWhereUniqueWithoutShopInput = {
  data: ShippingAddressUpdateWithoutShopInput;
  where: ShippingAddressWhereUniqueInput;
};

export type ShippingAddressUpdateWithoutShopInput = {
  address: Scalars['String'];
  country: Scalars['String'];
  default?: InputMaybe<Scalars['Boolean']>;
  /** Quận, huyện */
  district: Scalars['String'];
  districtId?: InputMaybe<Scalars['ID']>;
  fullName: Scalars['String'];
  latitude?: InputMaybe<Scalars['Float']>;
  longitude?: InputMaybe<Scalars['Float']>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  /** Tỉnh và thành phố trực thuộc trung ương */
  province: Scalars['String'];
  provinceId?: InputMaybe<Scalars['ID']>;
  /** Phường, xã */
  ward: Scalars['String'];
  wardId?: InputMaybe<Scalars['ID']>;
};

export type ShippingAddressWhereInput = {
  default?: InputMaybe<Scalars['Boolean']>;
  district?: InputMaybe<Scalars['String']>;
  /** Lọc theo khoảng cách */
  nearBy?: InputMaybe<NearByInput>;
  province?: InputMaybe<Scalars['String']>;
  user?: InputMaybe<UserWhereInput>;
};

export type ShippingAddressWhereUniqueInput = {
  id: Scalars['Int'];
};

export type ShippingFeeInput = {
  address?: InputMaybe<Scalars['String']>;
  deliverOption: DeliverOption;
  district: Scalars['String'];
  province: Scalars['String'];
  ward?: InputMaybe<Scalars['String']>;
  weight: Scalars['Float'];
};

export type Shop = {
  __typename?: 'Shop';
  bankCard?: Maybe<BankCard>;
  cover?: Maybe<Image>;
  createdAt: Scalars['DateTime'];
  description?: Maybe<Scalars['String']>;
  externalLinks?: Maybe<Array<ExternalLink>>;
  id: Scalars['Int'];
  name: Scalars['String'];
  nbOfShopProduct: Scalars['Int'];
  pickupAddress?: Maybe<ShippingAddress>;
  reviewsConnection: ReviewsConnection;
  shopRanking?: Maybe<ShopRanking>;
  status: ShopStatus;
  updatedAt: Scalars['DateTime'];
  user: User;
};


export type ShopReviewsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};

export type ShopCondition = {
  __typename?: 'ShopCondition';
  hasPickAddress: Scalars['Boolean'];
  isApproved: Scalars['Boolean'];
  isVerifiedPhone: Scalars['Boolean'];
};

export type ShopCreateInput = {
  avatar?: InputMaybe<ImageCreateOneWithoutShopInput>;
  cover?: InputMaybe<ImageCreateOneWithoutShopInput>;
  description?: InputMaybe<Scalars['String']>;
  externalLink?: InputMaybe<ExternalLinkCreateManyWithoutShopInput>;
  name: Scalars['String'];
  pickupAddress?: InputMaybe<ShippingAddressCreateOneWithoutShopInput>;
};

export type ShopCreateOneWithoutOrderInput = {
  connect: ShopWhereUniqueInput;
};

export type ShopCreateOneWithoutReviewsInput = {
  connect: ShopWhereUniqueInput;
};

export type ShopCreateOneWithoutShopToProductsInput = {
  connect: ShopWhereUniqueInput;
};

export type ShopMutationError = Error & {
  __typename?: 'ShopMutationError';
  code: ShopMutationErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum ShopMutationErrorCode {
  NameAlreadyInUse = 'NAME_ALREADY_IN_USE',
  YouAreNotTheOwner = 'YOU_ARE_NOT_THE_OWNER'
}

export type ShopOnReviewtyByProduct = {
  __typename?: 'ShopOnReviewtyByProduct';
  productId?: Maybe<Scalars['Int']>;
  reviewtyProduct: Product;
  reviewtyProductId?: Maybe<Scalars['Int']>;
  shop: Shop;
  shopId?: Maybe<Scalars['Int']>;
  shoppingProduct?: Maybe<ShopProduct>;
};

export type ShopOrMutationError = CommonError | Shop | ShopMutationError;

export type ShopOrderByInput = {
  id?: InputMaybe<OrderBy>;
  status?: InputMaybe<OrderBy>;
  updatedAt?: InputMaybe<OrderBy>;
};

export type ShopOrderInput = {
  shippingFee: Scalars['Float'];
  shopId: Scalars['Int'];
  shopPromotions?: InputMaybe<Array<Scalars['Int']>>;
  variantsInfo: Array<EcommerceOrderProductInput>;
};

export type ShopProduct = {
  __typename?: 'ShopProduct';
  avgRateOfShopProductReviews?: Maybe<Scalars['Float']>;
  blockedReason?: Maybe<Scalars['String']>;
  brand: Brand;
  brandId?: Maybe<Scalars['Int']>;
  categoryId?: Maybe<Scalars['Int']>;
  comparativePrice?: Maybe<Scalars['Float']>;
  content?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  createdUser: User;
  description?: Maybe<Scalars['String']>;
  flashsale?: Maybe<Array<EcommercePromotion>>;
  functions?: Maybe<FunctionProducts>;
  id: Scalars['Int'];
  images?: Maybe<Images>;
  isBlocked?: Maybe<Scalars['Boolean']>;
  isVisible?: Maybe<Scalars['Boolean']>;
  name?: Maybe<Scalars['String']>;
  nbReviewsOfShopProduct?: Maybe<Scalars['Int']>;
  newbiePromotion?: Maybe<Array<EcommercePromotion>>;
  productTags?: Maybe<Array<ObjectTagging>>;
  productVariants?: Maybe<Array<ProductVariant>>;
  productViewstores?: Maybe<Array<ProductViewStore>>;
  promotions?: Maybe<Array<EcommercePromotion>>;
  rangeOfOriginalPrices?: Maybe<Scalars['String']>;
  rangeOfPrices?: Maybe<Scalars['String']>;
  revenueOfShopProduct?: Maybe<Scalars['Float']>;
  reviewtyProduct?: Maybe<Product>;
  reviewtyProductId?: Maybe<Scalars['Int']>;
  shop: Shop;
  shopId?: Maybe<Scalars['Int']>;
  shopProductReviews?: Maybe<Array<EcommerceReview>>;
  totalQuantity?: Maybe<Scalars['Int']>;
  totalSoldQuantity?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  userId?: Maybe<Scalars['Int']>;
};


export type ShopProductProductVariantsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductVariantOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductVariantWhereInput>;
};


export type ShopProductProductViewstoresArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductViewStoreOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductViewStoreWhereInput>;
};

export type ShopProductElasticSearchOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
  rateAvg?: InputMaybe<OrderBy>;
};

export type ShopProductElasticSearchWhereInput = {
  brandIdsFilter?: InputMaybe<Array<Scalars['Int']>>;
  price?: InputMaybe<IntFilter>;
  shopIdsFilter?: InputMaybe<Array<Scalars['Int']>>;
  textSearch?: InputMaybe<Scalars['String']>;
  textSearchV2?: InputMaybe<Array<Scalars['String']>>;
};

export type ShopProductInsertInput = {
  brandId?: InputMaybe<Scalars['Int']>;
  categoryId?: InputMaybe<Scalars['Int']>;
  comparativePrice?: InputMaybe<Scalars['Float']>;
  content?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  functions?: InputMaybe<FunctionProductUpsertInput>;
  images?: InputMaybe<ImageUpsertInput>;
  name: Scalars['String'];
  reviewtyProductId?: InputMaybe<Scalars['Int']>;
  variants: Array<ProductVariantInsertInput>;
};

export type ShopProductOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
};

export type ShopProductSearchResult = {
  __typename?: 'ShopProductSearchResult';
  products: Array<ShopProduct>;
  total: Scalars['Int'];
};

export type ShopProductTagging = {
  __typename?: 'ShopProductTagging';
  id: Scalars['Int'];
  shopProduct?: Maybe<ShopProduct>;
};

export type ShopProductUpdateInput = {
  blockedReason?: InputMaybe<Scalars['String']>;
  brandId?: InputMaybe<Scalars['Int']>;
  categoryId?: InputMaybe<Scalars['Int']>;
  comparativePrice?: InputMaybe<Scalars['Float']>;
  content?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  functions?: InputMaybe<FunctionProductUpsertInput>;
  images?: InputMaybe<ImageUpsertInput>;
  isBlocked?: InputMaybe<Scalars['Boolean']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  name?: InputMaybe<Scalars['String']>;
  reviewtyProductId?: InputMaybe<Scalars['Int']>;
  variants?: InputMaybe<Array<ProductVariantUpdateInput>>;
};

export type ShopProductWhereInput = {
  brandName?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  ids?: InputMaybe<Array<Scalars['Int']>>;
  isBlocked?: InputMaybe<Scalars['Boolean']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  price?: InputMaybe<IntFilter>;
  productName?: InputMaybe<Scalars['String']>;
  quantity?: InputMaybe<IntFilter>;
  reviewtyProductId?: InputMaybe<Scalars['Int']>;
  shopId?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['Int']>;
};

export type ShopProductsAggregate = {
  __typename?: 'ShopProductsAggregate';
  count: Scalars['Int'];
};

export type ShopProductsConnection = {
  __typename?: 'ShopProductsConnection';
  aggregate: ShopProductsAggregate;
};

export type ShopRanking = {
  __typename?: 'ShopRanking';
  ranking: Scalars['Int'];
  rankingChange?: Maybe<Scalars['Int']>;
  shop: Shop;
};

export enum ShopStatus {
  Approved = 'APPROVED',
  Blocked = 'BLOCKED',
  Created = 'CREATED'
}

export type ShopToProduct = {
  __typename?: 'ShopToProduct';
  createdAt: Scalars['DateTime'];
  externalLinks?: Maybe<Array<ExternalLink>>;
  price?: Maybe<Scalars['Int']>;
  product: Product;
  shop: Shop;
  stockQuantity?: Maybe<Scalars['Int']>;
  updatedAt: Scalars['DateTime'];
};

export type ShopToProductCreateInput = {
  externalLink?: InputMaybe<ExternalLinkCreateManyWithoutShopToProductInput>;
  price: Scalars['Int'];
  product: ProductCreateOneWithoutShopToProductsInput;
  shop: ShopCreateOneWithoutShopToProductsInput;
  stockQuantity?: InputMaybe<Scalars['Int']>;
};

export type ShopToProductOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
};

export type ShopToProductUpdateInput = {
  externalLink?: InputMaybe<ExternalLinkUpdateManyWithoutShopToProductInput>;
  price?: InputMaybe<Scalars['Int']>;
  stockQuantity?: InputMaybe<Scalars['Int']>;
};

export type ShopToProductWhereInput = {
  ecommerceSite?: InputMaybe<EcommerceSite>;
  price?: InputMaybe<IntFilter>;
  product?: InputMaybe<ProductWhereInput>;
  shop?: InputMaybe<ShopWhereInput>;
};

export type ShopToProductWhereUniqueInput = {
  product: ProductWhereUniqueInput;
  shop: ShopWhereUniqueInput;
};

export type ShopToProductsAggregate = {
  __typename?: 'ShopToProductsAggregate';
  count: Scalars['Int'];
};

export type ShopToProductsConnection = {
  __typename?: 'ShopToProductsConnection';
  aggregate: ShopToProductsAggregate;
};

export type ShopUpdateInput = {
  avatar?: InputMaybe<ImageUpdateOneWithoutShopInput>;
  cover?: InputMaybe<ImageUpdateOneWithoutShopInput>;
  description?: InputMaybe<Scalars['String']>;
  externalLink?: InputMaybe<ExternalLinkUpdateManyWithoutShopInput>;
  name: Scalars['String'];
  pickupAddress?: InputMaybe<ShippingAddressUpdateOneWithoutShopInput>;
};

export type ShopUpdateOneWithoutReviewsInput = {
  connect: ShopWhereUniqueInput;
};

export type ShopWhereInput = {
  _id?: InputMaybe<IntFilter>;
  ecommerceSite?: InputMaybe<EcommerceSite>;
  followersSome?: InputMaybe<UserWhereInput>;
  followingsSome?: InputMaybe<UserWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  name?: InputMaybe<StringFilter>;
  owner?: InputMaybe<UserWhereUniqueInput>;
  pickupAddress?: InputMaybe<ShippingAddressWhereInput>;
  productsSome?: InputMaybe<ProductWhereInput>;
  status?: InputMaybe<ShopStatus>;
};

export type ShopWhereUniqueInput = {
  id: Scalars['Int'];
};

export type ShopsAggregate = {
  __typename?: 'ShopsAggregate';
  count: Scalars['Int'];
};

export type ShopsConnection = {
  __typename?: 'ShopsConnection';
  aggregate: ShopsAggregate;
};

export type SignUpInput = {
  accessToken?: InputMaybe<Scalars['String']>;
  account?: InputMaybe<Scalars['String']>;
  birthYear?: InputMaybe<Scalars['Int']>;
  displayName?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  gender?: InputMaybe<Gender>;
  oauthProvider?: InputMaybe<OauthProvider>;
  otp?: InputMaybe<Scalars['Int']>;
  password?: InputMaybe<Scalars['String']>;
  referralCode?: InputMaybe<Scalars['String']>;
  roleId?: InputMaybe<Scalars['Int']>;
};

export enum SkinType {
  Dry = 'DRY',
  Mixed = 'MIXED',
  Neutral = 'NEUTRAL',
  Oily = 'OILY',
  Sensitive = 'SENSITIVE'
}

export type SkinTypeObject = {
  __typename?: 'SkinTypeObject';
  ALL: Scalars['Boolean'];
  DRY: Scalars['Boolean'];
  MIXED: Scalars['Boolean'];
  NEUTRAL: Scalars['Boolean'];
  OILY: Scalars['Boolean'];
  SENSITIVE: Scalars['Boolean'];
};

export type SocialMedia = {
  __typename?: 'SocialMedia';
  id: Scalars['String'];
  provider: OauthProvider;
};

export type SocialMediaError = Error & {
  __typename?: 'SocialMediaError';
  code: SocialMediaErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum SocialMediaErrorCode {
  AccountAlreadyBeingUsed = 'ACCOUNT_ALREADY_BEING_USED'
}

export type SocialMediaOrError = CommonError | SocialMedia | SocialMediaError;

export type SpecialIngredientFunction = {
  __typename?: 'SpecialIngredientFunction';
  id: Scalars['Int'];
  symbolUrl?: Maybe<Scalars['String']>;
  type: SpecialIngredientFunctionType;
};

export type SpecialIngredientFunctionOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type SpecialIngredientFunctionTranslationWhereInput = {
  language?: InputMaybe<LanguageCode>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export enum SpecialIngredientFunctionType {
  Advantage = 'ADVANTAGE',
  Disadvantage = 'DISADVANTAGE',
  Recommendation = 'RECOMMENDATION'
}

export type SpecialIngredientFunctionWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  ingredientsSome?: InputMaybe<IngredientWhereInput>;
  translationsSome?: InputMaybe<SpecialIngredientFunctionTranslationWhereInput>;
  type?: InputMaybe<SpecialIngredientFunctionType>;
};

export type SpecialIngredientFunctionsAggregate = {
  __typename?: 'SpecialIngredientFunctionsAggregate';
  id: Scalars['Int'];
};

export type SpecialIngredientFunctionsConnection = {
  __typename?: 'SpecialIngredientFunctionsConnection';
  aggregate: SpecialIngredientFunctionsAggregate;
  id: Scalars['Int'];
};

export enum StatisticType {
  Day = 'DAY',
  Month = 'MONTH',
  Quarter = 'QUARTER',
  Week = 'WEEK'
}

export type StringBox = {
  __typename?: 'StringBox';
  value: Scalars['String'];
};

export type StringFilter = {
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  not?: InputMaybe<StringFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type SucessfullEcommOrder = {
  __typename?: 'SucessfullEcommOrder';
  message: Scalars['String'];
  paymentMethod: PaymentMethod;
};

export type SucessfullEcommOrderOrError = CommonError | EcommOrderCreateError | SucessfullEcommOrder;

export type SuggestKeywordInput = {
  keyword: Scalars['String'];
  page?: InputMaybe<Scalars['Int']>;
};

export type SystemConfig = {
  __typename?: 'SystemConfig';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  name: Scalars['String'];
  value: Scalars['String'];
};

export type SystemConfigCreateInput = {
  name: Scalars['String'];
  value: Scalars['String'];
};

export type SystemConfigOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type SystemConfigUpdateInput = {
  name?: InputMaybe<Scalars['String']>;
  value?: InputMaybe<Scalars['String']>;
};

export type SystemConfigWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  name?: InputMaybe<StringFilter>;
};

export type Tag = {
  __typename?: 'Tag';
  id: Scalars['Int'];
  translations: Array<TagTranslation>;
};


export type TagTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<TagTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<TagTranslationWhereInput>;
};

export enum TagCodeEnum {
  BannerExploreTag = 'BANNER_EXPLORE_TAG',
  BeautyTipsTag = 'BEAUTY_TIPS_TAG',
  BloggerTag = 'BLOGGER_TAG',
  DescriptionOfProductByImageTag = 'DESCRIPTION_OF_PRODUCT_BY_IMAGE_TAG',
  InformationOfProductTag = 'INFORMATION_OF_PRODUCT_TAG',
  IngredientOfProductTag = 'INGREDIENT_OF_PRODUCT_TAG',
  MaybeYouAlsoLikeTag = 'MAYBE_YOU_ALSO_LIKE_TAG',
  NewProductTag = 'NEW_PRODUCT_TAG',
  OliveyoungTag = 'OLIVEYOUNG_TAG',
  ProductOnVacationTag = 'PRODUCT_ON_VACATION_TAG',
  RealtimeReviewTag = 'REALTIME_REVIEW_TAG',
  ResolverTag = 'RESOLVER_TAG',
  ReviewtyRankingTag = 'REVIEWTY_RANKING_TAG',
  ReviewOfProductTag = 'REVIEW_OF_PRODUCT_TAG',
  SkincareGuideTag = 'SKINCARE_GUIDE_TAG',
  VideoOfProductTag = 'VIDEO_OF_PRODUCT_TAG'
}

export type TagEntity = {
  __typename?: 'TagEntity';
  applyFor?: Maybe<Array<ObjectTypeEnum>>;
  createdAt?: Maybe<Scalars['DateTime']>;
  createdBy?: Maybe<Scalars['Int']>;
  description?: Maybe<Scalars['String']>;
  fields?: Maybe<Array<FieldType>>;
  id: Scalars['Int'];
  isDeleted: Scalars['Boolean'];
  name: Scalars['String'];
  objectTaggings?: Maybe<Array<ObjectTagging>>;
  tagCode?: Maybe<TagCodeEnum>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};


export type TagEntityObjectTaggingsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<TagsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<TagsWhereInput>;
};

export type TagEntityInsertInput = {
  applyFor?: InputMaybe<Array<ObjectTypeEnum>>;
  description?: InputMaybe<Scalars['String']>;
  fields?: InputMaybe<Array<FieldTypeInput>>;
  name: Scalars['String'];
  tagCode?: InputMaybe<TagCodeEnum>;
};

export type TagEntityUpdateInput = {
  applyFor?: InputMaybe<Array<ObjectTypeEnum>>;
  description?: InputMaybe<Scalars['String']>;
  fields?: InputMaybe<Array<FieldTypeInput>>;
  isDeleted?: InputMaybe<Scalars['Boolean']>;
  name?: InputMaybe<Scalars['String']>;
  tagCode?: InputMaybe<TagCodeEnum>;
};

export type TagTranslation = {
  __typename?: 'TagTranslation';
  content: Scalars['String'];
  id: Scalars['Int'];
  language: LanguageCode;
};

export type TagTranslationOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type TagTranslationWhereInput = {
  contentContains?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Float']>;
  language?: InputMaybe<LanguageCode>;
};

export type TagsAggregate = {
  __typename?: 'TagsAggregate';
  countTag: Scalars['Int'];
};

export type TagsConnection = {
  __typename?: 'TagsConnection';
  aggregate: TagsAggregate;
};

export type TagsOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type TagsWhereInput = {
  fromDate?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['Int']>;
  ids?: InputMaybe<Array<Scalars['Int']>>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isDeleted?: InputMaybe<Scalars['Boolean']>;
  screenName?: InputMaybe<ScreenNameType>;
  textSearch?: InputMaybe<Scalars['String']>;
  toDate?: InputMaybe<Scalars['DateTime']>;
  topObjectTagging?: InputMaybe<Scalars['Int']>;
};

export type TopBuyer = {
  __typename?: 'TopBuyer';
  buyer: User;
  buyerId: Scalars['Int'];
  orderTotal?: Maybe<Scalars['Int']>;
};

export enum TopProductFilter {
  Revenue = 'REVENUE',
  SoldTotal = 'SOLD_TOTAL'
}

export type TopShopProduct = {
  __typename?: 'TopShopProduct';
  filterValue?: Maybe<Scalars['Int']>;
  productId: Scalars['Int'];
  shopProduct?: Maybe<ShopProduct>;
};

export type TopShopProductOrderByInput = {
  filterValue?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type TopShopProductWhereInput = {
  filter?: InputMaybe<TopProductFilter>;
  userId?: InputMaybe<Scalars['Int']>;
};

export enum TypeEnum {
  Array = 'ARRAY',
  Float = 'FLOAT',
  Int = 'INT',
  Json = 'JSON',
  String = 'STRING'
}

export type UnbookmarkHashtagPayload = CommonError | Hashtag;

export enum Unit {
  Cfu = 'CFU',
  Cal = 'cal',
  G = 'g',
  Kcal = 'kcal',
  L = 'l',
  Mcg = 'mcg',
  Mg = 'mg',
  Micronutrient = 'micronutrient',
  Ml = 'ml'
}

export type User = {
  __typename?: 'User';
  account: Scalars['String'];
  /** @deprecated use account instead */
  accountName: Scalars['String'];
  /** @deprecated This field will be removed in Sep, 2021 */
  address?: Maybe<Scalars['String']>;
  avatar?: Maybe<Image>;
  baumannAnswers?: Maybe<Array<BaumannAnswer>>;
  baumannSkinType?: Maybe<BaumannSkinType>;
  birthYear?: Maybe<Scalars['Int']>;
  brandAdmin?: Maybe<BrandAdmin>;
  brandsFollowingConnection: BrandsFollowingConnection;
  currentLevel?: Maybe<Level>;
  displayName?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  emailVerified: Scalars['Boolean'];
  /** @deprecated This field will be removed in Sep, 2021 */
  firstName?: Maybe<Scalars['String']>;
  followersConnection: FollowersConnection;
  followingsConnection: FollowingsConnection;
  gender?: Maybe<Gender>;
  id: Scalars['Int'];
  isAnonymous: Scalars['Boolean'];
  isBlockedByViewer?: Maybe<Scalars['Boolean']>;
  /** @deprecated use isFollowedByViewer instead */
  isFollowed?: Maybe<Scalars['Boolean']>;
  isFollowedByViewer?: Maybe<Scalars['Boolean']>;
  /** @deprecated use isFollowingViewer instead */
  isFollowing?: Maybe<Scalars['Boolean']>;
  isFollowingViewer?: Maybe<Scalars['Boolean']>;
  isOfficial?: Maybe<Scalars['Boolean']>;
  isVerified: Scalars['Boolean'];
  /** @deprecated This field will be removed in Sep, 2021 */
  lastName?: Maybe<Scalars['String']>;
  levelPoints: Scalars['Int'];
  nationality?: Maybe<Scalars['String']>;
  nextLevel?: Maybe<Level>;
  phoneNumber?: Maybe<Scalars['String']>;
  postsConnection: PostsConnection;
  profiles: Array<UserProfile>;
  redeemablePoints: Scalars['Int'];
  referralCode: Scalars['ID'];
  reviewReactionsConnection: ReviewReactionsConnection;
  reviewsConnection: ReviewsConnection;
  role: Scalars['String'];
  roleId?: Maybe<Scalars['Int']>;
  shippingAddressUser?: Maybe<ShippingAddress>;
  shop?: Maybe<Shop>;
  /** @deprecated This field will be replaced by Baumann skin types in Sep, 2021 */
  skinType: SkinType;
  socialMediaAccounts?: Maybe<Array<OauthProvider>>;
  totalPoints: Scalars['Int'];
  totalReactPoints?: Maybe<Scalars['Int']>;
  type?: Maybe<AccountType>;
  uid: Scalars['ID'];
  userPath?: Maybe<Scalars['String']>;
  userResources?: Maybe<Array<UserResource>>;
  userRole?: Maybe<Array<Roles>>;
  wishedProductsConnection: ProductsConnection;
  /** @deprecated This field will be removed in Sep, 2021 */
  zipCode?: Maybe<Scalars['Int']>;
};


export type UserBrandsFollowingConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandWhereInput>;
};


export type UserProfilesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserProfileWhereInput>;
};


export type UserReviewsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type UserUserResourcesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IamOrderByInput>;
  parentId?: InputMaybe<Scalars['Int']>;
  parentRoleId?: InputMaybe<Scalars['Int']>;
  resourceId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['Int']>;
};


export type UserUserRoleArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<RolesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<RolesWhereInput>;
};

export type UserDeleteInput = {
  note?: InputMaybe<Scalars['String']>;
  reason: UserDeleteReason;
};

export enum UserDeleteReason {
  AppIsSlowAndBugs = 'APP_IS_SLOW_AND_BUGS',
  InformationIsNotHelpful = 'INFORMATION_IS_NOT_HELPFUL',
  IngredientAreNotAccurate = 'INGREDIENT_ARE_NOT_ACCURATE',
  NeverWinGiveaways = 'NEVER_WIN_GIVEAWAYS',
  Other = 'OTHER',
  PostsAreUnhelpfulAndOrOffensive = 'POSTS_ARE_UNHELPFUL_AND_OR_OFFENSIVE',
  ReviewsAreNotHelpful = 'REVIEWS_ARE_NOT_HELPFUL',
  TakeToMuchEffortToEarnPoints = 'TAKE_TO_MUCH_EFFORT_TO_EARN_POINTS'
}

export type UserLevelObject = {
  __typename?: 'UserLevelObject';
  ALL: Scalars['Boolean'];
  LV1: Scalars['Boolean'];
  LV2: Scalars['Boolean'];
  LV3: Scalars['Boolean'];
  LV4: Scalars['Boolean'];
  LV5: Scalars['Boolean'];
  LV6: Scalars['Boolean'];
  LV7: Scalars['Boolean'];
  LV8: Scalars['Boolean'];
};

export type UserNotificationConfig = {
  __typename?: 'UserNotificationConfig';
  activity?: Maybe<Scalars['Boolean']>;
  events?: Maybe<Scalars['Boolean']>;
  message?: Maybe<Scalars['Boolean']>;
  news?: Maybe<Scalars['Boolean']>;
  points?: Maybe<Scalars['Boolean']>;
};

export type UserNotificationConfigUpdateInput = {
  noType: Scalars['String'];
  value: Scalars['Boolean'];
};

export type UserOrderByInput = {
  id?: InputMaybe<OrderBy>;
  reviewReactions?: InputMaybe<ReviewReactionsConnectionOrderByInput>;
  reviews?: InputMaybe<ReviewsConnectionOrderByInput>;
};

export type UserProfile = {
  __typename?: 'UserProfile';
  displayName: Scalars['String'];
  email?: Maybe<Scalars['String']>;
  externalId: Scalars['String'];
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  provider: OauthProvider;
  userId: Scalars['Int'];
};

export type UserProfileWhereInput = {
  provider?: InputMaybe<OauthProvider>;
  user?: InputMaybe<UserWhereInput>;
};

export type UserRequest = {
  __typename?: 'UserRequest';
  assignedIDsProductByAdminForUser?: Maybe<Array<Scalars['Float']>>;
  changeBy?: Maybe<Scalars['String']>;
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  images?: Maybe<Array<Scalars['String']>>;
  ownerId: Scalars['Int'];
  product?: Maybe<Product>;
  pushedNotificationAt?: Maybe<Scalars['DateTime']>;
  requestedObjectId?: Maybe<Scalars['Float']>;
  status?: Maybe<UserRequestStatus>;
  statusSendNotification?: Maybe<UserRequestSendNotificationStatus>;
  type: UserRequestType;
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type UserRequestAggreate = {
  __typename?: 'UserRequestAggreate';
  count: Scalars['Int'];
};

export type UserRequestConnection = {
  __typename?: 'UserRequestConnection';
  aggregate: UserRequestAggreate;
};

export type UserRequestInput = {
  brandName?: InputMaybe<Scalars['String']>;
  content: Scalars['String'];
  images?: InputMaybe<Array<Scalars['String']>>;
  productName?: InputMaybe<Scalars['String']>;
  requestedObjectId?: InputMaybe<Scalars['Int']>;
  type: UserRequestType;
};

export type UserRequestOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export enum UserRequestSendNotificationStatus {
  Done = 'Done',
  Fail = 'Fail',
  Inprogress = 'Inprogress',
  Unsent = 'Unsent'
}

export enum UserRequestStatus {
  Closed = 'Closed',
  Pending = 'Pending',
  Receive = 'Receive'
}

export enum UserRequestStatusAssign {
  Assigned = 'Assigned',
  Unassigned = 'Unassigned'
}

export enum UserRequestType {
  AddNewProduct = 'AddNewProduct',
  AddNewProductIngredients = 'AddNewProductIngredients',
  EditProductInformation = 'EditProductInformation',
  OthersShop = 'OthersShop',
  ShopReviewty = 'ShopReviewty'
}

export type UserRequestUpdateInput = {
  ids?: InputMaybe<Array<Scalars['Int']>>;
};

export type UserRequestUpdateResponse = {
  __typename?: 'UserRequestUpdateResponse';
  code: Scalars['String'];
  message: Scalars['String'];
  status: Scalars['Float'];
};

export type UserRequestUpdateStatusInput = {
  status: UserRequestStatus;
};

export type UserRequestWhereInput = {
  brandId?: InputMaybe<Scalars['Float']>;
  contentContains?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<Scalars['Int']>;
  idContains?: InputMaybe<Array<Scalars['Float']>>;
  notificationDeliveryStatus?: InputMaybe<UserRequestSendNotificationStatus>;
  ownerId?: InputMaybe<Scalars['Int']>;
  productId?: InputMaybe<Scalars['Int']>;
  productNameContains?: InputMaybe<Scalars['String']>;
  requestedObjectId?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<UserRequestStatus>;
  statusAssign?: InputMaybe<UserRequestStatusAssign>;
  type?: InputMaybe<UserRequestType>;
  userNameContains?: InputMaybe<Scalars['String']>;
};

export type UserRequestWhereUniqueInput = {
  id: Scalars['Int'];
};

export type UserResource = {
  __typename?: 'UserResource';
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['Int']>;
  isSaved?: Maybe<Scalars['Boolean']>;
  resourceId?: Maybe<Scalars['Int']>;
  status?: Maybe<RoleStatus>;
  userId?: Maybe<Scalars['Int']>;
};

export type UserUpdateInput = {
  account?: InputMaybe<Scalars['String']>;
  address?: InputMaybe<Scalars['String']>;
  avatar?: InputMaybe<ImageUpdateManyWithoutUserInput>;
  baumannSkinType?: InputMaybe<BaumannSkinType>;
  birthYear?: InputMaybe<Scalars['Int']>;
  displayName?: InputMaybe<Scalars['String']>;
  fullName?: InputMaybe<Scalars['String']>;
  fullname?: InputMaybe<Scalars['String']>;
  gender?: InputMaybe<Gender>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  role?: InputMaybe<Scalars['String']>;
  roleId?: InputMaybe<Scalars['Int']>;
  skinType?: InputMaybe<SkinType>;
  userPath?: InputMaybe<Scalars['String']>;
  zipCode?: InputMaybe<Scalars['Int']>;
};

export type UserWhereInput = {
  account?: InputMaybe<StringFilter>;
  accountContains?: InputMaybe<Scalars['String']>;
  accountStartsWith?: InputMaybe<Scalars['String']>;
  baumannSkinType?: InputMaybe<BaumannSkinTypeFilter>;
  birthYearOR?: InputMaybe<Array<IntFilter>>;
  displayName?: InputMaybe<StringFilter>;
  followersSome?: InputMaybe<UserWhereInput>;
  followingsSome?: InputMaybe<UserWhereInput>;
  gender?: InputMaybe<Gender>;
  genderIn?: InputMaybe<Array<Gender>>;
  id?: InputMaybe<Scalars['Int']>;
  isBlockedByViewer?: InputMaybe<Scalars['Boolean']>;
  skinType?: InputMaybe<SkinType>;
  skinTypeIn?: InputMaybe<Array<SkinType>>;
  userOnly?: InputMaybe<Scalars['Boolean']>;
};

export type UserWhereUniqueInput = {
  account?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  isOfficial?: InputMaybe<Scalars['Boolean']>;
};

export type UsersAggregate = {
  __typename?: 'UsersAggregate';
  count: Scalars['Int'];
};

export type UsersConnection = {
  __typename?: 'UsersConnection';
  aggregate: UsersAggregate;
};

export type VariantReviewInsertInput = {
  comment?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<Array<ImageReviewInput>>;
  productId: Scalars['Int'];
  rate: Scalars['Int'];
  variantId: Scalars['Int'];
};

export type Video = {
  __typename?: 'Video';
  details?: Maybe<VideoDetails>;
  status: VideoStatus;
  thumbnail?: Maybe<VideoThumbnail>;
};

export type VideoCreateManyWithoutPostInput = {
  create?: InputMaybe<Array<VideoCreateWithoutPostInput>>;
};

export type VideoCreateWithoutPostInput = {
  name?: InputMaybe<Scalars['String']>;
  thumbnailUrl?: InputMaybe<Scalars['String']>;
  url: Scalars['String'];
};

export type VideoDetails = {
  __typename?: 'VideoDetails';
  height: Scalars['Int'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type VideoDetailsUpdateInput = {
  height: Scalars['Int'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type VideoObject = {
  __typename?: 'VideoObject';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  name: Scalars['String'];
  thumbnailUrl?: Maybe<Scalars['String']>;
  url: Scalars['String'];
};

export type VideoOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export enum VideoStatus {
  Complete = 'COMPLETE',
  Error = 'ERROR',
  InProgressing = 'IN_PROGRESSING'
}

export type VideoThumbnail = {
  __typename?: 'VideoThumbnail';
  height: Scalars['Int'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type VideoThumbnailUpdateInput = {
  height: Scalars['Int'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type VideoUpdateManyWithoutPostInput = {
  update?: InputMaybe<Array<VideosUpdateWithoutPostInput>>;
};

export type VideoUpdateOneWithoutPostInput = {
  update: VideoUpdateWithoutPostInput;
};

export type VideoUpdateWithoutPostInput = {
  details: VideoDetailsUpdateInput;
  thumbnail: VideoThumbnailUpdateInput;
};

export type VideoWhereInput = {
  nonNull?: InputMaybe<Scalars['Boolean']>;
  status?: InputMaybe<VideoStatus>;
};

export type VideosUpdateWithoutPostInput = {
  id?: InputMaybe<Scalars['Int']>;
  thumbnailUrl?: InputMaybe<Scalars['String']>;
};

export type VideosWhereInput = {
  review?: InputMaybe<ReviewWhereInput>;
};

export type ViewedPostStatistic = {
  __typename?: 'ViewedPostStatistic';
  click?: Maybe<Scalars['Int']>;
  multiple?: Maybe<Scalars['Int']>;
  single?: Maybe<Scalars['Int']>;
};

export type VisitedContentArgsOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
};

export type VisitedContentCreateInput = {
  entityId?: InputMaybe<Scalars['Int']>;
  type: VisitedContentType;
};

export type VisitedContentEntityUnion = Post | Product;

export enum VisitedContentType {
  Post = 'Post',
  Product = 'Product'
}

export type VisitedContentWhereInput = {
  createdAt?: InputMaybe<DateTimeFilter>;
  type?: InputMaybe<VisitedContentType>;
};

export type VnBank = {
  __typename?: 'VnBank';
  bankName: Scalars['String'];
  brandName?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  logo: Scalars['String'];
};

export type Voucher = {
  __typename?: 'Voucher';
  barcode?: Maybe<Scalars['String']>;
  brandCode?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  expiryDate: Scalars['DateTime'];
  goodsCode: Scalars['String'];
  goodsList: GoodsList;
  id: Scalars['Int'];
  imgCoupon?: Maybe<Scalars['String']>;
  imgOrigin?: Maybe<Scalars['String']>;
  note: Scalars['String'];
  password?: Maybe<Scalars['String']>;
  pinNo: Scalars['String'];
  pinUrl: Scalars['String'];
  redeemOnDeepLink?: Maybe<Scalars['String']>;
  rpPoint?: Maybe<Scalars['Int']>;
  status?: Maybe<Scalars['String']>;
  storeCode?: Maybe<Scalars['String']>;
  trId: Scalars['String'];
  usedTime?: Maybe<Scalars['DateTime']>;
  user: User;
  userId: Scalars['Int'];
};

export type VoucherArgsOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type VoucherBrand = {
  __typename?: 'VoucherBrand';
  code: Scalars['String'];
  id: Scalars['Int'];
  logo: Scalars['String'];
  name: Scalars['String'];
};

export type VoucherCategories = {
  __typename?: 'VoucherCategories';
  code: Scalars['String'];
  id: Scalars['Int'];
  name: Scalars['String'];
  name_en: Scalars['String'];
  name_kr: Scalars['String'];
};

export type VoucherCategoriesWhereInput = {
  code?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type VoucherGiftpopListUpdateInput = {
  voucherList: Array<VoucherGiftpopUpdateInput>;
};

export type VoucherGiftpopUpdateInput = {
  storeCode: Scalars['String'];
  useStatus: Scalars['String'];
  usedTime: Scalars['String'];
  voucherId: Scalars['String'];
};

export type VoucherIssuesInput = {
  goodsId: Scalars['String'];
  quantity: Scalars['Int'];
};

export type VoucherOrder = {
  __typename?: 'VoucherOrder';
  createdAt?: Maybe<Scalars['DateTime']>;
  giftpopMessage?: Maybe<Scalars['String']>;
  giftpopStatus?: Maybe<Scalars['Boolean']>;
  goodsId?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  nbVouchers?: Maybe<Scalars['Int']>;
  pointAfter?: Maybe<Scalars['Int']>;
  pointBefore?: Maybe<Scalars['Int']>;
  userId?: Maybe<Scalars['Int']>;
};

export enum VoucherStatus {
  Expired = 'Expired',
  InUse = 'InUse',
  Used = 'Used'
}

export type VoucherStoreWhereInput = {
  brandCode: Scalars['String'];
};

export type VoucherVendor = {
  __typename?: 'VoucherVendor';
  id: Scalars['Int'];
  logo: Scalars['String'];
  name: Scalars['String'];
};

export type VoucherWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<VoucherStatus>;
};

export type WardInput = {
  districtCode: Scalars['String'];
};

export type Wards = {
  __typename?: 'Wards';
  district_code: Scalars['String'];
  id: Scalars['Int'];
  province_code: Scalars['String'];
  ward_code: Scalars['String'];
  ward_name: Scalars['String'];
  ward_normalized_name: Scalars['String'];
};

export type FundingDetailQueryVariables = Exact<{
  where: FundingWhereInput;
}>;


export type FundingDetailQuery = { __typename?: 'Query', funding: { __typename?: 'Funding', preOrder?: boolean | null, preOrderQuantity?: number | null, freeShip?: boolean | null, productNbOfPreOrder?: number | null, id: number, coverUrl?: string | null, shortDescription?: string | null, longDescription?: string | null, startDate?: any | null, endDate?: any | null, condition?: string | null, subTitle?: string | null, fundingProducts: Array<{ __typename?: 'FundingProduct', id: number, productId?: number | null, quantity?: number | null, discount?: number | null, product?: { __typename?: 'Product', price?: number | null, id: number, attributes?: { __typename?: 'ProductAttribute', netWeightPerPackage?: number | null } | null, thumbnail?: { __typename?: 'Image', url: string, small: { __typename?: 'Image', url: string } } | null, translations: Array<{ __typename?: 'ProductTranslation', name: string }>, brand: { __typename?: 'Brand', logoUrl: string, id: number, smallLogoUrl?: string | null, translations: Array<{ __typename?: 'BrandTranslation', name: string }> } } | null }> } };

export type FundingProductQueryVariables = Exact<{
  skip?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingWhereInput>;
  orderBy?: InputMaybe<FundingOrderByInput>;
}>;


export type FundingProductQuery = { __typename?: 'Query', fundingProduct: { __typename?: 'FundingProduct', id: number, discount?: number | null, quantity?: number | null, productId?: number | null, product?: { __typename?: 'Product', id: number, price?: number | null, reviews?: Array<{ __typename?: 'Review', rate: number, id: number, content?: string | null, user: { __typename?: 'User', gender?: Gender | null, account: string, baumannSkinType?: BaumannSkinType | null, birthYear?: number | null, avatar?: { __typename?: 'Image', url: string } | null } }> | null, reviewsConnection: { __typename?: 'ReviewsConnection', aggregate: { __typename?: 'ReviewsAggregate', count: number, avg: { __typename?: 'ReviewsAvg', rate: number } } }, thumbnail?: { __typename?: 'Image', url: string } | null, translations: Array<{ __typename?: 'ProductTranslation', name: string }>, brand: { __typename?: 'Brand', nbFollowing: number, logoUrl: string, id: number, smallLogoUrl?: string | null, translations: Array<{ __typename?: 'BrandTranslation', name: string }> } } | null, funding: { __typename?: 'Funding', preOrder?: boolean | null, preOrderQuantity?: number | null, freeShip?: boolean | null, productNbOfPreOrder?: number | null, nbOfPreOrder?: number | null, id: number, longDescription?: string | null, shortDescription?: string | null, endDate?: any | null, startDate?: any | null } } };

export type FundingsQueryVariables = Exact<{
  skip?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingWhereInput>;
  orderBy?: InputMaybe<FundingOrderByInput>;
}>;


export type FundingsQuery = { __typename?: 'Query', fundings: Array<{ __typename?: 'Funding', isVisible?: boolean | null, preOrderQuantity?: number | null, freeShip?: boolean | null, productNbOfPreOrder?: number | null, preOrder?: boolean | null, eventId?: number | null, id: number, coverUrl?: string | null, shortDescription?: string | null, longDescription?: string | null, startDate?: any | null, title?: string | null, subTitle?: string | null, endDate?: any | null, event?: { __typename?: 'Event', isParticipantOfCurrentUser?: boolean | null, coverUrlFunding?: string | null, startedAt: any, endedAt: any, coverUrl: string, name: string, id: number, currentNumberOfWinners: number, numberOfWinners?: number | null, products: Array<{ __typename?: 'Product', price?: number | null, id: number, thumbnail?: { __typename?: 'Image', url: string, small: { __typename?: 'Image', url: string } } | null, translations: Array<{ __typename?: 'ProductTranslation', name: string }>, brand: { __typename?: 'Brand', logoUrl: string, id: number, smallLogoUrl?: string | null, translations: Array<{ __typename?: 'BrandTranslation', name: string }> } }> } | null, fundingProducts: Array<{ __typename?: 'FundingProduct', id: number, productId?: number | null, quantity?: number | null, discount?: number | null, product?: { __typename?: 'Product', price?: number | null, id: number, attributes?: { __typename?: 'ProductAttribute', netWeightPerPackage?: number | null } | null, thumbnail?: { __typename?: 'Image', url: string, small: { __typename?: 'Image', url: string } } | null, translations: Array<{ __typename?: 'ProductTranslation', name: string }>, brand: { __typename?: 'Brand', logoUrl: string, id: number, smallLogoUrl?: string | null, translations: Array<{ __typename?: 'BrandTranslation', name: string }> } } | null }> }>, fundingsConnection: { __typename?: 'FundingsConnection', aggregate: { __typename?: 'FundingsAggregate', countFunding: number } } };

export type ReadEventCommentQueryVariables = Exact<{
  eventId?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
}>;


export type ReadEventCommentQuery = { __typename?: 'Query', eventComments: Array<{ __typename: 'EventComment', id: number, content: string, status: EventCommentStatus, createdAt: any, product?: { __typename: 'Product', id: number, translations: Array<{ __typename: 'ProductTranslation', name: string }>, thumbnail?: { __typename: 'Image', url: string } | null } | null, user: { __typename: 'User', id: number, email?: string | null, account: string, gender?: Gender | null, baumannSkinType?: BaumannSkinType | null, birthYear?: number | null, nationality?: string | null, isOfficial?: boolean | null, isFollowingViewer?: boolean | null, type?: AccountType | null, isBlockedByViewer?: boolean | null, isAnonymous: boolean, avatar?: { __typename: 'Image', url: string } | null, shop?: { __typename: 'Shop', id: number, name: string, status: ShopStatus } | null } }>, eventCommentsConnection: { __typename: 'EventCommentConnection', aggregate: { __typename: 'EventCommentAggregate', count: number } } };

export type EventCommentInfoFragment = { __typename: 'EventComment', id: number, content: string, status: EventCommentStatus, createdAt: any, product?: { __typename: 'Product', id: number, translations: Array<{ __typename: 'ProductTranslation', name: string }>, thumbnail?: { __typename: 'Image', url: string } | null } | null, user: { __typename: 'User', id: number, email?: string | null, account: string, gender?: Gender | null, baumannSkinType?: BaumannSkinType | null, birthYear?: number | null, nationality?: string | null, isOfficial?: boolean | null, isFollowingViewer?: boolean | null, type?: AccountType | null, isBlockedByViewer?: boolean | null, isAnonymous: boolean, avatar?: { __typename: 'Image', url: string } | null, shop?: { __typename: 'Shop', id: number, name: string, status: ShopStatus } | null } };

export type UserInfoShortFieldFragment = { __typename: 'User', id: number, email?: string | null, account: string, gender?: Gender | null, baumannSkinType?: BaumannSkinType | null, birthYear?: number | null, nationality?: string | null, isOfficial?: boolean | null, isFollowingViewer?: boolean | null, type?: AccountType | null, isBlockedByViewer?: boolean | null, isAnonymous: boolean, avatar?: { __typename: 'Image', url: string } | null, shop?: { __typename: 'Shop', id: number, name: string, status: ShopStatus } | null };

export type GetEventDetailQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type GetEventDetailQuery = { __typename?: 'Query', event?: { __typename: 'Event', id: number, name: string, startedAt: any, endedAt: any, content: string, points?: number | null, condition: string, coverUrl: string, coverUrlFunding?: string | null, type: EventType, numberOfWinners?: number | null, currentNumberOfWinners: number, referred_url?: string | null, commentsConnection: { __typename: 'EventCommentConnection', aggregate: { __typename: 'EventCommentAggregate', count: number } }, products: Array<{ __typename: 'Product', price?: number | null, id: number, reviews?: Array<{ __typename?: 'Review', rate: number, id: number, content?: string | null, user: { __typename?: 'User', gender?: Gender | null, account: string, baumannSkinType?: BaumannSkinType | null, birthYear?: number | null, avatar?: { __typename?: 'Image', url: string } | null } }> | null, reviewsConnection: { __typename?: 'ReviewsConnection', aggregate: { __typename?: 'ReviewsAggregate', count: number, avg: { __typename?: 'ReviewsAvg', rate: number } } }, translations: Array<{ __typename?: 'ProductTranslation', name: string }>, thumbnail?: { __typename?: 'Image', url: string, small: { __typename?: 'Image', url: string } } | null, brand: { __typename?: 'Brand', nbFollowing: number, logoUrl: string, id: number, smallLogoUrl?: string | null, translations: Array<{ __typename?: 'BrandTranslation', name: string }> } }> } | null };

export type EventInfoFragment = { __typename: 'Event', id: number, name: string, startedAt: any, endedAt: any, content: string, points?: number | null, condition: string, coverUrl: string, coverUrlFunding?: string | null, type: EventType, numberOfWinners?: number | null, currentNumberOfWinners: number, referred_url?: string | null, commentsConnection: { __typename: 'EventCommentConnection', aggregate: { __typename: 'EventCommentAggregate', count: number } }, products: Array<{ __typename: 'Product', price?: number | null, id: number, reviews?: Array<{ __typename?: 'Review', rate: number, id: number, content?: string | null, user: { __typename?: 'User', gender?: Gender | null, account: string, baumannSkinType?: BaumannSkinType | null, birthYear?: number | null, avatar?: { __typename?: 'Image', url: string } | null } }> | null, reviewsConnection: { __typename?: 'ReviewsConnection', aggregate: { __typename?: 'ReviewsAggregate', count: number, avg: { __typename?: 'ReviewsAvg', rate: number } } }, translations: Array<{ __typename?: 'ProductTranslation', name: string }>, thumbnail?: { __typename?: 'Image', url: string, small: { __typename?: 'Image', url: string } } | null, brand: { __typename?: 'Brand', nbFollowing: number, logoUrl: string, id: number, smallLogoUrl?: string | null, translations: Array<{ __typename?: 'BrandTranslation', name: string }> } }> };

export type GetEventListQueryVariables = Exact<{
  skip?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventWhereInput>;
  orderBy?: InputMaybe<EventOrderByInput>;
}>;


export type GetEventListQuery = { __typename?: 'Query', events: Array<{ __typename?: 'Event', id: number, name: string, startedAt: any, endedAt: any, coverUrl: string, type: EventType, referred_url?: string | null, currentNumberOfWinners: number, maximumNumberOfWinners?: number | null, commentsConnection: { __typename?: 'EventCommentConnection', aggregate: { __typename?: 'EventCommentAggregate', count: number } }, notifications: Array<{ __typename?: 'EventNotification', id: string }> }>, eventsConnection: { __typename?: 'EventConnection', aggregate: { __typename?: 'EventAggregate', count: number } } };

export const UserInfoShortFieldFragmentDoc = gql`
    fragment userInfoShortField on User {
  __typename
  id
  email
  account
  gender
  baumannSkinType
  birthYear
  avatar {
    __typename
    url
  }
  shop {
    __typename
    id
    name
    status
  }
  nationality
  isOfficial
  isFollowingViewer
  type
  isBlockedByViewer
  isAnonymous
}
    `;
export const EventCommentInfoFragmentDoc = gql`
    fragment eventCommentInfo on EventComment {
  __typename
  id
  content
  status
  product {
    __typename
    id
    translations(where: {language: VI}) {
      __typename
      name
    }
    thumbnail {
      __typename
      url
    }
  }
  user {
    __typename
    ...userInfoShortField
  }
  createdAt
}
    ${UserInfoShortFieldFragmentDoc}`;
export const EventInfoFragmentDoc = gql`
    fragment eventInfo on Event {
  __typename
  id
  name
  startedAt
  endedAt
  content
  points
  condition
  coverUrl
  coverUrlFunding
  type
  numberOfWinners
  currentNumberOfWinners
  commentsConnection {
    __typename
    aggregate {
      __typename
      count
    }
  }
  products {
    reviews {
      rate
      id
      content
      user {
        gender
        account
        baumannSkinType
        birthYear
        avatar {
          url
        }
      }
    }
    reviewsConnection {
      aggregate {
        count
        avg {
          rate
        }
      }
    }
    translations(where: {language: VI}) {
      name
    }
    price
    thumbnail {
      small: fixed(width: SMALL) {
        url
      }
      url
    }
    __typename
    id
    brand {
      nbFollowing
      logoUrl
      smallLogoUrl: fixedLogoUrl(width: SMALL)
      id
      translations {
        name
      }
    }
  }
  referred_url
}
    `;
export const FundingDetailDocument = gql`
    query fundingDetail($where: FundingWhereInput!) {
  funding(where: $where) {
    preOrder
    preOrderQuantity
    freeShip
    productNbOfPreOrder
    id
    coverUrl
    shortDescription
    longDescription
    startDate
    endDate
    condition
    subTitle
    fundingProducts {
      id
      productId
      quantity
      discount
      product {
        attributes {
          netWeightPerPackage
        }
        price
        thumbnail {
          small: fixed(width: SMALL) {
            url
          }
          url
        }
        translations(where: {language: VI}) {
          name
        }
        brand {
          logoUrl
          smallLogoUrl: fixedLogoUrl(width: SMALL)
          id
          translations {
            name
          }
        }
        id
      }
    }
  }
}
    `;

/**
 * __useFundingDetailQuery__
 *
 * To run a query within a React component, call `useFundingDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `useFundingDetailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFundingDetailQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useFundingDetailQuery(baseOptions: Apollo.QueryHookOptions<FundingDetailQuery, FundingDetailQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FundingDetailQuery, FundingDetailQueryVariables>(FundingDetailDocument, options);
      }
export function useFundingDetailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FundingDetailQuery, FundingDetailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FundingDetailQuery, FundingDetailQueryVariables>(FundingDetailDocument, options);
        }
export type FundingDetailQueryHookResult = ReturnType<typeof useFundingDetailQuery>;
export type FundingDetailLazyQueryHookResult = ReturnType<typeof useFundingDetailLazyQuery>;
export type FundingDetailQueryResult = Apollo.QueryResult<FundingDetailQuery, FundingDetailQueryVariables>;
export const FundingProductDocument = gql`
    query fundingProduct($skip: Int = 0, $after: ID, $before: ID, $first: Int = 10, $last: Int, $where: FundingWhereInput, $orderBy: FundingOrderByInput) {
  fundingProduct(
    skip: $skip
    after: $after
    before: $before
    first: $first
    last: $last
    where: $where
    orderBy: $orderBy
  ) {
    id
    discount
    quantity
    productId
    product {
      reviews {
        rate
        id
        content
        user {
          gender
          account
          baumannSkinType
          birthYear
          avatar {
            url
          }
        }
      }
      reviewsConnection {
        aggregate {
          count
          avg {
            rate
          }
        }
      }
      id
      thumbnail {
        url
      }
      price
      translations {
        name
      }
      brand {
        nbFollowing
        logoUrl
        smallLogoUrl: fixedLogoUrl(width: SMALL)
        id
        translations {
          name
        }
      }
    }
    funding {
      preOrder
      preOrderQuantity
      freeShip
      productNbOfPreOrder
      nbOfPreOrder
      id
      longDescription
      shortDescription
      endDate
      startDate
    }
  }
}
    `;

/**
 * __useFundingProductQuery__
 *
 * To run a query within a React component, call `useFundingProductQuery` and pass it any options that fit your needs.
 * When your component renders, `useFundingProductQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFundingProductQuery({
 *   variables: {
 *      skip: // value for 'skip'
 *      after: // value for 'after'
 *      before: // value for 'before'
 *      first: // value for 'first'
 *      last: // value for 'last'
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *   },
 * });
 */
export function useFundingProductQuery(baseOptions?: Apollo.QueryHookOptions<FundingProductQuery, FundingProductQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FundingProductQuery, FundingProductQueryVariables>(FundingProductDocument, options);
      }
export function useFundingProductLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FundingProductQuery, FundingProductQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FundingProductQuery, FundingProductQueryVariables>(FundingProductDocument, options);
        }
export type FundingProductQueryHookResult = ReturnType<typeof useFundingProductQuery>;
export type FundingProductLazyQueryHookResult = ReturnType<typeof useFundingProductLazyQuery>;
export type FundingProductQueryResult = Apollo.QueryResult<FundingProductQuery, FundingProductQueryVariables>;
export const FundingsDocument = gql`
    query fundings($skip: Int = 0, $after: ID, $before: ID, $first: Int = 10, $last: Int, $where: FundingWhereInput, $orderBy: FundingOrderByInput) {
  fundings(
    skip: $skip
    after: $after
    before: $before
    first: $first
    last: $last
    where: $where
    orderBy: $orderBy
  ) {
    isVisible
    preOrderQuantity
    freeShip
    productNbOfPreOrder
    preOrder
    eventId
    event {
      isParticipantOfCurrentUser
      coverUrlFunding
      startedAt
      endedAt
      coverUrl
      name
      id
      currentNumberOfWinners
      numberOfWinners
      products {
        price
        thumbnail {
          small: fixed(width: SMALL) {
            url
          }
          url
        }
        translations(where: {language: VI}) {
          name
        }
        brand {
          logoUrl
          smallLogoUrl: fixedLogoUrl(width: SMALL)
          id
          translations {
            name
          }
        }
        id
      }
    }
    id
    coverUrl
    shortDescription
    longDescription
    startDate
    title
    subTitle
    endDate
    fundingProducts {
      id
      productId
      quantity
      discount
      product {
        attributes {
          netWeightPerPackage
        }
        price
        thumbnail {
          small: fixed(width: SMALL) {
            url
          }
          url
        }
        translations(where: {language: VI}) {
          name
        }
        brand {
          logoUrl
          smallLogoUrl: fixedLogoUrl(width: SMALL)
          id
          translations {
            name
          }
        }
        id
      }
    }
  }
  fundingsConnection(
    skip: $skip
    after: $after
    before: $before
    first: $first
    last: $last
    where: $where
    orderBy: $orderBy
  ) {
    aggregate {
      countFunding
    }
  }
}
    `;

/**
 * __useFundingsQuery__
 *
 * To run a query within a React component, call `useFundingsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFundingsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFundingsQuery({
 *   variables: {
 *      skip: // value for 'skip'
 *      after: // value for 'after'
 *      before: // value for 'before'
 *      first: // value for 'first'
 *      last: // value for 'last'
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *   },
 * });
 */
export function useFundingsQuery(baseOptions?: Apollo.QueryHookOptions<FundingsQuery, FundingsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FundingsQuery, FundingsQueryVariables>(FundingsDocument, options);
      }
export function useFundingsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FundingsQuery, FundingsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FundingsQuery, FundingsQueryVariables>(FundingsDocument, options);
        }
export type FundingsQueryHookResult = ReturnType<typeof useFundingsQuery>;
export type FundingsLazyQueryHookResult = ReturnType<typeof useFundingsLazyQuery>;
export type FundingsQueryResult = Apollo.QueryResult<FundingsQuery, FundingsQueryVariables>;
export const ReadEventCommentDocument = gql`
    query readEventComment($eventId: Int, $userId: Int, $first: Int, $skip: Int) {
  eventComments(
    first: $first
    skip: $skip
    where: {event: {id: $eventId}, user: {id: $userId}}
    orderBy: {id: DESC}
  ) {
    __typename
    ...eventCommentInfo
  }
  eventCommentsConnection(where: {event: {id: $eventId}, user: {id: $userId}}) {
    __typename
    aggregate {
      __typename
      count
    }
  }
}
    ${EventCommentInfoFragmentDoc}`;

/**
 * __useReadEventCommentQuery__
 *
 * To run a query within a React component, call `useReadEventCommentQuery` and pass it any options that fit your needs.
 * When your component renders, `useReadEventCommentQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useReadEventCommentQuery({
 *   variables: {
 *      eventId: // value for 'eventId'
 *      userId: // value for 'userId'
 *      first: // value for 'first'
 *      skip: // value for 'skip'
 *   },
 * });
 */
export function useReadEventCommentQuery(baseOptions?: Apollo.QueryHookOptions<ReadEventCommentQuery, ReadEventCommentQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ReadEventCommentQuery, ReadEventCommentQueryVariables>(ReadEventCommentDocument, options);
      }
export function useReadEventCommentLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ReadEventCommentQuery, ReadEventCommentQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ReadEventCommentQuery, ReadEventCommentQueryVariables>(ReadEventCommentDocument, options);
        }
export type ReadEventCommentQueryHookResult = ReturnType<typeof useReadEventCommentQuery>;
export type ReadEventCommentLazyQueryHookResult = ReturnType<typeof useReadEventCommentLazyQuery>;
export type ReadEventCommentQueryResult = Apollo.QueryResult<ReadEventCommentQuery, ReadEventCommentQueryVariables>;
export const GetEventDetailDocument = gql`
    query getEventDetail($id: Int!) {
  event(where: {id: $id}) {
    __typename
    ...eventInfo
  }
}
    ${EventInfoFragmentDoc}`;

/**
 * __useGetEventDetailQuery__
 *
 * To run a query within a React component, call `useGetEventDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetEventDetailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetEventDetailQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetEventDetailQuery(baseOptions: Apollo.QueryHookOptions<GetEventDetailQuery, GetEventDetailQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetEventDetailQuery, GetEventDetailQueryVariables>(GetEventDetailDocument, options);
      }
export function useGetEventDetailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetEventDetailQuery, GetEventDetailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetEventDetailQuery, GetEventDetailQueryVariables>(GetEventDetailDocument, options);
        }
export type GetEventDetailQueryHookResult = ReturnType<typeof useGetEventDetailQuery>;
export type GetEventDetailLazyQueryHookResult = ReturnType<typeof useGetEventDetailLazyQuery>;
export type GetEventDetailQueryResult = Apollo.QueryResult<GetEventDetailQuery, GetEventDetailQueryVariables>;
export const GetEventListDocument = gql`
    query getEventList($skip: Int, $after: ID, $before: ID, $first: Int, $last: Int, $where: EventWhereInput, $orderBy: EventOrderByInput) {
  events(
    skip: $skip
    after: $after
    before: $before
    first: $first
    last: $last
    where: $where
    orderBy: $orderBy
  ) {
    id
    name
    startedAt
    endedAt
    coverUrl
    type
    referred_url
    commentsConnection {
      aggregate {
        count
      }
    }
    currentNumberOfWinners
    maximumNumberOfWinners
    notifications {
      id
    }
  }
  eventsConnection(
    skip: $skip
    after: $after
    before: $before
    first: $first
    last: $last
    where: $where
    orderBy: $orderBy
  ) {
    aggregate {
      count
    }
  }
}
    `;

/**
 * __useGetEventListQuery__
 *
 * To run a query within a React component, call `useGetEventListQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetEventListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetEventListQuery({
 *   variables: {
 *      skip: // value for 'skip'
 *      after: // value for 'after'
 *      before: // value for 'before'
 *      first: // value for 'first'
 *      last: // value for 'last'
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *   },
 * });
 */
export function useGetEventListQuery(baseOptions?: Apollo.QueryHookOptions<GetEventListQuery, GetEventListQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetEventListQuery, GetEventListQueryVariables>(GetEventListDocument, options);
      }
export function useGetEventListLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetEventListQuery, GetEventListQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetEventListQuery, GetEventListQueryVariables>(GetEventListDocument, options);
        }
export type GetEventListQueryHookResult = ReturnType<typeof useGetEventListQuery>;
export type GetEventListLazyQueryHookResult = ReturnType<typeof useGetEventListLazyQuery>;
export type GetEventListQueryResult = Apollo.QueryResult<GetEventListQuery, GetEventListQueryVariables>;