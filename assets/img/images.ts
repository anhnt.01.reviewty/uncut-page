import fbLogo from "@public/facebook.svg";
import nextStep from "@public/auth/next-step.svg";
import eye from "@public/auth/eye.svg";
import logoInline from "@public/logo-inline.png";
import eyeClose from "@public/auth/eye-closed.svg";
import card from "@public/card.svg";
import sideBar from "@public/sidebar.svg";
import search from "@public/search.svg";
import mainLogo from "@public/funding-logo.png";
import add from "@public/add.svg";
import toPay from "@public/to-pay.svg";
import toShip from "@public/to-ship.svg";
import toReceive from "@public/to-receive.svg";
import delivered from "@public/delivered.svg";
import arrowRight from "@public/arrow-right.svg";
import buySuccess from "@public/result/buy-success.svg";
import buyErrors from "@public/result/buy-error.svg";
import invoice from "@public/orders/invoice.svg";
import pickAddressIcon from "@public/orders/pick-address.svg";
import copyIcon from "@public/orders/copy.svg";
import closeIcon from "@public/close.svg";
import fb from "@public/facebook.svg";
import fbIconFillBlack from "@public/facebook-fill-black.svg";
import instaIconFillBlack from "@public/instagram.svg";
import appStore from "@public/footer/download-on-the-app-store-badge 1.png";
import googlePlay from "@public/footer/1024px-Google_Play_Store_badge_EN 1.png";
import reviewtyLogo from "@public/footer/reviewty.png";
import reviewtyLogo2 from "@public/footer/1.png";
import arrowDown from "@public/footer/arrow-down.svg";
import arrowDown2 from "@public/footer/arrow-down-2.svg";
import opps from "@public/opps.svg";
import wishlistIconProductCard from "@public/wishlist-2.svg";
import categoriesIconProductCard from "@public/categories.svg";
import faviconLogo from "@public/favicon-logo.ico";
import eventNotYet from "@public/event-not-yet.svg";
import language from "@public/lang/language-icon.svg";
import noOrder from "@public/orders/no-order.svg";
import waitDelivering from "@public/orders/wait-delivering.svg";
import delivering from "@public/orders/delivering.svg";
import completed from "@public/orders/completed.svg";
import cancelled from "@public/orders/cancelled.svg";
import closeWhite from "@public/close-white.svg";
import badIcon from "@public/emotion/bad.svg";
import notGoodIcon from "@public/emotion/not-good.svg";
import normallyIcon from "@public/emotion/normally.svg";
import goodIcon from "@public/emotion/good.svg";
import veryGoodIcon from "@public/emotion/very-good.svg";
import uploadImg from "@public/upload-img.svg";
import maleIcon from "@public/male.svg";
import femaleIcon from "@public/female.svg";
import QRCode from "@public/footer/QR.png";
import GGPlay from "@public/footer/google-play.png";
import AplleStore from "@public/footer/apple-store.png";
import SquareLogo from "@public/squares.png";
import VideoDefault from "@public/video-default.png";
import ImgLogin from "@public/faq/page-one/img-login.png";
import ImgHomePage from "@public/faq/page-one/img-home-page.png";
import ImgMenu from "@public/faq/page-one/img-menu.png";
import TwoWaysPayment from "@public/faq/page-two/two-ways-payment.png";
import FirstWay from "@public/faq/page-two/first-way.png";
import SecondWay1 from "@public/faq/page-two/second_way_1.png";
import SecondWay2 from "@public/faq/page-two/second_way_2.png";
import SecondWay3 from "@public/faq/page-two/second_way_3.png";
import MainPageImg from "@public/faq/page-five/main-page.png";
import OrderHistory from "@public/faq/page-five/order-history.png";
import OrderHistory2 from "@public/faq/page-five/order-history-2.png";
import Follower from "@public/follower-icon.svg";
import ShippingIcon from "@public/orders/shipping.svg";
import EventTag from "@public/event-tag.svg";
import FreeShip from "@public/product-detail/freeShip.png";
import PreOrder from "@public/pre-order.png";
import Event from "@public/pre-icon.png";
import Joined from "@public/joined.svg";
import CheckedJoin from "@public/checked-join.svg";
import confirmedMinistryOfIndustryAndTrade from "@public/confirmed.png";
const IMAGES = {
  confirmedMinistryOfIndustryAndTrade,
  arrowDown2,
  CheckedJoin,
  Event,
  Joined,
  PreOrder,
  FreeShip,
  EventTag,
  ShippingIcon,
  Follower,
  MainPageImg,
  OrderHistory,
  OrderHistory2,
  TwoWaysPayment,
  FirstWay,
  SecondWay1,
  SecondWay2,
  SecondWay3,
  ImgMenu,
  ImgHomePage,
  ImgLogin,
  VideoDefault,
  SquareLogo,
  GGPlay,
  AplleStore,
  QRCode,
  maleIcon,
  femaleIcon,
  uploadImg,
  veryGoodIcon,
  goodIcon,
  normallyIcon,
  notGoodIcon,
  badIcon,
  closeWhite,
  cancelled,
  completed,
  delivering,
  waitDelivering,
  noOrder,
  language,
  eventNotYet,
  faviconLogo,
  wishlistIconProductCard,
  categoriesIconProductCard,
  arrowDown,
  opps,
  appStore,
  googlePlay,
  reviewtyLogo2,
  reviewtyLogo,
  fbIconFillBlack,
  instaIconFillBlack,
  fb,
  closeIcon,
  buyErrors,
  toPay,
  toShip,
  toReceive,
  delivered,
  arrowRight,
  invoice,
  buySuccess,
  add,
  fbLogo,
  nextStep,
  eye,
  logoInline,
  eyeClose,
  card,
  sideBar,
  search,
  mainLogo,
  pickAddressIcon,
  copyIcon,
};

export default IMAGES;
