import authSlice from "@/components/auth/authSlice";
import mainSlice from "@/components/main/mainSlice";
import addressSlice from "@/components/address/addressSlice";
import checkoutSlice from "@/components/checkout/checkoutSlice";
import orderSlice from "@/components/orders/oderSlice";
import { configureStore, ThunkAction } from "@reduxjs/toolkit";
import { useMemo } from "react";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import { Store, Action } from "redux";

let store: Store | undefined;

const initialState = {};

function initStore(preloadedState = initialState) {
  return configureStore({
    preloadedState,
    reducer: {
      mainSlice: mainSlice,
      authSlice: authSlice,
      addressSlice: addressSlice,
      checkoutSlice: checkoutSlice,
      orderSlice:orderSlice
    },
  });
}

export const initializeStore = (preloadedState: typeof initialState) => {
  let _store = store ?? initStore(preloadedState);

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    });
    // Reset the current store
    store = undefined;
  }

  // For SSG and SSR always create a new store
  if (typeof window === "undefined") return _store;
  // Create the store once in the client
  if (!store) store = _store;

  return _store;
};

export function useStore(_initialState: typeof initialState) {
  const store = useMemo(() => initializeStore(_initialState), [_initialState]);
  return store;
}

export type AppStore = ReturnType<typeof initStore>;
export type AppState = ReturnType<AppStore["getState"]>;
export type AppDispatch = AppStore["dispatch"];
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  AppState,
  unknown,
  Action<string>
>;
export type AppThunkStatus = "idle" | "loading" | "succeeded" | "failed";

export const useAppSelector: TypedUseSelectorHook<AppState> = useSelector;

export const useAppDispatch = () => useDispatch<AppDispatch>();
