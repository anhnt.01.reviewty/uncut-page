export const en = {
  ///using for layout
  ///

  ///

  products: {
    soldOut: "Sold Out",
    detail: {
      participants: "Participants",
    },
  },

  title: {
    orderInformation: "Order Information",
    myAddress: "My Address",
    addNewAddress: "Add New Address",
    editAddress: "Edit Address",
    changePassword: "Change Password",
  },
  address: {
    setIsDefaultAddress: "Set Is Default Address",
    myAddress: "My Address",
    addNewAddress: "Add New Address",
    confirm: {
      noOrderAddressAddedYet: "No Order Address Added Yet",

      deleteOneLeftAddress:
        "Default address cannot be deleted when there is one left",
      delete: "Are you sure want to delete this?",
    },
    content: {
      noAddedAddressYet: "Looks like you haven't added a shipping address yet.",

      deleteOneLeftAddressContent:
        "You can add new address & delete address old",
    },
    province: "Province",
    district: "District",
    ward: "Ward",
    specificAddress: "Specific Address",
    fullName: "Full name",
    placeholder: {
      nameProvince: "Name Province",
      nameDistrict: "Name District",
      nameWard: "Name Ward",
    },
    phoneNumber: "Phone Number",
  },
  input: {
    errors: {
      minMaxCharacter:
        "This field must enter more than 1 character & small than 100 character ",
      formatEmailIncorrect: "Format Email Incorrect",
      required: "This field is required !",
      accountOrPasswordIncorrect: "Account or password incorrect !",
      phoneNumberInvalid: "Phone number is Invalid",
    },
  },
  login: {
    orSigninWith: "Or sign in with",
    canSigninWithReviewty: "You can sign in with Reviewty account",
    signinWithFacebook: "Sign in with Facebook",
  },
  forgotPassword: {
    heading: "Reset your password",
    paragraph:
      "To reset your password please enter your corresponding email address",
    resetPassword: "Reset password",
    back: "Back",
  },
  message: {
    copyToClipboard: "Copied !",
  },
  common: {
    reasonCancel: "Reanson Cancel",
    intoMoney: "Into Money",
    share: "Share",
    default: "Default",
    viewOrders: "View Orders",
    backToEvent: "Back To Event",
    address: "Address",
    payForYourOrderBefore: "Please pay for your order before",
    eventNotYetContent:
      "There are currently no events! We will update the latest events as soon as possible",
    eventNotYet: "No events yet",
    loadMore: "Load More",
    signIn: "Sign In",
    password: "Password",
    forgotPassword: "Forgot password",
    buyNow: "Buy Now",
    edit: "Edit",
    delete: "Delete",
    cancel: "Cancel",
    update: "Update",
    save: "Save",
    product: "Product",
    orderId: "Order Id",
    orderCreatedAt: "Order Created At",
    deliveryMethod: "Delivery Method",
    paymentMethod: "Payment Method",
    order: "Order",
    provisionalFee: "Provisional Fee",
    shippingFee: "Shipping Fee",
    totalMoney: "Total Money",
    completed: "Completed",
    cancelled: "Cancelled",
    payback: "Payback",
    repurchase: "Repurchase",
  },
  home: {
    common: {
      all: "All",
      inprogress: "Inprogress",
      upcomming: "Up Comming",
      stopped: "Stopped",
      product: "Product",
      information: "Information",
      comment: "Comment",
    },
    navigationMenu: {
      hi: "Hi",
      language: "Language",
      myOrder: "My Orders",
      orderHistory: "Order History",
      logout: "Logout",
      addressInformation: "Address Information",
      waitForPay: "Wait For Pay",
      waitDelivering: "Wait Delivering",
      delivering: "Delivering",
      delivered: "Completed",
    },
  },
  fundingDetail: {
    productInformation: "Product Information",
    participationTime: "Participation Time",
    conditionsOfParticipation: "Conditions Of Participation",
    relatedProducts: "Related Products",
  },
  footer: {
    followUs: "Follow Us",
    bussinessInformation: "Bussiness Information",
    aboutMe: "About Me",
    issuedBy: "Issued By",
    termAndPolicy: "Term & Policy",
    bussinessName: "Bussiness Name",
    bussinessID: "Bussiness ID",
    aboutCompany: "About Company",
    //term
    productInformation: "Product Information",
    securityPolicy: "Security Policy",
    productInspectionPolicy: "Product Inspection Policy",
    productRefundAndReturnPolicy: "Product Refund & Return Policy",
    paymentPolicy: "Payment Policy",
    shippingAndReceivingPolicy: "Shipping & Receiving Policy",
    buyerAndSellerResponsibility: "Buyer & Seller Responsibility",
  },
};
