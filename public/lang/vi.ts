// eslint-disable-next-line import/no-anonymous-default-export
export const vi = {
  ///using for layout
  ///

  ///
  products: {
    soldOut: "Hết hàng",
    detail: {
      participants: "Người tham gia",
    },
  },

  title: {
    review: "Đánh giá",
    orderInformation: "Thông tin đơn hàng",
    myAddress: "Địa chỉ của tôi",
    addNewAddress: "Thêm mới địa chỉ",
    editAddress: "Sửa địa chỉ",
    changePassword: "Đổi mật khẩu",
  },

  input: {
    errors: {
      minMaxCharacter: "Trường này phải nhập ít nhất 1 và nhỏ hơn 100 kí tự",
      formatEmailIncorrect: "Chưa đúng định dạng email",
      required: "Trường này là bắt buộc",
      accountOrPasswordIncorrect: "Sai tài khoản hoặc mật khẩu",
      phoneNumberInvalid: "Số điện thoại không hợp lệ",
    },
  },
  login: {
    orSigninWith: "Hoặc đăng nhập với",
    canSigninWithReviewty: "Bạn có thể đăng nhập với tài khoản Reviewty",
    signinWithFacebook: "Đăng nhập với Facebook",
  },
  forgotPassword: {
    heading: "Đặt lại mật khẩu của bạn",
    paragraph: "Để đặt lại mật khẩu vui lòng nhập chính xác email của bạn",
    resetPassword: "Đặt lại mật khẩu",
    back: "Trở về",
  },
  message: {
    copyToClipboard: "Đã sao chép !",
  },
  common: {
    reasonCancel: "Lý do hủy",

    intoMoney: "Thành tiền",
    share: "Chia sẻ",
    default: "Mặc định",
    viewOrders: "Xem danh sách đơn hàng",
    backToEvent: "Trở về sự kiện",
    address: "Địa chỉ",
    payForYourOrderBefore: "Vui lòng thanh toán đơn hàng trước",
    eventNotYetContent:
      "Hiện tại chưa có sự kiện nào! Chúng tôi sẽ cập nhật sự kiện mới nhất trong thời gian sớm nhất",
    eventNotYet: "Chưa có sự kiện nào",
    loadMore: "Xem thêm",
    password: "Mật khẩu",
    forgotPassword: "Quên mật khẩu",
    signIn: "Đăng nhập",
    buyNow: "Đặt mua ngay",
    edit: "Sửa",
    delete: "Xóa",
    cancel: "Hủy",
    update: "Cập nhật",
    save: "Lưu",
    product: "Sản phẩm",
    orderId: "Mã đơn hàng",
    orderCreatedAt: "Thời gian đặt hàng",
    deliveryMethod: "Phương thức vận chuyển",
    paymentMethod: "Phương thức thanh toán",
    order: "Đơn hàng",
    provisionalFee: "Tổng tiền hàng",
    shippingFee: "Phí vận chuyển",
    totalMoney: "Tổng tiền",
    completed: "Đã giao",
    cancelled: "Đã hủy",
    payback: "Thanh toán lại",
    repurchase: "Mua lại",
  },

  address: {
    setIsDefaultAddress: "Đặt làm địa chỉ mặc định",
    myAddress: "Địa chỉ của tôi",
    addNewAddress: "Thêm mới địa chỉ",
    confirm: {
      noOrderAddressAddedYet: "Chưa thêm địa chỉ đơn hàng",
      deleteOneLeftAddress:
        "Không thể xóa địa chỉ mặc định khi còn một địa chỉ",
      delete: "Bạn có chắc muốn xóa địa chỉ này ?",
    },
    content: {
      noAddedAddressYet: "Có vẻ như bạn chưa có địa chỉ nhận hàng nào.",
      deleteOneLeftAddressContent:
        "Bạn có thể thêm địa chỉ mới hoặc xóa địa chỉ cũ",
    },

    province: "Thành phố",
    district: "Quận/Huyện",
    ward: "Phường xã",
    specificAddress: "Địa chỉ cụ thể",
    fullName: "Họ và tên",
    placeholder: {
      nameProvince: "Tên thành phố",
      nameDistrict: "Tên Quận/Huyện",
      nameWard: "Tên xã",
    },
    phoneNumber: "Số điện thoại",
  },
  home: {
    common: {
      all: "Tất cả",
      inprogress: "Đang diễn ra",
      upcomming: "Sắp diễn ra",
      stopped: "Đã diễn ra",
      product: "Sản phẩm",
      information: "Thông tin",
      comment: "Bình luận",
    },
    navigationMenu: {
      hi: "Xin chào",
      language: "Ngôn ngữ",
      myOrder: "Đơn hàng của tôi",
      orderHistory: "Lịch sử đơn hàng",
      logout: "Đăng xuất",
      addressInformation: "Thông tin địa chỉ",
      waitForPay: "Chờ thanh toán",
      waitDelivering: "Chờ lấy hàng",
      delivering: "Đang giao",
      delivered: "Đã giao",
    },
  },
  fundingDetail: {
    productInformation: "Thông tin sản phẩm",
    participationTime: "Thời gian tham gia",
    conditionsOfParticipation: "Điều kiện tham gia",
    relatedProducts: "Sản phẩm liên quan",
  },
  footer: {
    followUs: "Theo dõi chúng tôi",
    bussinessInformation: "Thông tin doanh nghiệp",
    aboutMe: "Về chúng tôi",
    termAndPolicy: "Điều khoản sử dụng và chính sách",
    bussinessName: "Tên Doanh nghiệp",
    bussinessID: "Mã số doanh nghiệp",
    aboutCompany: "Giới thiệu công ty",
    //term
    productInformation: "Thông tin sản phẩm",
    securityPolicy: "Chính sách bảo mật",
    productInspectionPolicy: "Chinh sách kiểm hàng",
    productRefundAndReturnPolicy: "Chính sách đổi trả hàng và hoàn tiền",
    paymentPolicy: "Chính sách thanh toán",
    shippingAndReceivingPolicy: "Chính sách vận chuyển và giao nhận",
    buyerAndSellerResponsibility: "Nghĩa vụ của người bán và người mua",
  },
};
