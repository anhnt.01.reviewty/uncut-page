const withMDX = require("@next/mdx")({
  extension: /\.mdx?$/,

  options: {
    remarkPlugins: [],
    rehypePlugins: [],
    // If you use `MDXProvider`, uncomment the following line.
    providerImportSource: "@mdx-js/react",
  },
});

/** @type {import('next').NextConfig} */

const nextConfig = {
  reactStrictMode: true,
  experimental: {
    mdxRS: true,
    appDir: true,
    externalDir:
      true |
      {
        enabled: true,
        silent: true,
      },
  },

  images: {
    allowFutureImage: true,
    unoptimized: true,
    formats: ["image/webp"],
    domains: [
      "gw.alipayobjects.com",
      "online.gov.vn",
      "d9vmi5fxk1gsw.cloudfront.net",
      "image.oliveyoung.co.kr",
      "d27j9dvp3kyb6u.cloudfront.net",
      "d1ip8wajnedch4.cloudfront.net",
      "https://d9vmi5fxk1gsw.cloudfront.net",
      "live.staticflickr.com",
      "reviewty.ss-hn-1.vccloud.vn",
    ],
    deviceSizes: [390, 640],
    imageSizes: [16, 32, 48, 64, 96],
    minimumCacheTTL: 3600 * 24 * 30,
  },

  pageExtensions: ["md", "mdx", "tsx", "ts", "jsx", "js", "otf"],
  trailingSlash: true,
};

module.exports = withMDX(nextConfig);

// module.exports = withPWA({
//   dest: "public",
//   register: true,
//   skipWaiting: true,
//   disable: process.env.NODE_ENV === "development",
//   images: {
//     allowFutureImage: true,
//     domains: [
//       "d9vmi5fxk1gsw.cloudfront.net",
//       "image.oliveyoung.co.kr",
//       "d27j9dvp3kyb6u.cloudfront.net",
//       "d1ip8wajnedch4.cloudfront.net",
//       "https://d9vmi5fxk1gsw.cloudfront.net",
//     ],
//   },
// });
