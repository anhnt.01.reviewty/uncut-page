import { setIsOpen } from "@/components/main/mainSlice";
import { FundingOrderStatus } from "@/graphql/reviewty-user/graphql";
import { checkUserAgent } from "@/hooks/useAgent";
import useTrans from "@/hooks/useTrans";
import axios from "axios";
import Router, { useRouter } from "next/router";
import { useDispatch } from "react-redux";

export interface IUseViewGGANL {
  eventName: Gtag.EventNames;
  eventCategory: string;
  eventLabel: "VIEW" | "CLICK";
  asPath: string;
}
export const IS_MOBILE =
  typeof window !== "undefined" && window?.innerWidth < 600;

export const handleCheckAuth = () => {
  const arrWishListOrigin = JSON.parse(
    process.env.NEXT_PUBLIC_WISHLIST_ORIGIN_URL as any
  );
  const isWebViewAndroid =
    navigator.userAgent && /; wv/gi.test(navigator.userAgent);
  const isWebViewIOS =
    navigator.userAgent &&
    /WebViewApp/gi.test(navigator?.userAgent.toLowerCase());

  if (isWebViewAndroid || isWebViewIOS) {
    Router.push("https://review-ty.com/auth");
    return;
  }

  const isOriginWishList = arrWishListOrigin.some((url: string) =>
    url.includes(document?.referrer)
  );
  // debugger;

  if (isOriginWishList) {
    // window.top!.location = document?.referrer + "/auth/login";
    window.top!.location.replace(document?.referrer + "auth/login");
    return;
  }
  Router.push("/auth/login");
};

export const REVIEWTY_TOKEN = "rvt_tk";
export const REVIEWTY_LOCALE = "rvt_locale";
export const REVIEWTY_UUID = "uuid";

export const REGEX_EMAIL =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const REGEX_PHONE_NUMBER = /(84|0[2|3|5|7|8|9])+([0-9]{8})\b/g;

export const increaseDate = (date: any, minute: number) => {
  const currentDate = new Date(date);
  const increasedTime = new Date(currentDate.getTime() + minute * 60000);
  const day = increasedTime.getDate().toString().padStart(2, "0");
  const month = (increasedTime.getMonth() + 1).toString().padStart(2, "0");
  const year = increasedTime.getFullYear().toString();
  const hours = increasedTime.getHours().toString().padStart(2, "0");
  const minutes = increasedTime.getMinutes().toString().padStart(2, "0");

  const formattedDateTime = `${hours}:${minutes} ${day}/${month}/${year}`;

  return formattedDateTime;
};

export const onlyUnique = (value: number, index: number, array: number[]) => {
  return array.indexOf(value) === index;
};

interface ISendEventLog {
  eventName: string;
  type: "click" | "view" | "Errors";
  asPath?: string;
  userId?: number;
  orderId?: string;
  fundingIds?: number[];
  orderStatus?: string;
  productPrice?: number;
  shippingPrice?: number;
  productId?: number;
  fundingId?: number;
  errors?: string;
  encrpyt?: string;
}

export const sentEventLog = (data: ISendEventLog) => {
  if (process.env.NEXT_PUBLIC_TYPE !== "production") {
    return;
  }

  const splitLetter = data.eventName.split("");
  splitLetter[0].toUpperCase();
  const newLetter = splitLetter.join("");
  try {
    axios.post(process.env.NEXT_PUBLIC_REVIEWTY_USER_GRAPHQL_API_ENDPOINT!, {
      operationName: `${data.type}${newLetter}Funding`,
      variables: {
        data: {
          name: data.asPath,
          message: JSON.stringify({
            date: new Date(),
            "device-token": localStorage.getItem(REVIEWTY_UUID),
            userId: data.userId,
            orderId: data.orderId,
            fundingIds: data.fundingIds,
            orderStatus: data.orderStatus,
            productPrice: data.productPrice,
            shippingPrice: data.shippingPrice,
            productId: data.productId,
            fundingId: data.fundingId,
            errors: data.errors,
            encrpyt: data.encrpyt,
          }),
        },
      },
      query: `mutation ${data.type}${newLetter}Funding($data: ActivityLogCreateInput!) {\n  actitvityLogs(data: $data)\n}\n`,
    });
  } catch (error) {}
};

export const gTagGGAnalysis = ({
  eventCategory,
  eventLabel,
  eventName,
  asPath,
}: IUseViewGGANL) => {
  if (typeof window.gtag !== "undefined") {
    try {
      axios.post(process.env.NEXT_PUBLIC_REVIEWTY_USER_GRAPHQL_API_ENDPOINT!, {
        operationName: `click_${eventName}_funding`,
        variables: {
          data: {
            name: asPath,
            message: JSON.stringify({
              date: new Date(),
              "device-token": localStorage.getItem(REVIEWTY_UUID),
            }),
          },
        },
        query: `mutation click_${eventName}_funding($data: ActivityLogCreateInput!) {\n  actitvityLogs(data: $data)\n}\n`,
      });
    } catch (error) {}

    window.gtag("event", eventName, {
      event_category: `CLICK_ENTER_${eventCategory}`,
      event_label: eventLabel,
      value: localStorage.getItem(REVIEWTY_UUID) ?? "",
    });
  }
};

export const convertToText = (status: FundingOrderStatus) => {
  switch (status) {
    case FundingOrderStatus.InProcessing:
      return "Chờ thanh toán";

    case FundingOrderStatus.WaitDelivering:
      return "Chờ lấy hàng";

    case FundingOrderStatus.Completed:
      return "Hoàn thành";

    case FundingOrderStatus.WaitRefunding:
      return "Đang hoàn tiền";
    case FundingOrderStatus.Cancelled:
      return "Đã hủy";
    default:
      break;
  }
};

export const formatNumber = (num: number, between?: string) => {
  if (num === 0 || num === null) {
    return 0;
  }
  return num?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, between || ",");
};

export const handleClickViewMobileApp = (url: string, callback: Function) => {
  const { isMobile, isWebViewAndroid, isWebViewIOS } = checkUserAgent(
    navigator.userAgent
  );

  if (isWebViewAndroid || isWebViewIOS) {
    if (url) Router.push(url);
  } else {
    if (callback) callback();
  }
};
