const toCurrency = (price: number): string => {
  // return price.toLocaleString("vi", { style: "currency", currency: "VND" });
  if (price?.toLocaleString("en-US")) {
    return price?.toLocaleString("en-US") + "đ";
  }

  return "";
};
export default toCurrency;
