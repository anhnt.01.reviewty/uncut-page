// resetActions.js
import { resetAddressSlice } from "@/components/address/addressSlice";
import { resetAuthSlice } from "@/components/auth/authSlice";
import { resetMainSlice } from "@/components/main/mainSlice";
import { resetOrderSlice } from "@/components/orders/oderSlice";
import { NextRouter } from "next/router";
// import { resetSlice1 } from './slice1';
// import { resetSlice2 } from './slice2';

export const resetAllSlices = () => (dispatch: Function) => {
  dispatch(resetAuthSlice());
  dispatch(resetAddressSlice());
  dispatch(resetMainSlice({}));
  dispatch(resetOrderSlice());
};
