import { gql } from "@apollo/client";

export const GET_POST_LIST = gql`
  query fundings(
    $skip: Int = 0
    $after: ID
    $before: ID
    $first: Int = 10
    $last: Int
    $where: FundingWhereInput
    $orderBy: FundingOrderByInput
  ) {
    fundings(
      skip: $skip
      after: $after
      before: $before
      first: $first
      last: $last
      where: $where
      orderBy: $orderBy
    ) {
      id
      coverUrl
      shortDescription
      longDescription
      productId
      startDate
      endDate
      salePrice
      product {
        thumbnail {
          small: fixed(width: SMALL) {
            url
          }
          url
        }
        translations(where: { language: VI }) {
          name
        }
        brand {
          logoUrl
          smallLogoUrl: fixedLogoUrl(width: SMALL)

          id
          translations {
            name
          }
        }
        id
      }
    }
  }
`;
