import React, {
  createContext,
  FC,
  ReactNode,
  useEffect,
  useMemo,
  useState,
} from "react";

const SCREEN_BREAK_POINTS = {
  sm: 640,
  md: 768,
  lg: 1024,
};

import {
  checkUserAgent,
  isMobile as getIsMobile,
  isNativeApp as getIsNative,
  isWebViewAndroid,
  isWebViewIOS,
} from "@hooks/useAgent";

export const DeviceDetectContext = createContext({
  isWebViewAndroid: false,
  isWebViewIOS: false,
  isMobile: false,
  isAndroid: false,
  isIOS: false,
  isWebView: false,
});

interface Props {
  userAgent?: string;
  children: ReactNode;
}

// Responsive Context: determines whether mobile according 'userAgent' and 'resize' (matchMedia)

export const DeviceContext: FC<Props> = ({ userAgent, children }) => {
  const [devices, setDevices] = useState({
    isWebViewAndroid: false,
    isWebViewIOS: false,
    isMobile: false,
    isAndroid: false,
    isIOS: false,
    isWebView: false,
  });
  const isBrowser = typeof window !== "undefined";
  const [fontLoaded, setFontLoaded] = useState(false);

  useEffect(() => {
    setDevices(checkUserAgent(navigator.userAgent));
  }, []);

  return (
    <DeviceDetectContext.Provider value={devices}>
      {children}
    </DeviceDetectContext.Provider>
  );
};
