export const handleDiffDate = (endDate: string, type?: "event") => {
  const date1 = new Date();
  const date2 = new Date(endDate);

  // Calculate the time difference in milliseconds

  if (date2.getTime() < new Date().getTime()) {
    return "Hết hạn";
  }
  const timeDiff = date2.getTime() - date1.getTime();

  const millisecondsInASecond = 1000;
  const millisecondsInAMinute = 60 * millisecondsInASecond;
  const millisecondsInAnHour = 60 * millisecondsInAMinute;
  const millisecondsInADay = 24 * millisecondsInAnHour;

  const days = Math.floor(timeDiff / millisecondsInADay);
  const hours = Math.floor(
    (timeDiff % millisecondsInADay) / millisecondsInAnHour
  );
  const minutes = Math.floor(
    (timeDiff % millisecondsInAnHour) / millisecondsInAMinute
  );
  const seconds = Math.floor(
    (timeDiff % millisecondsInAMinute) / millisecondsInASecond
  );
  // Calculate the number of days, hours, minutes, and seconds in the time difference
  const daysDiff =
    date1.getTime() < date2.getTime()
      ? Math.floor(timeDiff / (1000 * 60 * 60 * 24))
      : 0;
  const hoursDiff = Math.floor((timeDiff / (1000 * 60 * 60)) % 24);
  const minutesDiff = Math.floor((timeDiff / (1000 * 60)) % 60);
  const secondsDiff = Math.floor((timeDiff / 1000) % 60);

  if (type) {
    return `${daysDiff ? daysDiff + " Ngày" : ""} ${
      hoursDiff ? hoursDiff + " Giờ" : ""
    } ${minutesDiff ? minutesDiff + " Phút" : ""}`;
  }

  if (daysDiff < 1) {
    return `Còn ${hoursDiff ? hoursDiff + "h " : ""}${minutesDiff} phút`;
  }
  return `Còn ${daysDiff} ngày`;

  // Output the time difference in a human-readable format
};

export const formatDate = (date: Date, format?: string) => {
  const newDate = new Date(date);

  if (newDate) {
    const year = newDate?.getFullYear();
    const month = String(newDate?.getMonth() + 1).padStart(2, "0");
    const day = String(newDate?.getDate()).padStart(2, "0");
    const hour = String(newDate?.getHours()).padStart(2, "0");
    const minute = String(newDate?.getMinutes()).padStart(2, "0");

    const formattedDate = `${day}.${month}.${year} ${hour}:${minute}`;
    return formattedDate;
  }
};

export const calculateOld = (birthYear: number) => {
  const newDate = new Date();
  const year = newDate?.getFullYear();

  return year - birthYear;
};

// Define the date object (replace with your desired date)
